The Dynamic.Speech.Authorization.Demo.exe.config file contains a few 
properties that need to be specified.

DeveloperKey - The key used to identify the developer account

ApplicationKey - The key used to identify the application instance

EnrollDirectory - The directory where enrollments are located. This is
in the form {root_enrollment_folder}/{client_id}/{enrollment_wav_files}*.wav

VerifyDirectory - The directory where verifications are located. This is
in the form {root_verification_folder}/{client_id}/{session_instance}/{verification_wav_files}*.wav

MultiVerifyDirectory - The directory where verifications are located. This is
in the form {root_multi_verify_folder}/{comma,separated,list,of,client_ids}/{session_instance}/{verify_wav_files}*.wav

The {session_instance} folders for VerifyDirectory and MultiVerifyDirectory are so you can try more than one set of verify files for a client_id(s) in one run of the application. 

```xml
<Dynamic.Properties.Settings>
	<setting name="DeveloperKey" serializeAs="String">
	  <value />
	</setting>
	<setting name="ApplicationKey" serializeAs="String">
	  <value />
	</setting>
	<setting name="EnrollDirectory" serializeAs="String">
	  <value />
	</setting>
	<setting name="VerifyDirectory" serializeAs="String">
	  <value />
	</setting>
	<setting name="MultiVerifyDirectory" serializeAs="String">
	  <value />
	</setting>
</Dynamic.Properties.Settings>
```

