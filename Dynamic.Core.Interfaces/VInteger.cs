﻿using System;

namespace Dynamic
{
    public class VInteger : IComparable, IFormattable, IConvertible, IComparable<VInteger>, IEquatable<VInteger>
    {
        public static readonly uint Min = 0;
        public static readonly uint Max = 536870911u;

        public uint Value { get; private set; }

        public VInteger()
        {
            Value = Min;
        }

        public VInteger(uint value)
        {
            if (value > Max)
                value = Max;

            Value = value;
        }

        public static implicit operator VInteger(uint value) { return new VInteger(value); }

        public static implicit operator uint(VInteger value) { return value.Value; }

        public static VInteger operator ++(VInteger value)
        {
            if (value.Value == Max)
            {
                value.Value = Min;
            }
            else
            {
                ++value.Value;
            }
            return value;
        }

        public static VInteger operator --(VInteger value)
        {
            if (value.Value == Min)
            {
                value.Value = Max;
            }
            else
            {
                --value.Value;
            }
            return value;
        }

        public static VInteger operator +(VInteger value1, VInteger value2) { return value1.Value + value2.Value; }

        public static VInteger operator -(VInteger value1, VInteger value2) { return value1.Value - value2.Value; }

        public static VInteger operator *(VInteger value1, VInteger value2) { return value1.Value * value2.Value; }

        public static VInteger operator /(VInteger value1, VInteger value2) { return value1.Value / value2.Value; }

        public static VInteger operator %(VInteger value1, VInteger value2) { return value1.Value % value2; }

        public static VInteger operator +(VInteger value1, uint value2) { return value1.Value + value2; }

        public static VInteger operator -(VInteger value1, uint value2) { return value1.Value - value2; }

        public static VInteger operator *(VInteger value1, uint value2) { return value1.Value * value2; }

        public static VInteger operator /(VInteger value1, uint value2) { return value1.Value / value2; }

        public static VInteger operator %(VInteger value1, uint value2) { return value1.Value % value2; }

        public static bool operator ==(VInteger value1, VInteger value2) { return value1.Value == value2.Value; }

        public static bool operator !=(VInteger value1, VInteger value2) { return value1.Value != value2.Value; }

        public static bool operator <(VInteger value1, VInteger value2) { return value1.Value < value2.Value; }

        public static bool operator >(VInteger value1, VInteger value2) { return value1.Value > value2.Value; }

        public static bool operator <=(VInteger value1, VInteger value2) { return (value1 == value2) || (value1 < value2); }

        public static bool operator >=(VInteger value1, VInteger value2) { return (value1 == value2) || (value1 > value2); }

        public static bool operator ==(VInteger value1, uint value2) { return value1.Value == value2; }

        public static bool operator !=(VInteger value1, uint value2) { return value1.Value != value2; }

        public static bool operator <(VInteger value1, uint value2) { return value1.Value < value2; }

        public static bool operator >(VInteger value1, uint value2) { return value1.Value > value2; }

        public static bool operator <=(VInteger value1, uint value2) { return (value1 == value2) || (value1 < value2); }

        public static bool operator >=(VInteger value1, uint value2) { return (value1 == value2) || (value1 > value2); }

        public bool Equals(VInteger other)
        {
            return other == this;
        }

        public override bool Equals(object obj)
        {
            if (obj is VInteger value)
            {
                return value == this;
            }
            else if (obj is uint uval)
            {
                return uval == this;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public TypeCode GetTypeCode()
        {
            return (TypeCode)249;
        }

        public bool ToBoolean(IFormatProvider provider)
        {
            return Convert.ToBoolean(Value);
        }

        public byte ToByte(IFormatProvider provider)
        {
            return Convert.ToByte(Value);
        }

        public char ToChar(IFormatProvider provider)
        {
            return Convert.ToChar(Value);
        }

        public DateTime ToDateTime(IFormatProvider provider)
        {
            return Convert.ToDateTime(Value);
        }

        public decimal ToDecimal(IFormatProvider provider)
        {
            return Convert.ToDecimal(Value);
        }

        public double ToDouble(IFormatProvider provider)
        {
            return Convert.ToDouble(Value);
        }

        public short ToInt16(IFormatProvider provider)
        {
            return Convert.ToInt16(Value);
        }

        public int ToInt32(IFormatProvider provider)
        {
            return Convert.ToInt32(Value);
        }

        public long ToInt64(IFormatProvider provider)
        {
            return Convert.ToInt64(Value);
        }

        public sbyte ToSByte(IFormatProvider provider)
        {
            return Convert.ToSByte(Value);
        }

        public float ToSingle(IFormatProvider provider)
        {
            return Convert.ToSingle(Value);
        }

        public string ToString(IFormatProvider provider)
        {
            return Convert.ToString(Value, provider);
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            return Value.ToString(format, formatProvider);
        }

        public object ToType(Type conversionType, IFormatProvider provider)
        {
            throw new NotSupportedException();
        }

        public ushort ToUInt16(IFormatProvider provider)
        {
            return Convert.ToUInt16(Value);
        }

        public uint ToUInt32(IFormatProvider provider)
        {
            return Convert.ToUInt32(Value);
        }

        public ulong ToUInt64(IFormatProvider provider)
        {
            return Convert.ToUInt64(Value);
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }
            else if (obj is VInteger vit)
            {
                return CompareTo(vit);
            }
            else if (obj is uint uit)
            {
                return CompareTo(uit);
            }
            else
            {
                return Value.CompareTo(obj);
            }
        }

        public int CompareTo(VInteger other)
        {
            return Value.CompareTo(other);
        }
    }
}
