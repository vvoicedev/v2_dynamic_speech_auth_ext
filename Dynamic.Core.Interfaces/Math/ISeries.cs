﻿
namespace Dynamic.Math
{
    public interface ISeries<T>
    {
        void Add(T value);

        T Get(int index);

        double GetValue(int index);

        void Clear();

        int Count { get; }

        int Capacity { get; }

        bool IsFull { get; }

        double Sum { get; }

        double SumSquared { get; }

        double Mean { get; }
        
    }
}
