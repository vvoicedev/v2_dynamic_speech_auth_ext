﻿using System;
using System.Text;

namespace Dynamic.Security
{
    [Flags]
    public enum AccessPermission
    {
        Deny = 0,
        Create = 0x01,
        Read = 0x02,
        Copy = Create | Read,
        Update = 0x04,
        Delete = 0x08,
        Write = Create | Update | Delete,
        ReadWrite = Read | Write,
        Execute = 0x10,
        Use = Read | Execute,
        UseElevated = Read | Write | Execute,
        Extra1 = 0x20,
        Extra2 = 0x40,
        Extra3 = 0x80,
        All = UseElevated | Extra1 | Extra2 | Extra3
    }

    public static class AccessPermissionExtensions
    {
        public static string ToDescription(this AccessPermission permissions)
        {
            switch (permissions)
            {
                case AccessPermission.Deny: return "Deny";
                case AccessPermission.Create: return "Create";
                case AccessPermission.Read: return "Read";
                case AccessPermission.Copy: return "Copy";
                case AccessPermission.Update: return "Update";
                case AccessPermission.Delete: return "Delete";
                case AccessPermission.Write: return "Write";
                case AccessPermission.ReadWrite: return "ReadWrite";
                case AccessPermission.Execute: return "Execute";
                case AccessPermission.Use: return "Use";
                case AccessPermission.UseElevated: return "UseElevated";
                case AccessPermission.Extra1: return "Extra1";
                case AccessPermission.Extra2: return "Extra2";
                case AccessPermission.Extra3: return "Extra3";
                case AccessPermission.All: return "All";
            }

            StringBuilder builder = new StringBuilder();

            if (!AppendIf(ref builder, permissions, AccessPermission.All))
            {
                if (!AppendIf(ref builder, permissions, AccessPermission.UseElevated))
                {
                    if (!AppendIf(ref builder, permissions, AccessPermission.ReadWrite))
                    {
                        AppendIf(ref builder, permissions, AccessPermission.Create);
                        if (!AppendIf(ref builder, permissions, AccessPermission.Use))
                        {
                            AppendIf(ref builder, permissions, AccessPermission.Read);
                            AppendIf(ref builder, permissions, AccessPermission.Execute);
                        }
                        AppendIf(ref builder, permissions, AccessPermission.Copy);
                        AppendIf(ref builder, permissions, AccessPermission.Update);
                        AppendIf(ref builder, permissions, AccessPermission.Delete);
                        AppendIf(ref builder, permissions, AccessPermission.Write);
                    }
                }
                AppendIf(ref builder, permissions, AccessPermission.Extra1);
                AppendIf(ref builder, permissions, AccessPermission.Extra2);
                AppendIf(ref builder, permissions, AccessPermission.Extra3);
            }

            if (builder.Length == 0)
            {
                builder.Append("Deny");
            }

            return builder.ToString();
        }

        private static bool AppendIf(ref StringBuilder builder, AccessPermission lhs, AccessPermission rhs)
        {
            if (lhs.HasFlag(rhs))
            {
                if (builder.Length > 0)
                {
                    builder.Append("|");
                }
                builder.Append(rhs.ToString());
                return true;
            }
            return false;
        }
    }

    public interface IAccessRequestId { }
    public interface IAccessControlId { }

    public interface IReadOnlyAccessControlList
    {
        bool Check(string aco, AccessPermission permission = AccessPermission.All);

        bool Check(IAccessControlId aco, AccessPermission permission = AccessPermission.All);

        bool Check(string aro, string aco, AccessPermission permission = AccessPermission.All);

        bool Check(IAccessRequestId aro, string aco, AccessPermission permission = AccessPermission.All);

        bool Check(IAccessRequestId aro, IAccessControlId aco, AccessPermission permission = AccessPermission.All);

        bool IsAllowed(string aco);

        bool IsAllowed(IAccessControlId aco);

        bool IsAllowed(string aro, string aco);

        bool IsAllowed(IAccessRequestId aro, string aco);

        bool IsAllowed(IAccessRequestId aro, IAccessControlId aco);

        bool IsDenied(string aco);

        bool IsDenied(IAccessControlId aco);

        bool IsDenied(string aro, string aco);

        bool IsDenied(IAccessRequestId aro, string aco);

        bool IsDenied(IAccessRequestId aro, IAccessControlId aco);

        IAccessRequestId GetARO(string aro);

        IAccessControlId GetACO(string aco);
    }

    public interface IAccessControlList : IReadOnlyAccessControlList
    {
        void Add(string aco, AccessPermission permissions, bool cascadePermissionsDown = false);

        void Add(IAccessControlId aco, AccessPermission permissions, bool cascadePermissionsDown = false);

        void Add(string aro, string aco, AccessPermission permissions, bool cascadePermissionsDown = false);

        void Add(IAccessRequestId aro, string aco, AccessPermission permissions, bool cascadePermissionsDown = false);

        void Add(IAccessRequestId aro, IAccessControlId aco, AccessPermission permissions, bool cascadePermissionsDown = false);

        void Allow(string aco);

        void Allow(IAccessControlId aco);

        void Allow(string aro, string aco);

        void Allow(IAccessRequestId aro, string aco);

        void Allow(IAccessRequestId aro, IAccessControlId aco);

        void Deny(string aco);

        void Deny(IAccessControlId aco);

        void Deny(string aro, string aco);

        void Deny(IAccessRequestId aro, string aco);

        void Deny(IAccessRequestId aro, IAccessControlId aco);

        void Remove(string aco);

        void Remove(IAccessControlId aco);

        void Remove(string aro, string aco);

        void Remove(IAccessRequestId aro, string aco);

        void Remove(IAccessRequestId aro, IAccessControlId aco);
    }
}
