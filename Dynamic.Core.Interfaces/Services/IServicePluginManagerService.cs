﻿using Dynamic.Plugins;

namespace Dynamic.Services
{
    public interface IServicePluginManagerService : IPluginManagerService
    {
        bool RegisterPlugin(IServicePlugin plugin);
    }
}
