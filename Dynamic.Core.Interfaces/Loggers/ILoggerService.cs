﻿using Dynamic.Services;

namespace Dynamic.Loggers
{
    public interface ILoggerService : ILogger, IService
    {
    }
}
