﻿using System;

namespace Dynamic.Loggers
{
    public interface ILogger : IDisposable
    {
        void WriteInfoLog(string msg, params object[] data);
        void WriteDebugLog(string msg, params object[] data);
        void WriteErrorLog(string msg, params object[] data);
        void WriteErrorLog(Exception ex);
        void WriteErrorLog(Exception ex, string msg, params object[] data);
        string LastError { get; }
    }
}
