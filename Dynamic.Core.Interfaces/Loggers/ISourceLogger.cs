﻿
namespace Dynamic.Loggers
{
    public interface ISourceLogger
    {
        string LastError { set; }
    }
}
