﻿using System;

namespace Dynamic.Loggers
{
    public interface ILoggerSource
    {
        void Clear();

        void Append(string text);
        void AppendLine(string text);

        void Log(string text, params object[] data);
        void LogLine(string text, params object[] data);

        void LogInfo(string msg, params object[] data);
        void LogDebug(string msg, params object[] data);
        void LogError(string msg, params object[] data);
        void LogError(Exception ex);
        void LogError(Exception ex, string msg, params object[] data);
    }
}
