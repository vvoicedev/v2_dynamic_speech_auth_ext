﻿using Dynamic.Services;

namespace Dynamic.Configuration
{
    public enum ConfigMode
    {
        Public,

        Private
    }

    public interface IConfigService : IService
    {
        IConfigContext DefaultConfiguration { get; }

        IConfigContext GetConfiguration(string name, ConfigMode mode);
    }
}
