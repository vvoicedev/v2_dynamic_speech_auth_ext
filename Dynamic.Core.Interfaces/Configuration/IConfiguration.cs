﻿using System;
using System.Collections;
using System.Collections.Generic;

using Dynamic.Extensions;
using Dynamic.Services;

namespace Dynamic.Configuration
{
    public interface IEditableConfigObject
    {
        void Put(StringId key, string value);
        void Put(StringId key, int value);
        void Put(StringId key, long value);
        void Put(StringId key, float value);
        void Put(StringId key, double value);
        void Put(StringId key, bool value);
        void Remove(StringId key);
        void Clear();
    }

    public sealed class ConfigObject : IEnumerable<KeyValuePair<StringId, object>>, IEnumerable
    {
        internal Dictionary<StringId, object> mData;

        public ConfigObject()
        {
            mData = new Dictionary<StringId, object>();
        }
        
        public int Count { get { return mData.Count; } }

        public bool Has(StringId key)
        {
            return mData.ContainsKey(key);
        }
        
        public string GetString(StringId key)
        {
            return GetString(key, null);
        }

        public string GetString(StringId key, string def)
        {
            if (mData.ContainsKey(key))
            {
                object o = mData[key];
                if (o is string str)
                {
                    return str;
                }
                else if (o.IsNumericType())
                {
                    return Convert.ToString(o);
                }
                else if (o is bool bl)
                {
                    return bl ? "true" : "false";
                }
            }
            return def;
        }

        public int GetInt(StringId key)
        {
            return GetInt(key, -1);
        }

        public int GetInt(StringId key, int def)
        {
            if (mData.ContainsKey(key))
            {
                object o = mData[key];
                if (o.IsNumericType())
                {
                    return Convert.ToInt32(o);
                }
            }
            return def;
        }

        public long GetLong(StringId key)
        {
            return GetLong(key, -1);
        }

        public long GetLong(StringId key, long def)
        {
            if (mData.ContainsKey(key))
            {
                object o = mData[key];
                if (o.IsNumericType())
                {
                    return Convert.ToInt64(o);
                }
            }
            return 0;
        }

        public float GetFloat(StringId key)
        {
            return GetFloat(key, -1);
        }

        public float GetFloat(StringId key, float def)
        {
            if (mData.ContainsKey(key))
            {
                object o = mData[key];
                if (o.IsNumericType())
                {
                    return Convert.ToSingle(o);
                }
            }
            return def;
        }

        public double GetDouble(StringId key)
        {
            return GetDouble(key, -1);
        }

        public double GetDouble(StringId key, double def)
        {
            if (mData.ContainsKey(key))
            {
                object o = mData[key];
                if (o.IsNumericType())
                {
                    return Convert.ToDouble(o);
                }
            }
            return def;
        }

        public bool GetBoolean(StringId key)
        {
            return GetBoolean(key, false);
        }

        public bool GetBoolean(StringId key, bool def)
        {
            if (mData.ContainsKey(key))
            {
                object o = mData[key];
                if (o.IsNumericType())
                {
                    return Convert.ToBoolean(o);
                }
            }
            return def;
        }
        
        public IEnumerator<KeyValuePair<StringId, object>> GetEnumerator()
        {
            return mData.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return mData.GetEnumerator();
        }
        
    }

    public interface IEditableConfigValue
    {
        string AsString { get; set; }
        int AsInt { get; set; }
        long AsLong { get; set; }
        float AsFloat { get; set; }
        double AsDouble { get; set; }
        IEditableConfigObject AsObject { get; }

        void Delete();
    }

    public sealed class ConfigValue
    {
        private object mValue;
        
        public ConfigValue()
        {
            Id = null;
            Uuid = Guid.NewGuid();
            mValue = null;
            IsModified = false;
        }

        public ConfigValue(object value)
        {
            Id = null;
            Uuid = Guid.NewGuid();
            mValue = value;
            IsModified = false;
        }

        public int? Id { get; private set; }

        public Guid Uuid { get; private set; }

        public Type Type { get { return mValue != null ? mValue.GetType() : null; } } 

        public bool IsNew { get { return Id == null; } }

        public bool IsModified { get; private set; }

        public bool IsDeleted
        {
            get
            {
                if(mValue == null)
                {
                    return true;
                }
                else if(mValue is ConfigObject co)
                {
                    return co.Count == 0;
                }
                return false;
            }
        }

        public bool IsString { get { return mValue is string; } }
        
        public string AsString()
        {
            return AsString("");
        }

        public string AsString(string def)
        {
            object o = mValue;
            if (o is string str)
            {
                return str;
            }
            else if (o.IsNumericType())
            {
                return Convert.ToString(o);
            }
            return def;
        }

        public bool IsInt { get { return mValue is int; } }

        public int AsInt()
        {
            return AsInt(0);
        }

        public int AsInt(int def)
        {
            object o = mValue;
            if (o.IsNumericType())
            {
                return Convert.ToInt32(o);
            }
            return def;
        }

        public bool IsLong { get { return mValue is long; } }

        public long AsLong()
        {
            return AsLong(0);
        }

        public long AsLong(long def)
        {
            object o = mValue;
            if (o.IsNumericType())
            {
                return Convert.ToInt64(o);
            }
            return def;
        }

        public bool IsFloat { get { return mValue is float; } }

        public float AsFloat()
        {
            return AsFloat(0);
        }

        public float AsFloat(float def)
        {
            object o = mValue;
            if (o.IsNumericType())
            {
                return Convert.ToSingle(o);
            }
            return def;
        }

        public bool IsDouble { get { return mValue is double; } }

        public double AsDouble()
        {
            return AsDouble(0);
        }

        public double AsDouble(double def)
        {
            object o = mValue;
            if (o.IsNumericType())
            {
                return Convert.ToDouble(o);
            }
            return def;
        }

        public bool IsBoolean { get { return mValue is bool; } }

        public bool AsBoolean()
        {
            return AsBoolean(false);
        }

        public bool AsBoolean(bool def)
        {
            object o = mValue;
            if (o.IsNumericType())
            {
                return Convert.ToBoolean(o);
            }
            return def;
        }

        public bool IsObject { get { return mValue is ConfigObject; } }

        public ConfigObject AsObject()
        {
            ConfigObject co = null;
            TryAsObject(out co);
            return co;
        }

        public bool TryAsObject(out ConfigObject obj)
        {
            if(mValue is ConfigObject co)
            {
                obj = co;
                return true;
            }

            obj = null;
            return false;
        }

        #region

        internal IEditableConfigValue Edit()
        {
            return new EditableConfigValue(this);
        }

        #endregion

        #region

        private class EditableConfigValue : IEditableConfigValue
        {
            private ConfigValue mConfigValue;

            internal EditableConfigValue(ConfigValue value)
            {
                mConfigValue = value;
            }
            
            public string AsString
            {
                get
                {
                    if(mConfigValue.mValue is string str)
                    {
                        return str;
                    }
                    return "";
                }
                set
                {
                    mConfigValue.mValue = value;
                    mConfigValue.IsModified = true;
                }
            }

            public int AsInt
            {
                get
                {
                    if (mConfigValue.mValue is int val)
                    {
                        return val;
                    }
                    return 0;
                }
                set
                {
                    mConfigValue.mValue = value;
                    mConfigValue.IsModified = true;
                }
            }

            public long AsLong
            {
                get
                {
                    if (mConfigValue.mValue is long val)
                    {
                        return val;
                    }
                    return 0;
                }
                set
                {
                    mConfigValue.mValue = value;
                    mConfigValue.IsModified = true;
                }
            }

            public float AsFloat
            {
                get
                {
                    if (mConfigValue.mValue is float val)
                    {
                        return val;
                    }
                    return 0;
                }
                set
                {
                    mConfigValue.mValue = value;
                    mConfigValue.IsModified = true;
                }
            }

            public double AsDouble
            {
                get
                {
                    if (mConfigValue.mValue is double val)
                    {
                        return val;
                    }
                    return 0;
                }
                set
                {
                    mConfigValue.mValue = value;
                    mConfigValue.IsModified = true;
                }
            }

            public IEditableConfigObject AsObject
            {
                get
                {
                    if (mConfigValue.mValue is ConfigObject co)
                    {
                        return new EditableConfigObject(mConfigValue, co);
                    }

                    var cobj = new ConfigObject();
                    mConfigValue.mValue = cobj;
                    mConfigValue.IsModified = true;
                    return new EditableConfigObject(mConfigValue, cobj);
                }
            }

            public void Delete()
            {
                mConfigValue.mValue = null;
                mConfigValue.IsModified = true;
            }
        }

        private class EditableConfigObject : IEditableConfigObject
        {
            private ConfigValue mConfigValue;
            private ConfigObject mConfigObject;

            internal EditableConfigObject(ConfigValue cv, ConfigObject co)
            {
                mConfigValue = cv;
                mConfigObject = co;
            }

            public void Put(StringId key, string value)
            {
                PutKeyValue(key, value);
            }

            public void Put(StringId key, int value)
            {
                PutKeyValue(key, value);
            }

            public void Put(StringId key, long value)
            {
                PutKeyValue(key, value);
            }

            public void Put(StringId key, float value)
            {
                PutKeyValue(key, value);
            }

            public void Put(StringId key, double value)
            {
                PutKeyValue(key, value);
            }

            public void Put(StringId key, bool value)
            {
                PutKeyValue(key, value);
            }

            private void PutKeyValue(StringId key, object value)
            {
                if(mConfigObject.mData.ContainsKey(key))
                {
                    mConfigObject.mData[key] = value;
                }
                else
                {
                    mConfigObject.mData.Add(key, value);
                }
                mConfigValue.IsModified = true;
            }

            public void Remove(StringId key)
            {
                if (mConfigObject.mData.ContainsKey(key))
                {
                    mConfigObject.mData.Remove(key);
                    mConfigValue.IsModified = true;
                }
            }

            public void Clear()
            {
                mConfigObject.mData.Clear();
                mConfigValue.IsModified = true;
            }
        }

        #endregion
    }

    public interface IConfigModel
    {
        string Uuid { get; }

        bool IsNew { get; }
        bool IsModified { get; }
        bool IsDeleted { get; set; }

        bool OnCommit(IEditableConfigValue editable);
    }

    public interface IEditableConfiguration : IDisposable
    {
        bool Commit(IConfigModel model);

        bool Commit(string key, IConfigModel model);

        IEditableConfigValue Put();

        IEditableConfigValue Put(StringId key);

        IEditableConfigValue Put(ConfigValue cv);

        IEditableConfigValue Put(StringId key, ConfigValue cv);

        IEditableConfigValue Edit(ConfigValue cv);

        void Remove(ConfigValue cv);

        void Clear();
    }

    public interface IConfiguration : IDisposable
    {
        StringId Name { get; }

        int Count { get; }

        IEnumerable GetKeys();

        bool Has(StringId key);

        string GetString(StringId key);

        string GetString(StringId key, string def);

        int GetInt(StringId key);

        int GetInt(StringId key, int def);

        long GetLong(StringId key);

        long GetLong(StringId key, long def);

        float GetFloat(StringId key);

        float GetFloat(StringId key, float def);

        double GetDouble(StringId key);

        double GetDouble(StringId key, double def);

        bool GetBoolean(StringId key);

        bool GetBoolean(StringId key, bool def);

        ConfigObject GetObject(StringId key);

        bool TryGetObject(StringId key, out ConfigObject value);

        ConfigValue GetValue(StringId key);

        bool TryGetValue(StringId key, out ConfigValue value);

        IConfiguration GetView(StringId prefixKey);

        IConfiguration Lock(string lockerName);

        IEditableConfiguration Edit(string editorName);
    }
    
    public enum ConfigurationMode
    {
        Public,

        Private
    }

    public interface IConfigurationService : IService
    {
        IConfiguration DefaultConfiguration { get; }

        IConfiguration GetConfiguration(StringId name, ConfigurationMode mode);
    }
}
