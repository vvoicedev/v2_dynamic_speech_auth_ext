﻿using System;
using System.Collections;

namespace Dynamic.Configuration
{
    public interface IConfigContext : IDisposable
    {
        int Count { get; }

        bool Has(string key);

        bool HasPrefix(string key);

        string Get(string key);

        string Get(string key, string def);

        IEnumerable GetKeys();

        IConfigContext GetView(string prefixKey);

        void Put(string key, string value);

        void Append(string key, string value);

        void Append(string key, string value, string seperator);

        void Remove(string key);

        void Remove(string key, string value);

        void Remove(string key, string value, string seperator);

        void Clear();

        void BeginUpdate();

        void EndUpdate();

        IConfigContext Lock(string lockerName);
    }
}
