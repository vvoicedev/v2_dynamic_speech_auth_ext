﻿using System.Collections.Generic;
    
namespace Dynamic.Analytics
{
    public interface IAnalytics
    {
        void TrackEvent(string name, Dictionary<string, object> data);
    }
}
