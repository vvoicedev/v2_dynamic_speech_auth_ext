﻿using System;

namespace Dynamic
{
    public class VLong : IComparable, IFormattable, IConvertible, IComparable<VLong>, IEquatable<VLong>
    {
        public static readonly ulong Min = 0;
        public static readonly ulong Max = 144115188075855871ul;

        public ulong Value { get; private set; }

        public VLong()
        {
            Value = Min;
        }

        public VLong(ulong value)
        {
            if (value > Max)
                value = Max;

            Value = value;
        }
        
        public static implicit operator VLong(ulong value) { return new VLong(value); }

        public static implicit operator ulong(VLong value) { return value.Value; }

        public static VLong operator ++(VLong value)
        {
            if (value.Value == Max)
            {
                value.Value = Min;
            }
            else
            {
                ++value.Value;
            }
            return value;
        }

        public static VLong operator --(VLong value)
        {
            if(value.Value == Min)
            {
                value.Value = Max;
            }
            else
            {
                --value.Value;
            }
            return value;
        }

        public static VLong operator +(VLong value1, VLong value2) { return value1.Value + value2.Value; }

        public static VLong operator -(VLong value1, VLong value2) { return value1.Value - value2.Value; }

        public static VLong operator *(VLong value1, VLong value2) { return value1.Value * value2.Value; }

        public static VLong operator /(VLong value1, VLong value2) { return value1.Value / value2.Value; }

        public static VLong operator %(VLong value1, VLong value2) { return value1.Value % value2; }

        public static VLong operator +(VLong value1, ulong value2) { return value1.Value + value2; }

        public static VLong operator -(VLong value1, ulong value2) { return value1.Value - value2; }

        public static VLong operator *(VLong value1, ulong value2) { return value1.Value * value2; }

        public static VLong operator /(VLong value1, ulong value2) { return value1.Value / value2; }

        public static VLong operator %(VLong value1, ulong value2) { return value1.Value % value2; }

        public static bool operator ==(VLong value1, VLong value2) { return value1.Value == value2.Value; }

        public static bool operator !=(VLong value1, VLong value2) { return value1.Value != value2.Value; }

        public static bool operator <(VLong value1, VLong value2) { return value1.Value < value2.Value; }

        public static bool operator >(VLong value1, VLong value2) { return value1.Value > value2.Value; }

        public static bool operator <=(VLong value1, VLong value2) { return (value1 == value2) || (value1 < value2); }

        public static bool operator >=(VLong value1, VLong value2) { return (value1 == value2) || (value1 > value2); }

        public static bool operator ==(VLong value1, ulong value2) { return value1.Value == value2; }

        public static bool operator !=(VLong value1, ulong value2) { return value1.Value != value2; }

        public static bool operator <(VLong value1, ulong value2) { return value1.Value < value2; }

        public static bool operator >(VLong value1, ulong value2) { return value1.Value > value2; }

        public static bool operator <=(VLong value1, ulong value2) { return (value1 == value2) || (value1 < value2); }

        public static bool operator >=(VLong value1, ulong value2) { return (value1 == value2) || (value1 > value2); }

        public bool Equals(VLong other)
        {
            return other == this;
        }
        
        public override bool Equals(object obj)
        {
            if (obj is VLong value)
            {
                return value == this;
            }
            else if(obj is ulong uval)
            {
                return uval == this;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public TypeCode GetTypeCode()
        {
            return (TypeCode)250;
        }

        public bool ToBoolean(IFormatProvider provider)
        {
            return Convert.ToBoolean(Value);
        }

        public byte ToByte(IFormatProvider provider)
        {
            return Convert.ToByte(Value);
        }

        public char ToChar(IFormatProvider provider)
        {
            return Convert.ToChar(Value);
        }

        public DateTime ToDateTime(IFormatProvider provider)
        {
            return Convert.ToDateTime(Value);
        }

        public decimal ToDecimal(IFormatProvider provider)
        {
            return Convert.ToDecimal(Value);
        }

        public double ToDouble(IFormatProvider provider)
        {
            return Convert.ToDouble(Value);
        }

        public short ToInt16(IFormatProvider provider)
        {
            return Convert.ToInt16(Value);
        }

        public int ToInt32(IFormatProvider provider)
        {
            return Convert.ToInt32(Value);
        }

        public long ToInt64(IFormatProvider provider)
        {
            return Convert.ToInt64(Value);
        }

        public sbyte ToSByte(IFormatProvider provider)
        {
            return Convert.ToSByte(Value);
        }

        public float ToSingle(IFormatProvider provider)
        {
            return Convert.ToSingle(Value);
        }

        public string ToString(IFormatProvider provider)
        {
            return Convert.ToString(Value, provider);
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            return Value.ToString(format, formatProvider);
        }

        public object ToType(Type conversionType, IFormatProvider provider)
        {
            throw new NotSupportedException();
        }

        public ushort ToUInt16(IFormatProvider provider)
        {
            return Convert.ToUInt16(Value);
        }

        public uint ToUInt32(IFormatProvider provider)
        {
            return Convert.ToUInt32(Value);
        }

        public ulong ToUInt64(IFormatProvider provider)
        {
            return Convert.ToUInt64(Value);
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }
            else if (obj is VLong vlng)
            {
                return CompareTo(vlng);
            }
            else if (obj is ulong ulng)
            {
                return CompareTo(ulng);
            }
            else
            {
                return Value.CompareTo(obj);
            }
        }

        public int CompareTo(VLong other)
        {
            return Value.CompareTo(other);
        }
    }
}
