﻿using System;

namespace Dynamic.Net.Rpc
{
    public enum RpcVersion
    {
        Unknown,
        Version_1
    }

    public interface IRpcRequest
    {
        RpcVersion Version { get; }

        string Method { get; }
        object Id { get; set; }
        object Params { get; set; }
    }

    public interface IRpcResponse
    {
        RpcVersion Version { get; }

        object Id { get; }
        int? Error { get; set; }
        object Results { get; set; }

        void SetError(int error);
        void SetError(int error, string message);
    }

    public delegate void PostCallback(IRpcResponse response);

    public interface IRpcConnection : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool Connect();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="callback"></param>
        void ConnectAsync(ConnectCallback callback);

        /// <summary>
        /// 
        /// </summary>
        void Close();

        /// <summary>
        /// Post a request and wait for a response
        /// </summary>
        /// <param name="request"></param>
        /// <returns>JsonRPCResponse</returns>
        IRpcResponse Post(IRpcRequest request);

        /// <summary>
        /// Post a request and wait for a response
        /// </summary>
        /// <param name="request"></param>
        /// <param name="timeoutMillis"></param>
        /// <returns>JsonRPCResponse can be null</returns>
        IRpcResponse Post(IRpcRequest request, int timeoutMillis);

        /// <summary>
        /// Post a request, does not expect a response.
        /// </summary>
        /// <param name="request"></param>
        void PostAsync(IRpcRequest request);

        /// <summary>
        /// Post a request, handle the response on the callback
        /// </summary>
        /// <param name="request"></param>
        /// <param name="callback"></param>
        bool PostAsync(IRpcRequest request, PostCallback callback);
    }
}
