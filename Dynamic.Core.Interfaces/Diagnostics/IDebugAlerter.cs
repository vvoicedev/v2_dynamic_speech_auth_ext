﻿
namespace Dynamic.Diagnostics
{
    public interface IDebugAlerter
    {
        void Show(string text, string caption);
    }
}
