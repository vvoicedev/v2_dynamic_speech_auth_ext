﻿
using System.Collections.Generic;

namespace Dynamic
{
    public class StringId
    {
        public static readonly StringId Empty = new StringId();

        public string Data { get; private set; }
        public uint Handle { get; private set; }

        public StringId()
        {
            Data = string.Intern("");
            Handle = 0;
        }

        public StringId(char[] data)
        {
            Data = string.Intern(new string(data));
            Handle = GetHandle(Data);
        }

        public StringId(string data)
        {
            Data = string.Intern(data);
            Handle = GetHandle(Data);
        }

        public StringId(uint handle)
        {
            Data = GetString(ref handle);
            Handle = handle;
        }

        private StringId(string data, uint handle)
        {
            Data = data;
            Handle = handle;
        }

        public override string ToString() => Data;

        public StringId PrependNew(string str)
        {
            string newString = string.Intern(str + Data);
            uint handle = GetHandle(newString);
            return new StringId(newString, handle);
        }

        public StringId PrependNew(StringId str)
        {
            string newString = string.Intern(str.Data + Data);
            uint handle = GetHandle(newString);
            return new StringId(newString, handle);
        }

        public StringId AppendNew(string str)
        {
            string newString = string.Intern(Data + str);
            uint handle = GetHandle(newString);
            return new StringId(newString, handle);
        }

        public StringId AppendNew(StringId str)
        {
            string newString = string.Intern(Data + str.Data);
            uint handle = GetHandle(newString);
            return new StringId(newString, handle);
        }

        public bool IsEmpty()
        {
            return Handle == 0 || string.IsNullOrEmpty(Data);
        }

        public override bool Equals(object obj)
        {
            if (obj is StringId id)
            {
                return Handle == id.Handle;
            }
            else if(obj is string str)
            {
                return str.Equals(Data);
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            int hash = base.GetHashCode();
            hash = (hash * 7) + Handle.GetHashCode();
            hash = (hash * 7) + Data.GetHashCode();
            return hash;
        }

        public static implicit operator StringId(char[] value) { return new StringId(value); }

        public static implicit operator StringId(string value) { return new StringId(value); }

        public static implicit operator string(StringId value) { return value.Data; }

        #region Private Static

        private static object mLock;
        private static string mEmptyString;
        private static List<string> mHandles;
        private static Dictionary<string, uint> mStrings;

        private static uint GetHandle(string str)
        {
            if (mStrings.ContainsKey(str))
            {
                return mStrings[str];
            }
            else
            {
                lock (mLock)
                {
                    uint handle = (uint)mHandles.Count;
                    mStrings.Add(str, handle);
                    mHandles.Add(str);
                    return handle;
                }
            }
        }

        private static string GetString(ref uint handle)
        {
            if (handle >= (uint)mHandles.Count)
            {
                handle = 0;
                return mEmptyString;
            }

            lock (mLock)
            {
                return mHandles[(int)handle];
            }
        }

        static StringId()
        {
            mLock = new object();
            mEmptyString = string.Intern("");
            mHandles = new List<string>();
            mStrings = new Dictionary<string, uint>();
            mStrings[mEmptyString] = (uint)mHandles.Count;
            mHandles.Add(mEmptyString);
        }

        #endregion
    }
}
