﻿using System;

using Dynamic.Extensions;

namespace Dynamic
{
    public abstract class BitmapBase : IConvertible
    {
        private int mMaxBits;

        protected ulong Bitmap { get; private set; }

        protected BitmapBase(ulong bitmap, int maxBits)
        {
            Bitmap = bitmap;
            mMaxBits = maxBits;
        }

        public bool IsBit(int bit)
        {
            if (bit < 0 || bit >= mMaxBits)
            {
                throw new IndexOutOfRangeException("bit out of range");
            }

            return Bitmap.IsBit(bit);
        }

        public bool IsConstBit(int bit)
        {
            return Bitmap.IsConstBit(bit);
        }

        public void SetBit(int bit)
        {
            if (bit < 0 || bit >= mMaxBits)
            {
                throw new IndexOutOfRangeException("bit out of range");
            }

            Bitmap.SetBit(bit);
        }

        public void SetConstBit(int bit)
        {
            Bitmap.SetConstBit(bit);
        }

        public void ClearBit(int bit)
        {
            if (bit < 0 || bit >= mMaxBits)
            {
                throw new IndexOutOfRangeException("bit out of range");
            }

            Bitmap.ClearBit(bit);
        }

        public void ClearConstBit(int bit)
        {
            Bitmap.ClearConstBit(bit);
        }

        public void IfBit( bool cond, int bit)
        {
            if (bit < 0 || bit >= mMaxBits)
            {
                throw new IndexOutOfRangeException("bit out of range");
            }

            Bitmap.IfBit(cond, bit);
        }

        public void IfConstBit(bool cond, int bit)
        {
            Bitmap.IfConstBit(cond, bit);
        }

        public void IfIsBit(int bit, Action action)
        {
            Bitmap.IfIsBit(bit, action);
        }

        public void IfIsConstBit(int bit, Action action)
        {
            Bitmap.IfIsConstBit(bit, action);
        }

        public void Clear()
        {
            Bitmap = 0;
        }

        #region IConvertible

        public virtual TypeCode GetTypeCode()
        {
            throw new NotImplementedException();
        }

        public bool ToBoolean(IFormatProvider provider)
        {
            return Convert.ToBoolean(Bitmap);
        }

        public char ToChar(IFormatProvider provider)
        {
            return Convert.ToChar(Bitmap);
        }

        public sbyte ToSByte(IFormatProvider provider)
        {
            return Convert.ToSByte(Bitmap);
        }

        public byte ToByte(IFormatProvider provider)
        {
            return Convert.ToByte(Bitmap);
        }

        public short ToInt16(IFormatProvider provider)
        {
            return Convert.ToInt16(Bitmap);
        }

        public ushort ToUInt16(IFormatProvider provider)
        {
            return Convert.ToUInt16(Bitmap);
        }

        public int ToInt32(IFormatProvider provider)
        {
            return Convert.ToInt32(Bitmap);
        }

        public uint ToUInt32(IFormatProvider provider)
        {
            return Convert.ToUInt32(Bitmap);
        }

        public long ToInt64(IFormatProvider provider)
        {
            return Convert.ToInt64(Bitmap);
        }

        public ulong ToUInt64(IFormatProvider provider)
        {
            return Convert.ToUInt64(Bitmap);
        }

        public float ToSingle(IFormatProvider provider)
        {
            return Convert.ToSingle(Bitmap);
        }

        public double ToDouble(IFormatProvider provider)
        {
            return Convert.ToDouble(Bitmap);
        }

        public decimal ToDecimal(IFormatProvider provider)
        {
            return Convert.ToDecimal(Bitmap);
        }

        public DateTime ToDateTime(IFormatProvider provider)
        {
            return Convert.ToDateTime(Bitmap);
        }

        public string ToString(IFormatProvider provider)
        {
            return Convert.ToString(Bitmap, provider);
        }

        public object ToType(Type conversionType, IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class Bitmap8 : BitmapBase
    {
        public Bitmap8()
            : base(0, 8)
        {

        }

        public Bitmap8(byte bitmap)
            : base(bitmap, 8)
        {

        }
        
        public override TypeCode GetTypeCode()
        {
            return (TypeCode)251;
        }

        public byte Value { get { return (byte)Bitmap; } }

        public static implicit operator Bitmap8(byte value) { return new Bitmap8(value); }

        public static implicit operator byte(Bitmap8 bitmap) { return bitmap.Value; }
    }

    public class Bitmap16 : BitmapBase
    {
        public Bitmap16()
            : base(0, 16)
        {

        }

        public Bitmap16(ushort bitmap)
            : base(bitmap, 16)
        {

        }

        public override TypeCode GetTypeCode()
        {
            return (TypeCode)252;
        }

        public ushort Value { get { return (ushort)Bitmap; } }

        public static implicit operator Bitmap16(ushort value) { return new Bitmap16(value); }

        public static implicit operator ushort(Bitmap16 bitmap) { return bitmap.Value; }
    }

    public class Bitmap32 : BitmapBase
    {
        public Bitmap32()
            : base(0, 32)
        {

        }

        public Bitmap32(uint bitmap)
            : base(bitmap, 32)
        {

        }

        public override TypeCode GetTypeCode()
        {
            return (TypeCode)253;
        }

        public uint Value { get { return (uint)Bitmap; } }

        public static implicit operator Bitmap32(uint value) { return new Bitmap32(value); }

        public static implicit operator uint(Bitmap32 bitmap) { return bitmap.Value; }
    }

    public class Bitmap64 : BitmapBase
    {
        public Bitmap64()
            : base(0, 64)
        {

        }

        public Bitmap64(ulong bitmap)
            : base(bitmap, 64)
        {

        }

        public override TypeCode GetTypeCode()
        {
            return (TypeCode)254;
        }

        public ulong Value { get { return Bitmap; } }

        public static implicit operator Bitmap64(ulong value) { return new Bitmap64(value); }

        public static implicit operator ulong(Bitmap64 bitmap) { return bitmap.Value; }
    }
}
