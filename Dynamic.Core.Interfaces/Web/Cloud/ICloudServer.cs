﻿
using Dynamic.Data;
using Dynamic.Plugins;

namespace Dynamic.Web.Cloud
{
    public interface ICloudServerPlugin : IPlugin
    {
    }

    public class CloudServerPluginBase : PluginBase, ICloudServerPlugin
    {

    }

    public interface ICloudServerRequest : IPluginRequest
    {
        IReadOnlyDataContainer Headers { get; }

        string Target { get; }
        string Source { get; }
        IReadOnlyDataContainer Message { get; }
    }

    public interface ICloudServerResponse : IPluginResponse
    {
        IDataContainer Headers { get; }

        string Target { get; }
        string Source { get; }
        IDataContainer Message { get; }
    }

}
