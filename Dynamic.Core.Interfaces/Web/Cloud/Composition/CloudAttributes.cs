﻿using System;
using System.Collections.Generic;
using System.Text;

using Dynamic.Plugins.Composition;

namespace Dynamic.Web.Cloud.Composition
{
    [AttributeUsage(AttributeTargets.Class)]
    public class CloudContentAttribute : Attribute
    {
        public CloudContentAttribute(string name)
        {
            ContentTypeName = name;
        }

        public string ContentTypeName { get; private set; }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class CloudPropertyAttribute : Attribute
    {
        public CloudPropertyAttribute(string name)
        {
            PropertyName = name;
        }

        public string PropertyName { get; private set; }
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class CloudClientFactoryAttribute : Attribute
    {
        public CloudClientFactoryAttribute()
        {
        }
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class CloudServerFactoryAttribute : Attribute
    {
        public CloudServerFactoryAttribute()
        {
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class CloudServiceAttribute : Attribute
    {
        public CloudServiceAttribute(Type type, string name)
        {
            ServiceType = type;
            ServiceName = name;
        }

        public Type ServiceType { get; private set; }
        public string ServiceName { get; private set; }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class CloudServerServiceAttribute : PluginInformationAttribute
    {
        public CloudServerServiceAttribute(Type type, string name)
            : base(0, name, name, true)
        {
            ServiceType = type;
            ServiceName = name;
        }

        public Type ServiceType { get; private set; }
        public string ServiceName { get; private set; }
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class CloudServerRouteAttribute : MethodAttribute
    {
        public CloudServerRouteAttribute(string verb)
            : base(0, BuildName(verb, null, out int args, out Dictionary<string, string> map))
        {
            Verb = verb;
            Path = "";
            Args = args;
            ArgMap = map;
        }

        public CloudServerRouteAttribute(string verb, string path)
            : base(0, BuildName(verb, path, out int args, out Dictionary<string, string> map))
        {
            Verb = verb;
            Path = path;
            Args = args;
            ArgMap = map;
        }

        public string Verb { get; private set; }

        public string Path { get; private set; }

        public int Args { get; private set; }

        public Dictionary<string, string> ArgMap { get; private set; }

        private static string BuildName(string verb, string path, out int args, out Dictionary<string, string> map)
        {
            args = 0;
            map = null;

            StringBuilder builder = new StringBuilder();
            builder.Append(verb);
            if (!string.IsNullOrEmpty(path))
            {
                if (path.Contains("/{") && path.Contains("}"))
                {
                    int open = path.IndexOf('{');
                    builder.Append("_").Append(path.Substring(0, open - 1));

                    map = new Dictionary<string, string>();
                    while (path.Contains("/{") && path.Contains("}"))
                    {
                        ++args;
                        int close = path.IndexOf('}');
                        map.Add("cloud_arg_" + args, path.Substring(open + 1, close - open - 1));
                        path = path.Substring(close + 1);
                    }

                    builder.Append("_args_").Append(args);
                }
                else
                {
                    builder.Append("_").Append(path);
                }
            }

            return builder.ToString().Replace("/", "_").ToLower();

        }

    }
   
}
