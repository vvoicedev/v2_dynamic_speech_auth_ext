﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using Dynamic.Extensions;
using Dynamic.Loggers;

#if !NO_DYNAMIC_THREADING
using Dynamic.Threading;
#endif

namespace Dynamic.Web
{
    public enum HttpVersion
    {
        Version1_0,
        Version1_1,
    }

    public enum HttpRouteStatus
    {
        Unresolved,
        Resolving,
        NotFound,
        Found,
    }

    public enum HttpContentEncoding
    {
        Unknown,
        MultipartForm,
        PropertiesForm,
        UrlForm,
        Json,
        Plain,
        Xml
    }

    public class HttpHeaders : Dictionary<string, string>
    {
        public HttpHeaders()
        {
        }

        public HttpHeaders(int capacity)
            : base(capacity)
        {
        }

        internal HttpHeaders(Dictionary<string, string> dictionary)
            : base(dictionary)
        {
        }
    }

    public interface IHttpRequest
    {
        string Method { get; set; }
        string Query { get; set; }
        string UserAgent { get; set; }
        HttpVersion ProtocolVersion { get; set; }
        bool KeepAlive { get; set; }
        HttpHeaders Headers { get; set; }
        string ContentType { get; set; }
        int ContentLength { get; set; }
        CookieContainer CookieContainer { get; set; }
        string ConnectionGroupName { get; set; }
        int Timeout { get; set; }
    }

    public interface IHttpResponse
    {
        HttpStatusCode StatusCode { get; }
        string StatusDescription { get; }
        WebHeaderCollection Headers { get; }
        HttpContentEncoding EncodingType { get; }
        string ContentType { get; }
        object ContentData { get; }
    }

    public interface IHttpRequestContent
    {
        string ContentType { get; }
        byte[] ContentData { get; }
    }

#if !NO_DYNAMIC_THREADING
    public interface IHttpAsyncRouteStatus : IAsyncTask<HttpRouteStatus>
    {
    }

    public interface IHttpAsyncResponse : IAsyncTask<IHttpResponse>
    {
    }
#endif

    public interface IHttpContentParser
    {
        string ContentType { get; }
        object Parse(WebHeaderCollection headers, Stream stream);
    }

    public interface IHttpContentPart : IDisposable
    {
        string ContentDisposition { get; }
        string ContentType { get; set; }
        string FileName { get; set; }
    }

    public sealed class HttpContentElement : IDisposable
    {
        public string Name { get; set; }
        public object Value { get; set; }

        public HttpContentElement(string name, object value)
        {
            Name = name;
            Value = value;
        }

        public void Dispose()
        {
            if (Value is IDisposable)
            {
                ((IDisposable)Value).Dispose();
            }
        }
    }

    public sealed class HttpContentParameters : IEnumerable<HttpContentElement>
    {
        private List<HttpContentElement> mList;

        public HttpContentParameters()
        {
            mList = new List<HttpContentElement>();
        }

        public HttpContentParameters(List<HttpContentElement> list)
        {
            if (list != null)
            {
                mList = new List<HttpContentElement>(list);
            }
            else
            {
                mList = new List<HttpContentElement>();
            }
        }

        public HttpContentParameters(Dictionary<string, object> elements)
        {
            mList = new List<HttpContentElement>();
            if (elements != null)
            {
                foreach (var elem in elements)
                {
                    Add(elem.Key, elem.Value);
                }
            }
        }

        public void Add(string name, object value)
        {
            mList.Add(new HttpContentElement(name, value));
        }

        public void Remove(string name)
        {
            mList.RemoveAll(item => item.Name == name);
        }

        public void Remove(string name, object value)
        {
            foreach (var item in mList)
            {
                if (item.Name.Equals(name) && item.Value.Equals(value))
                {
                    mList.Remove(item);
                    return;
                }
            }
        }

        public void Clear()
        {
            mList.Clear();
        }

        public object this[string name]
        {
            get
            {
                foreach (var item in mList)
                {
                    if (item.Name.Equals(name))
                    {
                        return item.Value;
                    }
                }

                // Element wasnt found
                var itm = new HttpContentElement(name, null);
                mList.Add(itm);
                return itm.Value;
            }

            set
            {
                foreach (var item in mList)
                {
                    if (item.Name.Equals(name))
                    {
                        item.Value = value;
                        return;
                    }
                }

                // Element wasnt found
                Add(name, value);
            }
        }

        public HttpContentElement this[int index]
        {
            get
            {
                return mList[index];
            }

            set
            {
                mList[index] = value;
            }
        }

        public int Count { get { return mList.Count; } }

        public IEnumerator<HttpContentElement> GetEnumerator()
        {
            return mList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return mList.GetEnumerator();
        }

        public HttpContentElement First
        {
            get
            {
                return mList.GetFirst();
            }
        }

        public HttpContentElement Last
        {
            get
            {
                return mList.GetLast();
            }
        }

        public List<HttpContentElement> ToList()
        {
            return mList;
        }

        public HttpContentParameters Clone()
        {
            return new HttpContentParameters(mList);
        }

        public IEnumerable ToKeys()
        {
            return new KeyEnumerator(this);
        }

        public IEnumerable ToValues()
        {
            return new ValueEnumerator(this);
        }

        public struct KeyEnumerator : IEnumerator<string>, IDisposable, IEnumerator, IEnumerable
        {
            private HttpContentParameters mHttpContentParameters;
            private IEnumerator<HttpContentElement> mEnumerator;

            internal KeyEnumerator(HttpContentParameters contentParameters)
            {
                mHttpContentParameters = contentParameters;
                mEnumerator = mHttpContentParameters.GetEnumerator();
                Current = null;
            }

            public string Current { get; private set; }

            object IEnumerator.Current => Current;

            public void Dispose()
            {
                mEnumerator = null;
            }

            public IEnumerator GetEnumerator()
            {
                return this;
            }

            public bool MoveNext()
            {
                if (mEnumerator.MoveNext())
                {
                    Current = mEnumerator.Current.Name;
                    return true;
                }
                return false;
            }

            public void Reset()
            {
                mEnumerator = mHttpContentParameters.GetEnumerator();
                Current = null;
            }
        }

        public struct ValueEnumerator : IEnumerator<object>, IDisposable, IEnumerator, IEnumerable
        {
            private HttpContentParameters mHttpContentParameters;
            private IEnumerator<HttpContentElement> mEnumerator;

            internal ValueEnumerator(HttpContentParameters contentParameters)
            {
                mHttpContentParameters = contentParameters;
                mEnumerator = mHttpContentParameters.GetEnumerator();
                Current = null;
            }

            public object Current { get; private set; }

            object IEnumerator.Current => Current;

            public void Dispose()
            {
                mEnumerator = null;
            }

            public IEnumerator GetEnumerator()
            {
                return this;
            }

            public bool MoveNext()
            {
                if (mEnumerator.MoveNext())
                {
                    Current = mEnumerator.Current.Value;
                    return true;
                }
                return false;
            }

            public void Reset()
            {
                mEnumerator = mHttpContentParameters.GetEnumerator();
                Current = null;
            }
        }
    }

    public delegate void HttpRouteCallback(HttpRouteStatus status);

    public delegate void HttpResponseCallback(IHttpResponse response);

    public delegate void HttpMultipartBuilder(IHttpContentPart part, string name, string boundary, Stream stream, Encoding encoding);

    public interface IHttpClient : IDisposable
    {
#region Public ReadOnly

        Uri BaseEndpoint { get; }

        string AgentType { get; }

        Dictionary<string, string> Headers { get; }

        HttpRouteStatus RouteStatus { get; }

        ILogger Logger { get; }

#endregion

#region Route Status Methods

        HttpRouteStatus ResolveRoute();

        Task<HttpRouteStatus> ResolveRouteTaskAsync();

#if !NO_DYNAMIC_THREADING
        IHttpAsyncRouteStatus ResolveRouteAsync();

        IHttpAsyncRouteStatus ResolveRouteCallback(HttpRouteCallback callback);
#endif

#endregion

#region Web Service Methods

        IHttpRequest CreateRequest();

        IHttpRequest CreateRequest(string method);

        IHttpRequest CreateRequest(string method, HttpVersion version);

        IHttpRequest CreateRequest(string method, string query);

        IHttpRequest CreateRequest(string method, HttpVersion version, string query);

        IHttpRequest CreateRequest(string method, HttpVersion version, string query, params object[] args);

        void SendRequest(IHttpRequest request);

        Stream SendContentRequest(IHttpRequest request);

        bool SendContentRequest(IHttpRequest request, IHttpRequestContent content);

        bool SendContentRequest(IHttpRequest request, HttpContentParameters contentParameters, HttpContentEncoding encoding);

        IHttpResponse ReceiveResponse();

#if !NO_DYNAMIC_THREADING
        IHttpAsyncResponse ReceiveAsyncResponse(IHttpRequest request);

        IHttpAsyncResponse ReceiveAsyncResponse(IHttpRequest request, IHttpRequestContent content);

        IHttpAsyncResponse ReceiveAsyncResponse(IHttpRequest request, HttpContentParameters parameters, HttpContentEncoding encoding);

        IHttpAsyncResponse ReceiveCallbackResponse(IHttpRequest request, HttpResponseCallback callback);

        IHttpAsyncResponse ReceiveCallbackResponse(IHttpRequest request, IHttpRequestContent content, HttpResponseCallback callback);

        IHttpAsyncResponse ReceiveCallbackResponse(IHttpRequest request, HttpContentParameters parameters, HttpContentEncoding encoding, HttpResponseCallback callback);
#endif

        Task<Stream> SendContentRequestAsync(IHttpRequest request);

        Task<bool> SendContentRequestAsync(IHttpRequest request, IHttpRequestContent content);

        Task<bool> SendContentRequestAsync(IHttpRequest request, HttpContentParameters contentParameters, HttpContentEncoding encoding);

        Task<IHttpResponse> ReceiveResponseAsync();

#endregion

#region Common Http / Rest Methods

#region Synchronous

        IHttpResponse Get(string query);

        IHttpResponse Get(HttpHeaders headers);

        IHttpResponse Get(string query, HttpHeaders headers);

        IHttpResponse Get(string query, HttpHeaders headers, CookieContainer cookieContainer);

        IHttpResponse Post(string query);

        IHttpResponse Post(HttpHeaders headers);

        IHttpResponse Post(string query, HttpHeaders headers);

        IHttpResponse Post(string query, HttpHeaders headers, CookieContainer cookieContainer);

        IHttpResponse Post(HttpContentParameters parameters, HttpContentEncoding encoding);

        IHttpResponse Post(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding);

        IHttpResponse Post(HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        IHttpResponse Post(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        IHttpResponse Post(string query, HttpContentParameters parameters, HttpContentEncoding encoding);

        IHttpResponse Post(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding);

        IHttpResponse Post(string query, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        IHttpResponse Post(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        IHttpResponse Put(string query);

        IHttpResponse Put(HttpHeaders headers);

        IHttpResponse Put(string query, HttpHeaders headers);

        IHttpResponse Put(string query, HttpHeaders headers, CookieContainer cookieContainer);

        IHttpResponse Put(HttpContentParameters parameters, HttpContentEncoding encoding);

        IHttpResponse Put(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding);

        IHttpResponse Put(HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        IHttpResponse Put(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        IHttpResponse Put(string query, HttpContentParameters parameters, HttpContentEncoding encoding);

        IHttpResponse Put(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding);

        IHttpResponse Put(string query, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        IHttpResponse Put(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        IHttpResponse Delete(string query);

        IHttpResponse Delete(HttpHeaders headers);

        IHttpResponse Delete(string query, HttpHeaders headers);

        IHttpResponse Delete(string query, HttpHeaders headers, CookieContainer cookieContainer);

        IHttpResponse Options(string query);

        IHttpResponse Options(HttpHeaders headers);

        IHttpResponse Options(string query, HttpHeaders headers);

        IHttpResponse Options(string query, HttpHeaders headers, CookieContainer cookieContainer);

#endregion

#region Asynchronous
#if !NO_DYNAMIC_THREADING

        IHttpAsyncResponse GetAsync(string query);

        IHttpAsyncResponse GetAsync(HttpHeaders headers);

        IHttpAsyncResponse GetAsync(string query, HttpHeaders headers);

        IHttpAsyncResponse GetAsync(string query, HttpHeaders headers, CookieContainer cookieContainer);

        IHttpAsyncResponse PostAsync(string query);

        IHttpAsyncResponse PostAsync(HttpHeaders headers);

        IHttpAsyncResponse PostAsync(string query, HttpHeaders headers);

        IHttpAsyncResponse PostAsync(string query, HttpHeaders headers, CookieContainer cookieContainer);

        IHttpAsyncResponse PostAsync(HttpContentParameters parameters, HttpContentEncoding encoding);

        IHttpAsyncResponse PostAsync(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding);

        IHttpAsyncResponse PostAsync(HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        IHttpAsyncResponse PostAsync(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        IHttpAsyncResponse PostAsync(string query, HttpContentParameters parameters, HttpContentEncoding encoding);

        IHttpAsyncResponse PostAsync(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding);

        IHttpAsyncResponse PostAsync(string query, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        IHttpAsyncResponse PostAsync(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        IHttpAsyncResponse PutAsync(string query);

        IHttpAsyncResponse PutAsync(HttpHeaders headers);

        IHttpAsyncResponse PutAsync(string query, HttpHeaders headers);

        IHttpAsyncResponse PutAsync(string query, HttpHeaders headers, CookieContainer cookieContainer);

        IHttpAsyncResponse PutAsync(HttpContentParameters parameters, HttpContentEncoding encoding);

        IHttpAsyncResponse PutAsync(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding);

        IHttpAsyncResponse PutAsync(HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        IHttpAsyncResponse PutAsync(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        IHttpAsyncResponse PutAsync(string query, HttpContentParameters parameters, HttpContentEncoding encoding);

        IHttpAsyncResponse PutAsync(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding);

        IHttpAsyncResponse PutAsync(string query, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        IHttpAsyncResponse PutAsync(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        IHttpAsyncResponse DeleteAsync(string query);

        IHttpAsyncResponse DeleteAsync(HttpHeaders headers);

        IHttpAsyncResponse DeleteAsync(string query, HttpHeaders headers);

        IHttpAsyncResponse DeleteAsync(string query, HttpHeaders headers, CookieContainer cookieContainer);

        IHttpAsyncResponse OptionsAsync(string query);

        IHttpAsyncResponse OptionsAsync(HttpHeaders headers);

        IHttpAsyncResponse OptionsAsync(string query, HttpHeaders headers);

        IHttpAsyncResponse OptionsAsync(string query, HttpHeaders headers, CookieContainer cookieContainer);

#endif
#endregion

#region Asynchronous Callbacks
#if !NO_DYNAMIC_THREADING

        IHttpAsyncResponse GetCallback(string query, HttpResponseCallback callback);

        IHttpAsyncResponse GetCallback(HttpHeaders headers, HttpResponseCallback callback);

        IHttpAsyncResponse GetCallback(string query, HttpHeaders headers, HttpResponseCallback callback);

        IHttpAsyncResponse GetCallback(string query, HttpHeaders headers, CookieContainer cookieContainer, HttpResponseCallback callback);

        IHttpAsyncResponse PostCallback(string query, HttpResponseCallback callback);

        IHttpAsyncResponse PostCallback(HttpHeaders headers, HttpResponseCallback callback);

        IHttpAsyncResponse PostCallback(string query, HttpHeaders headers, HttpResponseCallback callback);

        IHttpAsyncResponse PostCallback(string query, HttpHeaders headers, CookieContainer cookieContainer, HttpResponseCallback callback);

        IHttpAsyncResponse PostCallback(HttpContentParameters parameters, HttpContentEncoding encoding, HttpResponseCallback callback);

        IHttpAsyncResponse PostCallback(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, HttpResponseCallback callback);

        IHttpAsyncResponse PostCallback(HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer, HttpResponseCallback callback);

        IHttpAsyncResponse PostCallback(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer, HttpResponseCallback callback);

        IHttpAsyncResponse PostCallback(string query, HttpContentParameters parameters, HttpContentEncoding encoding, HttpResponseCallback callback);

        IHttpAsyncResponse PostCallback(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, HttpResponseCallback callback);

        IHttpAsyncResponse PostCallback(string query, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer, HttpResponseCallback callback);

        IHttpAsyncResponse PostCallback(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer, HttpResponseCallback callback);

        IHttpAsyncResponse PutCallback(string query, HttpResponseCallback callback);

        IHttpAsyncResponse PutCallback(HttpHeaders headers, HttpResponseCallback callback);

        IHttpAsyncResponse PutCallback(string query, HttpHeaders headers, HttpResponseCallback callback);

        IHttpAsyncResponse PutCallback(string query, HttpHeaders headers, CookieContainer cookieContainer, HttpResponseCallback callback);

        IHttpAsyncResponse PutCallback(HttpContentParameters parameters, HttpContentEncoding encoding, HttpResponseCallback callback);

        IHttpAsyncResponse PutCallback(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, HttpResponseCallback callback);
        
        IHttpAsyncResponse PutCallback(HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer, HttpResponseCallback callback);

        IHttpAsyncResponse PutCallback(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer, HttpResponseCallback callback);

        IHttpAsyncResponse PutCallback(string query, HttpContentParameters parameters, HttpContentEncoding encoding, HttpResponseCallback callback);

        IHttpAsyncResponse PutCallback(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, HttpResponseCallback callback);

        IHttpAsyncResponse PutCallback(string query, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer, HttpResponseCallback callback);

        IHttpAsyncResponse PutCallback(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer, HttpResponseCallback callback);

        IHttpAsyncResponse DeleteCallback(string query, HttpResponseCallback callback);

        IHttpAsyncResponse DeleteCallback(HttpHeaders headers, HttpResponseCallback callback);

        IHttpAsyncResponse DeleteCallback(string query, HttpHeaders headers, HttpResponseCallback callback);

        IHttpAsyncResponse DeleteCallback(string query, HttpHeaders headers, CookieContainer cookieContainer, HttpResponseCallback callback);

        IHttpAsyncResponse OptionsCallback(string query, HttpResponseCallback callback);

        IHttpAsyncResponse OptionsCallback(HttpHeaders headers, HttpResponseCallback callback);

        IHttpAsyncResponse OptionsCallback(string query, HttpHeaders headers, HttpResponseCallback callback);

        IHttpAsyncResponse OptionsCallback(string query, HttpHeaders headers, CookieContainer cookieContainer, HttpResponseCallback callback);

#endif
#endregion

#region Asynchronous Tasks

        Task<IHttpResponse> GetTaskAsync(string query);

        Task<IHttpResponse> GetTaskAsync(HttpHeaders headers);

        Task<IHttpResponse> GetTaskAsync(string query, HttpHeaders headers);

        Task<IHttpResponse> GetTaskAsync(string query, HttpHeaders headers, CookieContainer cookieContainer);

        Task<IHttpResponse> PostTaskAsync(string query);

        Task<IHttpResponse> PostTaskAsync(HttpHeaders headers);

        Task<IHttpResponse> PostTaskAsync(string query, HttpHeaders headers);

        Task<IHttpResponse> PostTaskAsync(string query, HttpHeaders headers, CookieContainer cookieContainer);

        Task<IHttpResponse> PostTaskAsync(HttpContentParameters parameters, HttpContentEncoding encoding);

        Task<IHttpResponse> PostTaskAsync(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding);

        Task<IHttpResponse> PostTaskAsync(HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        Task<IHttpResponse> PostTaskAsync(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        Task<IHttpResponse> PostTaskAsync(string query, HttpContentParameters parameters, HttpContentEncoding encoding);

        Task<IHttpResponse> PostTaskAsync(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding);

        Task<IHttpResponse> PostTaskAsync(string query, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        Task<IHttpResponse> PostTaskAsync(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        Task<IHttpResponse> PutTaskAsync(string query);

        Task<IHttpResponse> PutTaskAsync(HttpHeaders headers);

        Task<IHttpResponse> PutTaskAsync(string query, HttpHeaders headers);

        Task<IHttpResponse> PutTaskAsync(string query, HttpHeaders headers, CookieContainer cookieContainer);

        Task<IHttpResponse> PutTaskAsync(HttpContentParameters parameters, HttpContentEncoding encoding);

        Task<IHttpResponse> PutTaskAsync(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding);

        Task<IHttpResponse> PutTaskAsync(HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        Task<IHttpResponse> PutTaskAsync(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        Task<IHttpResponse> PutTaskAsync(string query, HttpContentParameters parameters, HttpContentEncoding encoding);

        Task<IHttpResponse> PutTaskAsync(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding);

        Task<IHttpResponse> PutTaskAsync(string query, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        Task<IHttpResponse> PutTaskAsync(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer);

        Task<IHttpResponse> DeleteTaskAsync(string query);

        Task<IHttpResponse> DeleteTaskAsync(HttpHeaders headers);

        Task<IHttpResponse> DeleteTaskAsync(string query, HttpHeaders headers);

        Task<IHttpResponse> DeleteTaskAsync(string query, HttpHeaders headers, CookieContainer cookieContainer);

        Task<IHttpResponse> OptionsTaskAsync(string query);

        Task<IHttpResponse> OptionsTaskAsync(HttpHeaders headers);

        Task<IHttpResponse> OptionsTaskAsync(string query, HttpHeaders headers);

        Task<IHttpResponse> OptionsTaskAsync(string query, HttpHeaders headers, CookieContainer cookieContainer);

#endregion

#endregion
    }

}
