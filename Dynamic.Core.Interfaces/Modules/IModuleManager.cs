﻿/* The MIT License (MIT)
* 
* Copyright (c) 2015 Marc Clifton
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel.Composition.Hosting;

namespace Dynamic.Modules
{
    public interface IModuleInfo
    {
        string Name { get; }
        Dictionary<string, object> Args { get; }
    }

    public interface IModuleManager
    {
        /// <summary>
        /// Register modules specified in a list of assembly filenames.
        /// </summary>
        /// <param name="moduleFilenames"></param>
        /// <param name="optionalPath"></param>
        /// <param name="resourceAssemblyResolver"></param>
        void RegisterModules(List<IModuleInfo> moduleItems, string optionalPath = null, Func<string, Assembly> resourceAssemblyResolver = null);
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="container"></param>
        /// <param name="moduleItems"></param>
        /// <param name="optionalPath"></param>
        /// <param name="resourceAssemblyResolver"></param>
        void RegisterModules(CompositionContainer container, List<IModuleInfo> moduleItems = null, string optionalPath = null, Func<string, Assembly> resourceAssemblyResolver = null);

        /// <summary>
        /// 
        /// </summary>
        ReadOnlyCollection<IModule> Modules { get; }
    }
}
