﻿
namespace Dynamic.Database
{
    public interface IReadableContentValues
    {
        bool GetBool(string key);

        short GetShort(string key);

        ushort GetUShort(string key);

        int GetInteger(string key);

        uint GetUInteger(string key);

        long GetLong(string key);

        ulong GetULong(string key);

        float GetSingle(string key);

        double GetDouble(string key);

        string GetString(string key);

        object GetObject(string key);

        bool Has(string key);

        int Count { get; }

        string[] Columns { get; }

        string ToText();
    }
}
