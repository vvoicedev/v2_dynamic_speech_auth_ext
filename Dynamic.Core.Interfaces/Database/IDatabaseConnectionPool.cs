﻿using System;

namespace Dynamic.Database
{
    public interface IDatabaseConnectionPool : IDisposable
    {
        IDatabaseConnection Acquire();

        void Release(IDatabaseConnection connection);

        bool IsDisposed { get; }

    }
}
