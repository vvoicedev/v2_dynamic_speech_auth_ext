﻿using System;

using Dynamic.Data;

namespace Dynamic.Database
{
    public interface IDatabaseConnection : IDisposable
    {
        bool ContainsTables(string[] tableNames);

        bool ContainsViews(string[] viewNames);

        bool ContainsFunctions(string[] functionNames);

        bool ContainsFeatures(string[] tableNames, string[] viewNames, string[] functionNames);

        IDataSet RawQuery(string sql, string[] selectionArgs);

        int RawNonQuery(string sql, string[] selectionArgs);

        bool RawQueryHas(string sql, string[] selectionArgs);

        int RawQueryId(string sql, string[] selectionArgs, string idField = "_ID");

        int[] RawQueryIdList(string sql, string[] selectionArgs, string idField = "_ID");

        IReadableContentValues RawQueryTopRow(string sql, string[] selectionArgs);

        object RawQueryField(string sql, string[] selectionArgs, string field);

        IDataSet Query(bool distinct, int top, string table, string[] columns, string selection, string[] selectionArgs, string groupBy, string having, string orderBy, string limit);

        IDataSet Query(bool distinct, string table, string[] columns, string selection, string[] selectionArgs, string groupBy, string having, string orderBy, string limit);

        IDataSet Query(string table, string[] columns, string selection, string[] selectionArgs, string groupBy, string having, string orderBy);

        IDataSet Query(string table, string[] columns, string selection, string[] selectionArgs, string groupBy, string having, string orderBy, string limit);

        int QueryId(string table, string selection, string[] selectionArgs, string idField = "_ID");

        int QueryId(string table, IReadableContentValues values, string idField = "_ID");

        int[] QueryIdList(string table, string selection, string[] selectionArgs, string idField = "_ID");

        IDataSource QueryDataSource(string table, string[] columns);

        IDataSource QueryDataSource(string table, string[] columns, string selection);

        int Insert(string table, ContentValues values);

        int InsertOrThrow(string table, ContentValues values);

        int InsertWithOnConflict(string table, ContentValues values, ConflictAlgorithm algo);

        IContentOutputValues Insert(string table, ContentValues values, ContentOutputStatements outputs);

        IContentOutputValues InsertOrThrow(string table, ContentValues values, ContentOutputStatements outputs);

        IContentOutputValues InsertWithOnConflict(string table, ContentValues values, ContentOutputStatements outputs, ConflictAlgorithm algo);

        int Update(string table, ContentValues values, string whereClause, string[] whereArgs);

        int UpdateWithOnConflict(string table, ContentValues values, string whereClause, string[] whereArgs, ConflictAlgorithm algo);

        IContentOutputValues Update(string table, ContentValues values, ContentOutputStatements outputs, string whereClause, string[] whereArgs);

        IContentOutputValues UpdateWithOnConflict(string table, ContentValues values, ContentOutputStatements outputs, string whereClause, string[] whereArgs, ConflictAlgorithm algo);

        int Upsert(string table, ContentValues values, string whereClause, string[] whereArgs);

        int UpsertWithOnConflict(string table, ContentValues values, string whereClause, string[] whereArgs, ConflictAlgorithm algo);

        IContentOutputValues Upsert(string table, ContentValues values, ContentOutputStatements outputs, string whereClause, string[] whereArgs);

        IContentOutputValues UpsertWithOnConflict(string table, ContentValues values, ContentOutputStatements outputs, string whereClause, string[] whereArgs, ConflictAlgorithm algo);

        int Delete(string table, string whereClause, string[] whereArgs);

        bool IsDisposed { get; }
    }
}
