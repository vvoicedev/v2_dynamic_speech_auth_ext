﻿using System;

namespace Dynamic.Database
{
    public class ConnectionStringBuilder
    {
        private string mDataSource;
        private ushort? mPort;
        private string mInitialCatalog;
        private string mUserId;
        private string mPassword;
        private bool mIsSQLiteBuilder;

        public ConnectionStringBuilder(bool isSQLiteBuilder = false)
        {
            mDataSource = null;
            mPort = null;
            mInitialCatalog = null;
            mUserId = null;
            mPassword = null;
            mIsSQLiteBuilder = isSQLiteBuilder;
        }

        public ConnectionStringBuilder SetDataSource(string dataSource)
        {
            mDataSource = dataSource;
            return this;
        }

        public ConnectionStringBuilder SetPort(ushort? port)
        {
            mPort = port;
            return this;
        }

        public ConnectionStringBuilder SetInitialCatalog(string initialCatalog)
        {
            mInitialCatalog = initialCatalog;
            return this;
        }

        public ConnectionStringBuilder SetUserId(string userId)
        {
            mUserId = userId;
            return this;
        }

        public ConnectionStringBuilder SetPassword(string password)
        {
            mPassword = password;
            return this;
        }

        public string Build()
        {
            if(string.IsNullOrEmpty(mDataSource))
            {
                throw new ArgumentException("Missing DataSource");
            }

            if (!mIsSQLiteBuilder)
            {
                if (string.IsNullOrEmpty(mInitialCatalog))
                {
                    throw new ArgumentException("Missing InitialCatalog");
                }

                if (string.IsNullOrEmpty(mUserId))
                {
                    throw new ArgumentException("Missing UserId");
                }

                if (string.IsNullOrEmpty(mPassword))
                {
                    throw new ArgumentException("Missing Password");
                }
            }

            if (!mIsSQLiteBuilder)
            {
                if (mPort == null)
                {
                    return string.Format(
                               "Data Source={0};Initial Catalog={1};User Id={2};Password={3}",
                               mDataSource,
                               mInitialCatalog,
                               mUserId,
                               mPassword
                           );
                }
                else
                {
                    return string.Format(
                                "Data Source={0},{1};Initial Catalog={2};User Id={3};Password={4}",
                                mDataSource,
                                mPort,
                                mInitialCatalog,
                                mUserId,
                                mPassword
                            );
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(mPassword))
                {
                    return string.Format(
                               "Data Source={0};Version=3;Password={1}",
                               mDataSource,
                               mPassword
                           );
                }
                else
                {
                    return string.Format(
                               "Data Source={0};Version=3;",
                               mDataSource
                           );
                }
            }
        }
    }
}
