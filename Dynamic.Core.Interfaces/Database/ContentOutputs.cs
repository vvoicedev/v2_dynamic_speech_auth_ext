﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Dynamic.Database
{

    public enum ContentOutputOperation
    {
        Either,
        Insert,
        Update
    }

    public class ContentOutputStatement
    {
        public string Name { get; private set; }
        public string Statement { get; private set; }
        public Type Type { get; private set; }
        public ContentOutputOperation Operation { get; private set; }

        public string OutputStatement
        {
            get
            {
                if (Statement.ToLower().Contains("as " + Name.ToLower()))
                {
                    return Statement;
                }
                else if(!Statement.StartsWith("("))
                {
                    return "(" + Statement + ") AS " + Name;
                }
                else
                {
                    return Statement + " AS " + Name;
                }
            }
        }

        internal ContentOutputStatement(string outputName, string outputStatement, Type outputType, ContentOutputOperation op)
        {
            Name = outputName;
            Statement = outputStatement;
            Type = outputType;
            Operation = op;
        }
    }

    public class ContentOutputStatements
    {
        private List<ContentOutputStatement> mStatements;

        public ContentOutputStatements()
        {
            mStatements = new List<ContentOutputStatement>();
        }

        public bool AddOutputStatement(string outputName, string outputStatement, Type outputType, ContentOutputOperation op = ContentOutputOperation.Either)
        {
            if (Has(outputName))
            {
                return false;
            }

            if (outputName.ToLower().Equals("_id"))
            {
                return false;
            }

            string lowerOutputStatement = outputStatement.ToLower();
            if ((lowerOutputStatement.Contains("inserted.") || lowerOutputStatement.Contains("deleted.")) && 
                lowerOutputStatement.Contains(outputName))
            {
                mStatements.Add(new ContentOutputStatement(outputName, outputStatement, outputType, op));
                return true;
            }

            return false;
        }

        public bool AddInsertOutputState(string outputName, string outputStatement, Type outputType)
        {
            return AddOutputStatement(outputName, outputStatement, outputType, ContentOutputOperation.Insert);
        }

        public bool AddUpdateOutputState(string outputName, string outputStatement, Type outputType)
        {
            return AddOutputStatement(outputName, outputStatement, outputType, ContentOutputOperation.Update);
        }

        public bool Has(string outputName, ContentOutputOperation op = ContentOutputOperation.Either)
        {
            return IndexOf(outputName, op) != -1;
        }

        public int IndexOf(string outputName, ContentOutputOperation op = ContentOutputOperation.Either)
        {
            int i = 0;
            foreach (ContentOutputStatement statement in mStatements)
            {
                if (statement.Name.Equals(outputName) && (op == ContentOutputOperation.Either ||
                    statement.Operation == ContentOutputOperation.Either || op == statement.Operation))
                {
                    return i;
                }
                ++i;
            }
            return -1;
        }

        public void Remove(string outputName, ContentOutputOperation op = ContentOutputOperation.Either)
        {
            int index = IndexOf(outputName, op);
            if (index == -1) return;
            mStatements.RemoveAt(index);
        }

        public void Clear()
        {
            mStatements.Clear();
        }

        public IEnumerable<ContentOutputStatement> GetInsertEnumerator()
        {
            foreach (ContentOutputStatement statement in mStatements)
            {
                if (statement.Operation == ContentOutputOperation.Insert ||
                    statement.Operation == ContentOutputOperation.Either)
                {
                    yield return statement;
                }
            }
        }

        public IEnumerable<ContentOutputStatement> GetUpdateEnumerator()
        {
            foreach (ContentOutputStatement statement in mStatements)
            {
                if (statement.Operation == ContentOutputOperation.Update ||
                    statement.Operation == ContentOutputOperation.Either)
                {
                    yield return statement;
                }
            }
        }

        public int Count { get { return mStatements.Count; } }
        
        public int InsertCount
        {
            get
            {
                int count = 0;
                foreach (ContentOutputStatement statement in mStatements)
                {
                    if (statement.Operation == ContentOutputOperation.Insert ||
                        statement.Operation == ContentOutputOperation.Either)
                    {
                        ++count;
                    }
                }
                return count;
            }
        }

        public int UpdateCount
        {
            get
            {
                int count = 0;
                foreach (ContentOutputStatement statement in mStatements)
                {
                    if (statement.Operation == ContentOutputOperation.Update ||
                        statement.Operation == ContentOutputOperation.Either)
                    {
                        ++count;
                    }
                }
                return count;
            }
        }
        
        public string[] InsertColumns
        {
            get
            {
                int i = 0;
                string[] a = new string[InsertCount];
                foreach (ContentOutputStatement statement in GetInsertEnumerator())
                {
                    a[i++] = statement.Name;
                }
                return a;
            }
        }

        public string[] UpdateColumns
        {
            get
            {
                int i = 0;
                string[] a = new string[UpdateCount];
                foreach (ContentOutputStatement statement in GetUpdateEnumerator())
                {
                    a[i++] = statement.Name;
                }
                return a;
            }
        }

    }

    public interface IContentOutputValue
    {
        string OutputName { get; }
        string OutputStatement { get; }
        Type ExpectedOutputType { get; }
        Type ActualOutputType { get; }
        object OutputValue { get; }
        bool OutputIsNull { get; }

        bool GetAsBool();
        short GetAsShort();
        ushort GetAsUShort();
        int GetAsInteger();
        uint GetAsUInteger();
        long GetAsLong();
        ulong GetAsULong();
        float GetAsSingle();
        double GetAsDouble();
        string GetAsString();
        object GetAsObject();
    }

    public interface IContentOutputValues : IEnumerable<KeyValuePair<string, IContentOutputValue>>, IEnumerable
    {
        int Id { get; }

        int RowsAffected { get; }

        string[] Columns { get; }

        bool Has(string outputName);

        IContentOutputValue this[string outputName] { get; }

    }
}
