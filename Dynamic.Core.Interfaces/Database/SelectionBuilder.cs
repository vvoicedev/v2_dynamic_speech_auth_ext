﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dynamic.Database
{
    /**
     * Helper for building selection clauses for {...}.
     *
     * <p>This class provides a convenient frontend for working with SQL. Instead of composing statements
     * manually using string concatenation, method calls are used to construct the statement one
     * clause at a time. These methods can be chained together.
     *
     * <p>If multiple where() statements are provided, they're combined using {@code AND}.
     *
     * <p>Example:
     *
     * <pre>
     *     SelectionBuilder builder = new SelectionBuilder();
     *     Cursor c = builder.table(FeedContract.Entry.TABLE_NAME)       // String TABLE_NAME = "entry"
     *                       .where(FeedContract.Entry._ID + "=?", id);  // String _ID = "_ID"
     *                       .query(db, projection, sortOrder)
     *
     * </pre>
     *
     * <p>In this example, the table name and filters ({@code WHERE} clauses) are both explicitly
     * specified via method call. SelectionBuilder takes care of issuing a "query" command to the
     * database, and returns the resulting {@link Cursor} object.
     *
     * <p>Inner {@code JOIN}s can be accomplished using the mapToTable() function. The map() function
     * can be used to create new columns based on arbitrary (SQL-based) criteria. In advanced usage,
     * entire subqueries can be passed into the map() function.
     *
     * <p>Advanced example:
     *
     * <pre>
     *     // String SESSIONS_JOIN_BLOCKS_ROOMS = "sessions "
     *     //        + "LEFT OUTER JOIN blocks ON sessions.block_id=blocks.block_id "
     *     //        + "LEFT OUTER JOIN rooms ON sessions.room_id=rooms.room_id";
     *
     *     // String Subquery.BLOCK_NUM_STARRED_SESSIONS =
     *     //       "(SELECT COUNT(1) FROM "
     *     //        + Tables.SESSIONS + " WHERE " + Qualified.SESSIONS_BLOCK_ID + "="
     *     //        + Qualified.BLOCKS_BLOCK_ID + " AND " + Qualified.SESSIONS_STARRED + "=1)";
     *
     *     String Subqery.BLOCK_SESSIONS_COUNT =
     *     Cursor c = builder.table(Tables.SESSIONS_JOIN_BLOCKS_ROOMS)
     *               .map(Blocks.NUM_STARRED_SESSIONS, Subquery.BLOCK_NUM_STARRED_SESSIONS)
     *               .mapToTable(Sessions._ID, Tables.SESSIONS)
     *               .mapToTable(Sessions.SESSION_ID, Tables.SESSIONS)
     *               .mapToTable(Sessions.BLOCK_ID, Tables.SESSIONS)
     *               .mapToTable(Sessions.ROOM_ID, Tables.SESSIONS)
     *               .where(Qualified.SESSIONS_BLOCK_ID + "=?", blockId);
     * </pre>
     *
     * <p>In this example, we have two different types of {@code JOIN}s: a left outer join using a
     * modified table name (since this class doesn't directly support these), and an inner join using
     * the mapToTable() function. The map() function is used to insert a count based on specific
     * criteria, executed as a sub-query.
     *
     * This class is <em>not</em> thread safe.
     */
    public class SelectionBuilder
    {
        private string mTable;
        private bool mDistinct;
        private int mTop;
        private Dictionary<string, string> mProjectionMap;
        private StringBuilder mSelection;
        private List<string> mSelectionArgs;

        public SelectionBuilder()
        {
            mTable = null;
            mDistinct = false;
            mTop = -1;
            mProjectionMap = new Dictionary<string, string>();
            mSelection = new StringBuilder();
            mSelectionArgs = new List<string>();
        }

        /**
         * Reset any internal state, allowing this builder to be recycled.
         *
         * <p>Calling this method is more efficient than creating a new SelectionBuilder object.
         *
         */
        public void Reset()
        {
            mTable = null;
            mDistinct = false;
            mTop = -1;
            mProjectionMap.Clear();
            mSelection.Length = 0;
            mSelectionArgs.Clear();
        }

        /**
         * Sets the query as distinct
         * 
         * @return SelectionBuilder
         */
        public SelectionBuilder Distinct()
        {
            mDistinct = true;
            return this;
        }

        /**
         * Sets the query as a limited selection query incuding the amount its limited to
         * 
         * @return SelectionBuilder
         */
        public SelectionBuilder Top(int amount)
        {
            mTop = amount > 0 ? amount : -1;
            return this;
        }

        /**
         * And Append the given selection clause to the internal state. Each clause is
         * surrounded with parenthesis and combined using {@code AND}.
         *
         * <p>In the most basic usage, simply provide a selection in SQL {@code WHERE} statement format.
         *
         * <p>Example:
         *
         * <pre>
         *     .where("blog_posts.category = 'PROGRAMMING');
         * </pre>
         *
         * <p>User input should never be directly supplied as as part of the selection statement.
         * Instead, use positional parameters in your selection statement, then pass the user input
         * in via the selectionArgs parameter. This prevents SQL escape characters in user input from
         * causing unwanted side effects. (Failure to follow this convention may have security
         * implications.)
         *
         * <p>Positional parameters are specified using the '?' character.
         *
         * <p>Example:
         * <pre>
         *     .where("blog_posts.title contains ?, userSearchString);
         * </pre>
         *
         * @param selection SQL where statement
         * @param selectionArgs Values to substitute for positional parameters ('?' characters in
         *                      {@code selection} statement. Will be automatically escaped.
         * @return SelectionBuilder
         */
        public SelectionBuilder Where(string selection, params string[] selectionArgs)
        {
            if (string.IsNullOrEmpty(selection))
            {
                if (selectionArgs != null && selectionArgs.Length > 0)
                {
                    throw new Exception("Valid selection required when including arguments");
                }
                return this;
            }

            if (mSelection.Length > 0)
            {
                mSelection.Append(" AND ");
            }

            mSelection.Append("(").Append(selection).Append(")");
            if (selectionArgs != null)
            {
                mSelectionArgs.AddRange(selectionArgs);
            }

            return this;
        }

        /**
         * Or Append the given selection clause to the internal state. Each clause is
         * surrounded with parenthesis and combined using {@code AND}.
         *
         * <p>In the most basic usage, simply provide a selection in SQL {@code WHERE} statement format.
         *
         * <p>Example:
         *
         * <pre>
         *     .where("blog_posts.category = 'PROGRAMMING');
         * </pre>
         *
         * <p>User input should never be directly supplied as as part of the selection statement.
         * Instead, use positional parameters in your selection statement, then pass the user input
         * in via the selectionArgs parameter. This prevents SQL escape characters in user input from
         * causing unwanted side effects. (Failure to follow this convention may have security
         * implications.)
         *
         * <p>Positional parameters are specified using the '?' character.
         *
         * <p>Example:
         * <pre>
         *     .where("blog_posts.title contains ?, userSearchString);
         * </pre>
         *
         * @param selection SQL where statement
         * @param selectionArgs Values to substitute for positional parameters ('?' characters in
         *                      {@code selection} statement. Will be automatically escaped.
         * @return SelectionBuilder
         */
        public SelectionBuilder WhereOr(string selection, params string[] selectionArgs)
        {
            if (string.IsNullOrEmpty(selection))
            {
                if (selectionArgs != null && selectionArgs.Length > 0)
                {
                    throw new Exception("Valid selection required when including arguments");
                }
                return this;
            }

            if (mSelection.Length > 0)
            {
                mSelection.Append(" OR ");
            }

            mSelection.Append("(").Append(selection).Append(")");
            if (selectionArgs != null)
            {
                mSelectionArgs.AddRange(selectionArgs);
            }

            return this;
        }

        public SelectionBuilder WhereIf(string selection, params string[] selectionArgs)
        {
            if (selectionArgs != null)
            {
                foreach (var arg in selectionArgs)
                {
                    if (string.IsNullOrEmpty(arg))
                    {
                        return this;
                    }
                }
                return Where(selection, selectionArgs);
            }
            return this;
        }

        public SelectionBuilder WhereIfOr(string selection, params string[] selectionArgs)
        {
            if (selectionArgs != null)
            {
                foreach (var arg in selectionArgs)
                {
                    if (string.IsNullOrEmpty(arg))
                    {
                        return this;
                    }
                }
                return WhereOr(selection, selectionArgs);
            }
            return this;
        }

        public SelectionBuilder WhereIn(string field, int[] items)
        {
            return Where(field + " IN ( " + string.Join(",", items) + " )", null);
        }

        public SelectionBuilder WhereIn(string field, string[] items)
        {
            return Where(field + " IN ( '" + string.Join("','", items) + "' )", null);
        }

        public SelectionBuilder WhereIn(string field, string inField, string inTable, string where, params string[] selectionArgs)
        {
            return Where(field + " IN ( SELECT " + inField + " FROM " + inTable + " WHERE " + where + " )", selectionArgs);
        }

        public SelectionBuilder WhereOrIn(string field, string inField, string inTable, string where, params string[] selectionArgs)
        {
            return WhereOr(field + " IN ( SELECT " + inField + " FROM " + inTable + " WHERE " + where + " )", selectionArgs);
        }

        public SelectionBuilder WhereNotIn(string field, int[] items)
        {
            return Where(field + " NOT IN ( " + string.Join(",", items) + " )", null);
        }

        public SelectionBuilder WhereNotIn(string field, string[] items)
        {
            return Where(field + " NOT IN ( '" + string.Join("','", items) + "' )", null);
        }

        public SelectionBuilder WhereNotIn(string field, string notInField, string notInTable, string where, params string[] selectionArgs)
        {
            return Where(field + " NOT IN ( SELECT " + notInField + " FROM " + notInTable + " WHERE " + where + " )", selectionArgs);
        }

        public SelectionBuilder WhereOrNotIn(string field, string notInField, string notInTable, string where, params string[] selectionArgs)
        {
            return WhereOr(field + " NOT IN ( SELECT " + notInField + " FROM " + notInTable + " WHERE " + where + " )", selectionArgs);
        }

        public SelectionBuilder WhereInUnion(string field, string inField, string leftTable, string rightTable, string where, params string[] selectionArgs)
        {
            var query = new StringBuilder()
                .Append(field).Append(" IN (")
                .AppendFormat(" SELECT {0} FROM {1} WHERE {2} ", inField, leftTable, where)
                .Append(" UNION ")
                .AppendFormat(" SELECT {0} FROM {1} WHERE {2} ", inField, rightTable, where)
                .Append(" )")
                .ToString();

            return Where(query, selectionArgs);
        }

        public SelectionBuilder WhereNotInUnion(string field, string inField, string leftTable, string rightTable, string where, params string[] selectionArgs)
        {
            var query = new StringBuilder()
                .Append(field).Append(" NOT IN (")
                .AppendFormat(" SELECT {0} FROM {1} WHERE {2} ", inField, leftTable, where)
                .Append(" UNION ")
                .AppendFormat(" SELECT {0} FROM {1} WHERE {2} ", inField, rightTable, where)
                .Append(" )")
                .ToString();

            return Where(query, selectionArgs);
        }

        /**
         * Table name to use for SQL {@code FROM} statement.
         *
         * <p>This method may only be called once. If multiple tables are required, concatenate them
         * in SQL-format (typically comma-separated).
         *
         * <p>If you need to do advanced {@code JOIN}s, they can also be specified here.
         *
         * See also: MapToTable()
         *
         * @param table Table name
         * @return SelectionBuilder
         */
        public SelectionBuilder Table(string table)
        {
            mTable = table;
            return this;
        }

        /**
         * Function name to use for SQL {@code FROM} statement.
         *
         * <p>This method may only be called once. If multiple functions are required, concatenate them
         * in SQL-format (typically comma-separated).
         *
         * <p>If you need to do advanced {@code JOIN}s, they can also be specified here.
         *
         * See also: MapToTable()
         *
         * @param function Function name
         * @param args Function args
         * @return SelectionBuilder
         */
        public SelectionBuilder Function(string function, params string[] args)
        {
            if (args != null && args.Length > 0)
            {
                mTable = string.Format(function, args);
            }
            else if (!function.EndsWith("()"))
            {
                mTable = function + "()";
            }
            else
            {
                mTable = function;
            }
            return this;
        }

        /**
         * Verify that a table name has been supplied using table().
         *
         * @throws IllegalStateException if table not set
         */
        private void AssertTable()
        {
            if (mTable == null)
            {
                throw new Exception("Table not specified");
            }
        }

        /**
         * Perform an inner join.
         *
         * <p>Map columns from a secondary table onto the current result set. References to the column
         * specified in {@code column} will be replaced with {@code table.column} in the SQL {@code
         * SELECT} clause.
         *
         * @param column Column name to join on. Must be the same in both tables.
         * @param table Secondary table to join.
         * @return SelectionBuilder
         */
        public SelectionBuilder MapToTable(string column, string table)
        {
            mProjectionMap.Add(column, table + "." + column);
            return this;
        }

        /**
         * Create a new column based on custom criteria (such as aggregate functions).
         *
         * <p>This adds a new column to the result set, based upon custom criteria in SQL format. This
         * is equivalent to the SQL statement: {@code SELECT toClause AS fromColumn}
         *
         * <p>This method is useful for executing SQL sub-queries.
         *
         * @param fromColumn Name of column for mapping
         * @param toClause SQL string representing data to be mapped
         * @return SelectionBuilder
         */
        public SelectionBuilder Map(string fromColumn, string toClause)
        {
            mProjectionMap.Add(fromColumn, toClause + " AS " + fromColumn);
            return this;
        }

        public SelectionBuilder MapSimpleCase(string fromColumn, object[] whenClauses, object[] thenClauses)
        {
            if (whenClauses == null || thenClauses == null ||
                whenClauses.Length == 0 || thenClauses.Length == 0 ||
                whenClauses.Length != thenClauses.Length)
            {
                throw new Exception("MapSimpleCase requires whenClauses and thenClauses to be non-null, greater than 0 and equal in length");
            }

            var builder = new StringBuilder();

            builder.Append(" CASE ").Append(fromColumn);

            for (int i = 0; i < whenClauses.Length; ++i)
            {
                string whenClause;
                if (whenClauses[i] == null)
                {
                    whenClause = " NULL ";
                }
                else if (whenClauses[i] is SubQuery)
                {
                    throw new Exception("SubQuery not allowed in the WhenClause");
                }
                else if (whenClauses[i] is string)
                {
                    whenClause = "'" + Convert.ToString(whenClauses[i]) + "'";
                }
                else
                {
                    whenClause = Convert.ToString(whenClauses[i]);
                }

                string thenClause;
                if (thenClauses[i] == null)
                {
                    thenClause = " NULL ";
                }
                else if (thenClauses[i] is SubQuery)
                {
                    thenClause = "(" + ((SubQuery)thenClauses[i]).Query + ")";
                }
                else if (thenClauses[i] is string)
                {
                    thenClause = "'" + Convert.ToString(thenClauses[i]) + "'";
                }
                else
                {
                    thenClause = Convert.ToString(thenClauses[i]);
                }

                builder.Append(" WHEN ").Append(whenClause).Append(" THEN ").Append(thenClause);
            }

            builder.Append(" END");

            mProjectionMap.Add(fromColumn, "(" + builder.ToString() + ") AS " + fromColumn);
            return this;
        }

        public SelectionBuilder MapSimpleCase(string fromColumn, object[] whenClauses, object[] thenClauses, object elseClause)
        {
            if (whenClauses == null || thenClauses == null ||
                whenClauses.Length == 0 || thenClauses.Length == 0 ||
                whenClauses.Length != thenClauses.Length)
            {
                throw new Exception("MapSimpleCase requires whenClauses and thenClauses to be non-null, greater than 0 and equal in length");
            }

            var builder = new StringBuilder();

            builder.Append(" CASE ").Append(fromColumn);

            for (int i = 0; i < whenClauses.Length; ++i)
            {
                string whenClause;
                if (whenClauses[i] == null)
                {
                    whenClause = " NULL ";
                }
                else if (whenClauses[i] is SubQuery)
                {
                    throw new Exception("SubQuery not allowed in the WhenClause");
                }
                else if (whenClauses[i] is string)
                {
                    whenClause = "'" + Convert.ToString(whenClauses[i]) + "'";
                }
                else
                {
                    whenClause = Convert.ToString(whenClauses[i]);
                }

                string thenClause;
                if (thenClauses[i] == null)
                {
                    thenClause = " NULL ";
                }
                else if (thenClauses[i] is SubQuery)
                {
                    thenClause = "(" + ((SubQuery)thenClauses[i]).Query + ")";
                }
                else if (thenClauses[i] is string)
                {
                    thenClause = "'" + Convert.ToString(thenClauses[i]) + "'";
                }
                else
                {
                    thenClause = Convert.ToString(thenClauses[i]);
                }

                builder.Append(" WHEN ").Append(whenClause).Append(" THEN ").Append(thenClause);
            }

            if (elseClause != null)
            {
                if (elseClause is string)
                {
                    builder.Append(" ELSE '").Append(Convert.ToString(elseClause)).Append("'");
                }
                else
                {
                    builder.Append(" ELSE ").Append(Convert.ToString(elseClause));
                }
            }
            else
            {
                builder.Append(" ELSE NULL ");
            }

            builder.Append(" END");

            mProjectionMap.Add(fromColumn, "(" + builder.ToString() + ") AS " + fromColumn);
            return this;
        }

        public SelectionBuilder MapSimpleCase(string toColumn, string whenColumn, object[] whenClauses, object[] thenClauses)
        {
            if (whenClauses == null || thenClauses == null ||
                whenClauses.Length == 0 || thenClauses.Length == 0 ||
                whenClauses.Length != thenClauses.Length)
            {
                throw new Exception("MapSimpleCase requires whenClauses and thenClauses to be non-null, greater than 0 and equal in length");
            }

            var builder = new StringBuilder();

            builder.Append(" CASE ").Append(whenColumn);

            for (int i = 0; i < whenClauses.Length; ++i)
            {
                string whenClause;
                if (whenClauses[i] == null)
                {
                    whenClause = " NULL ";
                }
                else if (whenClauses[i] is SubQuery)
                {
                    throw new Exception("SubQuery not allowed in the WhenClause");
                }
                else if (whenClauses[i] is string)
                {
                    whenClause = "'" + Convert.ToString(whenClauses[i]) + "'";
                }
                else
                {
                    whenClause = Convert.ToString(whenClauses[i]);
                }

                string thenClause;
                if (thenClauses[i] == null)
                {
                    thenClause = " NULL ";
                }
                else if (thenClauses[i] is SubQuery)
                {
                    thenClause = "(" + ((SubQuery)thenClauses[i]).Query + ")";
                }
                else if (thenClauses[i] is string)
                {
                    thenClause = "'" + Convert.ToString(thenClauses[i]) + "'";
                }
                else
                {
                    thenClause = Convert.ToString(thenClauses[i]);
                }

                builder.Append(" WHEN ").Append(whenClause).Append(" THEN ").Append(thenClause);
            }

            builder.Append(" END");

            mProjectionMap.Add(toColumn, "(" + builder.ToString() + ") AS " + toColumn);
            return this;
        }

        public SelectionBuilder MapSimpleCase(string toColumn, string whenColumn, object[] whenClauses, object[] thenClauses, object elseClause)
        {
            if (whenClauses == null || thenClauses == null ||
                whenClauses.Length == 0 || thenClauses.Length == 0 ||
                whenClauses.Length != thenClauses.Length)
            {
                throw new Exception("MapSimpleCase requires whenClauses and thenClauses to be non-null, greater than 0 and equal in length");
            }

            var builder = new StringBuilder();

            builder.Append(" CASE ").Append(whenColumn);

            for (int i = 0; i < whenClauses.Length; ++i)
            {
                string whenClause;
                if (whenClauses[i] == null)
                {
                    whenClause = " NULL ";
                }
                else if (whenClauses[i] is SubQuery)
                {
                    throw new Exception("SubQuery not allowed in the WhenClause");
                }
                else if (whenClauses[i] is string)
                {
                    whenClause = "'" + Convert.ToString(whenClauses[i]) + "'";
                }
                else
                {
                    whenClause = Convert.ToString(whenClauses[i]);
                }

                string thenClause;
                if (thenClauses[i] == null)
                {
                    thenClause = " NULL ";
                }
                else if (thenClauses[i] is SubQuery)
                {
                    thenClause = "(" + ((SubQuery)thenClauses[i]).Query + ")";
                }
                else if (thenClauses[i] is string)
                {
                    thenClause = "'" + Convert.ToString(thenClauses[i]) + "'";
                }
                else
                {
                    thenClause = Convert.ToString(thenClauses[i]);
                }

                builder.Append(" WHEN ").Append(whenClause).Append(" THEN ").Append(thenClause);
            }

            if (elseClause != null)
            {
                if (elseClause is string)
                {
                    builder.Append(" ELSE '").Append(Convert.ToString(elseClause)).Append("'");
                }
                else
                {
                    builder.Append(" ELSE ").Append(Convert.ToString(elseClause));
                }
            }
            else
            {
                builder.Append(" ELSE NULL ");
            }

            builder.Append(" END");

            mProjectionMap.Add(toColumn, "(" + builder.ToString() + ") AS " + toColumn);
            return this;
        }

        /**
         * Return selection string based on current internal state.
         *
         * @return Current selection as a SQL statement
         * @see #GetSelectionArgs()
         */
        public string GetSelection()
        {
            return mSelection.ToString();
        }

        /**
         * Return selection arguments based on current internal state.
         *
         * @see #GetSelection()
         */
        public string[] GetSelectionArgs()
        {
            return mSelectionArgs.ToArray();
        }

        /**
         * Process user-supplied projection (column list).
         *
         * <p>In cases where a column is mapped to another data source (either another table, or an
         * SQL sub-query), the column name will be replaced with a more specific, SQL-compatible
         * representation.
         *
         * Assumes that incoming columns are non-null.
         *
         * <p>See also: Map(), MapToTable()
         *
         * @param columns User supplied projection (column list).
         */
        private void MapColumns(string[] columns)
        {
            for (int i = 0; i < columns.Length; i++)
            {
                if (mProjectionMap.ContainsKey(columns[i]))
                {
                    string target = mProjectionMap[columns[i]];
                    if (target != null)
                    {
                        columns[i] = target;
                    }
                }
            }
        }

        /**
         * Return a description of this builder's state. Does NOT output SQL.
         *
         * @return Human-readable internal state
         */
        public override string ToString()
        {
            return "SelectionBuilder[table=" + mTable + ", selection=" + GetSelection() + ", selectionArgs=" + string.Join(",", GetSelectionArgs()) + "]";
        }

        /**
         * Execute query (SQL {@code SELECT}) against specified database.
         *
         * <p>Using a null projection (column list) is not supported.
         *
         * @param db Database to query.
         * @param columns Database projection (column list) to return, must be non-NULL.
         * @param orderBy How to order the rows, formatted as an SQL ORDER BY clause (excluding the
         *                ORDER BY itself). Passing null will use the default sort order, which may be
         *                unordered.
         * @return A {@link Cursor} object, which is positioned before the first entry. Note that
         *         {@link Cursor}s are not synchronized, see the documentation for more details.
         */
        public IDataSet Query(IDatabaseConnection db, string[] columns, string orderBy)
        {
            return Query(db, columns, null, null, orderBy, null);
        }

        /**
         * Execute query ({@code SELECT}) against database.
         *
         * <p>Using a null projection (column list) is not supported.
         *
         * @param db Database to query.
         * @param columns Database projection (column list) to return, must be non-null.
         * @param groupBy A filter declaring how to group rows, formatted as an SQL GROUP BY clause
         *                (excluding the GROUP BY itself). Passing null will cause the rows to not be
         *                grouped.
         * @param having A filter declare which row groups to include in the cursor, if row grouping is
         *               being used, formatted as an SQL HAVING clause (excluding the HAVING itself).
         *               Passing null will cause all row groups to be included, and is required when
         *               row grouping is not being used.
         * @param orderBy How to order the rows, formatted as an SQL ORDER BY clause (excluding the
         *                ORDER BY itself). Passing null will use the default sort order, which may be
         *                unordered.
         * @param limit Limits the number of rows returned by the query, formatted as LIMIT clause.
         *              Passing null denotes no LIMIT clause.
         * @return A {@link Cursor} object, which is positioned before the first entry. Note that
         *         {@link Cursor}s are not synchronized, see the documentation for more details.
         */
        public IDataSet Query(IDatabaseConnection db, string[] columns, string groupBy, string having, string orderBy, string limit)
        {
            AssertTable();
            if (columns != null) MapColumns(columns);
            //Log.v(TAG, "query(columns=" + Arrays.toString(columns) + ") " + this);
            return db.Query(mDistinct, mTop, mTable, columns, GetSelection(), GetSelectionArgs(), groupBy, having,
                    orderBy, limit);
        }

        /**
         */
        public int QueryId(IDatabaseConnection db, string idField = "_ID")
        {
            AssertTable();
            return db.QueryId(mTable, GetSelection(), GetSelectionArgs(), idField);
        }

        /**
         */
        public int[] QueryIdList(IDatabaseConnection db, string idField = "_ID")
        {
            AssertTable();
            return db.QueryIdList(mTable, GetSelection(), GetSelectionArgs(), idField);
        }

        /**
         * Execute an {@code UPDATE} against database.
         *
         * @param db Database to query.
         * @param values A map from column names to new column values. null is a valid value that will
         *               be translated to NULL
         * @return The number of rows affected.
         */
        public int Update(IDatabaseConnection db, ContentValues values)
        {
            AssertTable();
            //Log.v(TAG, "update() " + this);
            return db.Update(mTable, values, GetSelection(), GetSelectionArgs());
        }

        /**
         * Execute {@code DELETE} against database.
         *
         * @param db Database to query.
         * @return The number of rows affected.
         */
        public int Delete(IDatabaseConnection db)
        {
            AssertTable();
            //Log.v(TAG, "delete() " + this);
            return db.Delete(mTable, GetSelection(), GetSelectionArgs());
        }

        public class SubQuery
        {
            public string Query { get; private set; }

            public SubQuery(string subQuery)
            {
                Query = subQuery;
            }
        }

    }

}
