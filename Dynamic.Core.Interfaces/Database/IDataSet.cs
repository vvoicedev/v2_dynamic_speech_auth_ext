﻿using System;

namespace Dynamic.Database
{
    public interface IDataSet : IDisposable
    {
        int Count { get; }

        string[] Columns { get; }

        IReadableContentValues Get(int index);
    }
}
