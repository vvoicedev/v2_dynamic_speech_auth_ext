﻿using System;
using System.Collections.Generic;

namespace Dynamic.Database
{
    public class ContentValues : IReadableContentValues
    {
        private Dictionary<string, object> mContentValues;

        public ContentValues()
        {
            mContentValues = new Dictionary<string, object>();
        }

        public ContentValues Put(string key, bool value)
        {
            mContentValues[key] = value;
            return this;
        }

        public ContentValues Put(string key, short value)
        {
            mContentValues[key] = value;
            return this;
        }

        public ContentValues Put(string key, ushort value)
        {
            mContentValues[key] = value;
            return this;
        }

        public ContentValues Put(string key, int value)
        {
            mContentValues[key] = value;
            return this;
        }

        public ContentValues Put(string key, uint value)
        {
            mContentValues[key] = value;
            return this;
        }

        public ContentValues Put(string key, long value)
        {
            mContentValues[key] = value;
            return this;
        }

        public ContentValues Put(string key, ulong value)
        {
            mContentValues[key] = value;
            return this;
        }

        public ContentValues Put(string key, float value)
        {
            mContentValues[key] = value;
            return this;
        }

        public ContentValues Put(string key, double value)
        {
            mContentValues[key] = value;
            return this;
        }

        public ContentValues Put(string key, string value)
        {
            mContentValues[key] = value;
            return this;
        }

        public bool GetBool(string key)
        {
            return Convert.ToBoolean(mContentValues[key]);
        }

        public short GetShort(string key)
        {
            return Convert.ToInt16(mContentValues[key]);
        }

        public ushort GetUShort(string key)
        {
            return Convert.ToUInt16(mContentValues[key]);
        }

        public int GetInteger(string key)
        {
            return Convert.ToInt32(mContentValues[key]);
        }

        public uint GetUInteger(string key)
        {
            return Convert.ToUInt32(mContentValues[key]);
        }

        public long GetLong(string key)
        {
            return Convert.ToInt64(mContentValues[key]);
        }

        public ulong GetULong(string key)
        {
            return Convert.ToUInt64(mContentValues[key]);
        }

        public float GetSingle(string key)
        {
            return Convert.ToSingle(mContentValues[key]);
        }

        public double GetDouble(string key)
        {
            return Convert.ToDouble(mContentValues[key]);
        }

        public string GetString(string key)
        {
            return Convert.ToString(mContentValues[key]);
        }

        public object GetObject(string key)
        {
            return mContentValues[key];
        }

        public bool Has(string key)
        {
            return mContentValues.ContainsKey(key);
        }

        public ContentValues Remove(string key)
        {
            mContentValues.Remove(key);
            return this;
        }

        public ContentValues Clear()
        {
            mContentValues.Clear();
            return this;
        }

        public int Count { get { return mContentValues.Count; } }

        public string[] Columns
        {
            get
            {
                string[] a = new string[mContentValues.Count];
                mContentValues.Keys.CopyTo(a, 0);
                return a;
            }
        }

        public string ToText()
        {
            return string.Join(",", mContentValues);
        }
    }
}
