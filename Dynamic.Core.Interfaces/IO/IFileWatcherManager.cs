﻿using System.Collections.Generic;
using Dynamic.Services;

namespace Dynamic.IO
{
    public interface IFileWatcherManager : IService
    {
        bool AddPath(string path, IFileWatcher watcher);

        bool AddPaths(Dictionary<string, IFileWatcher> watchers);

        void RemovePath(string path, IFileWatcher watcher);

        void RemovePaths(Dictionary<string, IFileWatcher> watchers);
    }
}
