﻿using System;
using System.IO;

namespace Dynamic.IO
{
    public interface IFileWatcher : IDisposable
    {
        string[] Exts { get; }
        void OnCreated(object source, FileSystemEventArgs e);
        void OnChanged(object source, FileSystemEventArgs e);
        void OnDeleted(object source, FileSystemEventArgs e);
        void OnRenamed(object source, RenamedEventArgs e);
    }
}
