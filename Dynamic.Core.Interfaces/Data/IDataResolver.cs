﻿
namespace Dynamic.Data
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDataSource
    {
        /// <summary>
        /// Does the IDataSource contain the key to be resolved
        /// </summary>
        /// <param name="key"></param>
        /// <returns>True if the DataSource contains the key to be resolved</returns>
        bool Contains(string key);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        bool Contains(string key, string[] args);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        object Resolve(string key);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        object Resolve(string key, string[] args);
    }

    /// <summary>
    /// 
    /// </summary>
    public interface IDataManipulator
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        object Manipulate(object data);
    }

    public delegate void OnResolvedData(object data);

    /// <summary>
    /// 
    /// </summary>
    public interface IDataResolver
    {
        /// <summary>
        /// Adds an IDataSource to the end of the resolvers sources list
        /// </summary>
        /// <param name="source">Source of Resolution</param>
        /// <returns>True if the data source does not exist already and was added to the resolver list</returns>
        bool AddDataSource(IDataSource source);

        /// <summary>
        /// Adds an IDataSource to a prioritized index.
        /// 
        /// Note: 0 is highest priority.
        /// </summary>
        /// <param name="dataSource">Source of Resolution</param>
        /// <param name="priority">Priorty of Data Source</param>
        /// <returns>True if the data source does not exist already and was added to the resolver list</returns>
        bool AddDataSource(IDataSource dataSource, int priority);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataSource"></param>
        void RemoveDataSource(IDataSource dataSource);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="manipulatorName"></param>
        /// <param name="manipulator"></param>
        /// <returns></returns>
        bool AddDataManipulator(string manipulatorName, IDataManipulator manipulator);

        /// <summary>
        /// Synchronous Resolve handler
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        object Resolve(string data);

        /// <summary>
        /// Asynchronous Resolve handler
        /// </summary>
        /// <param name="data"></param>
        /// <param name="onResolvedData"></param>
        void Resolve(string data, OnResolvedData onResolvedData);

        /// <summary>
        /// Synchronous Resolve handler with arguments
        /// </summary>
        /// <param name="data"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        object Resolve(string data, params string[] args);

        /// <summary>
        /// Asynchronous Resolve handler with arguments
        /// </summary>
        /// <param name="data"></param>
        /// <param name="args"></param>
        /// <param name="onResolvedData"></param>
        /// <returns></returns>
        void Resolve(string data, OnResolvedData onResolvedData, params string[] args);

    }
}
