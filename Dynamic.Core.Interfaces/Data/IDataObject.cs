﻿
using System.Collections;

namespace Dynamic.Data
{
    public interface IReadOnlyDataContainer : IEnumerable
    {
        StringId Type { get; }

        object Get(uint key);

        object Get(StringId str);

        bool Has(uint key);

        bool Has(StringId key);

        int Count { get; }

        // Total elements across the container
        uint Elements { get; }

        // Total value space across the container
        uint ValueSpace { get; }
    }

    public interface IDataContainer : IReadOnlyDataContainer
    {
        void Put(object value);

        void Put(uint key, object value);

        void Put(StringId key, object value);

        void Put(object key, object value);
    }
}
