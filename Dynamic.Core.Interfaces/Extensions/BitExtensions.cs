﻿using System;

namespace Dynamic.Extensions
{
    public static class BitExtensionMethods
    {
        public static bool IsBit(this ulong data, int bit)
        {
            if(bit < 0 || bit >= 64)
            {
                throw new IndexOutOfRangeException("bit out of range");
            }

            ulong check_bit = (ulong)1 << bit;
            return (data & check_bit) == check_bit;
        }

        public static bool IsConstBit(this ulong data, int bit)
        {
            ulong check_bit = (ulong)bit;
            return (data & check_bit) == check_bit;
        }

        public static void SetBit(this ulong data, int bit)
        {
            if (bit < 0 || bit >= 64)
            {
                throw new IndexOutOfRangeException("bit out of range");
            }

            data |= ((ulong)1 << bit);
        }

        public static void SetConstBit(this ulong data, int bit)
        {
            data |= (uint)bit;
        }

        public static void ClearBit(this ulong data, int bit)
        {
            if (bit < 0 || bit >= 64)
            {
                throw new IndexOutOfRangeException("bit out of range");
            }

            data &= ~((ulong)1 << bit);
        }

        public static void ClearConstBit(this ulong data, int bit)
        {
            data &= ~((ulong)bit);
        }

        public static void IfBit(this ulong data, bool cond, int bit)
        {
            if (bit < 0 || bit >= 64)
            {
                throw new IndexOutOfRangeException("bit out of range");
            }

            if(cond)
            {
                data |= ((ulong)1 << bit);
            }
            else
            {
                data &= ~((ulong)1 << bit);
            }
        }

        public static void IfConstBit(this ulong data, bool cond, int bit)
        {
            if (cond)
            {
                data |= ((uint)bit);
            }
            else
            {
                data &= ~((ulong)bit);
            }
        }

        public static void IfIsBit(this ulong data, int bit, Action action)
        {
            if(data.IsBit(bit))
            {
                action.Invoke();
            }
        }

        public static void IfIsConstBit(this ulong data, int bit, Action action)
        {
            if (data.IsConstBit(bit))
            {
                action.Invoke();
            }
        }

        private static uint InterleaveEncode2x16(uint x)
        {
            x &= 0x0000ffff;                    // x = ---- ---- ---- ---- xxxx xxxx xxxx xxxx
            x = (x ^ (x << 8)) & 0x00ff00ff;    // x = ---- ---- xxxx xxxx ---- ---- xxxx xxxx
            x = (x ^ (x << 4)) & 0x0f0f0f0f;    // x = ---- xxxx ---- xxxx ---- xxxx ---- xxxx
            x = (x ^ (x << 2)) & 0x33333333;    // x = --xx --xx --xx --xx --xx --xx --xx --xx
            x = (x ^ (x << 1)) & 0x55555555;    // x = -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x
            return x;
        }

        private static ushort InterleaveDecode2x16(uint x)
        {
            x &= 0x55555555;                    // x = -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x
            x = (x ^ (x >> 1)) & 0x33333333;    // x = --xx --xx --xx --xx --xx --xx --xx --xx
            x = (x ^ (x >> 2)) & 0x0f0f0f0f;    // x = ---- xxxx ---- xxxx ---- xxxx ---- xxxx
            x = (x ^ (x >> 4)) & 0x00ff00ff;    // x = ---- ---- xxxx xxxx ---- ---- xxxx xxxx
            x = (x ^ (x >> 8)) & 0x0000ffff;    // x = ---- ---- ---- ---- xxxx xxxx xxxx xxxx
            return (ushort)x;
        }

        public static void InterleaveEncode(this uint data, ushort x, ushort y)
        {
            data = (InterleaveEncode2x16(y) << 1) + InterleaveEncode2x16(x);
        }

        public static void InterleaveDecode(this uint data, ref ushort x, ref ushort y)
        {
            x = InterleaveDecode2x16(data >> 0);
            y = InterleaveDecode2x16(data >> 1);
        }

        private static ulong InterleaveEncode2x32(ulong x)
        {
            x &= 0x00000000ffffffff;                        // x = ---- ---- ---- ---- ---- ---- ---- ---- xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx
            x = (x ^ (x << 16)) & 0x0000ffff0000ffff;		// x = ---- ---- ---- ---- xxxx xxxx xxxx xxxx ---- ---- ---- ---- xxxx xxxx xxxx xxxx
            x = (x ^ (x << 8)) & 0x00ff00ff00ff00ff;		// x = ---- ---- xxxx xxxx ---- ---- xxxx xxxx ---- ---- xxxx xxxx ---- ---- xxxx xxxx
            x = (x ^ (x << 4)) & 0x0f0f0f0f0f0f0f0f;		// x = ---- xxxx ---- xxxx ---- xxxx ---- xxxx ---- xxxx ---- xxxx ---- xxxx ---- xxxx
            x = (x ^ (x << 2)) & 0x3333333333333333;		// x = --xx --xx --xx --xx --xx --xx --xx --xx --xx --xx --xx --xx --xx --xx --xx --xx
            x = (x ^ (x << 1)) & 0x5555555555555555;		// x = -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x
            return x;
        }

        private static uint InterleaveDecode2x32(ulong x)
        {
            x &= 0x5555555555555555;		                // x = -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x -x-x
            x = (x ^ (x >> 1)) & 0x3333333333333333;		// x = --xx --xx --xx --xx --xx --xx --xx --xx --xx --xx --xx --xx --xx --xx --xx --xx
            x = (x ^ (x >> 2)) & 0x0f0f0f0f0f0f0f0f;		// x = ---- xxxx ---- xxxx ---- xxxx ---- xxxx ---- xxxx ---- xxxx ---- xxxx ---- xxxx
            x = (x ^ (x >> 4)) & 0x00ff00ff00ff00ff;		// x = ---- ---- xxxx xxxx ---- ---- xxxx xxxx ---- ---- xxxx xxxx ---- ---- xxxx xxxx
            x = (x ^ (x >> 8)) & 0x0000ffff0000ffff;        // x = ---- ---- ---- ---- xxxx xxxx xxxx xxxx ---- ---- ---- ---- xxxx xxxx xxxx xxxx
            x = (x ^ (x >> 16)) & 0x00000000ffffffff;		// x = ---- ---- ---- ---- ---- ---- ---- ---- xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx
            return (uint)x;
        }

        public static void InterleaveEncode(this ulong data, uint x, uint y)
        {
            data = (InterleaveEncode2x32(y) << 1) + InterleaveEncode2x32(x);
        }

        public static void InterleaveDecode(this ulong data, ref uint x, ref uint y)
        {
            x = InterleaveDecode2x32(data >> 0);
            y = InterleaveDecode2x32(data >> 1);
        }
    }
}
