﻿using System.Collections;
using System.Collections.Generic;

namespace Dynamic.Extensions
{
    public static class ContainerExtensions
    {
        public static T IndexAt<T>(this IEnumerable<T> source, int index)
        {
            int i = 0;
            foreach (var item in source)
            {
                if (i++ == index)
                    return item;
            }
            return default(T);
        }

        public static T GetFirst<T>(this IList<T> list)
        {
            if (list.Count == 0)
                return default(T);

            return list[0];
        }

        public static T GetLast<T>(this IList<T> list)
        {
            if (list.Count == 0)
                return default(T);

            return list[list.Count - 1];
        }

        public static IList Reverse(this IList list)
        {
            var al = new ArrayList(list);
            al.Reverse();
            return al;
        }
        
        public static object GetFirst(this IDictionary dict)
        {
            if (dict.Count == 0)
                return null;

            foreach (var o in dict)
            {
                return o;
            }

            return null;
        }
        
        public static T GetFirst<T>(this HashSet<T> set)
        {
            if (set.Count == 0)
                return default(T);

            foreach (T o in set)
            {
                return o;
            }

            return default(T);
        }
    }
}
