﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dynamic.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Takes a string like the following this=that&here=there and splits it into a dictionary of key/value pairs
        /// </summary>
        /// <param name="str"></param>
        /// <param name="splitter1"></param>
        /// <param name="splitter2"></param>
        /// <returns></returns>
        public static Dictionary<string, string> SplitPairs(this string str, string keySplitter, string valueSplitter)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();

            string[] keyValues = str.Split(keySplitter);
            foreach (var kv in keyValues)
            {
                string[] pair = kv.Split(valueSplitter);
                if (pair.Length != 2)
                    continue;
                if (dict.ContainsKey(pair[0]))
                    continue;
                dict.Add(pair[0], pair[1]);
            }

            return dict;
        }

        private static bool IsShortOpt(this string option)
        {
            return option.Length > 1 && option[0] == '-';
        }
        
        private static bool IsShortOpt(this string option, char check)
        {
            return check != '\0' &&
                option.Length > 1 &&
                option[0] == '-' &&
                option[1] == check;
        }
        
        private static bool IsLongOpt(this string option)
        {
            return option.Length > 2 &&
                option[0] == '-' &&
                option[1] == '-';
        }
        
        private static bool IsLongOpt(this string option, string check)
        {
            return !string.IsNullOrEmpty(check) &&
                option.Length > 2 &&
                option[0] == '-' &&
                option[1] == '-' &&
                option.Substring(2).Equals(check);
        }
        
        private static int FindOption(this string[] args, string longopt, char shortopt)
        {
            for (int i = 0; i < args.Length; ++i)
            {
                if (args[i].IsLongOpt(longopt) || args[i].IsShortOpt(shortopt))
                    return i;
            }

            return args.Length;
        }
        
        public static bool HasOption(this string[] args, string longopt, char shortopt = '\0')
        {
            return args.FindOption(longopt, shortopt) < args.Length;
        }
        
        public static string GetParameter(this string[] args, int i, string longopt, char shortopt = '\0')
        {
            int idx = args.FindOption(longopt, shortopt);
            if (idx + i < args.Length)
            {
                string parm = args[idx + i + 1];
                return (!parm.IsLongOpt() && !parm.IsShortOpt()) ? parm : null;
            }
            return null;
        }
        
        public static string GetParameter(this string[] args, int i, string longopt, string defvalue)
        {
            int idx = args.FindOption(longopt, '\0');
            if (idx + i < args.Length)
            {
                string parm = args[idx + i + 1];
                return (!parm.IsLongOpt() && !parm.IsShortOpt()) ? parm : defvalue;
            }
            return defvalue;
        }
        
        public static string GetParameter(this string[] args, int i, string longopt, char shortopt, string defvalue)
        {
            int idx = args.FindOption(longopt, shortopt);
            if (idx + i < args.Length)
            {
                string parm = args[idx + i + 1];
                return (!parm.IsLongOpt() && !parm.IsShortOpt()) ? parm : defvalue;
            }
            return defvalue;
        }
        
        public static int GetParameterCount(this string[] args, string longopt, char shortopt = '\0')
        {
            int idx = args.FindOption(longopt, shortopt);
            if (args.Length == idx || args.Length == idx + 1)
                return 0;

            int i = 0;
            for (++idx; idx < args.Length; ++idx, ++i)
            {
                string parm = args[idx];
                if (parm.IsLongOpt() || parm.IsShortOpt())
                    break;
            }
            return i;
        }
    }
}
