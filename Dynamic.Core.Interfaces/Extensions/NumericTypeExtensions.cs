﻿using System;

namespace Dynamic.Extensions
{
    public static class NumericTypeExtensions
    {
        public static bool IsNumericType(this object o)
        {
            if (o != null)
            {
                switch (Type.GetTypeCode(o.GetType()))
                {
                    case TypeCode.Boolean:
                    case TypeCode.Char:
                    case TypeCode.Byte:
                    case TypeCode.SByte:
                    case TypeCode.UInt16:
                    case TypeCode.UInt32:
                    case TypeCode.UInt64:
                    case TypeCode.Int16:
                    case TypeCode.Int32:
                    case TypeCode.Int64:
                    case TypeCode.Decimal:
                    case TypeCode.Single:
                    case TypeCode.Double:
                    case (TypeCode)249: // VInteger
                    case (TypeCode)250: // VLong
                    case (TypeCode)251: // Bitmap8
                    case (TypeCode)252: // Bitmap16
                    case (TypeCode)253: // Bitmap32
                    case (TypeCode)254: // Bitmap64
                        return true;
                }
            }
            return false;
        }

        public static bool IsNumericType<T>(this T type)
        {
            if (type != null)
            {
                switch (Type.GetTypeCode(type.GetType()))
                {
                    case TypeCode.Boolean:
                    case TypeCode.Char:
                    case TypeCode.Byte:
                    case TypeCode.SByte:
                    case TypeCode.UInt16:
                    case TypeCode.UInt32:
                    case TypeCode.UInt64:
                    case TypeCode.Int16:
                    case TypeCode.Int32:
                    case TypeCode.Int64:
                    case TypeCode.Decimal:
                    case TypeCode.Single:
                    case TypeCode.Double:
                    case (TypeCode)249: // VInteger
                    case (TypeCode)250: // VLong
                    case (TypeCode)251: // Bitmap8
                    case (TypeCode)252: // Bitmap16
                    case (TypeCode)253: // Bitmap32
                    case (TypeCode)254: // Bitmap64
                        return true;
                }
            }
            return false;
        }
    }
}
