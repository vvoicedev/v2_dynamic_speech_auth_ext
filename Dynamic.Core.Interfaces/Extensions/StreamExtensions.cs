﻿using System;
using System.IO;
using System.Text;

namespace Dynamic.Extensions
{
    public static class StreamExtensions
    {
        public static Half ReadHalf(this BinaryReader reader)
        {
            byte[] bytes = reader.ReadBytes(2);
            return Half.ToHalf(bytes, 0);
        }

        public static void WriteHalf(this BinaryWriter writer, Half half)
        {
            writer.Write(Half.GetBytes(half));
        }

        public static void WriteHalf(this BinaryWriter writer, float half)
        {
            writer.Write(Half.GetBytes(new Half(half)));
        }

        public static void WriteHalf(this BinaryWriter writer, double half)
        {
            writer.Write(Half.GetBytes(new Half(half)));
        }

        private static readonly byte[] IS_CONNECTED_BYTE_ARRAY = new byte[0];

        public static bool IsConnected(this Stream stream)
        {
            try
            {
                // Twice because the first time will return without issue but
                // cause the Stream to become closed (if the Stream is actually
                // closed.)
                stream.Write(IS_CONNECTED_BYTE_ARRAY, 0, IS_CONNECTED_BYTE_ARRAY.Length);
                stream.Write(IS_CONNECTED_BYTE_ARRAY, 0, IS_CONNECTED_BYTE_ARRAY.Length);
                return true;
            }
            catch (ObjectDisposedException)
            {
                // Since we're disposing of both Streams at the same time, one
                // of the streams will be checked after it is disposed.
                return false;
            }
            catch (IOException)
            {
                // This will be thrown on the second stream.Write when the Stream
                // is closed on the client side.
                return false;
            }
        }

        public static bool IsConnected(this BinaryReader reader)
        {
            return IsConnected(reader.BaseStream);
        }

        public static bool IsConnected(this BinaryWriter writer)
        {
            return IsConnected(writer.BaseStream);
        }

        public static uint ReadVInt32(this BinaryReader reader)
        {
            byte ch;
            int s = 0;
            uint v = 0;
            do
            {
                ch = reader.ReadByte();
                uint x = (uint)(ch & 0x7f);
                x <<= s;
                v += x;
                s += 7;
            } while ((ch & 0x80) != 0);
            return v;
        }

        public static ulong ReadVInt64(this BinaryReader reader)
        {
            byte ch;
            int s = 0;
            ulong v = 0;
            do
            {
                ch = reader.ReadByte();
                ulong x = (ulong)(ch & 0x7f);
                x <<= s;
                v += x;
                s += 7;
            } while ((ch & 0x80) != 0);
            return v;
        }

        public static string ReadMiniString(this BinaryReader reader)
        {
            return reader.ReadMiniString(Encoding.ASCII);
        }

        public static string ReadMiniString(this BinaryReader reader, Encoding encoding)
        {
            byte size = reader.ReadByte();
            byte[] data = reader.ReadBytes(size);
            return encoding.GetString(data);
        }

        public static string ReadShortString(this BinaryReader reader)
        {
            return reader.ReadShortString(Encoding.ASCII);
        }

        public static string ReadShortString(this BinaryReader reader, Encoding encoding)
        {
            ushort size = reader.ReadUInt16();
            byte[] data = reader.ReadBytes(size);
            return encoding.GetString(data);
        }

        public static string ReadNormalString(this BinaryReader reader)
        {
            return reader.ReadNormalString(Encoding.ASCII);
        }

        public static string ReadNormalString(this BinaryReader reader, Encoding encoding)
        {
            uint size = reader.ReadUInt32();
            byte[] data = reader.ReadBytes((int)size);
            return encoding.GetString(data);
        }

        public static string ReadLongString(this BinaryReader reader)
        {
            return reader.ReadLongString(Encoding.ASCII);
        }

        public static string ReadLongString(this BinaryReader reader, Encoding encoding)
        {
            ulong size = reader.ReadUInt64();
            byte[] data = reader.ReadBytes((int)size);
            return encoding.GetString(data);
        }

        public static string ReadVariableString(this BinaryReader reader)
        {
            return reader.ReadVarString(Encoding.ASCII);
        }

        public static string ReadVarString(this BinaryReader reader, Encoding encoding)
        {
            ulong size = reader.ReadVInt64();
            byte[] data = reader.ReadBytes((int)size);
            return encoding.GetString(data);
        }

        public static void WriteVInt32(this BinaryWriter writer, uint o)
        {
            byte ch = 0;
            do
            {
                ch = (byte)(o & 0x7f);
                o >>= 7;
                if (o != 0) ch |= 0x80;
                writer.Write(ch);
            } while (o != 0);
        }

        public static void Write(this BinaryWriter writer, VInteger o)
        {
            writer.WriteVInt32(o.Value);
        }

        public static void WriteVInt64(this BinaryWriter writer, ulong o)
        {
            byte ch = 0;
            do
            {
                ch = (byte)(o & 0x7f);
                o >>= 7;
                if (o != 0) ch |= 0x80;
                writer.Write(ch);
            } while (o != 0);
        }

        public static void Write(this BinaryWriter writer, VLong o)
        {
            writer.WriteVInt64(o.Value);
        }

        public static void WriteMiniString(this BinaryWriter writer, string s)
        {
            writer.Write((byte)s.Length);
            writer.Write(s.ToCharArray());
        }

        public static void WriteShortString(this BinaryWriter writer, string s)
        {
            writer.Write((ushort)s.Length);
            writer.Write(s.ToCharArray());
        }

        public static void WriteNormalString(this BinaryWriter writer, string s)
        {
            writer.Write((uint)s.Length);
            writer.Write(s.ToCharArray());
        }

        public static void WriteLongString(this BinaryWriter writer, string s)
        {
            writer.Write((ulong)s.Length);
            writer.Write(s.ToCharArray());
        }

        public static void WriteVariableString(this BinaryWriter writer, string s)
        {
            writer.WriteVInt64((ulong)s.Length);
            writer.Write(s.ToCharArray());
        }
    }
}
