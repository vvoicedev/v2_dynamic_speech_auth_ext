﻿using System;
using System.Collections.Generic;

using Dynamic.Math;

namespace Dynamic.Extensions
{
    public static class MathExtensions
    {
        public static double ComputeVariance<T>(this ISeries<T> series)
        {
            if (series.Count == 0)
                return 0;

            // Var = E[x^2] - ( E[x] )^2
            double countinv = 1.0 / series.Count;
            double mean2 = series.SumSquared * countinv;
            double mean = series.Sum * countinv;
            return (mean2 - (mean * mean));
        }
        
        public static double ComputeStandardDeviation<T>(this ISeries<T> series)
        {
            double mean = series.Mean;
            double sum = 0;

            for (int i = 0; i < series.Count; ++i)
            {
                double val = series.GetValue(i) - mean;
                sum += (val * val);
            }

            return System.Math.Sqrt(sum / (series.Count - 1));
        }

        public static List<double> ComputeMovingAverage<T>(this ISeries<T> series, int numberOfPeriods)
        {
            List<double> ma = new List<double>();

            double sum = 0;
            for(int i = 0; i < series.Count; ++i)
            {
                sum += series.GetValue(i);
                if(i >= numberOfPeriods)
                {
                    ma.Add(sum / numberOfPeriods);
                    sum -= series.GetValue(i - numberOfPeriods);
                }
            }

            return ma;
        }

        public static List<double> ComputeExponentialMovingAverage<T>(this ISeries<T> series, int numberOfPeriods)
        {
            List<double> ma = new List<double>();

            double sum = 0;
            double multipler = 2.0 / (numberOfPeriods - 1);
            for (int i = 0; i < series.Count; ++i)
            {
                double val = series.GetValue(i);
                sum += val;
                if (i == numberOfPeriods)
                {
                    ma.Add(sum / numberOfPeriods);
                    sum -= series.GetValue(i - numberOfPeriods);
                }
                else if(i > numberOfPeriods)
                {
                    ma.Add((1 - multipler) * ma[ma.Count - 1] + multipler * val);
                }
            }

            return ma;
        }

        public static double RangeVolatility<T>(this ISeries<T> series)
        {
            if (series.Count < 1)
                return 0;

            double min = series.GetValue(0);
            double max = min;

            for(int i = 1; i < series.Count; ++i)
            {
                double val = series.GetValue(i);

                if(val < min)
                {
                    min = val;
                }

                if(val > max)
                {
                    max = val;
                }
            }

            return max - min;
        }

        public static double AverageDailyRange<T>(this ISeries<T> series)
        {
            uint n = (uint)series.Count;
            if (n < 2)
                return 0;

            double previous = series.GetValue(0);
            double sum = 0;

            for(int i = 1; i < series.Count; ++i)
            {
                sum += System.Math.Abs(series.GetValue(i) - previous);
            }

            return sum / (n - 1);
        }
        
        public static double SmoothSampling<T>(this ISeries<T> series, int smoothFactor)
        {
            if (smoothFactor >= 1 && smoothFactor <= 99)
            {
                throw new ArgumentException("MUST be between 1 and 99", "smoothFactor");
            }

            double result = 0;
            double inverse = (100 - smoothFactor);
            for (int i = 0; 0 < series.Count; ++i)
            {
                result = ((smoothFactor * result) + (inverse * series.GetValue(i))) / 100;
            }
            return result;
        }

        public static double Correlation<T>(this ISeries<T> seriesA, ISeries<T> seriesB)
        {
            if (seriesA.Count != seriesB.Count)
                throw new ArgumentException("Series A and B are not of the same Length");

            double sum = 0;
            double meanA = seriesA.Mean;
            double meanB = seriesB.Mean;

            for (int i = 0; i < seriesA.Count; ++i)
            {
                sum += ((seriesA.GetValue(i) - meanA) * (seriesB.GetValue(i) * meanB));
            }

            double stdDevA = seriesA.ComputeStandardDeviation();
            double stdDevB = seriesB.ComputeStandardDeviation();
            sum /= (stdDevA * stdDevB);
            return sum / (seriesB.Count - 1);
        }

        public static double ConsecutiveSum<T>(this ISeries<T> series)
        {
            return (series.Count * (series.Count + 1)) / 2;
        }

        public static double ComputeSlope<T>(this ISeries<T> series)
        {
            if (series.Count == 0)
                return 0;

            double meanX = series.Mean;
            double meanY = series.ConsecutiveSum();
            double numerator = series.ComputeNumerator(meanX, meanY);
            double denominator = series.ComputeDenominator(meanX);

            return numerator / denominator;
        }

        public static double ComputeIntercept<T>(this ISeries<T> series)
        {
            if (series.Count == 0)
                return 0;

            double meanX = series.Mean;
            double meanY = series.ConsecutiveSum();
            double slope = series.ComputeSlope();

            return meanY - slope * meanX;
        }

        public static double ComputeSlope<T>(this ISeries<T> seriesX, ISeries<T> seriesY)
        {
            if (seriesX.Count != seriesY.Count)
                throw new ArgumentException("Series X and Y are not of the same Length");

            if (seriesX.Count == 0)
                return 0;

            double meanX = seriesX.Mean;
            double meanY = seriesY.Mean;
            double numerator = seriesX.ComputeNumerator(seriesY, meanX, meanY);
            double denominator = seriesX.ComputeDenominator(meanX);

            return numerator / denominator;
        }

        public static double ComputeIntercept<T>(this ISeries<T> seriesX, ISeries<T> seriesY)
        {
            if (seriesX.Count != seriesY.Count)
                throw new ArgumentException("Series X and Y are not of the same Length");

            if (seriesX.Count == 0)
                return 0;

            double meanX = seriesX.Mean;
            double meanY = seriesY.Mean;
            double slope = seriesX.ComputeSlope(seriesY);

            return meanY - slope * meanX;
        }

        private static double ComputeNumerator<T>(this ISeries<T> series, double meanX, double meanY)
        {
            double sum = 0;
            for(int i = 0; i < series.Count; ++i)
            {
                sum += ((series.GetValue(i) - meanX) * (i - meanY));
            }
            return sum;
        }

        private static double ComputeNumerator<T>(this ISeries<T> seriesX, ISeries<T> seriesY, double meanX, double meanY)
        {
            double sum = 0;
            for (int i = 0; i < seriesX.Count; ++i)
            {
                sum += ((seriesX.GetValue(i) - meanX) * (seriesY.GetValue(i) - meanY));
            }
            return sum;
        }

        private static double ComputeDenominator<T>(this ISeries<T> seriesX, double meanX)
        {
            double sum = 0;
            for (int i = 0; i < seriesX.Count; ++i)
            {
                sum += System.Math.Pow(seriesX.GetValue(i) - meanX, 2);
            }
            return sum;
        }
    }
}
