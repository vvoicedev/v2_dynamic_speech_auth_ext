﻿using System;
using System.Collections.Generic;
using System.Reflection;

using Dynamic.Plugins;

namespace Dynamic.Extensions
{
    public static class AttributeExtentions
    {
        public static Attr GetAttribute<Attr>(this Assembly assembly, bool inherit = true)
        {
            object[] attrs = assembly.GetCustomAttributes(typeof(Attr), inherit);
            if (attrs == null || attrs.Length == 0)
            {
                return default(Attr);
            }
            return (Attr)attrs[0];
        }

        public static List<Attr> GetAttributes<Attr>(this Assembly assembly, bool inherit = true)
        {
            object[] attrs = assembly.GetCustomAttributes(typeof(Attr), inherit);
            if (attrs == null || attrs.Length == 0)
            {
                return null;
            }

            var list = new List<Attr>(attrs.Length);
            foreach (var attr in attrs)
            {
                list.Add((Attr)attr);
            }
            return list;
        }

        public static Attr GetAttribute<Attr>(this Type type, bool inherit = false)
        {
            object[] attrs = type.GetCustomAttributes(typeof(Attr), inherit);
            if (attrs == null || attrs.Length == 0)
            {
                return default(Attr);
            }
            return (Attr)attrs[0];
        }

        public static Attr GetAttribute<Attr>(this MethodInfo info, bool inherit = false)
        {
            object[] attrs = info.GetCustomAttributes(typeof(Attr), inherit);
            if (attrs == null || attrs.Length == 0)
            {
                return default(Attr);
            }
            return (Attr)attrs[0];
        }

        public static List<Attr> GetAttributes<Attr>(this Type type, bool inherit = false)
        {
            object[] attrs = type.GetCustomAttributes(typeof(Attr), inherit);
            if (attrs == null || attrs.Length == 0)
            {
                return null;
            }

            var list = new List<Attr>(attrs.Length);
            foreach (var attr in attrs)
            {
                list.Add((Attr)attr);
            }
            return list;
        }

        public static List<Attr> GetAttributes<Attr>(this MethodInfo info, bool inherit = false)
        {
            object[] attrs = info.GetCustomAttributes(typeof(Attr), inherit);
            if (attrs == null || attrs.Length == 0)
            {
                return null;
            }

            var list = new List<Attr>(attrs.Length);
            foreach(var attr in attrs)
            {
                list.Add((Attr)attr);
            }
            return list;
        }

        public static Attr GetAttribute<Attr>(this PropertyInfo info, bool inherit = false)
        {
            object[] attrs = info.GetCustomAttributes(typeof(Attr), inherit);
            if (attrs == null || attrs.Length == 0)
            {
                return default(Attr);
            }
            return (Attr)attrs[0];
        }

        public static List<Attr> GetAttributes<Attr>(this PropertyInfo info, bool inherit = false)
        {
            object[] attrs = info.GetCustomAttributes(typeof(Attr), inherit);
            if (attrs == null || attrs.Length == 0)
            {
                return null;
            }

            var list = new List<Attr>(attrs.Length);
            foreach (var attr in attrs)
            {
                list.Add((Attr)attr);
            }
            return list;
        }
    }
}
