﻿using System;

using Dynamic.Services;

namespace Dynamic.Events
{
    public interface IEventManager
    {
        /// <summary>
        /// Adds a synchronous event handler to the event collection.
        /// </summary>
        /// <param name="name">The event name. This is NOT the method name, but an abstract label for referencing the event later.</param>
        /// <param name="eh">The method that handles the event. Using the EventHandler delegate, both static and non-static methods may be specified.</param>
        void AddSyncEventSink(string name, EventHandler eh);

        /// <summary>
        /// Adds an asynchronous event handler to the event collection.
        /// </summary>
        /// <param name="name">The event name. This is NOT the method name, but a abstract label for referencing the event later.</param>
        /// <param name="eh">The method that handles the event. Using the EventHandler delegate, both static and non-static methods may be specified.</param>
        void AddAsyncEventSink(string name, EventHandler eh);

        /// <summary>
        /// Adds a synchronous static event handler to the event collection using reflection.
        /// </summary>
        /// <param name="name">The event name. This is NOT the method name, but a abstract label for referencing the event later.</param>
        /// <param name="reflection">The reflected method name. This must be in the format: "assembly/namespace.class/method"</param>
        void AddSyncEventSink(string name, string reflection);

        /// <summary>
        /// Adds a synchronous instantiated event to the event collection using reflection.
        /// </summary>
        /// <param name="name">The event name.  This is NOT the method name, but a abstract label for referencing the event later.</param>
        /// <param name="instance">The instance containing the specified reflected method.</param>
        /// <param name="reflection">The reflected method name.  This must be in the format: "assembly/namespace.class/method"</param>
        void AddSyncEventSink(string name, object instance, string reflection);

        /// <summary>
        /// Adds an asynchronous static event handler to the event collection using reflection.
        /// </summary>
        /// <param name="name">The event name.  This is NOT the method name, but a abstract label for referencing the event later.</param>
        /// <param name="reflection">The reflected method name.  This must be in the format: "assembly/namespace.class/method"</param>
        void AddAsyncEventSink(string name, string reflection);

        /// <summary>
        /// Adds a synchronous instantiated event to the event collection using reflection.
        /// </summary>
        /// <param name="name">The event name.  This is NOT the method name, but a abstract label for referencing the event later.</param>
        /// <param name="instance">The instance containing the specified reflected method.</param>
        /// <param name="reflection">The reflected method name.  This must be in the format: "assembly/namespace.class/method"</param>
        void AddAsyncEventSink(string name, object instance, string reflection);

        /// <summary>
        /// Invoke an event.
        /// </summary>
        /// <param name="name">The abstract event name.</param>
        /// <returns>For synchronous events, returns null, otherwise returns an IAsyncResult object.</returns>
        IAsyncResult Invoke(string name);

        /// <summary>
        /// Invoke an event.
        /// </summary>
        /// <param name="name">The abstract event name.</param>
        /// <param name="sender">The instance of the sender.</param>
        /// <returns>For synchronous events, returns null, otherwise returns an IAsyncResult object.</returns>
        IAsyncResult Invoke(string name, object sender);

        /// <summary>
        /// Invoke an event.
        /// </summary>
        /// <param name="name">The abstract event name.</param>
        /// <param name="args">A list of parameters passed to the event handler.</param>
        /// <returns>For synchronous events, returns null, otherwise returns an IAsyncResult object.</returns>
        IAsyncResult Invoke(string name, object[] args);

        /// <summary>
        /// Invoke an event.
        /// </summary>
        /// <param name="name">The abstract event name.</param>
        /// <param name="sender">The instance of the sender.</param>
        /// <param name="args">A list of parameters passed to the event handler.</param>
        /// <returns>For synchronous events, returns null, otherwise returns an IAsyncResult object.</returns>
        IAsyncResult Invoke(string name, object sender, object[] args);

        /// <summary>
        /// Check to see if an event is available in the event manager
        /// </summary>
        /// <param name="name">The name of the event to check.</param>
        /// <returns>True if an event by the specified name is available in the EventManager, otherwise false.</returns>
        bool IsEventAvailable(string name);

    }
}
