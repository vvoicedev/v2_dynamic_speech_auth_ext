﻿using Dynamic.Services;

namespace Dynamic.Events
{
    public interface IEventManagerService : IEventManager, IService
    {
    }
}
