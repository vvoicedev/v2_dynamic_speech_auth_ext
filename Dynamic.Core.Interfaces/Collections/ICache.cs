﻿using System;

namespace Dynamic.Collections
{
    public interface ICache<TKey, TValue> : IDisposable
    {
        void Clear();

        void Store(TKey key, TValue cacheObject);

        void Store(TKey key, TValue cacheObject, int cacheTimeout);

        TValue this[TKey key] { get; }

        TValue Gather(TKey key);

        bool TryGet(TKey key, out TValue value);

        void Remove(TKey key);

        bool ContainsKey(TKey key);
    }

    public interface ICache<TValue> : ICache<StringId, TValue> { }

    public interface ICache : ICache<StringId, object> { }
}
