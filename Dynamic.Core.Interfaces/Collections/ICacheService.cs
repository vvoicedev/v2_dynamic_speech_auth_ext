﻿
using Dynamic.Services;

namespace Dynamic.Collections
{
    public interface ICacheService<TKey, TValue> : ICache<TKey, TValue>, IService { }

    public interface ICacheService<TValue> : ICache<StringId, TValue>, IService { }

    public interface ICacheService : ICache<StringId, object>, IService { }
}
