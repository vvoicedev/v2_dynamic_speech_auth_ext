﻿using System;

namespace Dynamic.Plugins.Composition
{
    [AttributeUsage(AttributeTargets.Method)]
    public class MethodAttribute : Attribute
    {
        public MethodAttribute(uint version, string name)
        {
            Version = version;
            Name = name;
            Description = "";
        }

        public MethodAttribute(uint version, string name, string description)
        {
            Version = version;
            Name = name;
            Description = description;
        }

        public uint Version { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }

        public string VersionedName { get { return Name + "." + Version; } }
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class MethodIsDeprecatedAttribute : Attribute
    {
        public MethodIsDeprecatedAttribute()
        {
            Message = null;
        }

        public MethodIsDeprecatedAttribute(string message)
        {
            Message = message;
        }

        public string Message { get; private set; }
    }
}
