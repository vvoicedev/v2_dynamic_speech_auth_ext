﻿using System;

namespace Dynamic.Plugins.Composition
{
    [AttributeUsage(AttributeTargets.Class)]
    public class PluginInformationAttribute : Attribute
    {
        public PluginInformationAttribute(uint version, string name, string description)
        {
            Version = version;
            Name = name;
            Description = description;
            Singleton = false;
        }

        public PluginInformationAttribute(uint version, string name, string description, bool singleton)
        {
            Version = version;
            Name = name;
            Description = description;
            Singleton = singleton;
        }

        public uint Version { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public bool Singleton { get; private set; }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class PluginIsDeprecated : Attribute
    {
        public PluginIsDeprecated()
        {
            Message = null;
        }

        public PluginIsDeprecated(string message)
        {
            Message = message;
        }

        public string Message { get; private set; }
    }
}
