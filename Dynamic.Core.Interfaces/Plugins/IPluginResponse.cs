﻿using System;

namespace Dynamic.Plugins
{
    public interface IPluginResponse
    {
        object Id { get; }
        int? Error { get; set; }
        object Results { get; set; }
        Exception Exception { get; set; }

        void SetError(int error);
        void SetError(int error, string message);
        void SetError(Exception ex);
    }
}
