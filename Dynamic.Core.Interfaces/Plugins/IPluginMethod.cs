﻿
namespace Dynamic.Plugins
{
    public interface IPluginMethod
    {
        PluginResult Execute(IPluginRequest request, IPluginResponse response);
    }

    public interface IPluginMethod<TRequest, TRepsonse> : IPluginMethod
        where TRequest : IPluginRequest
        where TRepsonse : IPluginResponse
    {
        PluginResult Execute(TRequest request, TRepsonse response);
    }
    
}
