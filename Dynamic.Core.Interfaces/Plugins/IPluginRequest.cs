﻿using System;

namespace Dynamic.Plugins
{
    public interface IPluginRequest
    {
        string Method { get; }
        object Id { get; set; }
        object Params { get; set; }
    }
}
