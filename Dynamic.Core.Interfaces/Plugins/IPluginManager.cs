﻿using System;
using System.Collections.Generic;

namespace Dynamic.Plugins
{
    public interface IPluginManager
    {
        void InitializePlugins(string pluginFile);

        void InitializePlugins(List<string> pluginPaths);

        void UpdatePlugins(string pluginFile);

        void UpdatePlugins(List<string> pluginPaths);

        void ClearPlugins();

        IPlugin FindPluginObject(string pluginName);

        Type FindPluginType(string pluginName);

        IPluginMethod FindPluginObjectMethod(string pluginMethod);

        IPlugin FindSingletonPluginObject(string pluginName);

        Type FindSingletonPluginType(string pluginName);

        IPluginMethod FindSingletonPluginObjectMethod(string pluginName);

        string[] PluginNames { get; }

        bool RegisterPlugin(string plugin);

        bool RegisterPlugin(Type plugin);

    }
}
