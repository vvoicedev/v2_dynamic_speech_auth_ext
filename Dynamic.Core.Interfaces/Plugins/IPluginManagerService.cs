﻿
using Dynamic.Services;

namespace Dynamic.Plugins
{
    public interface IPluginManagerService : IService, IPluginManager
    {
    }
}
