﻿
namespace Dynamic.Plugins
{
    public class PluginBase : IPlugin
    {
        protected PluginBase()
        {
            IsDisposed = false;
        }

        public bool IsDisposed { get; private set; }

        public void Dispose()
        {
            if(!IsDisposed)
            {
                IsDisposed = true;
                OnDispose();
            }
        }

        protected virtual void OnDispose()
        {

        }
    }
}
