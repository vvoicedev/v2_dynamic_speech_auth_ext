﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dynamic.Services;

namespace Dynamic.Plugins
{
    public abstract class ServicePluginBase : IServicePlugin
    {
        public IServiceManager ServiceManager { get; set; }

        public virtual void Initialize(IServiceManager svcMgr)
        {
            ServiceManager = svcMgr;
        }

        public virtual void Shutdown()
        {
            
        }

        public virtual void Dispose()
        {

        }
    }
}
