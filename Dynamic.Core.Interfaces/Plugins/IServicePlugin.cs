﻿
using Dynamic.Services;

namespace Dynamic.Plugins
{
    public interface IServicePlugin : IPlugin
    {
        /// <summary>
        /// 
        /// </summary>
        IServiceManager ServiceManager { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srvMgr"></param>
        void Initialize(IServiceManager srvMgr);

        /// <summary>
        /// 
        /// </summary>
        void Shutdown();
    }
}
