﻿#if !NO_DYNAMIC_THREADING

using System.Collections.Generic;
using System.Runtime.CompilerServices;

using Dynamic.Services;
using Dynamic.Threading.CompilerServices;

namespace Dynamic.Threading
{
    /// <summary>
    /// TaskId interface
    /// </summary>
    public interface ITaskId
    {
        ushort Id { get; }
        ushort Generation { get; }
        uint UniqueId { get; }
    }

    /// <summary>
    /// TaskStatus interface
    /// </summary>
    public interface ITaskStatus { }

    /// <summary>
    /// 
    /// </summary>
    public interface IAwaitResult
    {
        ITaskStatus TaskStatus { get; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IAwaitResult<out T> : IAwaitResult
    {
        T Result { get; }
    }

    /// <summary>
    /// 
    /// </summary>
    public interface IAwaiter : ICriticalNotifyCompletion
    {
        bool IsCompleted { get; }

        void GetResult();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IAwaiter<out T> : ICriticalNotifyCompletion
    {
        bool IsCompleted { get; }

        T GetResult();
    }

    /// <summary>
    /// 
    /// </summary>
    [AsyncMethodBuilder(typeof(SchedulerInterfaceAsyncMethodBuilder))]
    public interface IAwaitable : IAwaitResult
    {
        IAwaiter GetAwaiter();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [AsyncMethodBuilder(typeof(SchedulerInterfaceAsyncMethodBuilder<>))]
    public interface IAwaitable<out T> : IAwaitResult<T>
    {
        IAwaiter<T> GetAwaiter();
    }

    /// <summary>
    /// Task States
    /// </summary>
    public enum TaskState
    {
        Invalid,        // Task is invalid, ITaskId is not associated with a valid task
        Idle,           // Task has not been queued yet
        Waiting,        // Task is a continuation task and is waiting for parent task to complete to be posted to the pending queue
        Pending,        // Task is pending on the task queue
        Running,        // Task has been picked up by a thread and is running
        Yielding,       // Task is yielding
        Finished,       // Task is Finished
        Cancelled       // Task has been cancelled
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public delegate bool WaitCondition();

    /// <summary>
    /// 
    /// </summary>
    public delegate void TaskOperator();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    public delegate void ParameterizedTaskOperator(object data);

    /// <summary>
    /// 
    /// </summary>
    public delegate System.Threading.Tasks.Task AwaitableTaskOperator();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    public delegate System.Threading.Tasks.Task AwaitableParameterizedTaskOperator(object data);

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public delegate IEnumerable<ITaskStatus> CooperativeTaskOperator();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public delegate IEnumerable<ITaskStatus> CooperativeParameterizedTaskOperator(object data);

    /// <summary>
    /// 
    /// </summary>
    public interface IAsyncTask
    {
        ITaskId TaskId { get; }
        bool IsAlive { get; }
        void Wait();
        bool Wait(int millis);
        void Cancel();
        IAsyncTask ContinueWith(TaskOperator op);
        IAsyncTask ContinueWith(object data, ParameterizedTaskOperator op);
        IAsyncTask ContinueWith(AwaitableTaskOperator op);
        IAsyncTask ContinueWith(object data, AwaitableParameterizedTaskOperator op);
        IAsyncTask ContinueWith(CooperativeTaskOperator op);
        IAsyncTask ContinueWith(object data, CooperativeParameterizedTaskOperator op);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IAsyncTask<T> : IAsyncTask
    {
        T WaitForResult();
        T WaitForResult(int millis);
        T Result { get; }
    }

    /// <summary>
    /// 
    /// </summary>
    public interface ITaskBuilder
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        ITaskBuilder CreateTask();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskOperator"></param>
        /// <returns></returns>
        ITaskBuilder CreateTask(TaskOperator taskOperator);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="taskOperator"></param>
        /// <returns></returns>
        ITaskBuilder CreateTask(object data, ParameterizedTaskOperator taskOperator);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskOperator"></param>
        /// <returns></returns>
        ITaskBuilder CreateAwaitableTask(AwaitableTaskOperator taskOperator);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="taskOperator"></param>
        /// <returns></returns>
        ITaskBuilder CreateAwaitableTask(object data, AwaitableParameterizedTaskOperator taskOperator);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskOperator"></param>
        /// <returns></returns>
        ITaskBuilder CreateCooperativeTask(CooperativeTaskOperator taskOperator);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="taskOperator"></param>
        /// <returns></returns>
        ITaskBuilder CreateCooperativeTask(object data, CooperativeParameterizedTaskOperator taskOperator);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        ITaskBuilder CreateDependancyTask();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskOperator"></param>
        /// <returns></returns>
        ITaskBuilder CreateDependancyTask(TaskOperator taskOperator);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="taskOperator"></param>
        /// <returns></returns>
        ITaskBuilder CreateDependancyTask(object data, ParameterizedTaskOperator taskOperator);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskOperator"></param>
        /// <returns></returns>
        ITaskBuilder CreateAwaitableDependancyTask(AwaitableTaskOperator taskOperator);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="taskOperator"></param>
        /// <returns></returns>
        ITaskBuilder CreateAwaitableDependancyTask(object data, AwaitableParameterizedTaskOperator taskOperator);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskOperator"></param>
        /// <returns></returns>
        ITaskBuilder CreateCooperativeDependancyTask(CooperativeTaskOperator taskOperator);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="taskOperator"></param>
        /// <returns></returns>
        ITaskBuilder CreateCooperativeDependancyTask(object data, CooperativeParameterizedTaskOperator taskOperator);

        /// <summary>
        /// 
        /// </summary>
        void CancelTasks();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool PostTasks();
    }

    /// <summary>
    /// 
    /// </summary>
    public interface ISchedulerService : IService
    {

    }
}

#endif