﻿#if !NO_DYNAMIC_THREADING

using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

namespace Dynamic.Threading.CompilerServices
{
    [StructLayout(LayoutKind.Auto)]
    public struct SchedulerInterfaceAsyncMethodBuilder
    {
        public static SchedulerInterfaceAsyncMethodBuilder Create()
        {
            return default(SchedulerInterfaceAsyncMethodBuilder);
        }

        public void SetException(Exception ex) { }

        public void SetResult() { }

        public void SetStateMachine(IAsyncStateMachine stateMachine) { }

        [SecuritySafeCritical]
        [DebuggerStepThrough]
        public void Start<TStateMachine>(ref TStateMachine stateMachine)
            where TStateMachine : IAsyncStateMachine
        {
            if (stateMachine == null)
            {
                throw new ArgumentNullException("stateMachine");
            }
            stateMachine.MoveNext();
        }

        public void AwaitOnCompleted<TAwaiter, TStateMachine>(
            ref TAwaiter awaiter,
            ref TStateMachine stateMachine)
            where TAwaiter : INotifyCompletion
            where TStateMachine : IAsyncStateMachine
        {

        }

        public void AwaitUnsafeOnCompleted<TAwaiter, TStateMachine>(
            ref TAwaiter awaiter,
            ref TStateMachine stateMachine)
            where TAwaiter : ICriticalNotifyCompletion
            where TStateMachine : IAsyncStateMachine
        {

        }

        public IAwaitable Task { get; }
    }

    [StructLayout(LayoutKind.Auto)]
    public struct SchedulerInterfaceAsyncMethodBuilder<T>
    {
        public static SchedulerInterfaceAsyncMethodBuilder<T> Create()
        {
            return default(SchedulerInterfaceAsyncMethodBuilder<T>);
        }

        public void SetException(Exception ex) { }

        public void SetResult(T result) { }

        public void SetStateMachine(IAsyncStateMachine stateMachine) { }

        public void Start<TStateMachine>(ref TStateMachine stateMachine)
            where TStateMachine : IAsyncStateMachine
        {
            if (stateMachine == null)
            {
                throw new ArgumentNullException("stateMachine");
            }
            stateMachine.MoveNext();
        }

        public void AwaitOnCompleted<TAwaiter, TStateMachine>(
            ref TAwaiter awaiter,
            ref TStateMachine stateMachine)
            where TAwaiter : INotifyCompletion
            where TStateMachine : IAsyncStateMachine
        { }

        public void AwaitUnsafeOnCompleted<TAwaiter, TStateMachine>(
            ref TAwaiter awaiter,
            ref TStateMachine stateMachine)
            where TAwaiter : ICriticalNotifyCompletion
            where TStateMachine : IAsyncStateMachine
        { }

        public IAwaitable<T> Task { get; }
    }
}

#endif