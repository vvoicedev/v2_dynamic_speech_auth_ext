﻿using System;
using System.Globalization;

using Dynamic.Loggers;
using Dynamic.Web;

namespace Dynamic.Speech
{
    public class SpeechApi
    {
        public enum SpeechServer
        {
            /// <summary>
            /// 
            /// </summary>
            UserDefined,
            
            /// <summary>
            /// 
            /// </summary>
            Demo,

            /// <summary>
            /// 
            /// </summary>
            Production
        }

        public enum SpeechVersion
        {
            /// <summary>
            /// A place holder for an undecided Version
            /// </summary>
            Unknown = -1,

            /// <summary>
            /// [Deprecated][Unsupported] Pre-SaaS Interface
            /// 
            /// Version Number maintained for historical reference
            /// </summary>
            Version_0 = 0,

            /// <summary>
            /// SaaS REST Interface
            /// 
            /// Attempts to mimic Version_0 as much as possible in API call structure.
            /// 
            /// Differences:
            ///     * Session-Id is now a header field and not a Uri field
            ///     * Results are in Json, not inline in the StatusDescription
            ///     * No longer uses profiles, but rather a Developer-Key and an Application-Key
            /// </summary>
            Version_1 = 1,

            /// <summary>
            /// Under Development
            /// </summary>
            Version_2 = 2
        }

        public enum SpeechTransport
        {
            /// <summary>
            /// A place holder for an undecided transport
            /// </summary>
            Unknown = -1,

            /// <summary>
            /// 
            /// </summary>
            Rest = 0
        }

        public const string DEVELOPER_KEY = "Developer-Key";
        public const string APPLICATION_KEY = "Application-Key";
        public const string APP_VERSION_ID = "App-Version-Id";
        public const string INTERACTION_ID = "Interaction-Id";
        public const string INTERACTION_TAG = "Interaction-Tag";
        public const string INTERACTION_SOURCE = "Interaction-Source";
        public const string INTERACTION_AGENT = "Interaction-Agent";
        public const string SESSION_ID = "Vv-Session-Id";
        public const string OVERRIDE_TOKEN = "Vv-Override-Token";
        public const string FEEDBACK_BREAK_ATTEMPT = "Feedback-BreakAttempt";
        public const string FEEDBACK_RECORDING = "Feedback-Recording";
        public const string FEEDBACK_BACKGROUND_NOISE = "Feedback-BackgroundNoise";
        public const string FEEDBACK_COMMENTS = "Feedback-Comments";

        public const SpeechVersion DefaultVersion = SpeechVersion.Version_1;

        public static void Initialize(Configuration configuration)
        {
            if(configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            mConfiguration = configuration;
        }

        public static string DeveloperKey
        {
            get
            {
                if(mConfiguration == null)
                {
                    throw new Exception("Configuration Not Set");
                }
                return mConfiguration.DeveloperKey;
            }
        }

        public static string ApplicationKey
        {
            get
            {
                if (mConfiguration == null)
                {
                    throw new Exception("Configuration Not Set");
                }
                return mConfiguration.ApplicationKey;
            }
        }

        public static string ApplicationSource
        {
            get
            {
                if (mConfiguration == null)
                {
                    throw new Exception("Configuration Not Set");
                }
                return mConfiguration.ApplicationSource;
            }
        }

        public static string ApplicationSourceVersion
        {
            get
            {
                if (mConfiguration == null)
                {
                    throw new Exception("Configuration Not Set");
                }
                return mConfiguration.ApplicationUserAgent;
            }
        }

        public static SpeechServer ServerType
        {
            get
            {
                if (mConfiguration == null)
                {
                    throw new Exception("Configuration Not Set");
                }
                return mConfiguration.ServerType;
            }
        }

        public static SpeechTransport ServerTransport
        {
            get
            {
                if (mConfiguration == null)
                {
                    throw new Exception("Configuration Not Set");
                }
                return mConfiguration.ServerTransport;
            }
        }

        public static string Server
        {
            get
            {
                if (mConfiguration == null)
                {
                    throw new Exception("Configuration Not Set");
                }
                return mConfiguration.Server;
            }
        }

        public static SpeechVersion ServerVersion
        {
            get
            {
                if (mConfiguration == null)
                {
                    throw new Exception("Configuration Not Set");
                }
                return mConfiguration.ServerVersion;
            }
        }

        public static CultureInfo Culture
        {
            get
            {
                if (mConfiguration == null)
                {
                    throw new Exception("Configuration Not Set");
                }
                return mConfiguration.Culture;
            }
        }

        public static int MinimumVerifyCallsToPass
        {
            get
            {
                if (mConfiguration == null)
                {
                    throw new Exception("Configuration Not Set");
                }
                return mConfiguration.MinimumVerifyCallsToPass;
            }
        }

        public static int SocketReadTimeout
        {
            get
            {
                if (mConfiguration == null)
                {
                    throw new Exception("Configuration Not Set");
                }
                return mConfiguration.SocketReadTimeout;
            }
        }

        public static ILogger Logger
        {
            get
            {
                if (mConfiguration == null)
                {
                    throw new Exception("Configuration Not Set");
                }
                return mConfiguration.Logger;
            }
        }

        public static Configuration DefaultConfiguration
        {
            get
            {
                if (mConfiguration == null)
                {
                    throw new Exception("Configuration Not Set");
                }
                return mConfiguration;
            }
        }

        #region

        public sealed class Configuration
        {
            public string DeveloperKey { get; internal set; }
            public string ApplicationKey { get; internal set; }
            public string ApplicationSource { get; internal set; }
            public string ApplicationUserAgent { get; internal set; }
            public SpeechServer ServerType { get; internal set; }
            public string Server { get; internal set; }
            public SpeechTransport ServerTransport { get; internal set; }
            public SpeechVersion ServerVersion { get; internal set; }
            public object ServerTransportObject { get; internal set; }
            public CultureInfo Culture { get; private set; }
            public int MinimumVerifyCallsToPass { get; internal set; }
            public int SocketReadTimeout { get; internal set; }
            public ILogger Logger { get; internal set; }

            internal Configuration()
            {
                DeveloperKey = "";
                ApplicationKey = "";
                ApplicationSource = "Dynamic.SpeechApi/2.0";
                ApplicationUserAgent = "";
                ServerType = SpeechServer.UserDefined;
                Server = "";
                ServerTransport = SpeechTransport.Rest;
                ServerVersion = DefaultVersion;
                ServerTransportObject = null;
                Culture = new CultureInfo("en-US");
                MinimumVerifyCallsToPass = 0;
                SocketReadTimeout = 0;
                Logger = new NullLogger();
            }
        }

        public sealed class Builder
        {
            private Configuration mConfiguration;

            public Builder()
            {
                mConfiguration = new Configuration();
            }

            public Builder SetDeveloperKey(string key)
            {
                mConfiguration.DeveloperKey = key;
                return this;
            }

            public Builder SetApplicationKey(string key)
            {
                mConfiguration.ApplicationKey = key;
                return this;
            }

            public Builder SetApplicationSource(string source)
            {
                mConfiguration.ApplicationSource = source;
                return this;
            }

            public Builder SetApplicationUserAgent(string userAgent)
            {
                mConfiguration.ApplicationUserAgent = userAgent;
                return this;
            }

            public Builder SetServer(string server)
            {
                mConfiguration.ServerType = SpeechServer.UserDefined;
                mConfiguration.Server = server;
                return this;
            }

            public Builder SetServer(SpeechServer server)
            {
                if(server == SpeechServer.UserDefined)
                {
                    throw new ArgumentException("Invalid function for setting UserDefined Server");
                }
                mConfiguration.ServerType = server;
                switch(server)
                {
                    case SpeechServer.Demo:
                        mConfiguration.Server = V2_ON_DEMAND_DEMO_SERVER;
                        break;
                    case SpeechServer.Production:
                        mConfiguration.Server = V2_ON_DEMAND_PROD_SERVER;
                        break;
                }
                return this;
            }

            public Builder SetServerVersion(SpeechVersion version)
            {
                mConfiguration.ServerVersion = version;
                return this;
            }

            public Builder SetMinimumVerifyCallsToPass(int min)
            {
                mConfiguration.MinimumVerifyCallsToPass = min;
                return this;
            }

            public Builder SetSocketReadTimeout(int timeout)
            {
                mConfiguration.SocketReadTimeout = timeout;
                return this;
            }

            public Builder SetServerTransport(SpeechTransport transport)
            {
                mConfiguration.ServerTransport = transport;
                return this;
            }

            public Builder SetTransport(IHttpClient httpClient)
            {
                if (httpClient == null)
                {
                    throw new ArgumentNullException("httpClient");
                }

                if(httpClient.Headers != null && httpClient.Headers.Count > 0)
                {
                    if(string.IsNullOrEmpty(mConfiguration.DeveloperKey) && httpClient.Headers.ContainsKey(DEVELOPER_KEY))
                    {
                        SetDeveloperKey(httpClient.Headers[DEVELOPER_KEY]);
                    }

                    if (string.IsNullOrEmpty(mConfiguration.ApplicationKey) && httpClient.Headers.ContainsKey(APPLICATION_KEY))
                    {
                        SetApplicationKey(httpClient.Headers[APPLICATION_KEY]);
                    }

                    if (string.IsNullOrEmpty(mConfiguration.ApplicationSource) && httpClient.Headers.ContainsKey(INTERACTION_SOURCE))
                    {
                        SetApplicationSource(httpClient.Headers[INTERACTION_SOURCE]);
                    }

                    if (string.IsNullOrEmpty(mConfiguration.ApplicationUserAgent) && httpClient.Headers.ContainsKey(INTERACTION_AGENT))
                    {
                        SetApplicationUserAgent(httpClient.Headers[INTERACTION_AGENT]);
                    }
                    else if(string.IsNullOrEmpty(mConfiguration.ApplicationUserAgent) && httpClient.Headers.ContainsKey(APP_VERSION_ID))
                    {
                        SetApplicationUserAgent(httpClient.Headers[APP_VERSION_ID]);
                    }
                }

                SetServer(httpClient.BaseEndpoint.AbsoluteUri);
                SetServerTransport(SpeechTransport.Rest);
                mConfiguration.ServerTransportObject = httpClient;

                return this;
            }

            public Builder SetLogger(ILogger logger)
            {
                mConfiguration.Logger = logger;
                return this;
            }

            public Configuration Build()
            {
                if(string.IsNullOrEmpty(mConfiguration.DeveloperKey))
                {
                    throw new MissingFieldException("DeveloperKey");
                }
                else if (string.IsNullOrEmpty(mConfiguration.ApplicationKey))
                {
                    throw new MissingFieldException("ApplicationKey");
                }
                else if (string.IsNullOrEmpty(mConfiguration.Server))
                {
                    throw new MissingFieldException("Server");
                }
                return mConfiguration;
            }
        }

        #endregion

        #region
        
        private const string V2_ON_DEMAND_DEMO_SERVER = "https://demo.v2ondemandapis.com";
        private const string V2_ON_DEMAND_PROD_SERVER = "https://prod.v2ondemandapis.com";

        private static Configuration mConfiguration;

        static SpeechApi()
        {
            mConfiguration = null;
        }

        #endregion
    }
}
