﻿
using NAudio.Wave;

namespace Dynamic.Speech.Authorization.Utilities
{
    public class SveUtility
    {
        #region Internal Static Functions

        internal static WaveFormatEncoding GetCodec(string algo)
        {
            switch (algo.ToLower())
            {
                case "a": case "alaw": return WaveFormatEncoding.ALaw;
                case "p": case "pcm_little_endian": return WaveFormatEncoding.Pcm;
            }
            return WaveFormatEncoding.Unknown;
        }

        #endregion
    }
}
