﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Dynamic.Loggers;

using NAudio.Wave;

namespace Dynamic.Speech.Authorization.Enrollers
{
    internal interface ISveEnroller : IDisposable
    {
#region Public Properties

        string InteractionId { get; set; }

        string InteractionTag { get; set; }

        SveEnroller.Gender SubPopulation { get; set; }

        string ClientId { get; set; }

        string AuthToken { get; set; }

#endregion

#region Public Getters

        SpeechApi.Configuration Configuration { get;}

        CookieContainer CookieContainer { get; }

        string Server { get; }

        string SessionId { get; }

        string InteractionSource { get; }

        string InteractionAgent { get; }

        Dictionary<string, string> MetaData { get; }

        Dictionary<string, object> ExtraData { get; }

        bool IsSessionOpen { get; }

        bool IsSessionClosing { get; }

        Dictionary<uint, SveEnroller.Profile> Profiles { get; }

        WaveFormatEncoding Codec { get; }

        double SpeechRequired { get; }

        double SpeechExtracted { get; }

        int SpeechProgress { get; }

        double SpeechTrained { get; }

        SveEnroller.Result EnrollResult { get; }

        Dictionary<string, SveEnroller.InstanceResult> EnrollResults { get; }

        uint TotalProcessCalls { get; }

        long TotalAudioBytes { get; }

        bool HasEnoughSpeech { get; }

        bool IsTrained { get; }

        ILogger Logger { get; }

#endregion

#region Public Methods

        void SetMetaData(string name, bool value);

        void SetMetaData(string name, int value);

        void SetMetaData(string name, double value);

        void SetMetaData(string name, string value);

        bool Append(string filename);

        bool Append(WaveStream stream);

#endregion

#region Public Synchronous Methods

        SveEnroller.Profile PrefetchProfile();

        bool Start();

        SveEnroller.Result Post();

        SveEnroller.Result Post(string filename);

        SveEnroller.Result Post(WaveStream stream);

        SveEnroller.Result End();

        bool Cancel(string reason);

#endregion

#region Public Asynchronous Methods
#if !NO_DYNAMIC_THREADING

        bool PrefetchProfile(SveEnroller.ProfileCallback profileCallback);

        bool Start(SveEnroller.StartCallback callback);

        bool Post(SveEnroller.PostCallback callback);

        bool Post(string filename, SveEnroller.PostCallback callback);

        bool Post(WaveStream stream, SveEnroller.PostCallback callback);

        bool End(SveEnroller.EndCallback callback);

        bool Cancel(string reason, SveEnroller.CancelCallback callback);

#endif
#endregion

#region Public Asynchronous Methods

        Task<SveEnroller.Profile> PrefetchProfileAsync();

        Task<bool> StartAsync();

        Task<SveEnroller.Result> PostAsync();
        
        Task<SveEnroller.Result> PostAsync(string filename);
        
        Task<SveEnroller.Result> PostAsync(WaveStream stream);
        
        Task<SveEnroller.Result> EndAsync();

        Task<bool> CancelAsync(string reason);

#endregion
    }
}
