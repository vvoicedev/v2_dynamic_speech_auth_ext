﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Dynamic.Loggers;
using Dynamic.Speech.Authorization.Utilities;
using Dynamic.Web;
using Dynamic.Web.ContentParts;

using NAudio.Wave;

namespace Dynamic.Speech.Authorization.Enrollers
{
    internal class SveEnrollerRestBase : ISveEnroller
    {
#region Private Variables

        private IHttpClient mHttpClient;

#endregion

#region Life & Death

        protected SveEnrollerRestBase(SpeechApi.Configuration configuration)
        {
            Configuration = configuration;
            Profiles = new Dictionary<uint, SveEnroller.Profile>();
            EnrollResults = new Dictionary<string, SveEnroller.InstanceResult>(StringComparer.OrdinalIgnoreCase);
            MetaData = new Dictionary<string, string>();
            ExtraData = new Dictionary<string, object>();
            ContentParameters = new HttpContentParameters();
            ResetEnroller();
        }

        public void Dispose()
        {
            if(mHttpClient != null)
            {
                mHttpClient.Dispose();
                mHttpClient = null;
            }
        }

#endregion

#region Public Properties

        public string InteractionId { get; set; }

        public string InteractionTag { get; set; }

        public SveEnroller.Gender SubPopulation { get; set; }

        public string ClientId { get; set; }

        public string AuthToken { get; set; }

#endregion

#region Public Getters

        public SpeechApi.Configuration Configuration { get; private set; }

        public HttpContentParameters ContentParameters { get; private set; }

        public CookieContainer CookieContainer { get; private set; }

        public string Server { get { return Configuration.Server; } }

        public string SessionId { get; protected set; }

        public string InteractionSource { get { return Configuration.ApplicationSource; } }

        public string InteractionAgent { get { return Configuration.ApplicationUserAgent; } }

        public Dictionary<string, string> MetaData { get; private set; }

        public Dictionary<string, object> ExtraData { get; private set; }

        public bool IsSessionOpen { get; protected set; }

        public bool IsSessionClosing { get; protected set; }

        public Dictionary<uint, SveEnroller.Profile> Profiles { get; protected set; }

        public WaveFormatEncoding Codec { get; protected set; }

        public double SpeechRequired { get; protected set; }

        public double SpeechExtracted { get; protected set; }

        public int SpeechProgress
        {
            get
            {
                if (SpeechRequired <= 0) return 0;
                int progress = (int)System.Math.Round((SpeechExtracted / SpeechRequired) * 100);
                return (progress < 0) ? 0 : ((progress >= 100) ? (HasEnoughSpeech ? 100 : 99) : progress);
            }
        }

        public double SpeechTrained { get; protected set; }

        public SveEnroller.Result EnrollResult { get; protected set; }

        public Dictionary<string, SveEnroller.InstanceResult> EnrollResults { get; protected set; }

        public uint TotalProcessCalls { get; protected set; }

        public long TotalAudioBytes { get; protected set; }

        public bool HasEnoughSpeech { get { return SpeechExtracted >= SpeechRequired; } }

        public bool IsTrained { get; protected set; }

        public ILogger Logger { get { return Configuration.Logger; } }

#endregion

#region Public Methods

        public void SetMetaData(string name, bool value)
        {
            SetMetaData(name, value ? "1" : "0");
        }

        public void SetMetaData(string name, int value)
        {
            SetMetaData(name, Convert.ToString(value));
        }

        public void SetMetaData(string name, double value)
        {
            SetMetaData(name, Convert.ToString(value));
        }

        public void SetMetaData(string name, string value)
        {
            // For now, double up on these fields, until
            // they can be properly coded for server side
            MetaData[name] = value;
            MetaData["Meta-" + name] = value;
        }

        public virtual bool Append(string filename)
        {
            if (IsSessionOpen)
            {
                var waveStream = new WaveFileReader(filename);
                Logger.WriteDebugLog("SveEnroller.Append(): Filename: " + filename);
                return Append(waveStream);
            }
            return false;
        }

        public virtual bool Append(WaveStream stream)
        {
            if (IsSessionOpen)
            {
                if (stream.CanSeek)
                {
                    stream.Seek(0, SeekOrigin.Begin);
                }

                Logger.WriteDebugLog("SveEnroller.Append(): In-Length: " + stream.Length);
                Logger.WriteDebugLog("SveEnroller.Append(): In-WaveFormat: " + stream.WaveFormat);

                AudioWavContentPart waveParameter;
                if (Codec == WaveFormatEncoding.ALaw)
                {
                    waveParameter = new AudioWavContentPart(stream, WaveFormat.CreateALawFormat(8000, 1));
                }
                else if (Codec == WaveFormatEncoding.Pcm)
                {
                    waveParameter = new AudioWavContentPart(stream, new WaveFormat(8000, 16, 1));
                }
                else
                {
                    return false;
                }

                Logger.WriteDebugLog("SveEnroller.Append(): Append-Length: " + waveParameter.Stream.Length);
                Logger.WriteDebugLog("SveEnroller.Append(): Append-WaveFormat: " + waveParameter.Stream.WaveFormat);

                waveParameter.FileName = BuildAudioName();
                waveParameter.AsRaw = true;

                ContentParameters.Add("data", waveParameter);

                TotalAudioBytes += waveParameter.Stream.Length;

                return true;
            }
            return false;
        }

#endregion

#region Public Synchronous Methods

        public virtual SveEnroller.Profile PrefetchProfile()
        {
            return new SveEnroller.Profile();
        }

        public virtual bool Start()
        {
            return false;
        }

        public virtual SveEnroller.Result Post()
        {
            return SveEnroller.Result.Invalid;
        }

        public virtual SveEnroller.Result Post(string filename)
        {
            if (IsSessionOpen)
            {
                if (Append(filename))
                {
                    return Post();
                }
            }
            return SveEnroller.Result.Invalid;
        }

        public virtual SveEnroller.Result Post(WaveStream stream)
        {
            if (IsSessionOpen)
            {
                if (Append(stream))
                {
                    return Post();
                }
            }
            return SveEnroller.Result.Invalid;
        }

        public virtual SveEnroller.Result End()
        {
            return SveEnroller.Result.Invalid;
        }

        public virtual bool Cancel(string reason)
        {
            return false;
        }

#endregion

#region Public Asynchronous Methods
#if !NO_DYNAMIC_THREADING

        public virtual bool PrefetchProfile(SveEnroller.ProfileCallback profileCallback)
        {
            return false;
        }

        public virtual bool Start(SveEnroller.StartCallback callback)
        {
            callback(false);
            return false;
        }

        public virtual bool Post(SveEnroller.PostCallback callback)
        {
            callback(SveEnroller.Result.Error);
            return false;
        }

        public virtual bool Post(string filename, SveEnroller.PostCallback callback)
        {
            if (IsSessionOpen)
            {
                if (Append(filename))
                {
                    return Post(callback);
                }
            }
            return false;
        }

        public virtual bool Post(WaveStream stream, SveEnroller.PostCallback callback)
        {
            if (IsSessionOpen)
            {
                if (Append(stream))
                {
                    return Post(callback);
                }
            }
            return false;
        }

        public virtual bool End(SveEnroller.EndCallback callback)
        {
            callback(SveEnroller.Result.Error);
            return false;
        }

        public virtual bool Cancel(string reason, SveEnroller.CancelCallback callback)
        {
            callback();
            return false;
        }

#endif
#endregion

#region Public Asynchronous Task Methods
        
        public virtual Task<SveEnroller.Profile> PrefetchProfileAsync()
        {
            return Task.FromResult(new SveEnroller.Profile());
        }

        public virtual Task<bool> StartAsync()
        {
            return Task.FromResult(false);
        }

        public virtual Task<SveEnroller.Result> PostAsync()
        {
            return Task.FromResult(SveEnroller.Result.Invalid);
        }

        public virtual Task<SveEnroller.Result> PostAsync(string filename)
        {
            if (IsSessionOpen)
            {
                if (Append(filename))
                {
                    return PostAsync();
                }
            }
            return Task.FromResult(SveEnroller.Result.Invalid);
        }

        public virtual Task<SveEnroller.Result> PostAsync(WaveStream stream)
        {
            if (IsSessionOpen)
            {
                if (Append(stream))
                {
                    return PostAsync();
                }
            }
            return Task.FromResult(SveEnroller.Result.Invalid);
        }

        public virtual Task<SveEnroller.Result> EndAsync()
        {
            return Task.FromResult(SveEnroller.Result.Invalid);
        }

        public virtual Task<bool> CancelAsync(string reason)
        {
            return Task.FromResult(false);
        }

#endregion

#region Protected Methods

        protected void ResetEnroller()
        {
            SessionId = "";
            Codec = WaveFormatEncoding.Unknown;
            Profiles.Clear();
            EnrollResults.Clear();
            EnrollResult = SveEnroller.Result.NeedMore;
            MetaData.Clear();
            ExtraData.Clear();
            ContentParameters.Clear();
            CookieContainer = new CookieContainer();
            TotalAudioBytes = 0;
            TotalProcessCalls = 0;
            SpeechRequired = 0;
            SpeechExtracted = 0;
            IsSessionOpen = false;
            IsSessionClosing = false;

            var webAgent = GetWebAgent();
            webAgent.Headers.Remove(SpeechApi.SESSION_ID);
            webAgent.Headers.Remove(SpeechApi.INTERACTION_ID);
            webAgent.Headers.Remove(SpeechApi.INTERACTION_TAG);
        }

        protected uint AddProfile(Dictionary<string, object> data)
        {
            SveEnroller.Profile profile = new SveEnroller.Profile();

            profile.Codec = SveUtility.GetCodec(Convert.ToString(data["codec"]));
            profile.MinimumSecondsOfSpeech = Convert.ToDouble(data["min_seconds_of_speech"]);

            uint index = Convert.ToUInt32(data["index"]);
            Profiles.Add(index, profile);

            if (Codec == WaveFormatEncoding.Unknown)
            {
                Codec = profile.Codec;
            }

            Logger.WriteDebugLog("SveEnroller.AddProfile(): Codec: {0}", profile.Codec);
            Logger.WriteDebugLog("SveEnroller.AddProfile(): MinimumSecondsOfSpeech: {0}", profile.MinimumSecondsOfSpeech);
            Logger.WriteDebugLog("SveEnroller.AddProfile(): Index: {0}", index);

            SpeechRequired = profile.MinimumSecondsOfSpeech;

            return index;
        }

        protected void AddResult(string name, Dictionary<string, object> data)
        {
            SveEnroller.InstanceResult result = new SveEnroller.InstanceResult();

            result.ErrorCode = Convert.ToUInt32(data["error"]);
            data.Remove("error");

            uint index = Convert.ToUInt32(data["index"]);
            SveEnroller.Profile profile = Profiles[index];
            data.Remove("index");

            if (data.ContainsKey("seconds_extracted"))
            {
                result.SpeechExtracted = Convert.ToDouble(data["seconds_extracted"]);
                result.SpeechTrained = 0.0;
                data.Remove("seconds_extracted");
                if (result.SpeechExtracted >= profile.MinimumSecondsOfSpeech)
                {
                    result.Result = SveEnroller.Result.Success;
                }
                else
                {
                    result.Result = SveEnroller.Result.NeedMore;
                }
            }
            else if (data.ContainsKey("seconds_trained"))
            {
                result.Result = SveEnroller.Result.Success;

                result.SpeechExtracted = 0.0;
                result.SpeechTrained = Convert.ToDouble(data["seconds_trained"]);
                data.Remove("seconds_trained");
            }
            else
            {
                // Error
                result.Result = SveEnroller.Result.Invalid;
            }

            Logger.WriteDebugLog("SveEnroller.AddResult(): {0} - ErrorCode: {1}", name, result.ErrorCode);
            Logger.WriteDebugLog("SveEnroller.AddResult(): {0} - SpeechExtracted: {1}", name, result.SpeechExtracted);
            Logger.WriteDebugLog("SveEnroller.AddResult(): {0} - SpeechTrained: {1}", name, result.SpeechTrained);
            Logger.WriteDebugLog("SveEnroller.AddResult(): {0} - Result: {1}", name, result.Result);

            foreach (var extra in data)
            {
                result.Extra.Add(extra.Key, extra.Value);
                Logger.WriteDebugLog("SveEnroller.AddResult(): {0} - {1}: {2}", name, extra.Key, extra.Value);
            }

            EnrollResults.Add(name, result);
        }

        protected IHttpClient GetWebAgent()
        {
            if (mHttpClient == null)
            {
                var httpClient = Configuration.ServerTransportObject as IHttpClient;
                if (httpClient != null)
                {
                    mHttpClient = httpClient;
                    if (!mHttpClient.Headers.ContainsKey(SpeechApi.DEVELOPER_KEY))
                    {
                        mHttpClient.Headers.Add(SpeechApi.DEVELOPER_KEY, Configuration.DeveloperKey);
                    }

                    if (!mHttpClient.Headers.ContainsKey(SpeechApi.APPLICATION_KEY))
                    {
                        mHttpClient.Headers.Add(SpeechApi.APPLICATION_KEY, Configuration.ApplicationKey);
                    }

                    if (!mHttpClient.Headers.ContainsKey(SpeechApi.INTERACTION_SOURCE))
                    {
                        mHttpClient.Headers.Add(SpeechApi.INTERACTION_SOURCE, Configuration.ApplicationSource);
                    }

                    if (!mHttpClient.Headers.ContainsKey(SpeechApi.INTERACTION_AGENT))
                    {
                        mHttpClient.Headers.Add(SpeechApi.INTERACTION_AGENT, Configuration.ApplicationUserAgent);
                    }

                    if (!mHttpClient.Headers.ContainsKey(SpeechApi.APP_VERSION_ID))
                    {
                        mHttpClient.Headers.Add(SpeechApi.APP_VERSION_ID, Configuration.ApplicationSource);
                    }
                }
                else
                {
                    mHttpClient = new EnrollerAgent(Configuration);
                }
            }
            return mHttpClient;
        }

        protected SveEnroller.Result GetErrorResult(int errorCode)
        {
            errorCode = errorCode >= 0 ? errorCode : -errorCode;
            switch (errorCode)
            {
                case 104: return SveEnroller.Result.LimitReached;
                case 110: return SveEnroller.Result.Unauthorized;
                case 303: return SveEnroller.Result.NeedMore;
                case 311: return SveEnroller.Result.LimitReached;
                case 318: return SveEnroller.Result.AlreadyEnrolled;
                case 320: return SveEnroller.Result.TokenExists;
                case 321: return SveEnroller.Result.TokenRequired;
                default: return SveEnroller.Result.Error;
            }
        }

#endregion

#region Private Methods

        private string BuildAudioName()
        {
            StringBuilder builder = new StringBuilder();
            string iid = InteractionId;
            if (!string.IsNullOrEmpty(iid))
            {
                builder.Append(iid).Append("-");
            }
            return builder.Append(ClientId).Append("-")
                    .Append(TotalProcessCalls).Append("-")
                    .Append(ContentParameters.Count).Append(".raw").ToString();
        }

#endregion

#region Private Http Class

        private class EnrollerAgent : HttpClient
        {
            public EnrollerAgent(SpeechApi.Configuration configuration)
                : base(BuildEndpoint(configuration.Server), "SveEnroller", configuration.Logger)
            {
                Headers = new Dictionary<string, string>();
                Headers.Add(SpeechApi.DEVELOPER_KEY, configuration.DeveloperKey);
                Headers.Add(SpeechApi.APPLICATION_KEY, configuration.ApplicationKey);
                Headers.Add(SpeechApi.INTERACTION_SOURCE, configuration.ApplicationSource);
                Headers.Add(SpeechApi.INTERACTION_AGENT, configuration.ApplicationUserAgent);
                Headers.Add(SpeechApi.APP_VERSION_ID, configuration.ApplicationSource);
            }
        }

#endregion
    }
}
