﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dynamic.Json;
using Dynamic.Web;

namespace Dynamic.Speech.Authorization.Enrollers
{
    internal class SveEnrollerRest_1 : SveEnrollerRestBase
    {
        private const string URI_PATH_PROFILE = "/1/sve/Enrollment/Profile";
        private const string URI_PATH_START = "/1/sve/Enrollment/{0}/{1}";
        private const string URI_PATH_PROCESS = "/1/sve/Enrollment";
        private const string URI_PATH_END = "/1/sve/Enrollment";
        private const string URI_PATH_CANCEL = "/1/sve/Cancel/{0}";

        #region Private Variables

        private uint mLastProfileIndex;

        #endregion

        #region Life & Death

        internal SveEnrollerRest_1(SpeechApi.Configuration configuration)
            : base(configuration)
        {
        }

        #endregion

        #region Public Synchronous Methods

        public override SveEnroller.Profile PrefetchProfile()
        {
            ResetEnroller();

            Logger.WriteDebugLog("SveEnrollerRest_1.PreFetchProfile(): URI: " + URI_PATH_PROFILE);

            IHttpResponse response = GetWebAgent().Get(URI_PATH_PROFILE, null, CookieContainer);

            if(response == null)
            {
                Logger.WriteErrorLog("SveEnrollerRest_1.PrefetchProfile(): Timeout occurred");
                EnrollResult = SveEnroller.Result.Timeout;
                return null;
            }
            else if(response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                EnrollResult = HandleError(response, URI_PATH_PROFILE);
                return null;
            }
            else if(!HandleResponse(response, URI_PATH_PROFILE))
            {
                return null;
            }

            return Profiles[mLastProfileIndex];
        }

        public override bool Start()
        {
            ResetEnroller();

            var webAgent = GetWebAgent();

            if(string.IsNullOrEmpty(ClientId))
            {
                Logger.WriteErrorLog("SveEnrollerRest_1.Start(): Missing Client-Id");
                EnrollResult = SveEnroller.Result.Invalid;
                return false;
            }

            string queryPath = string.Format(URI_PATH_START, ClientId, SveEnroller.GetGender(SubPopulation));

            HttpHeaders httpHeaders = new HttpHeaders(MetaData.Count + 3);

            if (MetaData.Count > 0)
            {
                foreach (var item in MetaData)
                {
                    Logger.WriteDebugLog("{0}: {1}", item.Key, item.Value);
                    httpHeaders.Add(item.Key, item.Value);
                }
                MetaData.Clear();
            }

            if (!string.IsNullOrEmpty(InteractionId))
            {
                Logger.WriteDebugLog("{0}: {1}", SpeechApi.INTERACTION_ID, InteractionId);
                httpHeaders[SpeechApi.INTERACTION_ID] = InteractionId;
            }

            if (!string.IsNullOrEmpty(InteractionTag))
            {
                Logger.WriteDebugLog("{0}: {1}", SpeechApi.INTERACTION_TAG, InteractionTag);
                httpHeaders[SpeechApi.INTERACTION_TAG] = InteractionTag;
            }

            if (!string.IsNullOrEmpty(AuthToken))
            {
                Logger.WriteDebugLog("{0}: {1}", SpeechApi.OVERRIDE_TOKEN, AuthToken);
                httpHeaders[SpeechApi.OVERRIDE_TOKEN] = AuthToken;
            }

            var response = webAgent.Post(queryPath, httpHeaders, CookieContainer);

            if (response == null)
            {
                Logger.WriteErrorLog("SveEnrollerRest_1.Start(): Timeout occurred");
                EnrollResult = SveEnroller.Result.Timeout;
                return false;
            }
            else  if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                EnrollResult = HandleError(response, queryPath);
                return false;
            }
            else if(!HandleResponse(response, queryPath))
            {
                return false;
            }

            SessionId = response.Headers[SpeechApi.SESSION_ID];
            webAgent.Headers[SpeechApi.SESSION_ID] = SessionId;
            IsSessionOpen = true;

            Logger.WriteDebugLog("SveEnrollerRest_1.Start(): Session Open - Id: {0}", SessionId);

            return true;
        }

        public override SveEnroller.Result Post()
        {
            if(IsSessionOpen && !IsSessionClosing)
            {
                EnrollResults.Clear();

                IHttpResponse response;
                if (MetaData.Count > 0)
                {
                    HttpHeaders httpHeaders = new HttpHeaders(MetaData.Count);
                    foreach (var item in MetaData)
                    {
                        httpHeaders.Add(item.Key, item.Value);
                    }
                    MetaData.Clear();
                    response = GetWebAgent().Post(URI_PATH_PROCESS, httpHeaders, ContentParameters, HttpContentEncoding.MultipartForm, CookieContainer);
                }
                else
                {
                    response = GetWebAgent().Post(URI_PATH_PROCESS, ContentParameters, HttpContentEncoding.MultipartForm, CookieContainer);
                }

                ContentParameters.Clear();
                TotalProcessCalls++;

                if (response == null)
                {
                    Logger.WriteErrorLog("SveEnrollerRest_1.Start(): Timeout occurred");
                    return (EnrollResult = SveEnroller.Result.Timeout);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    HandleResponse(response, URI_PATH_PROCESS);
                    Logger.WriteDebugLog("SveEnrollerRest_1.Post(): Extracted Speech: " + SpeechExtracted + ", " + EnrollResult);
                    return EnrollResult;
                }
                else
                {
                    return (EnrollResult = HandleError(response, URI_PATH_PROCESS));
                }
            }
            return (EnrollResult = SveEnroller.Result.Invalid);
        }

        public override SveEnroller.Result End()
        {
            if (IsSessionOpen && !IsSessionClosing)
            {
                IsSessionClosing = true;
                EnrollResults.Clear();
                ContentParameters.Clear();

                IHttpResponse response;
                if (MetaData.Count > 0)
                {
                    HttpHeaders httpHeaders = new HttpHeaders(MetaData.Count);
                    foreach (var item in MetaData)
                    {
                        httpHeaders.Add(item.Key, item.Value);
                    }
                    MetaData.Clear();
                    response = GetWebAgent().Delete(URI_PATH_END, httpHeaders, CookieContainer);
                }
                else
                {
                    response = GetWebAgent().Delete(URI_PATH_END, null, CookieContainer);
                }

                if (response == null)
                {
                    Logger.WriteErrorLog("SveEnrollerRest_1.End(): Timeout occurred");
                    return (EnrollResult = SveEnroller.Result.Timeout);
                }
                else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    return (EnrollResult = HandleError(response, URI_PATH_END));
                }
                else
                {
                    IsSessionOpen = false;
                    IsSessionClosing = false;
                    SessionId = "";
                    GetWebAgent().Headers.Remove(SpeechApi.SESSION_ID);
                    if (HandleResponse(response, URI_PATH_END))
                    {
                        Logger.WriteDebugLog("Enroller.End(): Trained Speech: " + SpeechTrained + ", " + EnrollResult);
                        return EnrollResult;
                    }
                }
            }
            return (EnrollResult = SveEnroller.Result.Invalid);
        }

        public override bool Cancel(string reason)
        {
            if (IsSessionOpen && !IsSessionClosing)
            {
                IsSessionClosing = true;
                EnrollResults.Clear();
                ContentParameters.Clear();

                string queryPath = string.Format(URI_PATH_CANCEL, reason.Replace(' ', '-').Substring(0, System.Math.Min(reason.Length, 64)));

                IHttpResponse response = GetWebAgent().Delete(queryPath, null, CookieContainer);

                if (response == null)
                {
                    Logger.WriteErrorLog("SveEnrollerRest_1.Cancel(): Timeout occurred");
                    EnrollResult = SveEnroller.Result.Timeout;
                    return false;
                }
                else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                { 
                    EnrollResult = HandleError(response, queryPath);
                    return false;
                }
            }
            IsSessionOpen = false;
            IsSessionClosing = false;
            SessionId = "";
            GetWebAgent().Headers.Remove(SpeechApi.SESSION_ID);
            return true;
        }

        #endregion

        #region Public Asynchronous Methods
#if !NO_DYNAMIC_THREADING

        public override bool PrefetchProfile(SveEnroller.ProfileCallback profileCallback)
        {
            if(profileCallback == null)
            {
                Logger.WriteErrorLog("SveEnroller_1.PrefetchProfile(profileCallback): Callback was null");
                return false;
            }

            ResetEnroller();

            Logger.WriteDebugLog("SveEnroller_1.PreFetchProfile(): URI: " + URI_PATH_PROFILE);

            return GetWebAgent().GetCallback(URI_PATH_PROFILE, null, CookieContainer, delegate(IHttpResponse response) {

                if (response == null)
                {
                    Logger.WriteErrorLog("SveEnrollerRest_1.PrefetchProfile(profileCallback): Timeout occurred");
                    EnrollResult = SveEnroller.Result.Timeout;
                    profileCallback(null);
                    return;
                }
                else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    EnrollResult = HandleError(response, URI_PATH_PROFILE);
                    profileCallback(null);
                    return;
                }
                
                if (!HandleResponse(response, URI_PATH_PROFILE))
                {
                    profileCallback(null);
                    return;
                }

                profileCallback(Profiles[mLastProfileIndex]);

            }).IsAlive;
        }

        public override bool Start(SveEnroller.StartCallback startCallback)
        {
            if (startCallback == null)
            {
                Logger.WriteErrorLog("SveEnroller_1.Start(startCallback): Callback was null");
                return false;
            }

            ResetEnroller();

            var webAgent = GetWebAgent();

            if (string.IsNullOrEmpty(ClientId))
            {
                Logger.WriteErrorLog("SveEnrollerRest_1.Start(startCallback): Missing Client-Id");
                EnrollResult = SveEnroller.Result.Invalid;
                return false;
            }

            string queryPath = string.Format(URI_PATH_START, ClientId, SveEnroller.GetGender(SubPopulation));

            HttpHeaders httpHeaders = new HttpHeaders(MetaData.Count + 3);
            
            if (MetaData.Count > 0)
            {
                foreach (var item in MetaData)
                {
                    Logger.WriteDebugLog("{0}: {1}", item.Key, item.Value);
                    httpHeaders.Add(item.Key, item.Value);
                }
                MetaData.Clear();
            }

            if (!string.IsNullOrEmpty(InteractionId))
            {
                Logger.WriteDebugLog("{0}: {1}", SpeechApi.INTERACTION_ID, InteractionId);
                httpHeaders[SpeechApi.INTERACTION_ID] = InteractionId;
            }

            if (!string.IsNullOrEmpty(InteractionTag))
            {
                Logger.WriteDebugLog("{0}: {1}", SpeechApi.INTERACTION_TAG, InteractionTag);
                httpHeaders[SpeechApi.INTERACTION_TAG] = InteractionTag;
            }

            if (!string.IsNullOrEmpty(AuthToken))
            {
                Logger.WriteDebugLog("{0}: {1}", SpeechApi.OVERRIDE_TOKEN, AuthToken);
                httpHeaders[SpeechApi.OVERRIDE_TOKEN] = AuthToken;
            }

            return webAgent.PostCallback(queryPath, httpHeaders, CookieContainer, delegate (IHttpResponse response) {

                if (response == null)
                {
                    Logger.WriteErrorLog("SveEnrollerRest_1.Start(startCallback): Timeout occurred");
                    EnrollResult = SveEnroller.Result.Timeout;
                    startCallback(false);
                    return;
                }
                else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    EnrollResult = HandleError(response, queryPath);
                    startCallback(false);
                    return;
                }
                else if (!HandleResponse(response, queryPath))
                {
                    startCallback(false);
                    return;
                }

                SessionId = response.Headers[SpeechApi.SESSION_ID];
                webAgent.Headers[SpeechApi.SESSION_ID] = SessionId;
                IsSessionOpen = true;

                Logger.WriteDebugLog("SveEnrollerRest_1.Start(): Session Open - Id: {0}", SessionId);
                startCallback(true);

            }).IsAlive;
        }

        public override bool Post(SveEnroller.PostCallback postCallback)
        {
            if (postCallback == null)
            {
                Logger.WriteErrorLog("SveEnroller_1.Post(postCallback): Callback was null");
                return false;
            }

            if (IsSessionOpen && !IsSessionClosing)
            {
                EnrollResults.Clear();

                HttpHeaders httpHeaders = new HttpHeaders(MetaData.Count);
                if (MetaData.Count > 0)
                {
                    foreach (var item in MetaData)
                    {
                        httpHeaders.Add(item.Key, item.Value);
                    }
                    MetaData.Clear();
                }

                return GetWebAgent().PostCallback(URI_PATH_PROCESS, httpHeaders, ContentParameters, HttpContentEncoding.MultipartForm, CookieContainer, 
                    delegate (IHttpResponse response) {

                        ContentParameters.Clear();
                        TotalProcessCalls++;

                        if (response == null)
                        {
                            Logger.WriteErrorLog("SveEnrollerRest_1.Start(): Timeout occurred");
                            postCallback(EnrollResult = SveEnroller.Result.Timeout);
                        }
                        else if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            HandleResponse(response, URI_PATH_PROCESS);
                            Logger.WriteDebugLog("SveEnrollerRest_1.Post(): Extracted Speech: " + SpeechExtracted + ", " + EnrollResult);
                            postCallback(EnrollResult);
                        }
                        else
                        {
                            postCallback(EnrollResult = HandleError(response, URI_PATH_PROCESS));
                        }
                    }
                ).IsAlive;
            }
            return false;
        }

        public override bool End(SveEnroller.EndCallback endCallback)
        {
            if (endCallback == null)
            {
                Logger.WriteErrorLog("SveEnroller_1.End(endCallback): Callback was null");
                return false;
            }

            if (IsSessionOpen && !IsSessionClosing)
            {
                IsSessionClosing = true;
                EnrollResults.Clear();
                ContentParameters.Clear();

                return GetWebAgent().DeleteCallback(URI_PATH_END, null, CookieContainer, delegate (IHttpResponse response)
                {

                    if (response == null)
                    {
                        Logger.WriteErrorLog("SveEnrollerRest_1.End(): Timeout occurred");
                        endCallback(EnrollResult = SveEnroller.Result.Timeout);
                    }
                    else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        endCallback(EnrollResult = HandleError(response, URI_PATH_END));
                    }
                    else
                    {
                        IsSessionOpen = false;
                        IsSessionClosing = false;
                        SessionId = "";
                        GetWebAgent().Headers.Remove(SpeechApi.SESSION_ID);
                        if (HandleResponse(response, URI_PATH_END))
                        {
                            Logger.WriteDebugLog("SveEnrollerRest_1.End(): Trained Speech: " + SpeechTrained + ", " + EnrollResult);
                            endCallback(EnrollResult);
                        }
                        else
                        {
                            endCallback(EnrollResult = SveEnroller.Result.Error);
                        }
                    }

                }).IsAlive;
            }
            return false;
        }

        public override bool Cancel(string reason, SveEnroller.CancelCallback cancelCallback)
        {
            if (cancelCallback == null)
            {
                Logger.WriteErrorLog("SveEnrollerRest_1.Cancel(cancelCallback): Callback was null");
                return false;
            }

            if (IsSessionOpen && !IsSessionClosing)
            {
                IsSessionClosing = true;
                EnrollResults.Clear();
                ContentParameters.Clear();

                string queryPath = string.Format(URI_PATH_CANCEL, reason.Replace(' ', '-').Substring(0, System.Math.Min(reason.Length, 64)));

                return GetWebAgent().DeleteCallback(queryPath, null, CookieContainer, delegate (IHttpResponse response)
                {
                    if (response == null)
                    {
                        Logger.WriteErrorLog("SveEnrollerRest_1.Cancel(cancelCallback): Timeout occurred");
                        EnrollResult = SveEnroller.Result.Timeout;
                        cancelCallback();
                        return;
                    }
                    else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        EnrollResult = HandleError(response, queryPath);
                        cancelCallback();
                        return;
                    }
                    IsSessionOpen = false;
                    IsSessionClosing = false;
                    SessionId = "";
                    GetWebAgent().Headers.Remove(SpeechApi.SESSION_ID);
                    cancelCallback();
                }).IsAlive;
            }
            return true;
        }

#endif
        #endregion

        #region Public Asynchronous Tasks Methods

        public override async Task<SveEnroller.Profile> PrefetchProfileAsync()
        {
            ResetEnroller();

            Logger.WriteDebugLog("SveEnrollerRest_1.PreFetchProfile(): URI: " + URI_PATH_PROFILE);

            IHttpResponse response = await GetWebAgent().GetTaskAsync(URI_PATH_PROFILE, null, CookieContainer);

            if (response == null)
            {
                Logger.WriteErrorLog("SveEnrollerRest_1.PrefetchProfile(): Timeout occurred");
                EnrollResult = SveEnroller.Result.Timeout;
                return null;
            }
            else if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                EnrollResult = HandleError(response, URI_PATH_PROFILE);
                return null;
            }
            else if (!HandleResponse(response, URI_PATH_PROFILE))
            {
                return null;
            }

            return Profiles[mLastProfileIndex];
        }

        public override async Task<bool> StartAsync()
        {
            ResetEnroller();

            var webAgent = GetWebAgent();

            if (string.IsNullOrEmpty(ClientId))
            {
                Logger.WriteErrorLog("SveEnrollerRest_1.Start(): Missing Client-Id");
                EnrollResult = SveEnroller.Result.Invalid;
                return false;
            }

            string queryPath = string.Format(URI_PATH_START, ClientId, SveEnroller.GetGender(SubPopulation));

            HttpHeaders httpHeaders = new HttpHeaders(MetaData.Count + 3);

            if (MetaData.Count > 0)
            {
                foreach (var item in MetaData)
                {
                    Logger.WriteDebugLog("{0}: {1}", item.Key, item.Value);
                    httpHeaders.Add(item.Key, item.Value);
                }
                MetaData.Clear();
            }

            if (!string.IsNullOrEmpty(InteractionId))
            {
                Logger.WriteDebugLog("{0}: {1}", SpeechApi.INTERACTION_ID, InteractionId);
                httpHeaders[SpeechApi.INTERACTION_ID] = InteractionId;
            }

            if (!string.IsNullOrEmpty(InteractionTag))
            {
                Logger.WriteDebugLog("{0}: {1}", SpeechApi.INTERACTION_TAG, InteractionTag);
                httpHeaders[SpeechApi.INTERACTION_TAG] = InteractionTag;
            }

            if (!string.IsNullOrEmpty(AuthToken))
            {
                Logger.WriteDebugLog("{0}: {1}", SpeechApi.OVERRIDE_TOKEN, AuthToken);
                httpHeaders[SpeechApi.OVERRIDE_TOKEN] = AuthToken;
            }

            var response = await webAgent.PostTaskAsync(queryPath, httpHeaders, CookieContainer);

            if (response == null)
            {
                Logger.WriteErrorLog("SveEnrollerRest_1.Start(): Timeout occurred");
                EnrollResult = SveEnroller.Result.Timeout;
                return false;
            }
            else if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                EnrollResult = HandleError(response, queryPath);
                return false;
            }
            else if (!HandleResponse(response, queryPath))
            {
                return false;
            }

            SessionId = response.Headers[SpeechApi.SESSION_ID];
            webAgent.Headers[SpeechApi.SESSION_ID] = SessionId;
            IsSessionOpen = true;

            Logger.WriteDebugLog("SveEnrollerRest_1.Start(): Session Open - Id: {0}", SessionId);

            return true;
        }

        public override async Task<SveEnroller.Result> PostAsync()
        {
            if (IsSessionOpen && !IsSessionClosing)
            {
                EnrollResults.Clear();

                IHttpResponse response;
                if (MetaData.Count > 0)
                {
                    HttpHeaders httpHeaders = new HttpHeaders(MetaData.Count);
                    foreach (var item in MetaData)
                    {
                        httpHeaders.Add(item.Key, item.Value);
                    }
                    MetaData.Clear();
                    response = await GetWebAgent().PostTaskAsync(URI_PATH_PROCESS, httpHeaders, ContentParameters, HttpContentEncoding.MultipartForm, CookieContainer);
                }
                else
                {
                    response = await GetWebAgent().PostTaskAsync(URI_PATH_PROCESS, ContentParameters, HttpContentEncoding.MultipartForm, CookieContainer);
                }

                ContentParameters.Clear();
                TotalProcessCalls++;

                if (response == null)
                {
                    Logger.WriteErrorLog("SveEnrollerRest_1.Start(): Timeout occurred");
                    return (EnrollResult = SveEnroller.Result.Timeout);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    HandleResponse(response, URI_PATH_PROCESS);
                    Logger.WriteDebugLog("SveEnrollerRest_1.Post(): Extracted Speech: " + SpeechExtracted + ", " + EnrollResult);
                    return EnrollResult;
                }
                else
                {
                    return (EnrollResult = HandleError(response, URI_PATH_PROCESS));
                }
            }
            return (EnrollResult = SveEnroller.Result.Invalid);
        }

        public override async Task<SveEnroller.Result> EndAsync()
        {
            if (IsSessionOpen && !IsSessionClosing)
            {
                IsSessionClosing = true;
                EnrollResults.Clear();
                ContentParameters.Clear();

                IHttpResponse response;
                if (MetaData.Count > 0)
                {
                    HttpHeaders httpHeaders = new HttpHeaders(MetaData.Count);
                    foreach (var item in MetaData)
                    {
                        httpHeaders.Add(item.Key, item.Value);
                    }
                    MetaData.Clear();
                    response = await GetWebAgent().DeleteTaskAsync(URI_PATH_END, httpHeaders, CookieContainer);
                }
                else
                {
                    response = await GetWebAgent().DeleteTaskAsync(URI_PATH_END, null, CookieContainer);
                }

                if (response == null)
                {
                    Logger.WriteErrorLog("SveEnrollerRest_1.End(): Timeout occurred");
                    return (EnrollResult = SveEnroller.Result.Timeout);
                }
                else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    return (EnrollResult = HandleError(response, URI_PATH_END));
                }
                else
                {
                    IsSessionOpen = false;
                    IsSessionClosing = false;
                    SessionId = "";
                    GetWebAgent().Headers.Remove(SpeechApi.SESSION_ID);
                    if (HandleResponse(response, URI_PATH_END))
                    {
                        Logger.WriteDebugLog("Enroller.End(): Trained Speech: " + SpeechTrained + ", " + EnrollResult);
                        return EnrollResult;
                    }
                }
            }
            return (EnrollResult = SveEnroller.Result.Invalid);
        }

        public override async Task<bool> CancelAsync(string reason)
        {
            if (IsSessionOpen && !IsSessionClosing)
            {
                IsSessionClosing = true;
                EnrollResults.Clear();
                ContentParameters.Clear();

                string queryPath = string.Format(URI_PATH_CANCEL, reason.Replace(' ', '-').Substring(0, System.Math.Min(reason.Length, 64)));

                IHttpResponse response = await GetWebAgent().DeleteTaskAsync(queryPath, null, CookieContainer);

                if (response == null)
                {
                    Logger.WriteErrorLog("SveEnrollerRest_1.Cancel(): Timeout occurred");
                    EnrollResult = SveEnroller.Result.Timeout;
                    return false;
                }
                else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    EnrollResult = HandleError(response, queryPath);
                    return false;
                }
            }
            IsSessionOpen = false;
            IsSessionClosing = false;
            SessionId = "";
            GetWebAgent().Headers.Remove(SpeechApi.SESSION_ID);
            return true;
        }

        #endregion

        #region Private

        private bool HandleResponse(IHttpResponse response, string query_path)
        {
            if(response.ContentType.Equals("application/json") && response.ContentData is IDictionary)
            {
                Dictionary<string, object> data = (Dictionary<string, object>)response.ContentData;
                if (data.ContainsKey("error"))
                {
                    Logger.WriteErrorLog("SveEnrollerRest_1.HandleResult(): Error detected. Uri: {0}, Data:\r\n{1}", query_path, JsonDoctor.ToJson(data["error"]));
                    data.Remove("error");
                }

                foreach (var kv in data)
                {
                    if (kv.Key.Contains("profile.enroll") && kv.Value is IDictionary)
                    {
                        mLastProfileIndex = AddProfile((Dictionary<string, object>)kv.Value);
                    }
                    else if (kv.Key.Contains("result.enroll") && kv.Value is IDictionary)
                    {
                        Dictionary<string, object> result_data = (Dictionary<string, object>)kv.Value;
                        foreach (var result in result_data)
                        {
                            if (result.Value is IDictionary)
                            {
                                AddResult(result.Key, (Dictionary<string, object>)result.Value);
                            }
                        }

                        if (!string.IsNullOrEmpty(ClientId) && EnrollResults.ContainsKey(ClientId.ToLower()))
                        {
                            SveEnroller.InstanceResult result = EnrollResults[ClientId.ToLower()];
                            SpeechExtracted = result.SpeechExtracted;
                            SpeechTrained = result.SpeechTrained;
                            EnrollResult = result.Result;
                        }
                        else
                        {
                            SpeechExtracted = 0;
                            SpeechTrained = 0;
                            EnrollResult = SveEnroller.Result.Unknown;
                        }
                    }
                }
                return true;
            }
            else
            {
                Logger.WriteErrorLog("SveEnrollerRest_1.HandleResponse(): Expected Content Type: 'application/json'. Actual Content Type: '{0}'", response.ContentType);
                return false;
            }
        }

        private SveEnroller.Result HandleError(IHttpResponse response, string query_path)
        {
            if (response.StatusCode != System.Net.HttpStatusCode.RequestTimeout)
            {
                SveEnroller.Result result = SveEnroller.Result.Unknown;
                if (response.ContentType.Equals("application/json") && response.ContentData is IDictionary)
                {
                    Dictionary<string, object> data = (Dictionary<string, object>)response.ContentData;
                    if (data.ContainsKey("error") && data.ContainsKey("description"))
                    {
                        int code = Convert.ToInt32(data["error"]);
                        string descr = Convert.ToString(data["description"]);
                        result = GetErrorResult(code);
                        Logger.WriteErrorLog("SveEnrollerRest_1.HandleError(): Error detected.\r\n\tUri: {0}\r\n\tResult: {1}\r\n\tCode: {2}\r\n\tDescription: {3}", query_path, result, code, descr);
                        return result;
                    }
                }
                Logger.WriteErrorLog("SveVerifierRest_1.HandleError(): Unknown Error detected");
                return result;
            }
            return SveEnroller.Result.Timeout;
        }

#endregion
    }
}
