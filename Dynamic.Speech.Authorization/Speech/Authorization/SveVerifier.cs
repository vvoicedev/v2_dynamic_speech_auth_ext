﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;

using Dynamic.Loggers;
using Dynamic.Speech.Authorization.Context;
using Dynamic.Speech.Authorization.Verifiers;

using NAudio.Wave;

namespace Dynamic.Speech.Authorization
{
    public class SveVerifier : IDisposable
    {
#region Public Enum's, Classes, Interfaces, and Delegates

        public enum Result
        {
            [Description("Pass")]
            Pass,

            [Description("Pass - Is Alive")]
            PassIsAlive,

            [Description("Pass - Not Alive")]
            PassNotAlive,

            [Description("Ambiguous")]
            Ambiguous,

            [Description("Ambiguous - Is Alive")]
            AmbiguousIsAlive,

            [Description("Ambiguous - Not Alive")]
            AmbiguousNotAlive,

            [Description("Fail")]
            Fail,

            [Description("Fail - Is Alive")]
            FailIsAlive,

            [Description("Fail - Not Alive")]
            FailNotAlive,

            [Description("NeedMore")]
            NeedMore,

            [Description("NeedMore - Is Alive")]
            NeedMoreIsAlive,

            [Description("NeedMore - Not Alive")]
            NeedMoreNotAlive,

            [Description("Not Scored")]
            NotScored,

            [Description("Limit Reached")]
            LimitReached,

            [Description("Unauthorized")]
            Unauthorized,

            [Description("Not Found")]
            NotFound,

            [Description("Bad Enrollment")]
            BadEnrollment,

            [Description("Timeout")]
            Timeout,

            [Description("Invalid")]
            Invalid,

            [Description("Error")]
            Error,

            [Description("Unknown")]
            Unknown
        }

        public enum AliveResult
        {
            [Description("Untested")]
            Untested,

            [Description("Not Alive")]
            NotAlive,

            [Description("Alive")]
            Alive
        }

        public enum ProfileType
        {
            [Description("Single")]
            Single = 2,

            [Description("[Multi-Verify] Single Liveness")]
            SingleLivness = 3,

            [Description("[Multi-Verify] Batch")]
            Batch = 4,

            [Description("[Multi-Verify] Multi")]
            Multi = 6,

            [Description("[Multi-Verify] Group")]
            Group = 8,

            [Description("[Multi-Verify] Drop One")]
            DropOne = 10,

            [Description("[Multi-Verify] Recognition")]
            Recognition = 12,

            [Description("[Multi-Verify] Monitor")]
            Monitor = 14,
        }

        public class Profile
        {
            public WaveFormatEncoding Codec { get; internal set; }

            public ProfileType Type { get; internal set; }

            public double MinimumSecondsOfSpeech { get; internal set; }

            public double PassThreshold { get; internal set; }

            public double FailThreshold { get; internal set; }
        }

        public class InstanceResult
        {
            internal InstanceResult()
            {
                Extra = new Dictionary<string, object>();
            }

            public Result Result { get; internal set; }
            public double Score { get; internal set; }
            public double SpeechExtracted { get; internal set; }
            public uint ErrorCode { get; internal set; }
            public Dictionary<string, object> Extra { get; private set; }
        }

        public delegate void ProfileCallback(Profile profile);

        public delegate void StartCallback(bool started);

        public delegate void PostCallback(Result result);

        public delegate void EndCallback(Result result);

        public delegate void CancelCallback();

#endregion

#region Private

        private ISveVerifier mSveVerifier;

#endregion

#region Life & Death

        public SveVerifier()
            : this(SpeechApi.DefaultConfiguration)
        {
        }

        public SveVerifier(SpeechApi.Configuration configuration)
        {
            if (configuration.ServerTransport != SpeechApi.SpeechTransport.Rest)
            {
                throw new NotSupportedException("ServerTransport not supported");
            }
            else if (configuration.ServerVersion == SpeechApi.SpeechVersion.Version_1)
            {
                mSveVerifier = new SveVerifierRest_1(configuration);
            }
            else
            {
                throw new NotSupportedException("ServerVersion not supported");
            }
        }

        public void Dispose()
        {
            if (mSveVerifier != null)
            {
                mSveVerifier.Dispose();
                mSveVerifier = null;
            }
        }

        ~SveVerifier()
        {
            Dispose();
        }

#endregion

#region Public Properties

        public string InteractionId
        {
            get { return mSveVerifier.InteractionId; }
            set { mSveVerifier.InteractionId = value; }
        }

        public string InteractionTag
        {
            get { return mSveVerifier.InteractionTag; }
            set { mSveVerifier.InteractionTag = value; }
        }

        public string ClientId
        {
            get { return mSveVerifier.ClientId; }
            set { mSveVerifier.ClientId = value; }
        }

        public string GroupIds
        {
            get { return mSveVerifier.GroupIds; }
            set { mSveVerifier.GroupIds = value; }
        }

        public string AuthToken
        {
            get { return mSveVerifier.AuthToken; }
            set { mSveVerifier.AuthToken = value; }
        }

#endregion

#region Public Getters

        public string Server
        {
            get { return mSveVerifier.Server; }
        }

        public string SessionId
        {
            get { return mSveVerifier.SessionId; }
        }

        public string InteractionSource
        {
            get { return mSveVerifier.InteractionSource; }
        }

        public string InteractionAgent
        {
            get { return mSveVerifier.InteractionAgent; }
        }

        public bool IsSessionOpen
        {
            get { return mSveVerifier.IsSessionOpen; }
        }

        public bool IsSessionClosing
        {
            get { return mSveVerifier.IsSessionClosing; }
        }

        public bool IsOverridable
        {
            get { return mSveVerifier.IsOverridable; }
        }

        public bool IsAuthorized
        {
            get { return mSveVerifier.IsAuthorized; }
        }

        public Dictionary<uint, Profile> Profiles
        {
            get { return mSveVerifier.Profiles; }
        }

        public WaveFormatEncoding Codec
        {
            get { return mSveVerifier.Codec; }
        }

        public string VerifyClientId
        {
            get { return mSveVerifier.VerifyClientId; }
        }

        public double VerifyScore
        {
            get { return mSveVerifier.VerifyScore; }
        }

        public double SpeechRequired
        {
            get { return mSveVerifier.SpeechRequired; }
        }

        public double SpeechExtracted
        {
            get { return mSveVerifier.SpeechExtracted; }
        }

        public int SpeechProgress
        {
            get { return mSveVerifier.SpeechProgress; }
        }

        public bool HasEnoughSpeech
        {
            get { return mSveVerifier.HasEnoughSpeech; }
        }

        public Result VerifyRawResult
        {
            get { return mSveVerifier.VerifyRawResult; }
        }

        public Result VerifyResult
        {
            get { return mSveVerifier.VerifyResult; }
        }

        public Dictionary<string, InstanceResult> VerifyResults
        {
            get { return mSveVerifier.VerifyResults; }
        }

        public uint TotalProcessCalls
        {
            get { return mSveVerifier.TotalProcessCalls; }
        }

        public long TotalAudioBytes
        {
            get { return mSveVerifier.TotalAudioBytes; }
        }

        public bool IsLivenessRequired
        {
            get { return mSveVerifier.IsLivenessRequired; }
        }

        public bool IsVerified
        {
            get { return mSveVerifier.IsVerified; }
        }

        public AliveResult LivenessResult
        {
            get { return mSveVerifier.LivenessResult; }
        }

        public bool IsAlive
        {
            get { return mSveVerifier.IsAlive; }
        }

        public bool HasResult
        {
            get { return mSveVerifier.HasResult; }
        }

        public bool HasRawResult
        {
            get { return mSveVerifier.HasRawResult; }
        }

        public IReadOnlyDictionary<string, object> ExtraData
        {
            get { return mSveVerifier.ExtraData; }
        }

        public ILogger Logger
        {
            get { return mSveVerifier.Logger; }
        }

#endregion

#region Public Methods

        public void SetFeedback(bool isBreakAttempt, bool isRecording, bool isBackgroundNoise, string comments)
        {
            mSveVerifier.SetFeedback(isBreakAttempt, isRecording, isBackgroundNoise, comments);
        }

        public void SetMetaData(string name, bool value)
        {
            mSveVerifier.SetMetaData(name, value);
        }

        public void SetMetaData(string name, int value)
        {
            mSveVerifier.SetMetaData(name, value);
        }

        public void SetMetaData(string name, double value)
        {
            mSveVerifier.SetMetaData(name, value);
        }

        public void SetMetaData(string name, string value)
        {
            mSveVerifier.SetMetaData(name, value);
        }

        public bool Append(string filename)
        {
            return mSveVerifier.Append(filename);
        }

        public bool Append(string filename, SpeechContexts contexts)
        {
            return mSveVerifier.Append(filename, contexts);
        }

        public bool Append(WaveStream stream)
        {
            return mSveVerifier.Append(stream);
        }

        public bool Append(WaveStream stream, SpeechContexts contexts)
        {
            return mSveVerifier.Append(stream, contexts);
        }

#endregion

#region Public Synchronous Methods

        public Profile PrefetchProfile()
        {
            return mSveVerifier.PrefetchProfile();
        }

        public bool Start()
        {
            return mSveVerifier.Start();
        }

        public Result Post()
        {
            return mSveVerifier.Post();
        }

        public Result Post(string language_code, string liveness_text)
        {
            return mSveVerifier.Post(language_code, liveness_text);
        }

        public Result Post(string filename)
        {
            return mSveVerifier.Post(filename);
        }

        public Result Post(string filename, SpeechContexts contexts)
        {
            return mSveVerifier.Post(filename, contexts);
        }

        public Result Post(string filename, string language_code, string liveness_text)
        {
            return mSveVerifier.Post(filename, language_code, liveness_text);
        }

        public Result Post(WaveStream stream)
        {
            return mSveVerifier.Post(stream);
        }

        public Result Post(WaveStream stream, SpeechContexts contexts)
        {
            return mSveVerifier.Post(stream, contexts);
        }

        public Result Post(WaveStream stream, string language_code, string liveness_text)
        {
            return mSveVerifier.Post(stream, language_code, liveness_text);
        }

        public Result End()
        {
            return mSveVerifier.End();
        }

        public bool Cancel(string reason)
        {
            return mSveVerifier.Cancel(reason);
        }

#endregion

#region Public Asynchronous Callback Methods
#if !NO_DYNAMIC_THREADING

        public bool PrefetchProfile(ProfileCallback profileCallback)
        {
            return mSveVerifier.PrefetchProfile(profileCallback);
        }

        public bool Start(StartCallback callback)
        {
            return mSveVerifier.Start(callback);
        }

        public bool Post(PostCallback callback)
        {
            return mSveVerifier.Post(callback);
        }

        public bool Post(string language_code, string liveness_text, PostCallback callback)
        {
            return mSveVerifier.Post(language_code, liveness_text, callback);
        }

        public bool Post(string filename, PostCallback callback)
        {
            return mSveVerifier.Post(filename, callback);
        }

        public bool Post(string filename, SpeechContexts contexts, PostCallback callback)
        {
            return mSveVerifier.Post(filename, contexts, callback);
        }

        public bool Post(string filename, string language_code, string liveness_text, PostCallback callback)
        {
            return mSveVerifier.Post(filename, language_code, liveness_text, callback);
        }

        public bool Post(WaveStream stream, PostCallback callback)
        {
            return mSveVerifier.Post(stream, callback);
        }

        public bool Post(WaveStream stream, SpeechContexts contexts, PostCallback callback)
        {
            return mSveVerifier.Post(stream, contexts, callback);
        }

        public bool Post(WaveStream stream, string language_code, string liveness_text, PostCallback callback)
        {
            return mSveVerifier.Post(stream, language_code, liveness_text, callback);
        }

        public bool End(EndCallback callback)
        {
            return mSveVerifier.End(callback);
        }

        public bool Cancel(string reason, CancelCallback callback)
        {
            return mSveVerifier.Cancel(reason, callback);
        }

#endif
#endregion
        
#region Public Asynchronous Task Methods

        public Task<Profile> PrefetchProfileAsync()
        {
            return mSveVerifier.PrefetchProfileAsync();
        }

        public Task<bool> StartAsync()
        {
            return mSveVerifier.StartAsync();
        }

        public Task<Result> PostAsync()
        {
            return mSveVerifier.PostAsync();
        }

        public Task<Result> PostAsync(string language_code, string liveness_text)
        {
            return mSveVerifier.PostAsync(language_code, liveness_text);
        }

        public Task<Result> PostAsync(string filename)
        {
            return mSveVerifier.PostAsync(filename);
        }

        public Task<Result> PostAsync(string filename, SpeechContexts contexts)
        {
            return mSveVerifier.PostAsync(filename, contexts);
        }

        public Task<Result> PostAsync(string filename, string language_code, string liveness_text)
        {
            return mSveVerifier.PostAsync(filename, language_code, liveness_text);
        }

        public Task<Result> PostAsync(WaveStream stream)
        {
            return mSveVerifier.PostAsync(stream);
        }

        public Task<Result> PostAsync(WaveStream stream, SpeechContexts contexts)
        {
            return mSveVerifier.PostAsync(stream, contexts);
        }

        public Task<Result> PostAsync(WaveStream stream, string language_code, string liveness_text)
        {
            return mSveVerifier.PostAsync(stream, language_code, liveness_text);
        }

        public Task<Result> EndAsync()
        {
            return mSveVerifier.EndAsync();
        }

        public Task<bool> CancelAsync(string reason)
        {
            return mSveVerifier.CancelAsync(reason);
        }

#endregion

#region Public Utility Methods

        public static string GetResultDescrption(Result result)
        {
            switch (result)
            {
                case Result.Pass: return "Pass";
                case Result.PassIsAlive: return "PassIsAlive";
                case Result.PassNotAlive: return "PassNotAlive";
                case Result.Ambiguous: return "Ambiguous";
                case Result.AmbiguousIsAlive: return "AmbiguousIsAlive";
                case Result.AmbiguousNotAlive: return "AmbiguousNotAlive";
                case Result.Fail: return "Fail";
                case Result.FailIsAlive: return "FailIsAlive";
                case Result.FailNotAlive: return "FailNotAlive";
                case Result.NeedMore: return "NeedMore";
                case Result.NeedMoreIsAlive: return "NeedMoreIsAlive";
                case Result.NeedMoreNotAlive: return "NeedMoreNotAlive";
                case Result.LimitReached: return "LimitReached";
                case Result.Unauthorized: return "Unauthorized";
                case Result.NotFound: return "NotFound";
                case Result.BadEnrollment: return "BadEnrollment";
                case Result.Timeout: return "Timeout";
                case Result.Invalid: return "Invalid";
                case Result.Error: return "Error";
                case Result.Unknown: return "Unknown";
            }
            return "Unknown";
        }

        public static ProfileType GetProfileType(int profileType)
        {
            switch (profileType)
            {
                case 2: return ProfileType.Single;
                case 3: return ProfileType.SingleLivness;
                case 4:
                case 5: return ProfileType.Batch;
                case 6:
                case 7: return ProfileType.Multi;
                case 8:
                case 9: return ProfileType.Group;
                case 10:
                case 11: return ProfileType.DropOne;
                case 12:
                case 13: return ProfileType.Recognition;
                case 14:
                case 15: return ProfileType.Monitor;

            }
            return ProfileType.Single;
        }

        public static int GetProfileIndex(ProfileType profileType)
        {
            switch (profileType)
            {
                case ProfileType.Single: return 2;
                case ProfileType.SingleLivness: return 3;
                case ProfileType.Batch: return 4;
                case ProfileType.Multi: return 6;
                case ProfileType.Group: return 8;
                case ProfileType.DropOne: return 10;
                case ProfileType.Recognition: return 12;
                case ProfileType.Monitor: return 14;
            }
            return 2;
        }

#endregion
    }
}
