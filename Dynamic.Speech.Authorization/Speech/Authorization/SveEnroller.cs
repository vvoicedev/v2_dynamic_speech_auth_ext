﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Dynamic.Loggers;
using Dynamic.Speech.Authorization.Enrollers;

using NAudio.Wave;

namespace Dynamic.Speech.Authorization
{
    public class SveEnroller : IDisposable
    {
#region Public Enum's, Classes, Interfaces and Delegates

        public enum Result
        {
            [Description("Success")]
            Success,

            [Description("Need More Speech")]
            NeedMore,

            [Description("Already Enrolled")]
            AlreadyEnrolled,

            [Description("Token Exists")]
            TokenExists,

            [Description("Token Required")]
            TokenRequired,

            [Description("Limit Reached")]
            LimitReached,

            [Description("Unauthorized")]
            Unauthorized,

            [Description("Timeout")]
            Timeout,

            [Description("Invalid")]
            Invalid,

            [Description("Error")]
            Error,

            [Description("Unknown")]
            Unknown
        }

        public enum Gender
        {
            [Description("Unknown")]
            Unknown = -1,

            [Description("Male")]
            Male = 0,

            [Description("Female")]
            Female = 1
        }

        public class Profile
        {
            public WaveFormatEncoding Codec { get; internal set; }

            public double MinimumSecondsOfSpeech { get; internal set; }
        }

        public class InstanceResult
        {
            internal InstanceResult()
            {
                Extra = new Dictionary<string, object>();
            }

            public Result Result { get; internal set; }
            public double SpeechExtracted { get; internal set; }
            public double SpeechTrained { get; internal set; }
            public uint ErrorCode { get; internal set; }
            public Dictionary<string, object> Extra { get; private set; }
        }

        public delegate void ProfileCallback(Profile profile);

        public delegate void StartCallback(bool started);

        public delegate void PostCallback(Result result);

        public delegate void EndCallback(Result result);

        public delegate void CancelCallback();

#endregion

#region Private

        private ISveEnroller mSveEnroller;

#endregion

#region Life & Death

        public SveEnroller()
            : this(SpeechApi.DefaultConfiguration)
        {
        }

        public SveEnroller(SpeechApi.Configuration configuration)
        {
            if (configuration.ServerTransport != SpeechApi.SpeechTransport.Rest)
            {
                throw new NotSupportedException("ServerTransport not supported");
            }
            else if (configuration.ServerVersion == SpeechApi.SpeechVersion.Version_1)
            {
                mSveEnroller = new SveEnrollerRest_1(configuration);
            }
            else
            {
                throw new NotSupportedException("ServerVersion not supported");
            }
        }

        public void Dispose()
        {
            if(mSveEnroller != null)
            {
                mSveEnroller.Dispose();
                mSveEnroller = null;
            }
        }

        ~SveEnroller()
        {
            Dispose();
        }

#endregion

#region Public Properties

        public string InteractionId
        {
            get { return mSveEnroller.InteractionId; }
            set { mSveEnroller.InteractionId = value; }
        }

        public string InteractionTag
        {
            get { return mSveEnroller.InteractionTag; }
            set { mSveEnroller.InteractionTag = value; }
        }

        public Gender SubPopulation
        {
            get { return mSveEnroller.SubPopulation; }
            set { mSveEnroller.SubPopulation = value; }
        }

        public string ClientId
        {
            get { return mSveEnroller.ClientId; }
            set { mSveEnroller.ClientId = value; }
        }

        public string AuthToken
        {
            get { return mSveEnroller.AuthToken; }
            set { mSveEnroller.AuthToken = value; }
        }

#endregion

#region Public Getters

        public string Server
        {
            get { return mSveEnroller.Server; }
        }

        public string SessionId
        {
            get { return mSveEnroller.SessionId; }
        }

        public string InteractionSource
        {
            get { return mSveEnroller.InteractionSource; }
        }

        public string InteractionAgent
        {
            get { return mSveEnroller.InteractionAgent; }
        }

        public bool IsSessionOpen
        {
            get { return mSveEnroller.IsSessionOpen; }
        }

        public bool IsSessionClosing
        {
            get { return mSveEnroller.IsSessionClosing; }
        }

        public Dictionary<uint, Profile> Profiles
        {
            get { return mSveEnroller.Profiles; }
        }

        public WaveFormatEncoding Codec
        {
            get { return mSveEnroller.Codec; }
        }

        public double SpeechRequired
        {
            get { return mSveEnroller.SpeechRequired; }
        }

        public double SpeechExtracted
        {
            get { return mSveEnroller.SpeechExtracted; }
        }

        public int SpeechProgress
        {
            get { return mSveEnroller.SpeechProgress; }
        }

        public double SpeechTrained
        {
            get { return mSveEnroller.SpeechTrained; }
        }

        public Result EnrollResult
        {
            get { return mSveEnroller.EnrollResult; }
        }

        public Dictionary<string, InstanceResult> EnrollResults
        {
            get { return mSveEnroller.EnrollResults; }
        }

        public uint TotalProcessCalls
        {
            get { return mSveEnroller.TotalProcessCalls; }
        }

        public long TotalAudioBytes
        {
            get { return mSveEnroller.TotalAudioBytes; }
        }

        public bool HasEnoughSpeech
        {
            get { return mSveEnroller.HasEnoughSpeech; }
        }

        public bool IsTrained
        {
            get { return mSveEnroller.IsTrained; }
        }

        public IReadOnlyDictionary<string, object> ExtraData
        {
            get { return mSveEnroller.ExtraData; }
        }

        public ILogger Logger
        {
            get { return mSveEnroller.Logger; }
        }

#endregion

#region Public Methods

        public void SetMetaData(string name, bool value)
        {
            mSveEnroller.SetMetaData(name, value);
        }

        public void SetMetaData(string name, int value)
        {
            mSveEnroller.SetMetaData(name, value);
        }

        public void SetMetaData(string name, double value)
        {
            mSveEnroller.SetMetaData(name, value);
        }

        public void SetMetaData(string name, string value)
        {
            mSveEnroller.SetMetaData(name, value);
        }

        public bool Append(string filename)
        {
            return mSveEnroller.Append(filename);
        }

        public bool Append(WaveStream stream)
        {
            return mSveEnroller.Append(stream);
        }

#endregion

#region Public Synchronous Methods

        public Profile PrefetchProfile()
        {
            return mSveEnroller.PrefetchProfile();
        }

        public bool Start()
        {
            return mSveEnroller.Start();
        }

        public Result Post()
        {
            return mSveEnroller.Post();
        }

        public Result Post(string filename)
        {
            return mSveEnroller.Post(filename);
        }

        public Result Post(WaveStream stream)
        {
            return mSveEnroller.Post(stream);
        }

        public Result End()
        {
            return mSveEnroller.End();
        }

        public bool Cancel(string reason)
        {
            return mSveEnroller.Cancel(reason);
        }

#endregion

#region Public Asynchronous Methods
#if !NO_DYNAMIC_THREADING

        public bool PrefetchProfile(ProfileCallback profileCallback)
        {
            return mSveEnroller.PrefetchProfile(profileCallback);
        }

        public bool Start(StartCallback callback)
        {
            return mSveEnroller.Start(callback);
        }

        public bool Post(PostCallback callback)
        {
            return mSveEnroller.Post(callback);
        }

        public bool Post(string filename, PostCallback callback)
        {
            return mSveEnroller.Post(filename, callback);
        }

        public bool Post(WaveStream stream, PostCallback callback)
        {
            return mSveEnroller.Post(stream, callback);
        }

        public bool End(EndCallback callback)
        {
            return mSveEnroller.End(callback);
        }

        public bool Cancel(string reason, CancelCallback callback)
        {
            return mSveEnroller.Cancel(reason, callback);
        }

#endif
#endregion
        
#region Public Asynchronous Task Methods

        public Task<Profile> PrefetchProfileAsync()
        {
            return mSveEnroller.PrefetchProfileAsync();
        }

        public Task<bool> StartAsync()
        {
            return mSveEnroller.StartAsync();
        }

        public Task<Result> PostAsync()
        {
            return mSveEnroller.PostAsync();
        }

        public Task<Result> PostAsync(string filename)
        {
            return mSveEnroller.PostAsync(filename);
        }

        public Task<Result> PostAsync(WaveStream stream)
        {
            return mSveEnroller.PostAsync(stream);
        }

        public Task<Result> EndAsync()
        {
            return mSveEnroller.EndAsync();
        }

        public Task<bool> CancelAsync(string reason)
        {
            return mSveEnroller.CancelAsync(reason);
        }

#endregion

#region Public Utility Methods

        public static string GetResultDescrption(Result result)
        {
            switch (result)
            {
                case Result.Unknown: return "Unknown";
                case Result.Invalid: return "Invalid";
                case Result.Error: return "Error";
                case Result.Timeout: return "Timeout";
                case Result.AlreadyEnrolled: return "AlreadyEnrolled";
                case Result.NeedMore: return "NeedMore";
                case Result.Success: return "Success";
                case Result.LimitReached: return "LimitReached";
                case Result.Unauthorized: return "Unauthorized";
            }
            return "Unknown";
        }

        public static Gender GetGender(string gender)
        {
            switch (gender.ToLower())
            {
                case "u":
                case "unknown":
                case "m":
                case "male": return Gender.Male;
                case "f":
                case "female": return Gender.Female;
            }
            return Gender.Unknown;
        }

        public static string GetGender(Gender gender)
        {
            switch (gender)
            {
                case Gender.Male: return "m";
                case Gender.Female: return "f";
            }
            return "m";
        }

#endregion
    }
}
