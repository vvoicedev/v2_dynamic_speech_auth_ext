﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

using Dynamic.Loggers;
using Dynamic.Speech.Authorization.Context;

using NAudio.Wave;

namespace Dynamic.Speech.Authorization.Verifiers
{
    internal interface ISveVerifier : IDisposable
    {
#region Public Properties

        string InteractionId { get; set; }

        string InteractionTag { get; set; }

        string ClientId { get; set; }

        string GroupIds { get; set; }

        string AuthToken { get; set; }

#endregion

#region Public Getters

        SpeechApi.Configuration Configuration { get; }

        CookieContainer CookieContainer { get; }

        string Server { get; }

        string SessionId { get; }

        string InteractionSource { get; }

        string InteractionAgent { get; }

        Dictionary<string, string> MetaData { get; }

        Dictionary<string, object> ExtraData { get; }

        bool IsSessionOpen { get; }

        bool IsSessionClosing { get; }

        bool IsOverridable { get; }

        bool IsAuthorized { get; }

        Dictionary<uint, SveVerifier.Profile> Profiles { get; }

        WaveFormatEncoding Codec { get; }

        string VerifyClientId { get; }

        double VerifyScore { get; }

        double SpeechRequired { get; }

        double SpeechExtracted { get; }

        int SpeechProgress { get; }

        bool HasEnoughSpeech { get; }

        SveVerifier.Result VerifyRawResult { get; }

        SveVerifier.Result VerifyResult { get; }

        Dictionary<string, SveVerifier.InstanceResult> VerifyResults { get; }

        uint TotalProcessCalls { get; }

        long TotalAudioBytes { get; }

        bool IsLivenessRequired { get; }
        
        bool IsVerified { get; }

        SveVerifier.AliveResult LivenessResult { get; }

        bool IsAlive { get; }

        bool HasResult { get; }

        bool HasRawResult { get; }

        ILogger Logger { get; }

#endregion

#region Public Methods

        void SetFeedback(bool isBreakAttempt, bool isRecording, bool isBackgroundNoise, string comments);

        void SetMetaData(string name, bool value);

        void SetMetaData(string name, int value);

        void SetMetaData(string name, double value);

        void SetMetaData(string name, string value);

        bool Append(string filename);

        bool Append(string filename, SpeechContexts contexts);

        bool Append(WaveStream stream);

        bool Append(WaveStream stream, SpeechContexts contexts);

#endregion

#region Public Synchronous Methods

        SveVerifier.Profile PrefetchProfile();

        bool Start();

        SveVerifier.Result Post();

        SveVerifier.Result Post(string language_code, string liveness_text);

        SveVerifier.Result Post(string filename);

        SveVerifier.Result Post(string filename, SpeechContexts contexts);

        SveVerifier.Result Post(string filename, string language_code, string liveness_text);

        SveVerifier.Result Post(WaveStream stream);

        SveVerifier.Result Post(WaveStream stream, SpeechContexts contexts);

        SveVerifier.Result Post(WaveStream stream, string language_code, string liveness_text);

        SveVerifier.Result End();

        bool Cancel(string reason);

#endregion

#region Public Asynchronous Callback Methods
#if !NO_DYNAMIC_THREADING

        bool PrefetchProfile(SveVerifier.ProfileCallback profileCallback);

        bool Start(SveVerifier.StartCallback callback);

        bool Post(SveVerifier.PostCallback callback);

        bool Post(string language_code, string liveness_text, SveVerifier.PostCallback callback);

        bool Post(string filename, SveVerifier.PostCallback callback);

        bool Post(string filename, SpeechContexts contexts, SveVerifier.PostCallback callback);

        bool Post(string filename, string language_code, string liveness_text, SveVerifier.PostCallback callback);

        bool Post(WaveStream stream, SveVerifier.PostCallback callback);

        bool Post(WaveStream stream, SpeechContexts contexts, SveVerifier.PostCallback callback);

        bool Post(WaveStream stream, string language_code, string liveness_text, SveVerifier.PostCallback callback);

        bool End(SveVerifier.EndCallback callback);

        bool Cancel(string reason, SveVerifier.CancelCallback callback);

#endif
#endregion

#region Public Asynchronous Methods

        Task<SveVerifier.Profile> PrefetchProfileAsync();

        Task<bool> StartAsync();

        Task<SveVerifier.Result> PostAsync();

        Task<SveVerifier.Result> PostAsync(string language_code, string liveness_text);

        Task<SveVerifier.Result> PostAsync(string filename);

        Task<SveVerifier.Result> PostAsync(string filename, SpeechContexts contexts);

        Task<SveVerifier.Result> PostAsync(string filename, string language_code, string liveness_text);

        Task<SveVerifier.Result> PostAsync(WaveStream strea);

        Task<SveVerifier.Result> PostAsync(WaveStream stream, SpeechContexts contexts);

        Task<SveVerifier.Result> PostAsync(WaveStream stream, string language_code, string liveness_text);

        Task<SveVerifier.Result> EndAsync();

        Task<bool> CancelAsync(string reason);
        
#endregion
    }
}
