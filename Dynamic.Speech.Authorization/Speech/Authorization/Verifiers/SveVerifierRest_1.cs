﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dynamic.Extensions;
using Dynamic.Json;
using Dynamic.Web;

namespace Dynamic.Speech.Authorization.Verifiers
{
    internal class SveVerifierRest_1 : SveVerifierRestBase
    {
        private const string URI_PATH_PROFILE = "/1/sve/Verification/Profile";
        private const string URI_PATH_START = "/1/sve/Verification/{0}";
        private const string URI_PATH_PROCESS = "/1/sve/Verification";
        private const string URI_PATH_LIVENESS = "/1/sve/Verification/Liveness/{0}/{1}";
        private const string URI_PATH_END = "/1/sve/Verification";
        private const string URI_PATH_CANCEL = "/1/sve/Cancel/{0}";

#region Private Variables

        private uint mLastProfileIndex;

#endregion

#region Life & Death

        internal SveVerifierRest_1(SpeechApi.Configuration configuration)
            : base(configuration)
        {
        }

#endregion

#region Public Synchronous Methods

        public override SveVerifier.Profile PrefetchProfile()
        {
            ResetVerifier();

            Logger.WriteDebugLog("SveVerifierRest_1.PreFetchProfile(): URI: " + URI_PATH_PROFILE);

            IHttpResponse response = GetWebAgent().Get(URI_PATH_PROFILE, null, CookieContainer);

            if (response == null)
            {
                Logger.WriteErrorLog("SveVerifierRest_1.PrefetchProfile(): Timeout occurred");
                VerifyRawResult = SveVerifier.Result.Timeout;
                return null;
            }
            else if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                VerifyRawResult = HandleError(response, URI_PATH_PROFILE);
                return null;
            }
            else if (!HandleResponse(response, URI_PATH_PROFILE))
            {
                return null;
            }

            return Profiles[mLastProfileIndex];
        }

        public override bool Start()
        {
            ResetVerifier();

            string composedClientId = GetComposedClientId();

            if (composedClientId == string.Empty)
            {
                Logger.WriteErrorLog("SveVerifierRest_1.Start(): Missing ClientId and/or GroupIds.");
                VerifyRawResult = SveVerifier.Result.Invalid;
                return false;
            }

            string queryPath = string.Format(URI_PATH_START, composedClientId);

            HttpHeaders httpHeaders = new HttpHeaders();
            if (!string.IsNullOrEmpty(InteractionId))
            {
                Logger.WriteDebugLog("{0}: {1}", SpeechApi.INTERACTION_ID, InteractionId);
                httpHeaders[SpeechApi.INTERACTION_ID] = InteractionId;
            }

            if (!string.IsNullOrEmpty(InteractionTag))
            {
                Logger.WriteDebugLog("{0}: {1}", SpeechApi.INTERACTION_TAG, InteractionTag);
                httpHeaders[SpeechApi.INTERACTION_TAG] = InteractionTag;
            }

            if (MetaData.Count > 0)
            {
                foreach (var item in MetaData)
                {
                    httpHeaders.Add(item.Key, item.Value);
                }
                MetaData.Clear();
            }

            var response = GetWebAgent().Post(queryPath, httpHeaders, CookieContainer);

            if (response == null)
            {
                Logger.WriteErrorLog("SveVerifierRest_1.Start(): Timeout occurred");
                VerifyRawResult = SveVerifier.Result.Timeout;
                return false;
            }
            else if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                VerifyRawResult = HandleError(response, URI_PATH_START);
                return false;
            }
            else if (!HandleResponse(response, URI_PATH_START))
            {
                return false;
            }

            SessionId = response.Headers[SpeechApi.SESSION_ID];
            GetWebAgent().Headers[SpeechApi.SESSION_ID] = SessionId;
            IsSessionOpen = true;

            Logger.WriteDebugLog("SveVerifierRest_1.Start(): Session Open - Id: {0}", SessionId);

            return true;
        }

        public override SveVerifier.Result Post()
        {
            if (IsSessionOpen && !IsSessionClosing)
            {
                VerifyResults.Clear();

                IHttpResponse response;
                if (MetaData.Count > 0)
                {
                    HttpHeaders httpHeaders = new HttpHeaders(MetaData.Count);
                    foreach (var item in MetaData)
                    {
                        httpHeaders.Add(item.Key, item.Value);
                    }
                    MetaData.Clear();
                    response = GetWebAgent().Post(URI_PATH_PROCESS, httpHeaders, ContentParameters, HttpContentEncoding.MultipartForm, CookieContainer);
                }
                else
                {
                    response = GetWebAgent().Post(URI_PATH_PROCESS, ContentParameters, HttpContentEncoding.MultipartForm, CookieContainer);
                }

                ContentParameters.Clear();
                TotalProcessCalls++;

                if (response == null)
                {
                    Logger.WriteErrorLog("SveVerifierRest_1.Start(): Timeout occurred");
                    return (VerifyRawResult = SveVerifier.Result.Timeout);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    HandleResponse(response, URI_PATH_PROCESS);
                    Logger.WriteDebugLog("SveVerifierRest_1.Post(): Score: " + VerifyScore + ", " + VerifyResult);
                    return VerifyResult;
                }
                else
                {
                    return (VerifyRawResult = HandleError(response, URI_PATH_PROCESS));
                }
            }
            return (VerifyRawResult = SveVerifier.Result.Invalid);
        }

        public override SveVerifier.Result Post(string language_code, string liveness_text)
        {
            if(string.IsNullOrEmpty(language_code))
            {
                Logger.WriteErrorLog("SveVerifierRest_1.Post(liveness): Missing Language Code");
                return SveVerifier.Result.Invalid;
            }

            if (string.IsNullOrEmpty(liveness_text))
            {
                Logger.WriteErrorLog("SveVerifierRest_1.Post(liveness): Missing Liveness Text");
                return SveVerifier.Result.Invalid;
            }

            if (IsSessionOpen && !IsSessionClosing)
            {
                VerifyResults.Clear();

                string queryPath = string.Format(URI_PATH_LIVENESS, language_code, liveness_text);

                IHttpResponse response;
                if (MetaData.Count > 0)
                {
                    HttpHeaders httpHeaders = new HttpHeaders(MetaData.Count);
                    foreach (var item in MetaData)
                    {
                        httpHeaders.Add(item.Key, item.Value);
                    }
                    MetaData.Clear();
                    response = GetWebAgent().Post(queryPath, httpHeaders, ContentParameters, HttpContentEncoding.MultipartForm, CookieContainer);
                }
                else
                {
                    response = GetWebAgent().Post(queryPath, ContentParameters, HttpContentEncoding.MultipartForm, CookieContainer);
                }

                ContentParameters.Clear();
                TotalProcessCalls++;

                if (response == null)
                {
                    Logger.WriteErrorLog("SveVerifierRest_1.Start(liveness): Timeout occurred");
                    return (VerifyRawResult = SveVerifier.Result.Timeout);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    HandleResponse(response, queryPath);
                    Logger.WriteDebugLog("SveVerifierRest_1.Post(liveness): Score: " + VerifyScore + ", " + VerifyResult);
                    return VerifyResult;
                }
                else
                {
                    return (VerifyRawResult = HandleError(response, queryPath));
                }
            }
            return (VerifyRawResult = SveVerifier.Result.Invalid);
        }

        public override SveVerifier.Result End()
        {
            if (IsSessionOpen && !IsSessionClosing)
            {
                IsSessionClosing = true;
                VerifyResults.Clear();
                ContentParameters.Clear();

                HttpHeaders httpHeaders = new HttpHeaders(MetaData.Count + 1);

                if (MetaData.Count > 0)
                {
                    foreach (var item in MetaData)
                    {
                        Logger.WriteDebugLog("{0}: {1}", item.Key, item.Value);
                        httpHeaders.Add(item.Key, item.Value);
                    }
                    MetaData.Clear();
                }

                if (IsOverridable && !string.IsNullOrEmpty(AuthToken))
                {
                    Logger.WriteDebugLog("{0}: {1}", SpeechApi.OVERRIDE_TOKEN, AuthToken);
                    httpHeaders[SpeechApi.OVERRIDE_TOKEN] = AuthToken;
                }

                IHttpResponse response = GetWebAgent().Delete(URI_PATH_END, httpHeaders, CookieContainer);

                if (response == null)
                {
                    Logger.WriteErrorLog("SveVerifierRest_1.End(): Timeout occurred");
                    return (VerifyRawResult = SveVerifier.Result.Timeout);
                }
                else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    return (VerifyRawResult = HandleError(response, URI_PATH_END));
                }
                else
                {
                    IsSessionOpen = false;
                    IsSessionClosing = false;
                    SessionId = "";
                    GetWebAgent().Headers.Remove(SpeechApi.SESSION_ID);
                    if (HandleResponse(response, URI_PATH_END))
                    {
                        Logger.WriteDebugLog("SveVerifierRest_1.End(): Score: " + VerifyScore + ", " + VerifyResult);
                        return VerifyResult;
                    }
                }
            }
            return (VerifyRawResult = SveVerifier.Result.Invalid);
        }

        public override bool Cancel(string reason)
        {
            if (IsSessionOpen && !IsSessionClosing)
            {
                IsSessionClosing = true;
                VerifyResults.Clear();
                ContentParameters.Clear();

                string queryPath = string.Format(URI_PATH_CANCEL, reason.Replace(' ', '-').Substring(0, System.Math.Min(reason.Length, 64)));

                IHttpResponse response = GetWebAgent().Delete(queryPath, null, CookieContainer);

                if (response == null)
                {
                    Logger.WriteErrorLog("SveVerifierRest_1.Cancel(): Timeout occurred");
                    VerifyRawResult = SveVerifier.Result.Timeout;
                    return false;
                }
                else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    VerifyRawResult = HandleError(response, queryPath);
                    return false;
                }
            }
            IsSessionOpen = false;
            IsSessionClosing = false;
            SessionId = "";
            GetWebAgent().Headers.Remove(SpeechApi.SESSION_ID);
            return true;
        }

#endregion

#region Public Asynchronous Callback Methods
#if !NO_DYNAMIC_THREADING

        public override bool PrefetchProfile(SveVerifier.ProfileCallback profileCallback)
        {
            if (profileCallback == null)
            {
                Logger.WriteErrorLog("SveVerifierRest_1.PrefetchProfile(profileCallback): Callback was null");
                return false;
            }

            ResetVerifier();

            Logger.WriteDebugLog("SveVerifierRest_1.PreFetchProfile(): URI: " + URI_PATH_PROFILE);

            return GetWebAgent().GetCallback(URI_PATH_PROFILE, null, CookieContainer, delegate (IHttpResponse response) {

                if (response == null)
                {
                    Logger.WriteErrorLog("SveVerifierRest_1.PrefetchProfile(profileCallback): Timeout occurred");
                    VerifyRawResult = SveVerifier.Result.Timeout;
                    profileCallback(null);
                    return;
                }
                else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    VerifyRawResult = HandleError(response, URI_PATH_PROFILE);
                    profileCallback(null);
                    return;
                }

                if (!HandleResponse(response, URI_PATH_PROFILE))
                {
                    profileCallback(null);
                    return;
                }

                profileCallback(Profiles[mLastProfileIndex]);

            }).IsAlive;
        }

        public override bool Start(SveVerifier.StartCallback startCallback)
        {
            if (startCallback == null)
            {
                Logger.WriteErrorLog("SveVerifierRest_1.Start(startCallback): Callback was null");
                return false;
            }

            ResetVerifier();

            string composedClientId = GetComposedClientId();

            if (composedClientId == string.Empty)
            {
                Logger.WriteErrorLog("SveVerifierRest_1.Start(): Missing ClientId and/or GroupIds.");
                VerifyRawResult = SveVerifier.Result.Invalid;
                return false;
            }

            string queryPath = string.Format(URI_PATH_START, composedClientId);

            HttpHeaders httpHeaders = new HttpHeaders();
            if (!string.IsNullOrEmpty(InteractionId))
            {
                Logger.WriteDebugLog("{0}: {1}", SpeechApi.INTERACTION_ID, InteractionId);
                httpHeaders[SpeechApi.INTERACTION_ID] = InteractionId;
            }

            if (!string.IsNullOrEmpty(InteractionTag))
            {
                Logger.WriteDebugLog("{0}: {1}", SpeechApi.INTERACTION_TAG, InteractionTag);
                httpHeaders[SpeechApi.INTERACTION_TAG] = InteractionTag;
            }

            if (MetaData.Count > 0)
            {
                foreach (var item in MetaData)
                {
                    httpHeaders.Add(item.Key, item.Value);
                }
                MetaData.Clear();
            }

            return GetWebAgent().PostCallback(queryPath, httpHeaders, CookieContainer, delegate (IHttpResponse response) {

                if (response == null)
                {
                    Logger.WriteErrorLog("SveVerifierRest_1.Start(startCallback): Timeout occurred");
                    VerifyRawResult = SveVerifier.Result.Timeout;
                    startCallback(false);
                    return;
                }
                else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    VerifyRawResult = HandleError(response, queryPath);
                    startCallback(false);
                    return;
                }
                else if (!HandleResponse(response, queryPath))
                {
                    startCallback(false);
                    return;
                }

                SessionId = response.Headers[SpeechApi.SESSION_ID];
                GetWebAgent().Headers[SpeechApi.SESSION_ID] = SessionId;
                IsSessionOpen = true;

                Logger.WriteDebugLog("SveVerifierRest_1.Start(): Session Open - Id: {0}", SessionId);
                startCallback(true);

            }).IsAlive;
        }

        public override bool Post(SveVerifier.PostCallback postCallback)
        {
            if (postCallback == null)
            {
                Logger.WriteErrorLog("SveVerifierRest_1.Post(postCallback): Callback was null");
                return false;
            }

            if (IsSessionOpen && !IsSessionClosing)
            {
                VerifyResults.Clear();

                HttpHeaders httpHeaders = new HttpHeaders(MetaData.Count);
                if (MetaData.Count > 0)
                {
                    foreach (var item in MetaData)
                    {
                        httpHeaders.Add(item.Key, item.Value);
                    }
                    MetaData.Clear();
                }

                return GetWebAgent().PostCallback(URI_PATH_PROCESS, httpHeaders, ContentParameters, HttpContentEncoding.MultipartForm, CookieContainer,
                    delegate (IHttpResponse response) {

                        ContentParameters.Clear();
                        TotalProcessCalls++;

                        if (response == null)
                        {
                            Logger.WriteErrorLog("SveVerifierRest_1.Start(): Timeout occurred");
                            postCallback(VerifyRawResult = SveVerifier.Result.Timeout);
                        }
                        else if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            HandleResponse(response, URI_PATH_PROCESS);
                            Logger.WriteDebugLog("SveVerifierRest_1.Post(): Score: " + VerifyScore + ", " + VerifyResult);
                            postCallback(VerifyResult);
                        }
                        else
                        {
                            postCallback(VerifyRawResult = HandleError(response, URI_PATH_PROCESS));
                        }
                    }
                ).IsAlive;
            }
            return false;
        }

        public override bool Post(string language_code, string liveness_text, SveVerifier.PostCallback postCallback)
        {
            if (string.IsNullOrEmpty(language_code))
            {
                Logger.WriteErrorLog("SveVerifierRest_1.Post(liveness, postCallback): Missing Language Code");
                return false;
            }

            if (string.IsNullOrEmpty(liveness_text))
            {
                Logger.WriteErrorLog("SveVerifierRest_1.Post(liveness, postCallback): Missing Liveness Text");
                return false;
            }

            if (postCallback == null)
            {
                Logger.WriteErrorLog("SveVerifierRest_1.Post(liveness, postCallback): Callback was null");
                return false;
            }

            if (IsSessionOpen && !IsSessionClosing)
            {
                VerifyResults.Clear();

                HttpHeaders httpHeaders = new HttpHeaders(MetaData.Count);
                if (MetaData.Count > 0)
                {
                    foreach (var item in MetaData)
                    {
                        httpHeaders.Add(item.Key, item.Value);
                    }
                    MetaData.Clear();
                }

                string queryPath = string.Format(URI_PATH_LIVENESS, language_code, liveness_text);

                return GetWebAgent().PostCallback(queryPath, httpHeaders, ContentParameters, HttpContentEncoding.MultipartForm, CookieContainer,
                    delegate (IHttpResponse response) {

                        ContentParameters.Clear();
                        TotalProcessCalls++;

                        if (response == null)
                        {
                            Logger.WriteErrorLog("SveVerifierRest_1.Start(liveness, postCallback): Timeout occurred");
                            postCallback(VerifyRawResult = SveVerifier.Result.Timeout);
                        }
                        else if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            HandleResponse(response, queryPath);
                            Logger.WriteDebugLog("SveVerifierRest_1.Post(liveness, postCallback): Score: " + VerifyScore + ", " + VerifyResult);
                            postCallback(VerifyResult);
                        }
                        else
                        {
                            postCallback(VerifyRawResult = HandleError(response, queryPath));
                        }
                    }
                ).IsAlive;
            }
            return false;
        }

        public override bool End(SveVerifier.EndCallback endCallback)
        {
            if (endCallback == null)
            {
                Logger.WriteErrorLog("SveVerifierRest_1.End(endCallback): Callback was null");
                return false;
            }

            if (IsSessionOpen && !IsSessionClosing)
            {
                IsSessionClosing = true;
                VerifyResults.Clear();
                ContentParameters.Clear();

                HttpHeaders httpHeaders = new HttpHeaders(MetaData.Count + 1);

                if (MetaData.Count > 0)
                {
                    foreach (var item in MetaData)
                    {
                        Logger.WriteDebugLog("{0}: {1}", item.Key, item.Value);
                        httpHeaders.Add(item.Key, item.Value);
                    }
                    MetaData.Clear();
                }
                
                if (IsOverridable && !string.IsNullOrEmpty(AuthToken))
                {
                    Logger.WriteDebugLog("{0}: {1}", SpeechApi.OVERRIDE_TOKEN, AuthToken);
                    httpHeaders[SpeechApi.OVERRIDE_TOKEN] = AuthToken;
                }

                return GetWebAgent().DeleteCallback(URI_PATH_END, httpHeaders, CookieContainer, delegate (IHttpResponse response)
                {
                    if (response == null)
                    {
                        Logger.WriteErrorLog("SveVerifierRest_1.End(): Timeout occurred");
                        endCallback(VerifyRawResult = SveVerifier.Result.Timeout);
                    }
                    else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        endCallback(VerifyRawResult = HandleError(response, URI_PATH_END));
                    }
                    else
                    {
                        IsSessionOpen = false;
                        IsSessionClosing = false;
                        SessionId = "";
                        GetWebAgent().Headers.Remove(SpeechApi.SESSION_ID);
                        if (HandleResponse(response, URI_PATH_END))
                        {
                            Logger.WriteDebugLog("SveVerifierRest_1.End(): Score: " + VerifyScore + ", " + VerifyResult);
                            endCallback(VerifyResult);
                        }
                        else
                        {
                            endCallback(VerifyRawResult = SveVerifier.Result.Error);
                        }
                    }

                }).IsAlive;
            }
            return false;
        }

        public override bool Cancel(string reason, SveVerifier.CancelCallback cancelCallback)
        {
            if (cancelCallback == null)
            {
                Logger.WriteErrorLog("SveVerifierRest_1.Cancel(cancelCallback): Callback was null");
                return false;
            }

            if (IsSessionOpen && !IsSessionClosing)
            {
                IsSessionClosing = true;
                VerifyResults.Clear();
                ContentParameters.Clear();

                string queryPath = string.Format(URI_PATH_CANCEL, reason.Replace(' ', '-').Substring(0, System.Math.Min(reason.Length, 64)));

                return GetWebAgent().DeleteCallback(queryPath, null, CookieContainer, delegate (IHttpResponse response)
                {
                    if (response == null)
                    {
                        Logger.WriteErrorLog("SveVerifierRest_1.Cancel(cancelCallback): Timeout occurred");
                        VerifyRawResult = SveVerifier.Result.Timeout;
                        cancelCallback();
                        return;
                    }
                    else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        VerifyRawResult = HandleError(response, queryPath);
                        cancelCallback();
                        return;
                    }
                    IsSessionOpen = false;
                    IsSessionClosing = false;
                    SessionId = "";
                    GetWebAgent().Headers.Remove(SpeechApi.SESSION_ID);
                    cancelCallback();
                }).IsAlive;
            }
            return true;
        }

#endif
#endregion

#region Public Asynchronous Methods
        
        public override async Task<SveVerifier.Profile> PrefetchProfileAsync()
        {
            ResetVerifier();

            Logger.WriteDebugLog("SveVerifierRest_1.PreFetchProfile(): URI: " + URI_PATH_PROFILE);

            IHttpResponse response = await GetWebAgent().GetTaskAsync(URI_PATH_PROFILE, null, CookieContainer);

            if (response == null)
            {
                Logger.WriteErrorLog("SveVerifierRest_1.PrefetchProfile(): Timeout occurred");
                VerifyRawResult = SveVerifier.Result.Timeout;
                return null;
            }
            else if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                VerifyRawResult = HandleError(response, URI_PATH_PROFILE);
                return null;
            }
            else if (!HandleResponse(response, URI_PATH_PROFILE))
            {
                return null;
            }

            return Profiles[mLastProfileIndex];
        }

        public override async Task<bool> StartAsync()
        {
            ResetVerifier();

            string composedClientId = GetComposedClientId();

            if (composedClientId == string.Empty)
            {
                Logger.WriteErrorLog("SveVerifierRest_1.Start(): Missing ClientId and/or GroupIds.");
                VerifyRawResult = SveVerifier.Result.Invalid;
                return false;
            }

            string queryPath = string.Format(URI_PATH_START, composedClientId);

            HttpHeaders httpHeaders = new HttpHeaders();
            if (!string.IsNullOrEmpty(InteractionId))
            {
                Logger.WriteDebugLog("{0}: {1}", SpeechApi.INTERACTION_ID, InteractionId);
                httpHeaders[SpeechApi.INTERACTION_ID] = InteractionId;
            }

            if (!string.IsNullOrEmpty(InteractionTag))
            {
                Logger.WriteDebugLog("{0}: {1}", SpeechApi.INTERACTION_TAG, InteractionTag);
                httpHeaders[SpeechApi.INTERACTION_TAG] = InteractionTag;
            }

            if (MetaData.Count > 0)
            {
                foreach (var item in MetaData)
                {
                    httpHeaders.Add(item.Key, item.Value);
                }
                MetaData.Clear();
            }

            var response = await GetWebAgent().PostTaskAsync(queryPath, httpHeaders, CookieContainer);

            if (response == null)
            {
                Logger.WriteErrorLog("SveVerifierRest_1.Start(): Timeout occurred");
                VerifyRawResult = SveVerifier.Result.Timeout;
                return false;
            }
            else if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                VerifyRawResult = HandleError(response, URI_PATH_START);
                return false;
            }
            else if (!HandleResponse(response, URI_PATH_START))
            {
                return false;
            }

            SessionId = response.Headers[SpeechApi.SESSION_ID];
            GetWebAgent().Headers[SpeechApi.SESSION_ID] = SessionId;
            IsSessionOpen = true;

            Logger.WriteDebugLog("SveVerifierRest_1.Start(): Session Open - Id: {0}", SessionId);

            return true;
        }

        public override async Task<SveVerifier.Result> PostAsync()
        {
            if (IsSessionOpen && !IsSessionClosing)
            {
                VerifyResults.Clear();

                IHttpResponse response;
                if (MetaData.Count > 0)
                {
                    HttpHeaders httpHeaders = new HttpHeaders(MetaData.Count);
                    foreach (var item in MetaData)
                    {
                        httpHeaders.Add(item.Key, item.Value);
                    }
                    MetaData.Clear();
                    response = await GetWebAgent().PostTaskAsync(URI_PATH_PROCESS, httpHeaders, ContentParameters, HttpContentEncoding.MultipartForm, CookieContainer);
                }
                else
                {
                    response = await GetWebAgent().PostTaskAsync(URI_PATH_PROCESS, ContentParameters, HttpContentEncoding.MultipartForm, CookieContainer);
                }

                ContentParameters.Clear();
                TotalProcessCalls++;

                if (response == null)
                {
                    Logger.WriteErrorLog("SveVerifierRest_1.Start(): Timeout occurred");
                    return (VerifyRawResult = SveVerifier.Result.Timeout);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    HandleResponse(response, URI_PATH_PROCESS);
                    Logger.WriteDebugLog("SveVerifierRest_1.Post(): Score: " + VerifyScore + ", " + VerifyResult);
                    return VerifyResult;
                }
                else
                {
                    return (VerifyRawResult = HandleError(response, URI_PATH_PROCESS));
                }
            }
            return (VerifyRawResult = SveVerifier.Result.Invalid);
        }

        public override async Task<SveVerifier.Result> PostAsync(string language_code, string liveness_text)
        {
            if (string.IsNullOrEmpty(language_code))
            {
                Logger.WriteErrorLog("SveVerifierRest_1.Post(liveness): Missing Language Code");
                return SveVerifier.Result.Invalid;
            }

            if (string.IsNullOrEmpty(liveness_text))
            {
                Logger.WriteErrorLog("SveVerifierRest_1.Post(liveness): Missing Liveness Text");
                return SveVerifier.Result.Invalid;
            }

            if (IsSessionOpen && !IsSessionClosing)
            {
                VerifyResults.Clear();

                string queryPath = string.Format(URI_PATH_LIVENESS, language_code, liveness_text);

                IHttpResponse response;
                if (MetaData.Count > 0)
                {
                    HttpHeaders httpHeaders = new HttpHeaders(MetaData.Count);
                    foreach (var item in MetaData)
                    {
                        httpHeaders.Add(item.Key, item.Value);
                    }
                    MetaData.Clear();
                    response = await GetWebAgent().PostTaskAsync(queryPath, httpHeaders, ContentParameters, HttpContentEncoding.MultipartForm, CookieContainer);
                }
                else
                {
                    response = await GetWebAgent().PostTaskAsync(queryPath, ContentParameters, HttpContentEncoding.MultipartForm, CookieContainer);
                }

                ContentParameters.Clear();
                TotalProcessCalls++;

                if (response == null)
                {
                    Logger.WriteErrorLog("SveVerifierRest_1.Start(liveness): Timeout occurred");
                    return (VerifyRawResult = SveVerifier.Result.Timeout);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    HandleResponse(response, queryPath);
                    Logger.WriteDebugLog("SveVerifierRest_1.Post(liveness): Score: " + VerifyScore + ", " + VerifyResult);
                    return VerifyResult;
                }
                else
                {
                    return (VerifyRawResult = HandleError(response, queryPath));
                }
            }
            return (VerifyRawResult = SveVerifier.Result.Invalid);
        }

        public override async Task<SveVerifier.Result> EndAsync()
        {
            if (IsSessionOpen && !IsSessionClosing)
            {
                IsSessionClosing = true;
                VerifyResults.Clear();
                ContentParameters.Clear();

                HttpHeaders httpHeaders = new HttpHeaders(MetaData.Count + 1);

                if (MetaData.Count > 0)
                {
                    foreach (var item in MetaData)
                    {
                        Logger.WriteDebugLog("{0}: {1}", item.Key, item.Value);
                        httpHeaders.Add(item.Key, item.Value);
                    }
                    MetaData.Clear();
                }

                if (IsOverridable && !string.IsNullOrEmpty(AuthToken))
                {
                    Logger.WriteDebugLog("{0}: {1}", SpeechApi.OVERRIDE_TOKEN, AuthToken);
                    httpHeaders[SpeechApi.OVERRIDE_TOKEN] = AuthToken;
                }

                IHttpResponse response = await GetWebAgent().DeleteTaskAsync(URI_PATH_END, httpHeaders, CookieContainer);

                if (response == null)
                {
                    Logger.WriteErrorLog("SveVerifierRest_1.End(): Timeout occurred");
                    return (VerifyRawResult = SveVerifier.Result.Timeout);
                }
                else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    return (VerifyRawResult = HandleError(response, URI_PATH_END));
                }
                else
                {
                    IsSessionOpen = false;
                    IsSessionClosing = false;
                    SessionId = "";
                    GetWebAgent().Headers.Remove(SpeechApi.SESSION_ID);
                    if (HandleResponse(response, URI_PATH_END))
                    {
                        Logger.WriteDebugLog("SveVerifierRest_1.End(): Score: " + VerifyScore + ", " + VerifyResult);
                        return VerifyResult;
                    }
                }
            }
            return (VerifyRawResult = SveVerifier.Result.Invalid);
        }

        public override async Task<bool> CancelAsync(string reason)
        {
            if (IsSessionOpen && !IsSessionClosing)
            {
                IsSessionClosing = true;
                VerifyResults.Clear();
                ContentParameters.Clear();

                string queryPath = string.Format(URI_PATH_CANCEL, reason.Replace(' ', '-').Substring(0, System.Math.Min(reason.Length, 64)));

                IHttpResponse response = await GetWebAgent().DeleteTaskAsync(queryPath, null, CookieContainer);

                if (response == null)
                {
                    Logger.WriteErrorLog("SveVerifierRest_1.Cancel(): Timeout occurred");
                    VerifyRawResult = SveVerifier.Result.Timeout;
                    return false;
                }
                else if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    VerifyRawResult = HandleError(response, queryPath);
                    return false;
                }
            }
            IsSessionOpen = false;
            IsSessionClosing = false;
            SessionId = "";
            GetWebAgent().Headers.Remove(SpeechApi.SESSION_ID);
            return true;
        }

#endregion

#region Private

        private string GetComposedClientId()
        {
            bool alreadyGrouped = false;
            string composedClientId = "";
            if (!string.IsNullOrEmpty(ClientId))
            {
                composedClientId += ClientId;
                if (ClientId.Contains("$"))
                {
                    alreadyGrouped = true;
                    ClientId = ClientId.Substring(0, ClientId.IndexOf('$'));
                }
                Logger.WriteDebugLog("SveVerifierRest_1.GetComposedClientId(): Client-Id: " + ClientId);
            }

            if (!alreadyGrouped && !string.IsNullOrEmpty(GroupIds))
            {
                if (composedClientId != string.Empty)
                {
                    composedClientId += "$";
                }
                composedClientId += GroupIds;
                Logger.WriteDebugLog("SveVerifierRest_1.GetComposedClientId(): Group-Ids: " + GroupIds);
            }

            return composedClientId;
        }

        private bool HandleResponse(IHttpResponse response, string query_path)
        {
            if (response.ContentType.Equals("application/json") && response.ContentData is IDictionary)
            {
                Dictionary<string, object> data = (Dictionary<string, object>)response.ContentData;
                if (data.ContainsKey("error"))
                {
                    Logger.WriteErrorLog("SveVerifierRest_1.HandleResult(): Error detected. Uri: {0}, Data:\r\n{1}", query_path, JsonDoctor.ToJson(data["error"]));
                    data.Remove("error");
                }

                foreach (var kv in data)
                {
                    if (kv.Key.Contains("profile.verify") && kv.Value is IDictionary)
                    {
                        mLastProfileIndex = AddProfile((Dictionary<string, object>)kv.Value);
                    }
                    else if (kv.Key.Contains("result.verify") && kv.Value is IDictionary)
                    {
                        Dictionary<string, object> result_data = (Dictionary<string, object>)kv.Value;
                        foreach (var result in result_data)
                        {
                            if (result.Value is IDictionary)
                            {
                                AddResult(result.Key, (Dictionary<string, object>)result.Value);
                            }
                        }

                        if (result_data.Count > 0 && !string.IsNullOrEmpty(ClientId) && VerifyResults.ContainsKey(ClientId))
                        {
                            SveVerifier.InstanceResult result = VerifyResults[ClientId];
                            VerifyClientId = ClientId;
                            VerifyScore = result.Score;
                            SpeechExtracted = result.SpeechExtracted;
                            VerifyRawResult = result.Result;
                        }
                        else if (Profiles[1].Type == SveVerifier.ProfileType.Multi && result_data.Count == 1)
                        {
                            var obj = (DictionaryEntry)VerifyResults.GetFirst();
                            SveVerifier.InstanceResult result = (SveVerifier.InstanceResult)obj.Value;
                            VerifyClientId = Convert.ToString(obj.Key);
                            VerifyScore = result.Score;
                            SpeechExtracted = result.SpeechExtracted;
                            VerifyRawResult = result.Result;
                        }
                    }
                    else if (kv.Key.Contains("result.liveness") && kv.Value is IDictionary)
                    {
                        Dictionary<string, object> result_data = (Dictionary<string, object>)kv.Value;
                        if (result_data.ContainsKey("is_alive"))
                        {
                            LivenessResult = Convert.ToBoolean(result_data["is_alive"]) ? 
                                SveVerifier.AliveResult.Alive : 
                                SveVerifier.AliveResult.NotAlive;
                        }
                        else
                        {
                            LivenessResult = SveVerifier.AliveResult.NotAlive;
                        }
                    }
                }
                return true;
            }
            else
            {
                Logger.WriteErrorLog("SveVerifierRest_1.HandleResponse(): Expected Content Type: 'application/json'. Actual Content Type: '{0}'", response.ContentType);
                return false;
            }
        }

        private SveVerifier.Result HandleError(IHttpResponse response, string query_path)
        {
            if (response.StatusCode != System.Net.HttpStatusCode.RequestTimeout)
            {
                SveVerifier.Result result = SveVerifier.Result.Unknown;
                if (response.ContentType.Equals("application/json") && response.ContentData is IDictionary)
                {
                    Dictionary<string, object> data = (Dictionary<string, object>)response.ContentData;
                    if (data.ContainsKey("error") && data.ContainsKey("description"))
                    {
                        int code = Convert.ToInt32(data["error"]);
                        string descr = Convert.ToString(data["description"]);
                        result = GetErrorResult(code);
                        Logger.WriteErrorLog("SveVerifierRest_1.HandleError(): Error detected.\r\n\tUri: {0}\r\n\tResult: {1}\r\n\tCode: {2}\r\n\tDescription: {3}", query_path, result, code, descr);
                        return result;
                    }
                }
                Logger.WriteErrorLog("SveVerifierRest_1.HandleError(): Unknown Error detected");
                return result;
            }
            return SveVerifier.Result.Timeout;
        }

#endregion

    }
}
