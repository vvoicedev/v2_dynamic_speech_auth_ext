﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Dynamic.Loggers;
using Dynamic.Speech.Authorization.Context;
using Dynamic.Speech.Authorization.Utilities;
using Dynamic.Web;
using Dynamic.Web.ContentParts;

using NAudio.Wave;

namespace Dynamic.Speech.Authorization.Verifiers
{
    internal class SveVerifierRestBase : ISveVerifier
    {
#region Private Variables

        private IHttpClient mHttpClient;

#endregion

#region Life & Death

        protected SveVerifierRestBase(SpeechApi.Configuration configuration)
        {
            Configuration = configuration;
            Profiles = new Dictionary<uint, SveVerifier.Profile>();
            VerifyResults = new Dictionary<string, SveVerifier.InstanceResult>(StringComparer.OrdinalIgnoreCase);
            MetaData = new Dictionary<string, string>();
            ExtraData = new Dictionary<string, object>();
            ContentParameters = new HttpContentParameters();
            ResetVerifier();
        }

        public void Dispose()
        {
            if (mHttpClient != null)
            {
                mHttpClient.Dispose();
                mHttpClient = null;
            }
        }

#endregion

#region Public Properties

        public string InteractionId { get; set; }

        public string InteractionTag { get; set; }

        public string ClientId { get; set; }

        public string GroupIds { get; set; }

        public string AuthToken { get; set; }

#endregion

#region Public Getters

        public SpeechApi.Configuration Configuration { get; }

        public HttpContentParameters ContentParameters { get; private set; }

        public CookieContainer CookieContainer { get; private set; }

        public string Server { get { return Configuration.Server; } }

        public string SessionId { get; protected set; }

        public string InteractionSource { get { return Configuration.ApplicationSource; } }

        public string InteractionAgent { get { return Configuration.ApplicationUserAgent; } }

        public Dictionary<string, string> MetaData { get; private set; }

        public Dictionary<string, object> ExtraData { get; private set; }

        public bool IsSessionOpen { get; protected set; }

        public bool IsSessionClosing { get; protected set; }

        public bool IsOverridable { get; private set; }

        public bool IsAuthorized { get; private set; }

        public Dictionary<uint, SveVerifier.Profile> Profiles { get; protected set; }

        public WaveFormatEncoding Codec { get; protected set; }

        public string VerifyClientId { get; protected set; }

        public double VerifyScore { get; protected set; }

        public double SpeechRequired { get; protected set; }

        public double SpeechExtracted { get; protected set; }

        public int SpeechProgress
        {
            get
            {
                if (SpeechRequired <= 0) return 0;
                int progress = (int)System.Math.Round((SpeechExtracted / SpeechRequired) * 100);
                return (progress < 0) ? 0 : ((progress >= 100) ? (HasEnoughSpeech ? 100 : 99) : progress);
            }
        }

        public bool HasEnoughSpeech { get { return SpeechExtracted >= SpeechRequired; } }

        public SveVerifier.Result VerifyRawResult { get; protected set; }

        public SveVerifier.Result VerifyResult
        {
            get
            {
                if(IsLivenessRequired)
                {
                    switch(VerifyRawResult)
                    {
                        case SveVerifier.Result.Pass: return IsAlive ? SveVerifier.Result.PassIsAlive : SveVerifier.Result.PassNotAlive;
                        case SveVerifier.Result.Ambiguous: return IsAlive ? SveVerifier.Result.AmbiguousIsAlive : SveVerifier.Result.AmbiguousNotAlive;
                        case SveVerifier.Result.Fail: return IsAlive ? SveVerifier.Result.FailIsAlive : SveVerifier.Result.FailNotAlive;
                        case SveVerifier.Result.NeedMore:
                            switch (LivenessResult)
                            {
                                case SveVerifier.AliveResult.Untested: return SveVerifier.Result.NeedMore;
                                case SveVerifier.AliveResult.Alive: return SveVerifier.Result.NeedMoreIsAlive;
                                case SveVerifier.AliveResult.NotAlive: return SveVerifier.Result.NeedMoreNotAlive;
                            }
                            break;
                    }
                }
                return VerifyRawResult;
            }
        }

        public Dictionary<string, SveVerifier.InstanceResult> VerifyResults { get; protected set; }

        public uint TotalProcessCalls { get; protected set; }

        public long TotalAudioBytes { get; protected set; }

        public bool IsLivenessRequired { get; protected set; }

        public bool IsVerified
        {
            get
            {
                int minCallsToPass = Configuration.MinimumVerifyCallsToPass;
                return (minCallsToPass == 0 || TotalProcessCalls >= minCallsToPass) &&
                    (VerifyRawResult == SveVerifier.Result.Pass && !IsLivenessRequired || IsAlive);
            }
        }

        public SveVerifier.AliveResult LivenessResult { get; protected set; }

        public bool IsAlive { get { return LivenessResult == SveVerifier.AliveResult.Alive; } }

        public bool HasResult
        {
            get
            {
                int minCallsToPass = Configuration.MinimumVerifyCallsToPass;
                return (minCallsToPass == 0 || TotalProcessCalls >= minCallsToPass) &&
                    (VerifyRawResult == SveVerifier.Result.Pass ||
                        VerifyRawResult == SveVerifier.Result.Ambiguous ||
                        VerifyRawResult == SveVerifier.Result.Fail);
            }
        }

        public bool HasRawResult
        {
            get
            {
                return VerifyRawResult == SveVerifier.Result.Pass ||
                    VerifyRawResult == SveVerifier.Result.Ambiguous ||
                    VerifyRawResult == SveVerifier.Result.Fail;
            }
        }

        public ILogger Logger { get { return Configuration.Logger; } }

#endregion

#region Public Methods

        public void SetFeedback(bool isBreakAttempt, bool isRecording, bool isBackgroundNoise, string comments)
        {
            comments = string.IsNullOrEmpty(comments) ? "N/A" : comments.Substring(0, System.Math.Min(comments.Length, 256));

            // Populate the MetaData using custom interface (old way of storing feedback)
            MetaData[SpeechApi.FEEDBACK_BREAK_ATTEMPT] = isBreakAttempt ? "1" : "0";
            MetaData[SpeechApi.FEEDBACK_RECORDING] = isRecording ? "1" : "0";
            MetaData[SpeechApi.FEEDBACK_BACKGROUND_NOISE] = isRecording ? "1" : "0";
            MetaData[SpeechApi.FEEDBACK_COMMENTS] = comments;

            // Populate the MetaData using the MetaData interface (new way of adding meta information)
            SetMetaData(SpeechApi.FEEDBACK_BREAK_ATTEMPT, isBreakAttempt);
            SetMetaData(SpeechApi.FEEDBACK_RECORDING, isRecording);
            SetMetaData(SpeechApi.FEEDBACK_BACKGROUND_NOISE, isBackgroundNoise);
            SetMetaData(SpeechApi.FEEDBACK_COMMENTS, comments);
        }

        public void SetMetaData(string name, bool value)
        {
            SetMetaData(name, value ? "1" : "0");
        }

        public void SetMetaData(string name, int value)
        {
            SetMetaData(name, Convert.ToString(value));
        }

        public void SetMetaData(string name, double value)
        {
            SetMetaData(name, Convert.ToString(value));
        }

        public void SetMetaData(string name, string value)
        {
            MetaData["Meta-" + name] = value;
        }

        public bool Append(string filename)
        {
            if (IsSessionOpen)
            {
                return Append(new WaveFileReader(filename));
            }
            return false;
        }

        public bool Append(string filename, SpeechContexts contexts)
        {
            if (IsSessionOpen)
            {
                var waveStream = new WaveFileReader(filename);
                if(Append(waveStream))
                {
                    return AppendContexts(contexts);
                }
            }
            return false;
        }

        public virtual bool Append(WaveStream stream)
        {
            if (IsSessionOpen)
            {
                if (stream.CanSeek)
                {
                    stream.Seek(0, SeekOrigin.Begin);
                }

                Logger.WriteDebugLog("SveVerifier.Append(): In-Length: " + stream.Length);
                Logger.WriteDebugLog("SveVerifier.Append(): In-WaveFormat: " + stream.WaveFormat);

                AudioWavContentPart waveParameter;
                if (Codec == WaveFormatEncoding.ALaw)
                {
                    waveParameter = new AudioWavContentPart(stream, WaveFormat.CreateALawFormat(8000, 1));
                }
                else if (Codec == WaveFormatEncoding.Pcm)
                {
                    waveParameter = new AudioWavContentPart(stream, new WaveFormat(8000, 16, 1));
                }
                else
                {
                    return false;
                }

                Logger.WriteDebugLog("SveVerifier.Append(): Append-Length: " + waveParameter.Stream.Length);
                Logger.WriteDebugLog("SveVerifier.Append(): Append-WaveFormat: " + waveParameter.Stream.WaveFormat);

                waveParameter.FileName = BuildAudioName();
                waveParameter.AsRaw = true;

                ContentParameters.Add("data", waveParameter);

                TotalAudioBytes += waveParameter.Stream.Length;

                return true;
            }
            return false;
        }

        public bool Append(WaveStream stream, SpeechContexts contexts)
        {
            if (IsSessionOpen)
            {
                if (Append(stream))
                {
                    return AppendContexts(contexts);
                }
            }
            return false;
        }

        public virtual bool AppendContexts(SpeechContexts contexts)
        {
            if (IsSessionOpen)
            {
                if (contexts != null)
                {
                    IsLivenessRequired = true;
                    foreach (var ctx in contexts)
                    {
                        ContentParameters.Add("speech", new JsonContentPart(ctx.GetSpeechContext(Configuration.ServerVersion)));
                    }
                }
                return true;
            }
            return false;
        }

#endregion

#region Public Synchronous Methods

        public virtual SveVerifier.Profile PrefetchProfile()
        {
            return new SveVerifier.Profile();
        }

        public virtual bool Start()
        {
            return false;
        }

        public virtual SveVerifier.Result Post()
        {
            return SveVerifier.Result.Invalid;
        }

        public virtual SveVerifier.Result Post(string language_code, string liveness_text)
        {
            throw new NotSupportedException("Post(liveness)");
        }

        public virtual SveVerifier.Result Post(string filename)
        {
            if (IsSessionOpen)
            {
                if (Append(filename))
                {
                    return Post();
                }
            }
            return SveVerifier.Result.Invalid;
        }

        public virtual SveVerifier.Result Post(string filename, SpeechContexts contexts)
        {
            if (IsSessionOpen)
            {
                if (Append(filename, contexts))
                {
                    return Post();
                }
            }
            return SveVerifier.Result.Invalid;
        }
        
        public SveVerifier.Result Post(string filename, string language_code, string liveness_text)
        {
            if (IsSessionOpen)
            {
                if (Append(filename))
                {
                    IsLivenessRequired = true;
                    return Post(language_code, liveness_text);
                }
            }
            return SveVerifier.Result.Invalid;
        }

        public virtual SveVerifier.Result Post(WaveStream stream)
        {
            if (IsSessionOpen)
            {
                if (Append(stream))
                {
                    return Post();
                }
            }
            return SveVerifier.Result.Invalid;
        }

        public virtual SveVerifier.Result Post(WaveStream stream, SpeechContexts contexts)
        {
            if (IsSessionOpen)
            {
                if (Append(stream, contexts))
                {
                    return Post();
                }
            }
            return SveVerifier.Result.Invalid;
        }

        public virtual SveVerifier.Result Post(WaveStream stream, string language_code, string liveness_text)
        {
            if (IsSessionOpen)
            {
                if (Append(stream))
                {
                    IsLivenessRequired = true;
                    return Post(language_code, liveness_text);
                }
            }
            return SveVerifier.Result.Invalid;
        }

        public virtual SveVerifier.Result End()
        {
            return SveVerifier.Result.Invalid;
        }

        public virtual bool Cancel(string reason)
        {
            return false;
        }

#endregion

#region Public Asynchronous (Dynamic.Threading.Tasks) Methods
#if !NO_DYNAMIC_THREADING

        public virtual bool PrefetchProfile(SveVerifier.ProfileCallback profileCallback)
        {
            return false;
        }

        public virtual bool Start(SveVerifier.StartCallback callback)
        {
            callback(false);
            return false;
        }

        public virtual bool Post(SveVerifier.PostCallback callback)
        {
            callback(SveVerifier.Result.Error);
            return false;
        }

        public virtual bool Post(string language_code, string liveness_text, SveVerifier.PostCallback callback)
        {
            throw new NotSupportedException("Post(liveness, callback)");
        }

        public virtual bool Post(string filename, SveVerifier.PostCallback callback)
        {
            if (IsSessionOpen)
            {
                if (Append(filename))
                {
                    return Post(callback);
                }
            }
            return false;
        }

        public virtual bool Post(string filename, SpeechContexts contexts, SveVerifier.PostCallback callback)
        {
            if (IsSessionOpen)
            {
                if (Append(filename, contexts))
                {
                    return Post(callback);
                }
            }
            return false;
        }

        public virtual bool Post(string filename, string language_code, string liveness_text, SveVerifier.PostCallback callback)
        {
            if (IsSessionOpen)
            {
                if (Append(filename))
                {
                    return Post(language_code, liveness_text, callback);
                }
            }
            return false;
        }

        public virtual bool Post(WaveStream stream, SveVerifier.PostCallback callback)
        {
            if (IsSessionOpen)
            {
                if (Append(stream))
                {
                    return Post(callback);
                }
            }
            return false;
        }

        public virtual bool Post(WaveStream stream, SpeechContexts contexts, SveVerifier.PostCallback callback)
        {
            if (IsSessionOpen)
            {
                if (Append(stream, contexts))
                {
                    return Post(callback);
                }
            }
            return false;
        }

        public virtual bool Post(WaveStream stream, string language_code, string liveness_text, SveVerifier.PostCallback callback)
        {
            if (IsSessionOpen)
            {
                if (Append(stream))
                {
                    return Post(language_code, liveness_text, callback);
                }
            }
            return false;
        }

        public virtual bool End(SveVerifier.EndCallback callback)
        {
            callback(SveVerifier.Result.Error);
            return false;
        }

        public virtual bool Cancel(string reason, SveVerifier.CancelCallback callback)
        {
            callback();
            return false;
        }

#endif
#endregion

#region Public Asynchronous (System.Threading.Tasks) Methods

        public virtual Task<SveVerifier.Profile> PrefetchProfileAsync()
        {
            return Task.FromResult(new SveVerifier.Profile());
        }

        public virtual Task<bool> StartAsync()
        {
            return Task.FromResult(false);
        }

        public virtual Task<SveVerifier.Result> PostAsync()
        {
            return Task.FromResult(SveVerifier.Result.Invalid);
        }

        public virtual Task<SveVerifier.Result> PostAsync(string language_code, string liveness_text)
        {
            throw new NotSupportedException("Post(liveness)");
        }

        public virtual Task<SveVerifier.Result> PostAsync(string filename)
        {
            if (IsSessionOpen)
            {
                if (Append(filename))
                {
                    return PostAsync();
                }
            }
            return Task.FromResult(SveVerifier.Result.Invalid);
        }

        public virtual Task<SveVerifier.Result> PostAsync(string filename, SpeechContexts contexts)
        {
            if (IsSessionOpen)
            {
                if (Append(filename, contexts))
                {
                    return PostAsync();
                }
            }
            return Task.FromResult(SveVerifier.Result.Invalid);
        }

        public Task<SveVerifier.Result> PostAsync(string filename, string language_code, string liveness_text)
        {
            if (IsSessionOpen)
            {
                if (Append(filename))
                {
                    IsLivenessRequired = true;
                    return PostAsync(language_code, liveness_text);
                }
            }
            return Task.FromResult(SveVerifier.Result.Invalid);
        }

        public virtual Task<SveVerifier.Result> PostAsync(WaveStream stream)
        {
            if (IsSessionOpen)
            {
                if (Append(stream))
                {
                    return PostAsync();
                }
            }
            return Task.FromResult(SveVerifier.Result.Invalid);
        }

        public virtual Task<SveVerifier.Result> PostAsync(WaveStream stream, SpeechContexts contexts)
        {
            if (IsSessionOpen)
            {
                if (Append(stream, contexts))
                {
                    return PostAsync();
                }
            }
            return Task.FromResult(SveVerifier.Result.Invalid);
        }

        public virtual Task<SveVerifier.Result> PostAsync(WaveStream stream, string language_code, string liveness_text)
        {
            if (IsSessionOpen)
            {
                if (Append(stream))
                {
                    IsLivenessRequired = true;
                    return PostAsync(language_code, liveness_text);
                }
            }
            return Task.FromResult(SveVerifier.Result.Invalid);
        }

        public virtual Task<SveVerifier.Result> EndAsync()
        {
            return Task.FromResult(SveVerifier.Result.Invalid);
        }

        public virtual Task<bool> CancelAsync(string reason)
        {
            return Task.FromResult(false);
        }

#endregion

#region Protected Methods

        protected void ResetVerifier()
        {
            SessionId = "";
            Codec = WaveFormatEncoding.Unknown;
            Profiles.Clear();
            VerifyResults.Clear();
            VerifyRawResult = SveVerifier.Result.NeedMore;
            MetaData.Clear();
            ExtraData.Clear();
            ContentParameters.Clear();
            CookieContainer = new CookieContainer();
            TotalAudioBytes = 0;
            TotalProcessCalls = 0;
            SpeechRequired = 0;
            SpeechExtracted = 0;
            VerifyClientId = "";
            VerifyScore = 0;
            IsSessionOpen = false;
            IsSessionClosing = false;
            IsAuthorized = false;
            IsOverridable = false;
            IsLivenessRequired = false;
            LivenessResult = SveVerifier.AliveResult.Untested;

            var webAgent = GetWebAgent();
            webAgent.Headers.Remove(SpeechApi.SESSION_ID);
            webAgent.Headers.Remove(SpeechApi.INTERACTION_ID);
            webAgent.Headers.Remove(SpeechApi.INTERACTION_TAG);
        }

        protected uint AddProfile(Dictionary<string, object> data)
        {
            SveVerifier.Profile profile = new SveVerifier.Profile();

            profile.Type = GetProfileType(Convert.ToUInt32(data["type"]));
            profile.Codec = SveUtility.GetCodec(Convert.ToString(data["codec"]));
            profile.PassThreshold = Convert.ToDouble(data["pass"]);
            profile.FailThreshold = Convert.ToDouble(data["fail"]);

            profile.MinimumSecondsOfSpeech = 0;
            if (data.ContainsKey("min_seconds_of_speech"))
            {
                profile.MinimumSecondsOfSpeech = Convert.ToDouble(data["min_seconds_of_speech"]);
            }
            SpeechRequired = profile.MinimumSecondsOfSpeech;

            uint index = Convert.ToUInt32(data["index"]);
            Profiles.Add(index, profile);

            if (Codec == WaveFormatEncoding.Unknown)
            {
                Codec = profile.Codec;
            }

            Logger.WriteDebugLog("SveVerifier.AddProfile(): Type: {0}", profile.Type);
            Logger.WriteDebugLog("SveVerifier.AddProfile(): Codec: {0}", profile.Codec);
            Logger.WriteDebugLog("SveVerifier.AddProfile(): MinimumSecondsOfSpeech: {0}", profile.MinimumSecondsOfSpeech);
            Logger.WriteDebugLog("SveVerifier.AddProfile(): Pass Threshold: {0}", profile.PassThreshold);
            Logger.WriteDebugLog("SveVerifier.AddProfile(): Fail Threshold: {0}", profile.FailThreshold);
            Logger.WriteDebugLog("SveVerifier.AddProfile(): Index: {0}", index);

            return index;
        }
        
        protected void AddResult(string name, Dictionary<string, object> data)
        {
            SveVerifier.InstanceResult result = new SveVerifier.InstanceResult();

            result.ErrorCode = Convert.ToUInt32(data["error"]);
            result.Score = Convert.ToDouble(data["score"]);
            result.SpeechExtracted = Convert.ToDouble(data["seconds_extracted"]);

            data.Remove("error");
            data.Remove("score");
            data.Remove("seconds_extracted");

            uint index = Convert.ToUInt32(data["index"]);
            SveVerifier.Profile profile = Profiles[index];
            data.Remove("index");

            if (data.ContainsKey("status"))
            {
                char ch = Convert.ToChar(data["status"]);
                switch (ch)
                {
                    case 'P': result.Result = SveVerifier.Result.Pass; break;
                    case 'A': result.Result = SveVerifier.Result.Ambiguous; break;
                    case 'F': result.Result = SveVerifier.Result.Fail; break;
                    case 'M': result.Result = SveVerifier.Result.NeedMore; break;
                    case 'N': result.Result = SveVerifier.Result.NotScored; break;
                    default: result.Result = SveVerifier.Result.Unknown; break;
                }
                data.Remove("status");
            }
            else if (result.Score == 0.0)
            {
                result.Result = SveVerifier.Result.NotScored;
            }
            else if (result.Score >= profile.PassThreshold)
            {
                result.Result = SveVerifier.Result.Pass;
            }
            else if (result.Score <= profile.FailThreshold)
            {
                result.Result = SveVerifier.Result.Fail;
            }
            else
            {
                result.Result = SveVerifier.Result.Ambiguous;
            }

            Logger.WriteDebugLog("SveVerifier.AddResult(): {0} - Index: {1}", name, index);
            Logger.WriteDebugLog("SveVerifier.AddResult(): {0} - Error: {1}", name, result.ErrorCode);
            Logger.WriteDebugLog("SveVerifier.AddResult(): {0} - Score: {1}", name, result.Score);
            Logger.WriteDebugLog("SveVerifier.AddResult(): {0} - SpeechExtracted: {1}", name, result.SpeechExtracted);
            Logger.WriteDebugLog("SveVerifier.AddResult(): {0} - Result: {1}", name, result.Result);

            if (data.ContainsKey("overridable"))
            {
                IsOverridable = Convert.ToBoolean(data["overridable"]);
                Logger.WriteDebugLog("SveVerifier.AddResult(): {0} - IsOverridable: {1}", name, IsOverridable);
            }

            if (data.ContainsKey("authorized"))
            {
                IsAuthorized = Convert.ToBoolean(data["authorized"]);
                Logger.WriteDebugLog("SveVerifier.AddResult(): {0} - IsAuthorized: {1}", name, IsAuthorized);
            }

            foreach (var extra in data)
            {
                result.Extra.Add(extra.Key, extra.Value);
                Logger.WriteDebugLog("SveVerifier.AddResult(): {0} - {1}: {2}", name, extra.Key, extra.Value);
            }

            VerifyResults.Add(name, result);
        }

        protected IHttpClient GetWebAgent()
        {
            if (mHttpClient == null)
            {
                var httpClient = Configuration.ServerTransportObject as IHttpClient;
                if (httpClient != null)
                {
                    mHttpClient = httpClient;
                    if (!mHttpClient.Headers.ContainsKey(SpeechApi.DEVELOPER_KEY))
                    {
                        mHttpClient.Headers.Add(SpeechApi.DEVELOPER_KEY, Configuration.DeveloperKey);
                    }

                    if (!mHttpClient.Headers.ContainsKey(SpeechApi.APPLICATION_KEY))
                    {
                        mHttpClient.Headers.Add(SpeechApi.APPLICATION_KEY, Configuration.ApplicationKey);
                    }

                    if (!mHttpClient.Headers.ContainsKey(SpeechApi.INTERACTION_SOURCE))
                    {
                        mHttpClient.Headers.Add(SpeechApi.INTERACTION_SOURCE, Configuration.ApplicationSource);
                    }

                    if (!mHttpClient.Headers.ContainsKey(SpeechApi.INTERACTION_AGENT))
                    {
                        mHttpClient.Headers.Add(SpeechApi.INTERACTION_AGENT, Configuration.ApplicationUserAgent);
                    }

                    if (!mHttpClient.Headers.ContainsKey(SpeechApi.APP_VERSION_ID))
                    {
                        mHttpClient.Headers.Add(SpeechApi.APP_VERSION_ID, Configuration.ApplicationUserAgent);
                    }
                }
                else
                {
                    mHttpClient = new VerifierAgent(Configuration);
                }
            }
            return mHttpClient;
        }

        protected SveVerifier.Result GetErrorResult(int errorCode)
        {
            errorCode = errorCode >= 0 ? errorCode : -errorCode;
            switch (errorCode)
            {
                case 104: return SveVerifier.Result.LimitReached;
                case 110: return SveVerifier.Result.Unauthorized;
                case 403: return SveVerifier.Result.NeedMore;
                case 410: return SveVerifier.Result.NotFound;
                case 412: return SveVerifier.Result.LimitReached;
                case 420: return SveVerifier.Result.BadEnrollment;
                default: return SveVerifier.Result.Error;
            }
        }

#endregion

#region Private Methods

        private string BuildAudioName()
        {
            StringBuilder builder = new StringBuilder();
            string iid = InteractionId;
            if (!string.IsNullOrEmpty(iid))
            {
                builder.Append(iid).Append("-");
            }
            return builder.Append(ClientId).Append("-")
                    .Append(TotalProcessCalls).Append("-")
                    .Append(ContentParameters.Count).Append(".raw").ToString();
        }

        private SveVerifier.ProfileType GetProfileType(uint profileType)
        {
            switch (profileType)
            {
                case 2: return SveVerifier.ProfileType.Single;
                case 3: return SveVerifier.ProfileType.SingleLivness;
                case 4:
                case 5: return SveVerifier.ProfileType.Batch;
                case 6:
                case 7: return SveVerifier.ProfileType.Multi;
                case 8:
                case 9: return SveVerifier.ProfileType.Group;
                case 10:
                case 11: return SveVerifier.ProfileType.DropOne;
                case 12:
                case 13: return SveVerifier.ProfileType.Recognition;
                case 14:
                case 15: return SveVerifier.ProfileType.Monitor;
            }
            return SveVerifier.ProfileType.Single;
        }

#endregion

#region Private Http Class

        private class VerifierAgent : HttpClient
        {
            public VerifierAgent(SpeechApi.Configuration configuration)
                : base(BuildEndpoint(configuration.Server), "SveVerifier", configuration.Logger)
            {
                Headers = new Dictionary<string, string>();
                Headers.Add(SpeechApi.DEVELOPER_KEY, configuration.DeveloperKey);
                Headers.Add(SpeechApi.APPLICATION_KEY, configuration.ApplicationKey);
                Headers.Add(SpeechApi.APP_VERSION_ID, configuration.ApplicationUserAgent);
                Headers.Add(SpeechApi.INTERACTION_SOURCE, configuration.ApplicationSource);
                Headers.Add(SpeechApi.INTERACTION_AGENT, configuration.ApplicationUserAgent);
            }
        }

#endregion
    }
}
