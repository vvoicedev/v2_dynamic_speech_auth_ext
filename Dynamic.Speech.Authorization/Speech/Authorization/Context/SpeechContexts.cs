﻿using System.Collections.Generic;
using System.Text;

namespace Dynamic.Speech.Authorization.Context
{
    public class SpeechContexts : List<SpeechContext>
    {
        public SpeechContexts()
        {
        }

        internal SpeechContexts(int capacity)
            : base(capacity)
        {
        }

        internal SpeechContexts(List<SpeechContext> list)
            : base(list)
        {
        }

        public void Add(string languageCode, string phrase)
        {
            Add(new SpeechContext(languageCode, new List<string> { phrase }));
        }

        public void Add(string languageCode, List<string> phrases)
        {
            Add(new SpeechContext(languageCode, phrases));
        }

        public void Add(string name, string languageCode, List<string> phrases)
        {
            Add(new SpeechContext(name, languageCode, phrases));
        }

        public void Add(string name, string grammar, string languageCode, List<string> phrases)
        {
            Add(new SpeechContext(name, grammar, languageCode, phrases));
        }

        public void Add(string name, string grammar, string languageCode, List<string> phrases, uint flags)
        {
            Add(new SpeechContext(name, grammar, languageCode, phrases, flags));
        }

        public SpeechContexts Clone()
        {
            SpeechContexts contexts = new SpeechContexts(Count);

            foreach (var context in this)
            {
                contexts.Add(context.Clone());
            }

            return contexts;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach (var elem in this)
            {
                builder.Append(elem.ToString());
            }
            return builder.ToString();
        }
    }
}
