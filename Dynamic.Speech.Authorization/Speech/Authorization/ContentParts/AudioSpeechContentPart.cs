﻿using System;
using System.IO;

using NAudio.Wave;

using Dynamic.Json;
using Dynamic.Web.ContentParts;

using Dynamic.Speech.Authorization.Context;

namespace Dynamic.Speech.Authorization.ContentParts
{
    class AudioSpeechContentPart : HttpContentPartBase
    {
        public WaveStream Stream { get; set; }
        public SpeechContexts Contexts { get; set; }

        private WaveFormat Format { get; set; }
        private IWaveProvider Provider { get; set; }

        public AudioSpeechContentPart()
            : base("audio/wav")
        {
            Stream = null;
            FileName = "";
            Contexts = new SpeechContexts();
        }

        public AudioSpeechContentPart(WaveStream stream)
            : this(stream, "unknown.wav")
        {
        }

        public AudioSpeechContentPart(WaveStream stream, WaveFormat format)
            : this(stream, "unknown.wav")
        {
            if (!Stream.WaveFormat.Equals(format))
            {
                Format = format;
                Provider = new MediaFoundationResampler(Stream, format);
            }
        }

        public AudioSpeechContentPart(WaveStream stream, string filename)
            : this(stream, filename, "audio/wav")
        {
            Contexts = new SpeechContexts();
        }

        public AudioSpeechContentPart(WaveStream stream, string filename, string contentType)
            : base(contentType, "")
        {
            Stream = stream;
            FileName = filename;
            Contexts = new SpeechContexts();
        }

        public AudioSpeechContentPart(string filename)
            : base("audio/wav", "")
        {
            FileName = Path.GetFileName(filename);
            Stream = new WaveFileReader(filename);
            Contexts = new SpeechContexts();
        }

        public AudioSpeechContentPart(string filename, WaveFormat format)
            : base("audio/wav", "")
        {
            FileName = Path.GetFileName(filename);
            Stream = new WaveFileReader(filename);
            if (!Stream.WaveFormat.Equals(format))
            {
                Format = format;
                Provider = new MediaFoundationResampler(Stream, format);
            }
            Contexts = new SpeechContexts();
        }

        protected override void OnDispose()
        {
            Stream.Dispose();
        }

        internal static void Serialize(JsonSerializer.ObjectWriter writer, object x)
        {
            AudioSpeechContentPart contentPart = (AudioSpeechContentPart)x;

            WaveStream waveStream = contentPart.Stream;
            WaveFormat waveFormat = contentPart.Format;
            IWaveProvider waveProvider = contentPart.Provider;

            if (waveFormat == null)
            {
                waveFormat = waveStream.WaveFormat;
            }

            if (waveProvider == null)
            {
                waveProvider = waveStream;
            }

            MemoryStream ms = new MemoryStream();
            long outputLength = 0;
            var buffer = new byte[waveFormat.AverageBytesPerSecond * waveFormat.Channels];
            while (true)
            {
                int bytesRead = waveProvider.Read(buffer, 0, buffer.Length);
                if (bytesRead == 0)
                {
                    // end of source provider
                    break;
                }
                outputLength += bytesRead;
                if (outputLength > int.MaxValue)
                {
                    throw new InvalidOperationException("WAV File cannot be greater than 2GB. Check that sourceProvider is not an endless stream.");
                }
                ms.Write(buffer, 0, bytesRead);
            }

            writer.WritePairFast("content", Convert.ToBase64String(ms.ToArray(), Base64FormattingOptions.None));

            if(contentPart.Contexts.Count > 0)
            {
                writer.WritePair("speech", contentPart.Contexts);
            }
        }
    }
}
