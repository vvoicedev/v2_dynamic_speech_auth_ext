﻿
using System;
using System.IO;
using System.Threading.Tasks;

using Dynamic.Loggers;

namespace Dynamic.Speech.Authorization.Demo
{
    static class Program
    {
        private static ILogger Logger = new ConsoleLogger();

        static void Main(string[] args)
        {
            SpeechApi.Initialize(
                new SpeechApi.Builder()
                    .SetDeveloperKey(Properties.Settings.Default.DeveloperKey)
                    .SetApplicationKey(Properties.Settings.Default.ApplicationKey)
                    .SetApplicationSource("Dynamic.Speech.Authorization.Demo")
                    .SetServerVersion(SpeechApi.SpeechVersion.Version_1)
                    .SetServerTransport(SpeechApi.SpeechTransport.Rest)
                    .SetLogger(Logger)
                    .SetServer(SpeechApi.SpeechServer.Demo)
                    .Build()
                );

            if(!string.IsNullOrEmpty(Properties.Settings.Default.EnrollDirectory) && Directory.Exists(Properties.Settings.Default.EnrollDirectory))
            {
                // {EnrollDirectory}/{ClientId}/{ClientEnrollmentFiles}.wav

                string[] clients = Directory.GetDirectories(Properties.Settings.Default.EnrollDirectory);

                foreach(var clientPath in clients)
                {
                    string clientId = Path.GetFileName(clientPath);
                    string[] enrollmentFiles = Directory.GetFiles(clientPath, "*.wav");
                    if(enrollmentFiles == null || enrollmentFiles.Length == 0)
                    {
                        Logger.WriteErrorLog("No Enrollment Files Found for ClientId Folder: {0}", clientId);
                        continue;
                    }

                    Task.WaitAll(PerformEnroll(clientId, enrollmentFiles));
                }
            }

            if (!string.IsNullOrEmpty(Properties.Settings.Default.VerifyDirectory) && Directory.Exists(Properties.Settings.Default.VerifyDirectory))
            {
                // {VerifyDirectory}/{ClientId}/{SessionInstance}/{ClientVerificationFiles}.wav
                // This one is different, because there might be cases when you want to use the same client id on more
                // than one set of files at a time

                string[] clients = Directory.GetDirectories(Properties.Settings.Default.VerifyDirectory);

                foreach (var clientPath in clients)
                {
                    string clientId = Path.GetFileName(clientPath);
                    string[] sessionInstances = Directory.GetDirectories(clientPath);
                    if (sessionInstances == null || sessionInstances.Length == 0)
                    {
                        Logger.WriteErrorLog("No Verification Session Folders Found for ClientId Folder: {0}", clientId);
                        continue;
                    }

                    foreach (var sessionPath in sessionInstances)
                    {
                        string sessionInstance = Path.GetFileName(sessionPath);
                        string[] verifyFiles = Directory.GetFiles(sessionPath, "*.wav");
                        if (verifyFiles == null || verifyFiles.Length == 0)
                        {
                            Logger.WriteErrorLog("No Verification Files Found for ClientId Folder: {0}", clientId);
                            continue;
                        }

                        Task.WaitAll(PerformVerify(clientId, sessionInstance, verifyFiles));
                    }
                }
            }

            if (!string.IsNullOrEmpty(Properties.Settings.Default.MultiVerifyDirectory) && Directory.Exists(Properties.Settings.Default.MultiVerifyDirectory))
            {
                // {MultiVerifyDirectory}/{Comma,Separated,List,Of,ClientIds}/{SessionInstance}/{ClientVerificationFiles}.wav
                // This one is different, because there might be cases when you want to use the same client id list on more
                // than one set of files at a time

                string[] clients = Directory.GetDirectories(Properties.Settings.Default.MultiVerifyDirectory);

                foreach (var clientPath in clients)
                {
                    string clientIds = Path.GetFileName(clientPath);
                    string[] sessionInstances = Directory.GetDirectories(clientPath);
                    if (sessionInstances == null || sessionInstances.Length == 0)
                    {
                        Logger.WriteErrorLog("No Multi-Verification Session Folders Found for ClientId Folder: {0}", clientIds);
                        continue;
                    }

                    foreach (var sessionPath in sessionInstances)
                    {
                        string sessionInstance = Path.GetFileName(sessionPath);
                        string[] verifyFiles = Directory.GetFiles(sessionPath, "*.wav");
                        if (verifyFiles == null || verifyFiles.Length == 0)
                        {
                            Logger.WriteErrorLog("No Multi-Verification Session Files Found for ClientId Folder: {0}", clientIds);
                            continue;
                        }

                        Task.WaitAll(PerformMultiVerify(clientIds, sessionInstance, verifyFiles));
                    }
                }
            }
        }

        private static async Task PerformEnroll(string clientId, string[] enrollmentFiles)
        {
            var enroller = new SveEnroller();

            enroller.ClientId = clientId;

            try
            {
                if(await enroller.StartAsync())
                {
                    foreach (var file in enrollmentFiles)
                    {
                        await enroller.PostAsync(file);
                        Logger.WriteInfoLog("Speech-Extracted: {0}", enroller.SpeechExtracted);
                    }

                    var result = await enroller.EndAsync();
                    
                    Logger.WriteInfoLog("Has-Enough-Speech: {0}", enroller.HasEnoughSpeech);
                    Logger.WriteInfoLog("Is-Trained: {0}", enroller.IsTrained);
                    Logger.WriteInfoLog("Result: {0}", result);
                    Logger.WriteInfoLog("Speech-Trained: {0}", enroller.SpeechTrained);

                    if (result == SveEnroller.Result.Success)
                    {
                        Logger.WriteInfoLog("ClientId: {0}, Successfully Enrolled", clientId);
                    }
                    else
                    {
                        Logger.WriteErrorLog("Error Found for ClientId: {0} - {1}", clientId, result);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteErrorLog(ex);
                if(enroller.IsSessionOpen)
                {
                    await enroller.CancelAsync("Exception Occurred");
                }
            }
        }

        private static async Task PerformVerify(string clientId, string sessionInstance, string[] verifyFiles)
        {
            var verifier = new SveVerifier();

            verifier.ClientId = clientId;
            verifier.InteractionTag = sessionInstance;

            try
            {
                if (await verifier.StartAsync())
                {
                    foreach (var file in verifyFiles)
                    {
                        await verifier.PostAsync(file);
                        Logger.WriteInfoLog("Speech-Extracted: {0}", verifier.SpeechExtracted);
                        Logger.WriteInfoLog("Verify-Score: {0}", verifier.VerifyScore);
                    }

                    var result = await verifier.EndAsync();

                    Logger.WriteInfoLog("Has-Enough-Speech: {0}", verifier.HasEnoughSpeech);
                    Logger.WriteInfoLog("Has-Result: {0}", verifier.HasResult);
                    Logger.WriteInfoLog("Result: {0}", result);
                    Logger.WriteInfoLog("Is-Authorized: {0}", verifier.IsAuthorized);
                    Logger.WriteInfoLog("Is-Verified: {0}", verifier.IsVerified);
                    Logger.WriteInfoLog("Speech-Extracted: {0}", verifier.SpeechExtracted);
                    Logger.WriteInfoLog("Verify-Score: {0}", verifier.VerifyScore);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteErrorLog(ex);
                if (verifier.IsSessionOpen)
                {
                    await verifier.CancelAsync("Exception Occurred");
                }
            }
        }

        private static async Task PerformMultiVerify(string clientIds, string sessionInstance, string[] verifyFiles)
        {
            var verifier = new SveVerifier();

            verifier.GroupIds = clientIds;
            verifier.InteractionTag = sessionInstance;

            try
            {
                if (await verifier.StartAsync())
                {
                    foreach (var file in verifyFiles)
                    {
                        await verifier.PostAsync(file);
                        Logger.WriteInfoLog("Speech-Extracted: {0}", verifier.SpeechExtracted);
                        Logger.WriteInfoLog("Verify-Score: {0}", verifier.VerifyScore);
                    }

                    var result = await verifier.EndAsync();

                    Logger.WriteInfoLog("Has-Enough-Speech: {0}", verifier.HasEnoughSpeech);
                    Logger.WriteInfoLog("Has-Result: {0}", verifier.HasResult);
                    Logger.WriteInfoLog("Result: {0}", result);
                    Logger.WriteInfoLog("Is-Authorized: {0}", verifier.IsAuthorized);
                    Logger.WriteInfoLog("Is-Verified: {0}", verifier.IsVerified);
                    Logger.WriteInfoLog("Speech-Extracted: {0}", verifier.SpeechExtracted);
                    Logger.WriteInfoLog("Verify-Score: {0}", verifier.VerifyScore);
                    Logger.WriteInfoLog("Highest-Scored-Client-Id: {0}", verifier.VerifyClientId);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteErrorLog(ex);
                if (verifier.IsSessionOpen)
                {
                    await verifier.CancelAsync("Exception Occurred");
                }
            }
        }

    }
}
