﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Dynamic.Extensions;
using Dynamic.Json;
using Dynamic.Loggers;
#if !NO_DYNAMIC_THREADING
using Dynamic.Threading;
#endif
using Dynamic.Web.ContentParsers;

namespace Dynamic.Web
{
    public class HttpRequest : IHttpRequest
    {
        public string Method { get; set; }
        public string Query { get; set; }
        public string UserAgent { get; set; }
        public HttpVersion ProtocolVersion { get; set; }
        public bool KeepAlive { get; set; }
        public HttpHeaders Headers { get; set; }
        public string ContentType { get; set; }
        public int ContentLength { get; set; }
        public CookieContainer CookieContainer { get; set; }
        public string ConnectionGroupName { get; set; }
        public int Timeout { get; set; }
    }

    public class HttpResponse : IHttpResponse
    {
        internal HttpResponse()
        {
            StatusCode = HttpStatusCode.RequestTimeout;
            StatusDescription = "RequestTimeout";
            Headers = new WebHeaderCollection();
            ContentType = null;
            ContentData = null;
        }

        internal HttpResponse(HttpWebResponse response, object data)
        {
            StatusCode = response.StatusCode;
            StatusDescription = response.StatusDescription;
            Headers = new WebHeaderCollection();
            Headers.Add(response.Headers);
            ContentType = response.ContentType;
            ContentData = data;
            response.Close();
        }

        public HttpStatusCode StatusCode { get; private set; }
        public string StatusDescription { get; private set; }
        public WebHeaderCollection Headers { get; private set; }
        public HttpContentEncoding EncodingType { get; private set; }
        public string ContentType { get; private set; }
        public object ContentData { get; private set; }
    }

#if !NO_DYNAMIC_THREADING
    public class HttpAsyncResponse : IHttpAsyncResponse
    {
        private HttpClient mHttpClient;
        private HttpRequest mRequest;
        private HttpResponseCallback mCallback;
        private IHttpRequestContent mRequestContent;
        private HttpContentParameters mContentParameters;
        private HttpContentEncoding mContentEncoding;

        internal HttpAsyncResponse(HttpClient httpClient)
        {
            mHttpClient = httpClient;
            mRequest = null;
            mCallback = null;
            mRequestContent = null;
            mContentParameters = null;
            mContentEncoding = HttpContentEncoding.Unknown;
            TaskId = null;
        }

        internal HttpAsyncResponse SetCallback(HttpResponseCallback callback)
        {
            mCallback = callback;
            return this;
        }

        internal HttpAsyncResponse SetContentRequest(IHttpRequestContent content)
        {
            mRequestContent = content;
            return this;
        }

        internal HttpAsyncResponse SetContentData(HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            mContentParameters = parameters;
            mContentEncoding = encoding;
            return this;
        }

        internal HttpAsyncResponse QueueRequest(HttpRequest request)
        {
            if (TaskId != null)
            {
                throw new Exception("Task already running, can't run another");
            }

            mRequest = request;

            if (mRequestContent == null && mContentParameters == null)
            {
                TaskId = Scheduler.CreateTask(this, RequestTarget);
            }
            else
            {
                TaskId = Scheduler.CreateTask(this, RequestContentTarget);
            }

            Scheduler.PostTask(TaskId);
            return this;
        }

        internal HttpResponse Response { get; private set; }

        public ITaskId TaskId { get; private set; }

        public bool IsAlive { get { return TaskId != null && !Scheduler.IsFinished(TaskId); } }

        public void Wait()
        {
            if (TaskId != null)
            {
                Scheduler.WaitForTask(TaskId);
            }
        }

        public bool Wait(int millisTimeout)
        {
            if (TaskId != null)
            {
                return Scheduler.TryWaitForTask(TaskId, millisTimeout);
            }
            return false;
        }

        public IHttpResponse WaitForResult()
        {
            if (TaskId != null)
            {
                Scheduler.WaitForTask(TaskId);
                return Response;
            }
            return null;
        }

        public IHttpResponse WaitForResult(int millisTimeout)
        {
            if (TaskId != null && Scheduler.TryWaitForTask(TaskId, millisTimeout))
            {
                return Response;
            }
            return null;
        }

        public IHttpResponse Result { get { return Response; } }

        public void Cancel()
        {
            if (mHttpClient.mWebRequest != null)
            {
                mHttpClient.mWebRequest.Abort();
            }
            if (TaskId != null)
            {
                Scheduler.CancelTask(TaskId);
            }
        }

        private void SetResponse(HttpResponse response)
        {
            Response = response;
            mCallback?.Invoke(response);
        }
        
        public IAsyncTask ContinueWith(TaskOperator op)
        {
            var asyncTask = new AsyncTask(Scheduler.CreateTask(op));
            if (!Scheduler.AddContinuation(TaskId, asyncTask.TaskId))
            {
                Scheduler.PostTask(asyncTask.TaskId);
            }
            return asyncTask;
        }

        public IAsyncTask ContinueWith(object data, ParameterizedTaskOperator op)
        {
            var asyncTask = new AsyncTask(Scheduler.CreateTask(data, op));
            if (!Scheduler.AddContinuation(TaskId, asyncTask.TaskId))
            {
                Scheduler.PostTask(asyncTask.TaskId);
            }
            return asyncTask;
        }

        public IAsyncTask ContinueWith(AwaitableTaskOperator op)
        {
            var asyncTask = new AsyncTask(Scheduler.CreateAwaitableTask(op));
            if (!Scheduler.AddContinuation(TaskId, asyncTask.TaskId))
            {
                Scheduler.PostTask(asyncTask.TaskId);
            }
            return asyncTask;
        }

        public IAsyncTask ContinueWith(object data, AwaitableParameterizedTaskOperator op)
        {
            var asyncTask = new AsyncTask(Scheduler.CreateAwaitableTask(data, op));
            if (!Scheduler.AddContinuation(TaskId, asyncTask.TaskId))
            {
                Scheduler.PostTask(asyncTask.TaskId);
            }
            return asyncTask;
        }

        public IAsyncTask ContinueWith(CooperativeTaskOperator op)
        {
            var asyncTask = new AsyncTask(Scheduler.CreateCooperativeTask(op));
            if (!Scheduler.AddContinuation(TaskId, asyncTask.TaskId))
            {
                Scheduler.PostTask(asyncTask.TaskId);
            }
            return asyncTask;
        }

        public IAsyncTask ContinueWith(object data, CooperativeParameterizedTaskOperator op)
        {
            var asyncTask = new AsyncTask(Scheduler.CreateCooperativeTask(data, op));
            if (!Scheduler.AddContinuation(TaskId, asyncTask.TaskId))
            {
                Scheduler.PostTask(asyncTask.TaskId);
            }
            return asyncTask;
        }

#region

        private static void RequestTarget(object obj)
        {
            var httpAsync = (HttpAsyncResponse)obj;

            try
            {
                httpAsync.mHttpClient.SendRequest(httpAsync.mRequest);

                httpAsync.SetResponse(httpAsync.mHttpClient.ReceiveResponse() as HttpResponse);
            }
            catch(Exception ex)
            {
                httpAsync.mHttpClient.Logger.WriteErrorLog(ex);
                httpAsync.SetResponse(new HttpResponse());
            }
        }

        private static void RequestContentTarget(object obj)
        {
            var httpAsync = (HttpAsyncResponse)obj;

            try
            {
                if(httpAsync.mRequestContent != null)
                {
                    if (!httpAsync.mHttpClient.SendContentRequest(httpAsync.mRequest, httpAsync.mRequestContent))
                    {
                        httpAsync.SetResponse(null);
                        return;
                    }
                }
                else
                {
                    if (!httpAsync.mHttpClient.SendContentRequest(httpAsync.mRequest, httpAsync.mContentParameters, httpAsync.mContentEncoding))
                    {
                        httpAsync.SetResponse(null);
                        return;
                    }
                }

                httpAsync.SetResponse(httpAsync.mHttpClient.ReceiveResponse() as HttpResponse);
            }
            catch(Exception ex)
            {
                httpAsync.mHttpClient.Logger.WriteErrorLog(ex);
                httpAsync.SetResponse(new HttpResponse());
            }
        }

#endregion

    }
#endif

    public class HttpRequestContent : IHttpRequestContent
    {
        internal HttpRequestContent()
        {
            ContentType = null;
            ContentData = null;
        }

        internal HttpRequestContent(string contentType, byte[] contentData)
        {
            ContentType = contentType;
            ContentData = contentData;
        }

        public string ContentType { get; private set; }

        public byte[] ContentData { get; private set; }
    }

#if !NO_DYNAMIC_THREADING
    public class HttpAsyncRouteStatus : IHttpAsyncRouteStatus
    {
        private HttpClient mHttpClient;
        private IHttpAsyncResponse mHttpAsyncResponse;

        internal HttpAsyncRouteStatus(HttpClient httpClient)
        {
            mHttpClient = httpClient;
            mHttpClient.RouteStatus = HttpRouteStatus.Resolving;
            mHttpAsyncResponse = mHttpClient.GetAsync(null, null);
        }

        internal HttpAsyncRouteStatus(HttpClient httpClient, HttpRouteCallback callback)
        {
            mHttpClient = httpClient;
            mHttpClient.RouteStatus = HttpRouteStatus.Resolving;
            mHttpAsyncResponse = mHttpClient.GetCallback(null, null, (IHttpResponse result) => {
                mHttpClient.RouteStatus = (result != null ? HttpRouteStatus.Found : HttpRouteStatus.NotFound);
                callback(mHttpClient.RouteStatus);
            });
        }

        public ITaskId TaskId { get { return mHttpAsyncResponse.TaskId; } }

        public bool IsAlive { get { return mHttpAsyncResponse.IsAlive; } }

        public void Wait()
        {
            mHttpAsyncResponse.Wait();
        }

        public bool Wait(int millisecondsTimeout)
        {
            return mHttpAsyncResponse.Wait(millisecondsTimeout);
        }

        public HttpRouteStatus WaitForResult()
        {
            mHttpClient.RouteStatus = (mHttpAsyncResponse.WaitForResult() != null) ? HttpRouteStatus.Found : HttpRouteStatus.NotFound;
            return mHttpClient.RouteStatus;
        }

        public HttpRouteStatus WaitForResult(int millisecondsTimeout)
        {
            mHttpClient.RouteStatus = (mHttpAsyncResponse.WaitForResult(millisecondsTimeout) != null) ? HttpRouteStatus.Found : HttpRouteStatus.NotFound;
            return mHttpClient.RouteStatus;
        }

        public HttpRouteStatus Result { get { return mHttpClient.RouteStatus; } }

        public void Cancel()
        {
            mHttpAsyncResponse.Cancel();
        }
        
        public IAsyncTask ContinueWith(TaskOperator op)
        {
            var asyncTask = new AsyncTask(Scheduler.CreateTask(op));
            if (!Scheduler.AddContinuation(TaskId, asyncTask.TaskId))
            {
                Scheduler.PostTask(asyncTask.TaskId);
            }
            return asyncTask;
        }

        public IAsyncTask ContinueWith(object data, ParameterizedTaskOperator op)
        {
            var asyncTask = new AsyncTask(Scheduler.CreateTask(data, op));
            if (!Scheduler.AddContinuation(TaskId, asyncTask.TaskId))
            {
                Scheduler.PostTask(asyncTask.TaskId);
            }
            return asyncTask;
        }

        public IAsyncTask ContinueWith(AwaitableTaskOperator op)
        {
            var asyncTask = new AsyncTask(Scheduler.CreateAwaitableTask(op));
            if (!Scheduler.AddContinuation(TaskId, asyncTask.TaskId))
            {
                Scheduler.PostTask(asyncTask.TaskId);
            }
            return asyncTask;
        }

        public IAsyncTask ContinueWith(object data, AwaitableParameterizedTaskOperator op)
        {
            var asyncTask = new AsyncTask(Scheduler.CreateAwaitableTask(data, op));
            if (!Scheduler.AddContinuation(TaskId, asyncTask.TaskId))
            {
                Scheduler.PostTask(asyncTask.TaskId);
            }
            return asyncTask;
        }

        public IAsyncTask ContinueWith(CooperativeTaskOperator op)
        {
            var asyncTask = new AsyncTask(Scheduler.CreateCooperativeTask(op));
            if (!Scheduler.AddContinuation(TaskId, asyncTask.TaskId))
            {
                Scheduler.PostTask(asyncTask.TaskId);
            }
            return asyncTask;
        }

        public IAsyncTask ContinueWith(object data, CooperativeParameterizedTaskOperator op)
        {
            var asyncTask = new AsyncTask(Scheduler.CreateCooperativeTask(data, op));
            if (!Scheduler.AddContinuation(TaskId, asyncTask.TaskId))
            {
                Scheduler.PostTask(asyncTask.TaskId);
            }
            return asyncTask;
        }
    }
#endif

    public class HttpClient : IHttpClient
    {
#region Private Variables

        internal HttpWebRequest mWebRequest;
        private bool mDisposed;

#endregion

#region Constructors

        public HttpClient(Uri baseEndpoint, ILogger logger)
            : this(baseEndpoint, null, logger)
        {
        }

        public HttpClient(Uri baseEndpoint, string agentType, ILogger logger)
        {
            mDisposed = false;
            BaseEndpoint = baseEndpoint;
            AgentType = agentType;
            RouteStatus = HttpRouteStatus.Unresolved;
            Logger = logger;
            Headers = new Dictionary<string, string>();
#if !NO_DYNAMIC_THREADING
            Scheduler.Initialize();
#endif
        }

        public void Dispose()
        {
            if (mDisposed)
                return;

            mDisposed = true;
#if !NO_DYNAMIC_THREADING
            Scheduler.Shutdown();
#endif
        }

        ~HttpClient()
        {
            Dispose();
        }

#endregion

#region Public ReadOnly

        public Uri BaseEndpoint { get; protected set; }

        public string AgentType { get; protected set; }

        public Dictionary<string, string> Headers { get; protected set; }

        public HttpRouteStatus RouteStatus { get; internal set; }

        public ILogger Logger { get; private set; }

#endregion

#region Public Endpoint Builders

        public static Uri BuildEndpoint(string address)
        {
            return BuildEndpoint(address, "");
        }

        public static Uri BuildEndpoint(string address, string path, params object[] args)
        {
            StringBuilder builder = new StringBuilder(address);
            if (!address.StartsWith("http://") && !address.StartsWith("https://"))
            {
                builder.Insert(0, "http://");
            }
            if (!address.EndsWith("/") && !path.StartsWith("/"))
            {
                builder.Append("/").AppendFormat(path, args);
            }
            else if (address.EndsWith("/") && path.StartsWith("/"))
            {
                builder.AppendFormat(path.Substring(1), args);
            }
            else
            {
                builder.AppendFormat(path, args);
            }
            Uri uri;
            if (Uri.TryCreate(builder.Replace("//", "/", 8, builder.Length - 8).ToString(), UriKind.Absolute, out uri))
            {
                return uri;
            }
            return null;
        }

#endregion

#region Public Route Status Methods

        public HttpRouteStatus ResolveRoute()
        {
            IHttpResponse response = null;
            try
            {
                response = Get(null, null);
            }
            catch
            {
                return HttpRouteStatus.NotFound;
            }
            return response == null || response.StatusCode == HttpStatusCode.RequestTimeout ? HttpRouteStatus.NotFound : HttpRouteStatus.Found;
        }

        public async Task<HttpRouteStatus> ResolveRouteTaskAsync()
        {
            IHttpResponse response = null;
            try
            {
                response = await GetTaskAsync(null, null);
            }
            catch
            {
                return HttpRouteStatus.NotFound;
            }
            return response == null || response.StatusCode == HttpStatusCode.RequestTimeout ? HttpRouteStatus.NotFound : HttpRouteStatus.Found;
        }

#if !NO_DYNAMIC_THREADING
        public IHttpAsyncRouteStatus ResolveRouteAsync()
        {
            return new HttpAsyncRouteStatus(this);
        }

        public IHttpAsyncRouteStatus ResolveRouteCallback(HttpRouteCallback callback)
        {
            return new HttpAsyncRouteStatus(this, callback);
        }
#endif

#endregion

#region Public IHttpClient Methods

        public IHttpRequest CreateRequest()
        {
            var request = new HttpRequest();
            request.ProtocolVersion = HttpVersion.Version1_0;
            request.KeepAlive = false;

            if (Headers.Count > 0)
            {
                request.Headers = new HttpHeaders();
                foreach (var header in Headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return request;
        }

        public IHttpRequest CreateRequest(string method)
        {
            var request = CreateRequest();
            request.Method = method;
            return request;
        }

        public IHttpRequest CreateRequest(string method, HttpVersion version)
        {
            var request = CreateRequest(method);
            request.ProtocolVersion = version;
            return request;
        }

        public IHttpRequest CreateRequest(string method, string query)
        {
            var request = CreateRequest(method);
            request.Query = Uri.EscapeUriString(query);
            return request;
        }

        public IHttpRequest CreateRequest(string method, HttpVersion version, string query)
        {
            var request = CreateRequest(method, version);
            request.Query = Uri.EscapeUriString(query);
            return request;
        }

        public IHttpRequest CreateRequest(string method, HttpVersion version, string query, params object[] args)
        {
            var request = CreateRequest(method, version);
            request.Query = Uri.EscapeUriString(string.Format(query, args));
            return request;
        }

        public void SendRequest(IHttpRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("IHttpRequest is null");
            }
            else if (mWebRequest != null)
            {
                throw new Exception("Connection in use");
            }

            OnPrepareRequest(ref request);

            var endpoint = BuildFinalEndpoint(BaseEndpoint, request.Query);

            Logger.WriteDebugLog("NewHttpClient.SendRequest(): URI(" + request.Method + "): " + endpoint);

            mWebRequest = WebRequest.Create(endpoint) as HttpWebRequest;

            if (mWebRequest == null)
            {
                Logger.WriteDebugLog("NewHttpClient.SendRequest(): Null Reference Request[" + request.Method + "]( " + endpoint + " ) is not valid");
                throw new NullReferenceException("Request[" + request.Method + "]( " + endpoint + " ) is not valid");
            }

            mWebRequest.Method = request.Method;

            mWebRequest.UserAgent = USER_AGENT;
            if (!string.IsNullOrEmpty(AgentType))
            {
                mWebRequest.UserAgent += " (" + AgentType + ")";
            }
            if (!string.IsNullOrEmpty(request.UserAgent))
            {
                mWebRequest.UserAgent += " (" + request.UserAgent + ")";
            }

            Logger.WriteDebugLog("NewHttpClient.SendRequest(): UserAgent: " + mWebRequest.UserAgent);

            mWebRequest.ProtocolVersion = request.ProtocolVersion == HttpVersion.Version1_0 ?
                System.Net.HttpVersion.Version10 :
                System.Net.HttpVersion.Version11;

            mWebRequest.KeepAlive = request.KeepAlive;
            if (request.Timeout > 0)
            {
                mWebRequest.Timeout = request.Timeout;
            }

            if (request.Headers != null)
            {
                foreach (var header in request.Headers)
                {
                    Logger.WriteDebugLog("NewHttpClient.SendRequest(): Header: " + header.Key + " - " + header.Value);
                    mWebRequest.Headers[header.Key] = header.Value;
                }
            }

            if (!string.IsNullOrEmpty(request.ContentType))
            {
                Logger.WriteDebugLog("NewHttpClient.SendRequest(): ContentType: " + request.ContentType);
                Logger.WriteDebugLog("NewHttpClient.SendRequest(): ContentLength: " + request.ContentLength);
                mWebRequest.ContentType = request.ContentType;
                mWebRequest.ContentLength = request.ContentLength;
            }

            if (request.CookieContainer != null)
            {
                mWebRequest.CookieContainer = request.CookieContainer;
            }

            if (!string.IsNullOrEmpty(request.ConnectionGroupName))
            {
                mWebRequest.ConnectionGroupName = request.ConnectionGroupName;
            }
        }

        public Stream SendContentRequest(IHttpRequest request)
        {
            SendRequest(request);

            try
            {
                return mWebRequest.GetRequestStream();
            }
            catch
            {
                if (mWebRequest != null)
                {
                    mWebRequest.Abort();
                    mWebRequest = null;
                }
                throw;
            }
        }

        public async Task<Stream> SendContentRequestAsync(IHttpRequest request)
        {
            SendRequest(request);

            try
            {
                return await mWebRequest.GetRequestStreamAsync();
            }
            catch
            {
                if (mWebRequest != null)
                {
                    mWebRequest.Abort();
                    mWebRequest = null;
                }
                throw;
            }
        }

        public bool SendContentRequest(IHttpRequest request, IHttpRequestContent content)
        {
            if (request != null)
            {
                try
                {
                    if (!string.IsNullOrEmpty(content.ContentType))
                    {
                        request.ContentType = content.ContentType;
                        request.ContentLength = content.ContentData.Length;
                        using (Stream requestStream = SendContentRequest(request))
                        {
                            requestStream.Write(content.ContentData, 0, content.ContentData.Length);
                            requestStream.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (mWebRequest != null)
                    {
                        mWebRequest.Abort();
                        mWebRequest = null;
                    }
                    Logger.WriteErrorLog(ex);
                    return false;
                }
                return true;
            }
            return false;
        }

        public async Task<bool> SendContentRequestAsync(IHttpRequest request, IHttpRequestContent content)
        {
            if (request != null)
            {
                try
                {
                    if (!string.IsNullOrEmpty(content.ContentType))
                    {
                        request.ContentType = content.ContentType;
                        request.ContentLength = content.ContentData.Length;
                        using (Stream requestStream = await SendContentRequestAsync(request))
                        {
                            await requestStream.WriteAsync(content.ContentData, 0, content.ContentData.Length);
                            requestStream.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (mWebRequest != null)
                    {
                        mWebRequest.Abort();
                        mWebRequest = null;
                    }
                    Logger.WriteErrorLog(ex);
                    return false;
                }
                return true;
            }
            return false;
        }

        public bool SendContentRequest(IHttpRequest request, HttpContentParameters contentParameters, HttpContentEncoding encoding)
        {
            if (request != null)
            {
                try
                {
                    OnPrepareContent(ref contentParameters, ref encoding);
                    HttpRequestContent content = BuildEncodingContent(contentParameters, encoding);
                    if (!string.IsNullOrEmpty(content.ContentType))
                    {
                        request.ContentType = content.ContentType;
                        request.ContentLength = content.ContentData.Length;
                        using (Stream requestStream = SendContentRequest(request))
                        {
                            requestStream.Write(content.ContentData, 0, content.ContentData.Length);
                            requestStream.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (mWebRequest != null)
                    {
                        mWebRequest.Abort();
                        mWebRequest = null;
                    }
                    Logger.WriteErrorLog(ex);
                    return false;
                }
                return true;
            }
            return false;
        }

        public async Task<bool> SendContentRequestAsync(IHttpRequest request, HttpContentParameters contentParameters, HttpContentEncoding encoding)
        {
            if (request != null)
            {
                try
                {
                    OnPrepareContent(ref contentParameters, ref encoding);
                    HttpRequestContent content = BuildEncodingContent(contentParameters, encoding);
                    if (!string.IsNullOrEmpty(content.ContentType))
                    {
                        request.ContentType = content.ContentType;
                        request.ContentLength = content.ContentData.Length;
                        using (Stream requestStream = await SendContentRequestAsync(request))
                        {
                            await requestStream.WriteAsync(content.ContentData, 0, content.ContentData.Length);
                            requestStream.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (mWebRequest != null)
                    {
                        mWebRequest.Abort();
                        mWebRequest = null;
                    }
                    Logger.WriteErrorLog(ex);
                    return false;
                }
                return true;
            }
            return false;
        }

        public IHttpResponse ReceiveResponse()
        {
            if (mWebRequest == null)
            {
                throw new Exception("Unable to recieve response. No request was sent.");
            }

            HttpWebResponse response;
            try
            {
                response = mWebRequest.GetResponse() as HttpWebResponse;
            }
            catch (NullReferenceException ex)
            {
                mWebRequest = null;
                Logger.WriteErrorLog(ex, "NewHttpClient.ReceiveResponse()");
                return new HttpResponse();
            }
            catch (WebException ex)
            {
                Logger.WriteErrorLog(ex, "NewHttpClient.ReceiveResponse()");
                response = (HttpWebResponse)ex.Response;
            }

            if (response == null)
            {
                mWebRequest = null;
                Logger.WriteErrorLog("NewHttpClient.ReceiveResponse(): Timeout");
                return new HttpResponse();
            }

            string transferEncoding = response.Headers["Transfer-Encoding"];

            HttpResponse result;
            if (!string.IsNullOrEmpty(transferEncoding))
            {
                Logger.WriteInfoLog("WebServicer.RequestTarget(): Received Response with {0} Body", transferEncoding);
                result = ParseHttpWebResponseBody(response, Logger);
                mWebRequest = null;
            }
            else if (response.ContentLength > 0)
            {
                Logger.WriteInfoLog("WebServicer.RequestTarget(): Received Response with Body");
                result = ParseHttpWebResponseBody(response, Logger);
                mWebRequest = null;
            }
            else
            {
                Logger.WriteInfoLog("WebServicer.RequestTarget(): Received Response");
                result = new HttpResponse(response, null);
                mWebRequest = null;
            }

            return result;
        }

        public async Task<IHttpResponse> ReceiveResponseAsync()
        {
            if (mWebRequest == null)
            {
                throw new Exception("Unable to recieve response. No request was sent.");
            }

            HttpWebResponse response;
            try
            {
                response = await mWebRequest.GetResponseAsync() as HttpWebResponse;
            }
            catch (NullReferenceException ex)
            {
                mWebRequest = null;
                Logger.WriteErrorLog(ex, "NewHttpClient.ReceiveResponse()");
                return new HttpResponse();
            }
            catch (WebException ex)
            {
                Logger.WriteErrorLog(ex, "NewHttpClient.ReceiveResponse()");
                response = (HttpWebResponse)ex.Response;
            }

            if (response == null)
            {
                mWebRequest = null;
                Logger.WriteErrorLog("NewHttpClient.ReceiveResponse(): Timeout");
                return new HttpResponse();
            }

            string transferEncoding = response.Headers["Transfer-Encoding"];

            HttpResponse result;
            if (!string.IsNullOrEmpty(transferEncoding))
            {
                Logger.WriteInfoLog("WebServicer.RequestTarget(): Received Response with {0} Body", transferEncoding);
                result = ParseHttpWebResponseBody(response, Logger);
                mWebRequest = null;
            }
            else if (response.ContentLength > 0)
            {
                Logger.WriteInfoLog("WebServicer.RequestTarget(): Received Response with Body");
                result = ParseHttpWebResponseBody(response, Logger);
                mWebRequest = null;
            }
            else
            {
                Logger.WriteInfoLog("WebServicer.RequestTarget(): Received Response");
                result = new HttpResponse(response, null);
                mWebRequest = null;
            }

            return result;
        }

#if !NO_DYNAMIC_THREADING
        public IHttpAsyncResponse ReceiveAsyncResponse(IHttpRequest request)
        {
            if (request is HttpRequest httpRequest)
            {
                return new HttpAsyncResponse(this).QueueRequest(httpRequest);
            }
            return null;
        }

        public IHttpAsyncResponse ReceiveAsyncResponse(IHttpRequest request, IHttpRequestContent content)
        {
            if (request is HttpRequest httpRequest)
            {
                return new HttpAsyncResponse(this).SetContentRequest(content).QueueRequest(httpRequest);
            }
            return null;
        }

        public IHttpAsyncResponse ReceiveAsyncResponse(IHttpRequest request, HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            if (request is HttpRequest httpRequest)
            {
                return new HttpAsyncResponse(this).SetContentData(parameters, encoding).QueueRequest(httpRequest);
            }
            return null;
        }

        public IHttpAsyncResponse ReceiveCallbackResponse(IHttpRequest request, HttpResponseCallback callback)
        {
            if (request is HttpRequest httpRequest)
            {
                return new HttpAsyncResponse(this).SetCallback(callback).QueueRequest(httpRequest);
            }
            return null;
        }

        public IHttpAsyncResponse ReceiveCallbackResponse(IHttpRequest request, IHttpRequestContent content, HttpResponseCallback callback)
        {
            if (request is HttpRequest httpRequest)
            {
                return new HttpAsyncResponse(this).SetCallback(callback).SetContentRequest(content).QueueRequest(httpRequest);
            }
            return null;
        }

        public IHttpAsyncResponse ReceiveCallbackResponse(IHttpRequest request, HttpContentParameters parameters, HttpContentEncoding encoding, HttpResponseCallback callback)
        {
            if (request is HttpRequest httpRequest)
            {
                return new HttpAsyncResponse(this).SetCallback(callback).SetContentData(parameters, encoding).QueueRequest(httpRequest);
            }
            return null;

        }
#endif

#region Common Http Handlers

#region Synchronous

        public IHttpResponse Get(string query)
        {
            var request = CreateRequest("GET", query);
            SendRequest(request);
            return ReceiveResponse();
        }

        public IHttpResponse Get(HttpHeaders headers)
        {
            var request = CreateRequest("GET");

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            SendRequest(request);
            return ReceiveResponse();
        }

        public IHttpResponse Get(string query, HttpHeaders headers)
        {
            var request = CreateRequest("GET", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            SendRequest(request);
            return ReceiveResponse();
        }

        public IHttpResponse Get(string query, HttpHeaders headers, CookieContainer cookieContainer)
        {
            var request = CreateRequest("GET", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            SendRequest(request);
            return ReceiveResponse();
        }

        public IHttpResponse Post(string query)
        {
            var request = CreateRequest("POST", query);
            SendContentRequest(request).Close();
            return ReceiveResponse();
        }

        public IHttpResponse Post(HttpHeaders headers)
        {
            var request = CreateRequest("POST");

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            SendContentRequest(request).Close();
            return ReceiveResponse();
        }

        public IHttpResponse Post(string query, HttpHeaders headers)
        {
            var request = CreateRequest("POST", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            SendContentRequest(request).Close();
            return ReceiveResponse();
        }

        public IHttpResponse Post(string query, HttpHeaders headers, CookieContainer cookieContainer)
        {
            var request = CreateRequest("POST", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            SendContentRequest(request).Close();
            return ReceiveResponse();
        }

        public IHttpResponse Post(HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("POST");
            if (!SendContentRequest(request, parameters, encoding))
            {
                return new HttpResponse();
            }
            return ReceiveResponse();
        }

        public IHttpResponse Post(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("POST");

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (!SendContentRequest(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return ReceiveResponse();
        }

        public IHttpResponse Post(HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("POST");

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            if (!SendContentRequest(request, parameters, encoding))
            {
                return new HttpResponse();
            }
            return ReceiveResponse();
        }

        public IHttpResponse Post(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("POST");

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            if (!SendContentRequest(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return ReceiveResponse();
        }

        public IHttpResponse Post(string query, HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("POST", query);

            if (!SendContentRequest(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return ReceiveResponse();
        }

        public IHttpResponse Post(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("POST", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (!SendContentRequest(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return ReceiveResponse();
        }

        public IHttpResponse Post(string query, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("POST", query);

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            if (!SendContentRequest(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return ReceiveResponse();
        }

        public IHttpResponse Post(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("POST", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            if (!SendContentRequest(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return ReceiveResponse();
        }

        public IHttpResponse Put(string query)
        {
            var request = CreateRequest("PUT", query);
            SendContentRequest(request).Close();
            return ReceiveResponse();
        }

        public IHttpResponse Put(HttpHeaders headers)
        {
            var request = CreateRequest("PUT");

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            SendContentRequest(request).Close();
            return ReceiveResponse();
        }

        public IHttpResponse Put(string query, HttpHeaders headers)
        {
            var request = CreateRequest("PUT", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            SendContentRequest(request).Close();
            return ReceiveResponse();
        }

        public IHttpResponse Put(string query, HttpHeaders headers, CookieContainer cookieContainer)
        {
            var request = CreateRequest("PUT", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            SendContentRequest(request).Close();
            return ReceiveResponse();
        }

        public IHttpResponse Put(HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("PUT");
            if (!SendContentRequest(request, parameters, encoding))
            {
                return new HttpResponse();
            }
            return ReceiveResponse();
        }

        public IHttpResponse Put(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("PUT");

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (!SendContentRequest(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return ReceiveResponse();
        }

        public IHttpResponse Put(HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("PUT");

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            if (!SendContentRequest(request, parameters, encoding))
            {
                return new HttpResponse();
            }
            return ReceiveResponse();
        }

        public IHttpResponse Put(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("PUT");

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            if (!SendContentRequest(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return ReceiveResponse();
        }

        public IHttpResponse Put(string query, HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("PUT", query);

            if (!SendContentRequest(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return ReceiveResponse();
        }

        public IHttpResponse Put(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("PUT", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (!SendContentRequest(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return ReceiveResponse();
        }

        public IHttpResponse Put(string query, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("PUT", query);

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            if (!SendContentRequest(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return ReceiveResponse();
        }

        public IHttpResponse Put(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("PUT", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            if (!SendContentRequest(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return ReceiveResponse();
        }

        public IHttpResponse Delete(string query)
        {
            var request = CreateRequest("DELETE", query);
            SendRequest(request);
            return ReceiveResponse();
        }

        public IHttpResponse Delete(HttpHeaders headers)
        {
            var request = CreateRequest("DELETE");

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            SendRequest(request);
            return ReceiveResponse();
        }

        public IHttpResponse Delete(string query, HttpHeaders headers)
        {
            var request = CreateRequest("DELETE", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            SendRequest(request);
            return ReceiveResponse();
        }

        public IHttpResponse Delete(string query, HttpHeaders headers, CookieContainer cookieContainer)
        {
            var request = CreateRequest("DELETE", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            SendRequest(request);
            return ReceiveResponse();
        }

        public IHttpResponse Options(string query)
        {
            var request = CreateRequest("OPTIONS", query);
            SendRequest(request);
            return ReceiveResponse();
        }

        public IHttpResponse Options(HttpHeaders headers)
        {
            var request = CreateRequest("OPTIONS");

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            SendRequest(request);
            return ReceiveResponse();
        }

        public IHttpResponse Options(string query, HttpHeaders headers)
        {
            var request = CreateRequest("OPTIONS", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            SendRequest(request);
            return ReceiveResponse();
        }

        public IHttpResponse Options(string query, HttpHeaders headers, CookieContainer cookieContainer)
        {
            var request = CreateRequest("OPTIONS", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            SendRequest(request);
            return ReceiveResponse();
        }

#endregion

#region Asynchronous
#if !NO_DYNAMIC_THREADING

        public IHttpAsyncResponse GetAsync(string query)
        {
            var request = CreateRequest("GET", query) as HttpRequest;

            return ReceiveAsyncResponse(request);
        }

        public IHttpAsyncResponse GetAsync(HttpHeaders headers)
        {
            var request = CreateRequest("GET") as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveAsyncResponse(request);
        }

        public IHttpAsyncResponse GetAsync(string query, HttpHeaders headers)
        {
            var request = CreateRequest("GET", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveAsyncResponse(request);
        }

        public IHttpAsyncResponse GetAsync(string query, HttpHeaders headers, CookieContainer cookieContainer)
        {
            var request = CreateRequest("GET", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveAsyncResponse(request);
        }

        public IHttpAsyncResponse PostAsync(string query)
        {
            var request = CreateRequest("POST", query) as HttpRequest;

            return ReceiveAsyncResponse(request);
        }

        public IHttpAsyncResponse PostAsync(HttpHeaders headers)
        {
            var request = CreateRequest("POST") as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveAsyncResponse(request);
        }

        public IHttpAsyncResponse PostAsync(string query, HttpHeaders headers)
        {
            var request = CreateRequest("POST", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveAsyncResponse(request);
        }

        public IHttpAsyncResponse PostAsync(string query, HttpHeaders headers, CookieContainer cookieContainer)
        {
            var request = CreateRequest("POST", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveAsyncResponse(request);
        }

        public IHttpAsyncResponse PostAsync(HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("POST") as HttpRequest;

            return ReceiveAsyncResponse(request, parameters, encoding);
        }

        public IHttpAsyncResponse PostAsync(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("POST") as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveAsyncResponse(request, parameters, encoding);
        }

        public IHttpAsyncResponse PostAsync(HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("POST") as HttpRequest;

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveAsyncResponse(request, parameters, encoding);
        }

        public IHttpAsyncResponse PostAsync(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("POST") as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveAsyncResponse(request, parameters, encoding);
        }

        public IHttpAsyncResponse PostAsync(string query, HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("POST", query) as HttpRequest;

            return ReceiveAsyncResponse(request, parameters, encoding);
        }

        public IHttpAsyncResponse PostAsync(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("POST", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveAsyncResponse(request, parameters, encoding);
        }

        public IHttpAsyncResponse PostAsync(string query, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("POST", query) as HttpRequest;

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveAsyncResponse(request, parameters, encoding);
        }

        public IHttpAsyncResponse PostAsync(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("POST", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveAsyncResponse(request, parameters, encoding);
        }

        public IHttpAsyncResponse PutAsync(string query)
        {
            var request = CreateRequest("PUT", query) as HttpRequest;

            return ReceiveAsyncResponse(request);
        }

        public IHttpAsyncResponse PutAsync(HttpHeaders headers)
        {
            var request = CreateRequest("PUT") as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveAsyncResponse(request);
        }

        public IHttpAsyncResponse PutAsync(string query, HttpHeaders headers)
        {
            var request = CreateRequest("PUT", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveAsyncResponse(request);
        }

        public IHttpAsyncResponse PutAsync(string query, HttpHeaders headers, CookieContainer cookieContainer)
        {
            var request = CreateRequest("PUT", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveAsyncResponse(request);
        }

        public IHttpAsyncResponse PutAsync(HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("PUT") as HttpRequest;

            return ReceiveAsyncResponse(request, parameters, encoding);
        }

        public IHttpAsyncResponse PutAsync(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("PUT") as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveAsyncResponse(request, parameters, encoding);
        }

        public IHttpAsyncResponse PutAsync(HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("PUT") as HttpRequest;

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveAsyncResponse(request, parameters, encoding);
        }

        public IHttpAsyncResponse PutAsync(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("PUT") as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveAsyncResponse(request, parameters, encoding);
        }

        public IHttpAsyncResponse PutAsync(string query, HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("PUT", query) as HttpRequest;

            return ReceiveAsyncResponse(request, parameters, encoding);
        }

        public IHttpAsyncResponse PutAsync(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("PUT", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveAsyncResponse(request, parameters, encoding);
        }

        public IHttpAsyncResponse PutAsync(string query, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("PUT", query) as HttpRequest;

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveAsyncResponse(request, parameters, encoding);
        }

        public IHttpAsyncResponse PutAsync(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("PUT", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveAsyncResponse(request, parameters, encoding);
        }

        public IHttpAsyncResponse DeleteAsync(string query)
        {
            var request = CreateRequest("DELETE", query) as HttpRequest;

            return ReceiveAsyncResponse(request);
        }

        public IHttpAsyncResponse DeleteAsync(HttpHeaders headers)
        {
            var request = CreateRequest("DELETE") as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveAsyncResponse(request);
        }

        public IHttpAsyncResponse DeleteAsync(string query, HttpHeaders headers)
        {
            var request = CreateRequest("DELETE", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveAsyncResponse(request);
        }

        public IHttpAsyncResponse DeleteAsync(string query, HttpHeaders headers, CookieContainer cookieContainer)
        {
            var request = CreateRequest("DELETE", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveAsyncResponse(request);
        }

        public IHttpAsyncResponse OptionsAsync(string query)
        {
            var request = CreateRequest("OPTIONS", query) as HttpRequest;

            return ReceiveAsyncResponse(request);
        }

        public IHttpAsyncResponse OptionsAsync(HttpHeaders headers)
        {
            var request = CreateRequest("OPTIONS") as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveAsyncResponse(request);
        }

        public IHttpAsyncResponse OptionsAsync(string query, HttpHeaders headers)
        {
            var request = CreateRequest("OPTIONS", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveAsyncResponse(request);
        }

        public IHttpAsyncResponse OptionsAsync(string query, HttpHeaders headers, CookieContainer cookieContainer)
        {
            var request = CreateRequest("OPTIONS", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveAsyncResponse(request);
        }

#endif
#endregion

#region Asynchronous Callbacks
#if !NO_DYNAMIC_THREADING

        public IHttpAsyncResponse GetCallback(string query, HttpResponseCallback callback)
        {
            var request = CreateRequest("GET", query) as HttpRequest;

            return ReceiveCallbackResponse(request, callback);
        }

        public IHttpAsyncResponse GetCallback(HttpHeaders headers, HttpResponseCallback callback)
        {
            var request = CreateRequest("GET") as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveCallbackResponse(request, callback);
        }

        public IHttpAsyncResponse GetCallback(string query, HttpHeaders headers, HttpResponseCallback callback)
        {
            var request = CreateRequest("GET", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveCallbackResponse(request, callback);
        }

        public IHttpAsyncResponse GetCallback(string query, HttpHeaders headers, CookieContainer cookieContainer, HttpResponseCallback callback)
        {
            var request = CreateRequest("GET", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveCallbackResponse(request, callback);
        }

        public IHttpAsyncResponse PostCallback(string query, HttpResponseCallback callback)
        {
            var request = CreateRequest("POST", query) as HttpRequest;

            return ReceiveCallbackResponse(request, callback);
        }

        public IHttpAsyncResponse PostCallback(HttpHeaders headers, HttpResponseCallback callback)
        {
            var request = CreateRequest("POST") as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveCallbackResponse(request, callback);
        }

        public IHttpAsyncResponse PostCallback(string query, HttpHeaders headers, HttpResponseCallback callback)
        {
            var request = CreateRequest("POST", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveCallbackResponse(request, callback);
        }

        public IHttpAsyncResponse PostCallback(string query, HttpHeaders headers, CookieContainer cookieContainer, HttpResponseCallback callback)
        {
            var request = CreateRequest("POST", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveCallbackResponse(request, callback);
        }

        public IHttpAsyncResponse PostCallback(HttpContentParameters parameters, HttpContentEncoding encoding, HttpResponseCallback callback)
        {
            var request = CreateRequest("POST") as HttpRequest;

            return ReceiveCallbackResponse(request, parameters, encoding, callback);
        }

        public IHttpAsyncResponse PostCallback(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, HttpResponseCallback callback)
        {
            var request = CreateRequest("POST") as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveCallbackResponse(request, parameters, encoding, callback);
        }

        public IHttpAsyncResponse PostCallback(HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer, HttpResponseCallback callback)
        {
            var request = CreateRequest("POST") as HttpRequest;

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveCallbackResponse(request, parameters, encoding, callback);
        }

        public IHttpAsyncResponse PostCallback(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer, HttpResponseCallback callback)
        {
            var request = CreateRequest("POST") as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveCallbackResponse(request, parameters, encoding, callback);
        }

        public IHttpAsyncResponse PostCallback(string query, HttpContentParameters parameters, HttpContentEncoding encoding, HttpResponseCallback callback)
        {
            var request = CreateRequest("POST", query) as HttpRequest;

            return ReceiveCallbackResponse(request, parameters, encoding, callback);
        }

        public IHttpAsyncResponse PostCallback(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, HttpResponseCallback callback)
        {
            var request = CreateRequest("POST", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveCallbackResponse(request, parameters, encoding, callback);
        }

        public IHttpAsyncResponse PostCallback(string query, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer, HttpResponseCallback callback)
        {
            var request = CreateRequest("POST", query) as HttpRequest;

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveCallbackResponse(request, parameters, encoding, callback);
        }

        public IHttpAsyncResponse PostCallback(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer, HttpResponseCallback callback)
        {
            var request = CreateRequest("POST", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveCallbackResponse(request, parameters, encoding, callback);
        }

        public IHttpAsyncResponse PutCallback(string query, HttpResponseCallback callback)
        {
            var request = CreateRequest("PUT", query) as HttpRequest;

            return ReceiveCallbackResponse(request, callback);
        }

        public IHttpAsyncResponse PutCallback(HttpHeaders headers, HttpResponseCallback callback)
        {
            var request = CreateRequest("PUT") as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveCallbackResponse(request, callback);
        }

        public IHttpAsyncResponse PutCallback(string query, HttpHeaders headers, HttpResponseCallback callback)
        {
            var request = CreateRequest("PUT", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveCallbackResponse(request, callback);
        }

        public IHttpAsyncResponse PutCallback(string query, HttpHeaders headers, CookieContainer cookieContainer, HttpResponseCallback callback)
        {
            var request = CreateRequest("PUT", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveCallbackResponse(request, callback);
        }

        public IHttpAsyncResponse PutCallback(HttpContentParameters parameters, HttpContentEncoding encoding, HttpResponseCallback callback)
        {
            var request = CreateRequest("PUT") as HttpRequest;

            return ReceiveCallbackResponse(request, parameters, encoding, callback);
        }

        public IHttpAsyncResponse PutCallback(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, HttpResponseCallback callback)
        {
            var request = CreateRequest("PUT") as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveCallbackResponse(request, parameters, encoding, callback);
        }

        public IHttpAsyncResponse PutCallback(HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer, HttpResponseCallback callback)
        {
            var request = CreateRequest("PUT") as HttpRequest;

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveCallbackResponse(request, parameters, encoding, callback);
        }

        public IHttpAsyncResponse PutCallback(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer, HttpResponseCallback callback)
        {
            var request = CreateRequest("PUT") as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveCallbackResponse(request, parameters, encoding, callback);
        }

        public IHttpAsyncResponse PutCallback(string query, HttpContentParameters parameters, HttpContentEncoding encoding, HttpResponseCallback callback)
        {
            var request = CreateRequest("PUT", query) as HttpRequest;

            return ReceiveCallbackResponse(request, parameters, encoding, callback);
        }

        public IHttpAsyncResponse PutCallback(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, HttpResponseCallback callback)
        {
            var request = CreateRequest("PUT", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveCallbackResponse(request, parameters, encoding, callback);
        }

        public IHttpAsyncResponse PutCallback(string query, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer, HttpResponseCallback callback)
        {
            var request = CreateRequest("PUT", query) as HttpRequest;

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveCallbackResponse(request, parameters, encoding, callback);
        }

        public IHttpAsyncResponse PutCallback(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer, HttpResponseCallback callback)
        {
            var request = CreateRequest("PUT", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveCallbackResponse(request, parameters, encoding, callback);
        }

        public IHttpAsyncResponse DeleteCallback(string query, HttpResponseCallback callback)
        {
            var request = CreateRequest("DELETE", query) as HttpRequest;

            return ReceiveCallbackResponse(request, callback);
        }

        public IHttpAsyncResponse DeleteCallback(HttpHeaders headers, HttpResponseCallback callback)
        {
            var request = CreateRequest("DELETE") as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveCallbackResponse(request, callback);
        }

        public IHttpAsyncResponse DeleteCallback(string query, HttpHeaders headers, HttpResponseCallback callback)
        {
            var request = CreateRequest("DELETE", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveCallbackResponse(request, callback);
        }

        public IHttpAsyncResponse DeleteCallback(string query, HttpHeaders headers, CookieContainer cookieContainer, HttpResponseCallback callback)
        {
            var request = CreateRequest("DELETE", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveCallbackResponse(request, callback);
        }

        public IHttpAsyncResponse OptionsCallback(string query, HttpResponseCallback callback)
        {
            var request = CreateRequest("OPTIONS", query) as HttpRequest;

            return ReceiveCallbackResponse(request, callback);
        }

        public IHttpAsyncResponse OptionsCallback(HttpHeaders headers, HttpResponseCallback callback)
        {
            var request = CreateRequest("OPTIONS") as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveCallbackResponse(request, callback);
        }

        public IHttpAsyncResponse OptionsCallback(string query, HttpHeaders headers, HttpResponseCallback callback)
        {
            var request = CreateRequest("OPTIONS", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            return ReceiveCallbackResponse(request, callback);
        }

        public IHttpAsyncResponse OptionsCallback(string query, HttpHeaders headers, CookieContainer cookieContainer, HttpResponseCallback callback)
        {
            var request = CreateRequest("OPTIONS", query) as HttpRequest;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            return ReceiveCallbackResponse(request, callback);
        }

#endif
#endregion

#region Asynchronous Tasks

        public Task<IHttpResponse> GetTaskAsync(string query)
        {
            var request = CreateRequest("GET", query);
            SendRequest(request);
            return ReceiveResponseAsync();
        }

        public Task<IHttpResponse> GetTaskAsync(HttpHeaders headers)
        {
            var request = CreateRequest("GET");

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            SendRequest(request);
            return ReceiveResponseAsync();
        }

        public Task<IHttpResponse> GetTaskAsync(string query, HttpHeaders headers)
        {
            var request = CreateRequest("GET", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            SendRequest(request);
            return ReceiveResponseAsync();
        }

        public Task<IHttpResponse> GetTaskAsync(string query, HttpHeaders headers, CookieContainer cookieContainer)
        {
            var request = CreateRequest("GET", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            SendRequest(request);
            return ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PostTaskAsync(string query)
        {
            var request = CreateRequest("POST", query);
            var stream = await SendContentRequestAsync(request);
            stream.Close();

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PostTaskAsync(HttpHeaders headers)
        {
            var request = CreateRequest("POST");

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            var stream = await SendContentRequestAsync(request);
            stream.Close();

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PostTaskAsync(string query, HttpHeaders headers)
        {
            var request = CreateRequest("POST", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            var stream = await SendContentRequestAsync(request);
            stream.Close();

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PostTaskAsync(string query, HttpHeaders headers, CookieContainer cookieContainer)
        {
            var request = CreateRequest("POST", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            var stream = await SendContentRequestAsync(request);
            stream.Close();

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PostTaskAsync(HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("POST");

            if (!await SendContentRequestAsync(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PostTaskAsync(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("POST");

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (!await SendContentRequestAsync(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PostTaskAsync(HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("POST");

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            if (!await SendContentRequestAsync(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PostTaskAsync(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("POST");

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            if (!await SendContentRequestAsync(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PostTaskAsync(string query, HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("POST", query);

            if (!await SendContentRequestAsync(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PostTaskAsync(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("POST", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (!await SendContentRequestAsync(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PostTaskAsync(string query, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("POST", query);

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            if (!await SendContentRequestAsync(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PostTaskAsync(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("POST", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            if (!await SendContentRequestAsync(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PutTaskAsync(string query)
        {
            var request = CreateRequest("PUT", query);

            var stream = await SendContentRequestAsync(request);
            stream.Close();

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PutTaskAsync(HttpHeaders headers)
        {
            var request = CreateRequest("PUT");

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            var stream = await SendContentRequestAsync(request);
            stream.Close();

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PutTaskAsync(string query, HttpHeaders headers)
        {
            var request = CreateRequest("PUT", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            var stream = await SendContentRequestAsync(request);
            stream.Close();

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PutTaskAsync(string query, HttpHeaders headers, CookieContainer cookieContainer)
        {
            var request = CreateRequest("PUT", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            var stream = await SendContentRequestAsync(request);
            stream.Close();

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PutTaskAsync(HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("PUT");

            if (!await SendContentRequestAsync(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PutTaskAsync(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("PUT");

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (!await SendContentRequestAsync(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PutTaskAsync(HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("PUT");

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            if (!await SendContentRequestAsync(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PutTaskAsync(HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("PUT");

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            if (!await SendContentRequestAsync(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PutTaskAsync(string query, HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("PUT", query);

            if (!await SendContentRequestAsync(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PutTaskAsync(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding)
        {
            var request = CreateRequest("PUT", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (!await SendContentRequestAsync(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PutTaskAsync(string query, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("PUT", query);

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            if (!await SendContentRequestAsync(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return await ReceiveResponseAsync();
        }

        public async Task<IHttpResponse> PutTaskAsync(string query, HttpHeaders headers, HttpContentParameters parameters, HttpContentEncoding encoding, CookieContainer cookieContainer)
        {
            var request = CreateRequest("PUT", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            if (!await SendContentRequestAsync(request, parameters, encoding))
            {
                return new HttpResponse();
            }

            return await ReceiveResponseAsync();
        }

        public Task<IHttpResponse> DeleteTaskAsync(string query)
        {
            var request = CreateRequest("DELETE", query);
            SendRequest(request);
            return ReceiveResponseAsync();
        }

        public Task<IHttpResponse> DeleteTaskAsync(HttpHeaders headers)
        {
            var request = CreateRequest("DELETE");

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            SendRequest(request);
            return ReceiveResponseAsync();
        }

        public Task<IHttpResponse> DeleteTaskAsync(string query, HttpHeaders headers)
        {
            var request = CreateRequest("DELETE", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            SendRequest(request);
            return ReceiveResponseAsync();
        }

        public Task<IHttpResponse> DeleteTaskAsync(string query, HttpHeaders headers, CookieContainer cookieContainer)
        {
            var request = CreateRequest("DELETE", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            SendRequest(request);
            return ReceiveResponseAsync();
        }

        public Task<IHttpResponse> OptionsTaskAsync(string query)
        {
            var request = CreateRequest("OPTIONS", query);
            SendRequest(request);
            return ReceiveResponseAsync();
        }

        public Task<IHttpResponse> OptionsTaskAsync(HttpHeaders headers)
        {
            var request = CreateRequest("OPTIONS");

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            SendRequest(request);
            return ReceiveResponseAsync();
        }

        public Task<IHttpResponse> OptionsTaskAsync(string query, HttpHeaders headers)
        {
            var request = CreateRequest("OPTIONS", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            SendRequest(request);
            return ReceiveResponseAsync();
        }

        public Task<IHttpResponse> OptionsTaskAsync(string query, HttpHeaders headers, CookieContainer cookieContainer)
        {
            var request = CreateRequest("OPTIONS", query);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (cookieContainer != null)
            {
                request.CookieContainer = cookieContainer;
            }

            SendRequest(request);
            return ReceiveResponseAsync();
        }

#endregion

#endregion

#endregion

#region Protected Methods

        protected virtual void OnPrepareRequest(ref IHttpRequest request)
        {
        }

        protected virtual void OnPrepareContent(ref HttpContentParameters contentParameters, ref HttpContentEncoding encoding)
        {
        }

#endregion

#region Private Static Variables

        private static Encoding FORM_DATA_ENCODING = Encoding.UTF8;
        private static string USER_AGENT = "Dynamic.Core.Web/2.0";

#endregion

#region Private Static Methods

        private static Uri BuildFinalEndpoint(Uri baseEndpoint, string queryPath)
        {
            if (!string.IsNullOrEmpty(queryPath) && !queryPath.Contains('?'))
            {
                UriBuilder builder = new UriBuilder(baseEndpoint);
                if (string.IsNullOrEmpty(builder.Path))
                {
                    builder.Path = queryPath;
                }
                else if (builder.Path[builder.Path.Length - 1] == '/')
                {
                    if (queryPath[0] == '/')
                    {
                        builder.Path += queryPath.Substring(1);
                    }
                    else
                    {
                        builder.Path += queryPath;
                    }
                }
                else if (queryPath[0] != '/')
                {
                    builder.Path += ("/" + queryPath);
                }
                return builder.Uri;
            }
            return baseEndpoint;
        }

        private static HttpRequestContent BuildEncodingContent(HttpContentParameters contentParameters, HttpContentEncoding encoding)
        {
            if (encoding == HttpContentEncoding.PropertiesForm)
            {
                return new HttpRequestContent(
                    "application/x-www-form-properties",
                    BuildPropertiesEncodedFormData(contentParameters)
                );
            }
            else if (encoding == HttpContentEncoding.UrlForm)
            {
                return new HttpRequestContent(
                    "application/x-www-form-urlencoded",
                    BuildURLEncodedFormData(contentParameters)
                );
            }
            else if (encoding == HttpContentEncoding.MultipartForm)
            {
                string formDataBoundary = string.Format("----------{0:N}", Guid.NewGuid());
                return new HttpRequestContent(
                    "multipart/form-data; boundary=" + formDataBoundary,
                    BuildMultipartFormData(contentParameters, formDataBoundary)
                );
            }
            else if (encoding == HttpContentEncoding.Json)
            {
                JsonParameters jsonParameters = new JsonParameters();
                jsonParameters.UseExtensions = false;
                return new HttpRequestContent(
                    "application/json",
                    FORM_DATA_ENCODING.GetBytes(JsonDoctor.ToJson(contentParameters, jsonParameters))
                );
            }
            return new HttpRequestContent();
        }

        private static HttpResponse ParseHttpWebResponseBody(HttpWebResponse response, ILogger logger)
        {
            try
            {
                Stream stream = response.GetResponseStream();
                if (stream != null)
                {
                    IHttpContentParser parser = null;
                    string contentType = response.ContentType;
                    if (!string.IsNullOrEmpty(contentType))
                    {
                        logger.WriteInfoLog("HttpClient.ParseHttpWebResponseBody(): ContentType: {0}, ContentLength: ({1})", response.ContentType, response.ContentLength);
                        if (contentType.Contains(";"))
                        {
                            contentType = contentType.Substring(0, contentType.IndexOf(";"));
                        }
                        contentType = contentType.Trim().ToLower();
                        if (HttpRegistry.ContentParsers.TryGetValue(contentType, out parser))
                        {
                            // UGLY HACK ... WHY DOES THIS NOT WORK PROPERLY
                            logger.WriteErrorLog("HttpClient.ParseHttpWebResponseBody(): Failed to find content parser for: " + response.ContentType);
                            logger.WriteErrorLog("HttpClient.ParseHttpWebResponseBody(): Available keys: " + string.Join(";", HttpRegistry.ContentParsers.Select(x => x.Key).ToArray()));
                            foreach (var value in HttpRegistry.ContentParsers)
                            {
                                if (value.Key.ToLower().Contains(contentType))
                                {
                                    parser = value.Value;
                                    break;
                                }
                            }
                            if (parser == null)
                            {
                                logger.WriteErrorLog("HttpClient.ParseHttpWebResponseBody(): Defaulting to StreamContent Parser");
                                parser = new StreamContentParser();
                            }
                        }
                    }
                    else
                    {
                        logger.WriteErrorLog("HttpClient.ParseHttpWebResponseBody(): ContentType not defined: Defaulting to StreamContent Parser");
                        parser = new StreamContentParser();
                    }
                    logger.WriteInfoLog("HttpClient.ParseHttpWebResponseBody(): Using Content Parser: {0}", parser.GetType().FullName);
                    return new HttpResponse(response, parser.Parse(response.Headers, stream));
                }
                logger.WriteInfoLog("HttpClient.ParseHttpWebResponseBody(): No Content");
                return new HttpResponse(response, null);
            }
            catch (Exception ex)
            {
                logger.WriteErrorLog(ex);
            }
            return null;
        }

#region 

        private static byte[] BuildPropertiesEncodedFormData(HttpContentParameters contentParameters)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var param in contentParameters)
            {
                if (param.Value is string str)
                {
                    sb.AppendFormat(
                        "{0}: {1}",
                        param.Name,
                        str
                    );
                }
                else if (param.Value is bool bl)
                {
                    sb.AppendFormat(
                        "{0}: {1}",
                        param.Name,
                        bl ? "1" : "0"
                    );
                }
                else if (param.Value.IsNumericType() || param.Value is IConvertible)
                {
                    sb.AppendFormat(
                        "{0}: {1}",
                        param.Name,
                        Convert.ToString(param.Value)
                    );
                }
                else if (param.Value is DateTime dt)
                {
                    sb.AppendFormat(
                        "{0}: {1}",
                        param.Name,
                        dt.ToString("yyyy-MM-dd HH:mm:ss")
                    );
                }
                param.Dispose();
            }

            string urlEncodedData = Uri.EscapeUriString(sb.ToString());
            if (urlEncodedData.Length > 0)
            {
                urlEncodedData = urlEncodedData.Replace("(", "%28").Replace(")", "%29").Replace(":", "%3A");
                return FORM_DATA_ENCODING.GetBytes(urlEncodedData.ToCharArray());
            }

            return new byte[0];
        }

        private static byte[] BuildURLEncodedFormData(HttpContentParameters contentParameters)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var param in contentParameters)
            {
                if (sb.Length != 0)
                {
                    sb.Append("&");
                }
                if (param.Value is string str)
                {
                    sb.AppendFormat(
                        "{0}={1}",
                        param.Name,
                        str
                    );
                }
                else if (param.Value is bool bl)
                {
                    sb.AppendFormat(
                        "{0}={1}",
                        param.Name,
                        bl ? "1" : "0"
                    );
                }
                else if (param.Value.IsNumericType() || param.Value is IConvertible)
                {
                    sb.AppendFormat(
                        "{0}={1}",
                        param.Name,
                        Convert.ToString(param.Value)
                    );
                }
                else if (param.Value is DateTime dt)
                {
                    sb.AppendFormat(
                        "{0}={1}",
                        param.Name,
                        dt.ToString("yyyy-MM-dd HH:mm:ss")
                    );
                }
                param.Dispose();
            }

            string urlEncodedData = Uri.EscapeUriString(sb.ToString());
            if (urlEncodedData.Length > 0)
            {
                urlEncodedData = urlEncodedData.Replace("(", "%28").Replace(")", "%29").Replace(":", "%3A");
                return FORM_DATA_ENCODING.GetBytes(urlEncodedData.ToCharArray());
            }

            return new byte[0];
        }

        private static byte[] BuildMultipartFormData(HttpContentParameters contentParameters, string boundary)
        {
            Stream formDataStream = new MemoryStream();
            bool needsCLRF = false;

            foreach (var param in contentParameters)
            {
                // Thanks to feedback from commenters, add a CRLF to allow multiple parameters to be added.
                // Skip it on the first parameter, add it to subsequent parameters.
                if (needsCLRF)
                {
                    formDataStream.Write(FORM_DATA_ENCODING.GetBytes("\r\n"), 0, FORM_DATA_ENCODING.GetByteCount("\r\n"));
                }

                needsCLRF = true;

                if (param.Value is IHttpContentPart part)
                {
                    Type type = param.Value.GetType();
                    if (HttpRegistry.ContentBuilders.ContainsKey(type))
                    {
                        HttpRegistry.ContentBuilders[type].Invoke(part, param.Name, boundary, formDataStream, FORM_DATA_ENCODING);
                    }
                }
                else if (param.Value is bool bl)
                {
                    string postData = string.Format(
                        "--{0}\r\nContent-Disposition: form-data; name=\"{1}\"\r\n\r\n{2}",
                        boundary,
                        param.Name,
                        bl ? "1" : "0"
                    );
                    formDataStream.Write(FORM_DATA_ENCODING.GetBytes(postData), 0, FORM_DATA_ENCODING.GetByteCount(postData));
                }
                else if (param.Value is DateTime dt)
                {
                    string postData = string.Format(
                        "--{0}\r\nContent-Disposition: form-data; name=\"{1}\"\r\n\r\n{2}",
                        boundary,
                        param.Name,
                        dt.ToString("yyyy-MM-dd HH:mm:ss")
                    );
                    formDataStream.Write(FORM_DATA_ENCODING.GetBytes(postData), 0, FORM_DATA_ENCODING.GetByteCount(postData));
                }
                else
                {
                    string postData = string.Format(
                        "--{0}\r\nContent-Disposition: form-data; name=\"{1}\"\r\n\r\n{2}",
                        boundary,
                        param.Name,
                        param.Value
                    );
                    formDataStream.Write(FORM_DATA_ENCODING.GetBytes(postData), 0, FORM_DATA_ENCODING.GetByteCount(postData));
                }

                param.Dispose();
            }

            // Add the end of the request.  Start with a newline
            string footer = "\r\n--" + boundary + "--\r\n";
            formDataStream.Seek(0, SeekOrigin.End);
            formDataStream.Write(FORM_DATA_ENCODING.GetBytes(footer), 0, FORM_DATA_ENCODING.GetByteCount(footer));

            // Dump the Stream into a byte[]
            formDataStream.Position = 0;
            byte[] formData = new byte[formDataStream.Length];
            formDataStream.Read(formData, 0, formData.Length);
            formDataStream.Close();

            return formData;
        }

#endregion

        static HttpClient()
        {
            HttpRegistry.Touch();
        }

#endregion
    }
}
