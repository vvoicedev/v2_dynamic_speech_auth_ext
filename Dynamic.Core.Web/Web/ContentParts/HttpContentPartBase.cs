﻿
namespace Dynamic.Web.ContentParts
{
    public class HttpContentPartBase : IHttpContentPart
    {
        public string ContentDisposition { get; internal set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }

        public HttpContentPartBase(string type)
        {
            ContentDisposition = "form-data";
            ContentType = type;
            FileName = "";
        }

        public HttpContentPartBase(string type, string disposition)
        {
            ContentDisposition = disposition;
            ContentType = type;
            FileName = "";
        }

        public void Dispose()
        {
            OnDispose();
        }

        protected virtual void OnDispose()
        {

        }

    }
}
