﻿using System;
using System.IO;
using System.Text;

using Dynamic.Json;

namespace Dynamic.Web.ContentParts
{
    public class FileContentPart : HttpContentPartBase
    {
        public byte[] File { get; set; }

        public FileContentPart()
            : this(null, null)
        {
        }

        public FileContentPart(byte[] file)
            : this(file, null)
        {
        }

        public FileContentPart(byte[] file, string filename)
            : this(file, filename, "text/plain")
        {
        }

        public FileContentPart(byte[] file, string filename, string contenttype)
            : base(contenttype)
        {
            File = file;
            FileName = filename;
        }

        internal static void BuildMultipart(IHttpContentPart part, string name, string boundary, Stream stream, Encoding encoding)
        {
            FileContentPart contentPart = (FileContentPart)part;

            // Add just the first part of this param, since we will write the file data directly to the Stream
            string header = string.Format(
                "--{0}\r\nContent-Disposition: {1}; name=\"{2}\"; filename=\"{3}\";\r\nContent-Type: {4}\r\n\r\n",
                boundary,
                contentPart.ContentDisposition,
                name,
                contentPart.FileName ?? name,
                contentPart.ContentType ?? "application/octet-stream"
            );

            stream.Write(encoding.GetBytes(header), 0, encoding.GetByteCount(header));

            // Write the file data directly to the Stream, rather than serializing it to a string.
            stream.Write(contentPart.File, 0, contentPart.File.Length);
        }

        internal static void Serialize(JsonSerializer.ObjectWriter writer, object x)
        {
            FileContentPart contentPart = (FileContentPart)x;
            if (!string.IsNullOrEmpty(contentPart.ContentType))
            {
                writer.WritePairFast("contentType", contentPart.ContentType);
            }

            if (!string.IsNullOrEmpty(contentPart.FileName))
            {
                writer.WritePairFast("contentType", contentPart.FileName);
            }

            writer.WritePairFast("content", Convert.ToBase64String(contentPart.File, Base64FormattingOptions.None));
        }
    }
}
