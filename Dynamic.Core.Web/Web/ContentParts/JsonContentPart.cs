﻿using System;
using System.IO;
using System.Text;

using Dynamic.Json;
using Dynamic.Json.Composition;

namespace Dynamic.Web.ContentParts
{
    public class JsonContentPart : HttpContentPartBase
    {
        public object JsonValue { get; internal set; }

        public JsonContentPart()
            : base("application/json")
        {
            FileName = "json";
        }

        public JsonContentPart(object obj)
            : base("application/json")
        {
            FileName = "json";
            JsonValue = obj;
        }

        internal static void BuildMultipart(IHttpContentPart part, string name, string boundary, Stream stream, Encoding encoding)
        {
            JsonContentPart contentPart = (JsonContentPart)part;

            JsonParameters jsonParameters = new JsonParameters();
            jsonParameters.UseExtensions = false;
            jsonParameters.SerializeCaseNames = JsonKeys.LowerCamelCase;
            string jsonData = JsonDoctor.ToJson(contentPart.JsonValue, jsonParameters);
            string postData;
            if (!string.IsNullOrEmpty(contentPart.FileName))
            {
                postData = string.Format(
                    "--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\";\r\n\r\n{3}",
                    boundary,
                    name,
                    contentPart.FileName,
                    jsonData
                );
            }
            else
            {
                postData = string.Format(
                    "--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\";\r\n\r\n{3}",
                    boundary,
                    name,
                    name,
                    jsonData
                );
            }
            stream.Write(encoding.GetBytes(postData), 0, encoding.GetByteCount(postData));
        }

        internal static void Serialize(JsonSerializer.ObjectWriter writer, object x)
        {
            JsonContentPart contentPart = (JsonContentPart)x;
            if (!string.IsNullOrEmpty(contentPart.ContentType))
            {
                writer.WritePairFast("contentType", contentPart.ContentType);
            }

            if (!string.IsNullOrEmpty(contentPart.FileName))
            {
                writer.WritePairFast("contentType", contentPart.FileName);
            }

            writer.WritePair("content", contentPart.JsonValue);
        }
    }
}
