﻿using System;
using System.IO;
using System.Text;

using Dynamic.Json;

namespace Dynamic.Web.ContentParts
{
    public class PayloadContentPart : HttpContentPartBase
    {
        public Stream Payload { get; set; }
        public string Header { get; set; }
        public bool Encrypt { get; set; }

        public PayloadContentPart(Stream stream)
            : this(stream, null)
        {
        }

        public PayloadContentPart(Stream stream, string filename)
            : this(stream, filename, string.Empty)
        {
        }

        public PayloadContentPart(Stream stream, string filename, string contenttype)
            : this(stream, filename, contenttype, string.Empty)
        {
        }

        public PayloadContentPart(Stream stream, string filename, string contenttype, string header)
            : this(stream, filename, contenttype, header, false)
        {
        }

        public PayloadContentPart(Stream stream, string filename, string contenttype, string header, bool encrypt)
            : base(contenttype)
        {
            Payload = stream;
            FileName = filename;
            Header = header;
            Encrypt = encrypt;
        }

        protected override void OnDispose()
        {
            Payload.Dispose();
        }

        internal static void BuildMultipart(IHttpContentPart part, string name, string boundary, Stream stream, Encoding encoding)
        {
            PayloadContentPart contentPart = (PayloadContentPart)part;

            // Add just the first part of this param, since we will write the file data directly to the Stream
            string header = string.Format(
                "--{0}\r\nContent-Disposition: {1}; name=\"{2}\"; filename=\"{3}\"; header=\"{4}\"; encrypt=\"{5}\";\r\nContent-Type: {6}\r\n\r\n",
                boundary,
                contentPart.ContentDisposition,
                name,
                contentPart.FileName,
                contentPart.Header,
                contentPart.Encrypt ? "true" : "false",
                contentPart.ContentType ?? "application/octet-stream"
            );

            stream.Write(encoding.GetBytes(header), 0, encoding.GetByteCount(header));

            StreamReader reader = new StreamReader(contentPart.Payload);
            while (!reader.EndOfStream)
            {
                stream.WriteByte((byte)reader.Read());
            }
        }

        internal static void Serialize(JsonSerializer.ObjectWriter writer, object x)
        {
            PayloadContentPart contentPart = (PayloadContentPart)x;
            if (!string.IsNullOrEmpty(contentPart.ContentType))
            {
                writer.WritePairFast("contentType", contentPart.ContentType);
            }

            if (!string.IsNullOrEmpty(contentPart.FileName))
            {
                writer.WritePairFast("contentType", contentPart.FileName);
            }

            if (!string.IsNullOrEmpty(contentPart.Header))
            {
                writer.WritePairFast("payloadHeader", contentPart.Header);
            }

            byte[] inArray = new byte[(int)contentPart.Payload.Length];
            contentPart.Payload.Read(inArray, 0, (int)contentPart.Payload.Length);

            writer.WritePairFast("content", Convert.ToBase64String(inArray, Base64FormattingOptions.None));
        }
    }
}
