﻿using System;
using System.IO;
using System.Text;

using NAudio.Wave;

using Dynamic.Json;

namespace Dynamic.Web.ContentParts
{
    public class AudioWavContentPart : HttpContentPartBase
    {
        public WaveStream Stream { get; set; }
        public bool AsPayload { get; set; }
        public bool AsRaw { get; set; }

        private WaveFormat Format { get; set; }
        private IWaveProvider Provider { get; set; }

        public AudioWavContentPart()
            : base("audio/wav")
        {
            AsPayload = false;
            AsRaw = false;
            Stream = null;
            FileName = "";
        }

        public AudioWavContentPart(WaveStream stream)
            : this(stream, "unknown.wav")
        {
        }

        public AudioWavContentPart(WaveStream stream, WaveFormat format)
            : this(stream, "unknown.wav")
        {
            if (!Stream.WaveFormat.Equals(format))
            {
                Format = format;
                Provider = new MediaFoundationResampler(Stream, format);
            }
        }

        public AudioWavContentPart(WaveStream stream, string filename)
            : this(stream, filename, "audio/wav")
        {
        }

        public AudioWavContentPart(WaveStream stream, string filename, string contentType)
            : base(contentType)
        {
            AsPayload = false;
            AsRaw = false;
            Stream = stream;
            FileName = filename;
        }

        public AudioWavContentPart(string filename)
            : base("audio/wav")
        {
            AsPayload = false;
            AsRaw = false;
            FileName = Path.GetFileName(filename);
            Stream = new WaveFileReader(filename);
        }

        public AudioWavContentPart(string filename, WaveFormat format)
            : base("audio/wav")
        {
            AsPayload = false;
            AsRaw = false;
            FileName = Path.GetFileName(filename);
            Stream = new WaveFileReader(filename);
            if (!Stream.WaveFormat.Equals(format))
            {
                Format = format;
                Provider = new MediaFoundationResampler(Stream, format);
            }
        }

        protected override void OnDispose()
        {
            Stream.Dispose();
        }

        internal static void BuildMultipart(IHttpContentPart part, string name, string boundary, Stream stream, Encoding encoding)
        {
            AudioWavContentPart contentPart = (AudioWavContentPart)part;

            if (contentPart.Provider == null)
            {
                contentPart.Provider = contentPart.Stream;
            }
            if (contentPart.Format == null)
            {
                contentPart.Format = contentPart.Provider.WaveFormat;
            }

            // Add just the first part of this param, since we will write the file data directly to the Stream
            string header;
            if (!contentPart.AsPayload)
            {
                header = string.Format(
                    "--{0}\r\nContent-Disposition: {1}; name=\"{2}\"; filename=\"{3}\";\r\nContent-Type: {4}\r\n\r\n",
                    boundary,
                    contentPart.ContentDisposition,
                    name,
                    contentPart.FileName,
                    contentPart.ContentType ?? "audio/wav"
                );
            }
            else
            {
                MemoryStream ms = new MemoryStream();
                BinaryWriter writer = new BinaryWriter(ms, Encoding.ASCII);
                contentPart.Format.Serialize(writer);
                ms.Position = 0;
                header = string.Format(
                    "--{0}\r\nContent-Disposition: payload-data; name=\"{1}\"; filename=\"{2}\"; header=\"{3}\";\r\nContent-Type: {4}\r\n\r\n",
                    boundary,
                    name,
                    contentPart.FileName,
                    Convert.ToBase64String(ms.ToArray()),
                    contentPart.ContentType ?? "audio/wav"
                );
            }

            stream.Write(encoding.GetBytes(header), 0, encoding.GetByteCount(header));

            if (!contentPart.AsRaw)
            {
                using (var waveFileWriter = new WaveFileWriter(stream, contentPart.Format))
                {
                    long outputLength = 0;
                    var buffer = new byte[contentPart.Format.AverageBytesPerSecond * contentPart.Format.Channels];
                    while (true)
                    {
                        int bytesRead = contentPart.Provider.Read(buffer, 0, buffer.Length);
                        if (bytesRead == 0)
                        {
                            // end of source provider
                            break;
                        }
                        outputLength += bytesRead;
                        if (outputLength > int.MaxValue)
                        {
                            throw new InvalidOperationException("WAV File cannot be greater than 2GB. Check that sourceProvider is not an endless stream.");
                        }
                        waveFileWriter.Write(buffer, 0, bytesRead);
                    }
                }
            }
            else
            {
                long outputLength = 0;
                var buffer = new byte[contentPart.Format.AverageBytesPerSecond * contentPart.Format.Channels];
                while (true)
                {
                    int bytesRead = contentPart.Provider.Read(buffer, 0, buffer.Length);
                    if (bytesRead == 0)
                    {
                        // end of source provider
                        break;
                    }
                    outputLength += bytesRead;
                    if (outputLength > int.MaxValue)
                    {
                        throw new InvalidOperationException("WAV File cannot be greater than 2GB. Check that sourceProvider is not an endless stream.");
                    }
                    stream.Write(buffer, 0, bytesRead);
                }
            }
        }

        internal static void Serialize(JsonSerializer.ObjectWriter writer, object x)
        {
            AudioWavContentPart contentPart = (AudioWavContentPart)x;

            WaveStream waveStream = contentPart.Stream;
            WaveFormat waveFormat = contentPart.Format;
            IWaveProvider waveProvider = contentPart.Provider;

            if (waveFormat == null)
            {
                waveFormat = waveStream.WaveFormat;
            }

            if (waveProvider == null)
            {
                waveProvider = waveStream;
            }

            if (!string.IsNullOrEmpty(contentPart.ContentType))
            {
                writer.WritePairFast("contentType", contentPart.ContentType);
            }

            if (!string.IsNullOrEmpty(contentPart.FileName))
            {
                writer.WritePairFast("fileName", contentPart.FileName);
            }

            if (contentPart.AsPayload)
            {
                MemoryStream ms = new MemoryStream();
                BinaryWriter bw = new BinaryWriter(ms, Encoding.ASCII);
                waveFormat.Serialize(bw);
                ms.Position = 0;
                writer.WritePairFast("payloadHeader", Convert.ToBase64String(ms.ToArray(), Base64FormattingOptions.None));
            }

            {
                MemoryStream ms = new MemoryStream();
                if (contentPart.AsRaw)
                {
                    long outputLength = 0;
                    var buffer = new byte[waveFormat.AverageBytesPerSecond * waveFormat.Channels];
                    while (true)
                    {
                        int bytesRead = waveProvider.Read(buffer, 0, buffer.Length);
                        if (bytesRead == 0)
                        {
                            // end of source provider
                            break;
                        }
                        outputLength += bytesRead;
                        if (outputLength > int.MaxValue)
                        {
                            throw new InvalidOperationException("WAV File cannot be greater than 2GB. Check that sourceProvider is not an endless stream.");
                        }
                        ms.Write(buffer, 0, bytesRead);
                    }
                }
                else
                {
                    using (var waveFileWriter = new WaveFileWriter(ms, waveFormat))
                    {
                        long outputLength = 0;
                        var buffer = new byte[waveFormat.AverageBytesPerSecond * waveFormat.Channels];
                        while (true)
                        {
                            int bytesRead = waveProvider.Read(buffer, 0, buffer.Length);
                            if (bytesRead == 0)
                            {
                                // end of source provider
                                break;
                            }
                            outputLength += bytesRead;
                            if (outputLength > int.MaxValue)
                            {
                                throw new InvalidOperationException("WAV File cannot be greater than 2GB. Check that sourceProvider is not an endless stream.");
                            }
                            waveFileWriter.Write(buffer, 0, bytesRead);
                        }
                    }
                }
                writer.WritePairFast("content", Convert.ToBase64String(ms.ToArray(), Base64FormattingOptions.None));
            }
        }
    }
}
