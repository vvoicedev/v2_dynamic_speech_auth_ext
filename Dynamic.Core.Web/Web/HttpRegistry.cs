﻿using System;
using System.Collections.Generic;
using System.Net;
using Dynamic.Json;
using Dynamic.Web.ContentParsers;
using Dynamic.Web.ContentParts;

namespace Dynamic.Web
{
    public static class HttpRegistry
    {
        public static bool RegisterContentParser(string mimeType, IHttpContentParser parser)
        {
            if(!ContentParsers.ContainsKey(mimeType))
            {
                ContentParsers.Add(mimeType, parser);
                return true;
            }
            return false;
        }

        public static bool RegisterContentBuilder(Type type, HttpMultipartBuilder multipartBuilder, CustomObjectWriter objectWriter)
        {
            if (!ContentBuilders.ContainsKey(type))
            {
                ContentBuilders.Add(type, multipartBuilder);
                return JsonDoctor.RegisterCustomType(type, objectWriter);
            }
            return false;
        }

        internal static Dictionary<string, IHttpContentParser> ContentParsers { get; private set; }

        internal static Dictionary<Type, HttpMultipartBuilder> ContentBuilders { get; private set; }

        // Used to allow static initialization to occur
        internal static void Touch() { }

        static HttpRegistry()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            ContentParsers = new Dictionary<string, IHttpContentParser>();
            ContentParsers.Add("audio/wav", new AudioWavContentParser());
            ContentParsers.Add("audio/x-wav", new AudioWavContentParser());
            ContentParsers.Add("text/plain", new TextPlainContentParser());
            ContentParsers.Add("text/html", new TextHtmlContentParser());
            ContentParsers.Add("application/json", new JsonContentParser());
            ContentParsers.Add("application/properties", new PropertiesContentParser());
            ContentParsers.Add("application/xml", new XmlContentParser());
            ContentParsers.Add("*/*", new StreamContentParser());

            ContentBuilders = new Dictionary<Type, HttpMultipartBuilder>();
            ContentBuilders.Add(typeof(AudioWavContentPart), AudioWavContentPart.BuildMultipart);
            ContentBuilders.Add(typeof(FileContentPart), FileContentPart.BuildMultipart);
            ContentBuilders.Add(typeof(JsonContentPart), JsonContentPart.BuildMultipart);
            ContentBuilders.Add(typeof(PayloadContentPart), PayloadContentPart.BuildMultipart);

            JsonDoctor.RegisterCustomType(typeof(HttpContentParameters), HttpContentParametersSerialize);
            JsonDoctor.RegisterCustomType(typeof(AudioWavContentPart), AudioWavContentPart.Serialize);
            JsonDoctor.RegisterCustomType(typeof(FileContentPart), FileContentPart.Serialize);
            JsonDoctor.RegisterCustomType(typeof(JsonContentPart), JsonContentPart.Serialize);
            JsonDoctor.RegisterCustomType(typeof(PayloadContentPart), PayloadContentPart.Serialize);
        }

        #region

        private static void HttpContentParametersSerialize(JsonSerializer.ObjectWriter writer, object x)
        {
            HttpContentParameters contentParams = (HttpContentParameters)x;
            foreach (var item in contentParams.ToList())
            {
                writer.WritePair(item.Name, item.Value);
            }
        }

        #endregion
    }
}
