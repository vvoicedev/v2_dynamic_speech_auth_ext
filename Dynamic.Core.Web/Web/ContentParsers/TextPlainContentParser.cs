﻿using System.IO;
using System.Net;

namespace Dynamic.Web.ContentParsers
{
    internal class TextPlainContentParser : IHttpContentParser
    {
        public string ContentType { get { return "text/plain"; } }

        public object Parse(WebHeaderCollection headers, Stream stream)
        {
            return (new StreamReader(stream)).ReadToEnd();
        }
    }
}
