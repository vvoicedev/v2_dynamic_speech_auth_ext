﻿using System.IO;
using System.Net;

namespace Dynamic.Web.ContentParsers
{
    internal class StreamContentParser : IHttpContentParser
    {
        public string ContentType { get { return "*/*"; } }

        public object Parse(WebHeaderCollection headers, Stream stream)
        {
            MemoryStream memoryStream = new MemoryStream();
            StreamCopyTo(stream, memoryStream);
            return memoryStream;
        }

        private static void StreamCopyTo(Stream input, Stream output)
        {
            byte[] buffer = new byte[16 * 1024];
            int bytesRead;

            while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, bytesRead);
            }

            output.Position = 0;
        }

    }
}
