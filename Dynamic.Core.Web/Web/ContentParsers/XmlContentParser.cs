﻿using System.IO;
using System.Net;
using System.Xml;

namespace Dynamic.Web.ContentParsers
{
    internal class XmlContentParser : IHttpContentParser
    {
        public string ContentType { get { return "application/xml"; } }

        public object Parse(WebHeaderCollection headers, Stream stream)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(stream);
            return doc;
        }

    }
}
