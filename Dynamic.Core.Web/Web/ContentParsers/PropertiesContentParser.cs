﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace Dynamic.Web.ContentParsers
{
    internal class PropertiesContentParser : IHttpContentParser
    {
        public string ContentType { get { return "application/properties"; } }

        public object Parse(WebHeaderCollection headers, Stream stream)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            StreamReader reader = new StreamReader(stream);
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                if (!string.IsNullOrEmpty(line))
                {
                    int index = line.IndexOf(':');
                    if (index != -1)
                    {
                        string key = line.Substring(0, index);
                        string val = line.Substring(index + 1);
                        data[key.Trim()] = val.Trim();
                    }
                }
            }
            return data;
        }
    }
}
