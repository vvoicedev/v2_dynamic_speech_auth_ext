﻿using System;
using System.IO;
using System.Net;

using NAudio.Wave;

namespace Dynamic.Web.ContentParsers
{
    internal class AudioWavContentParser : IHttpContentParser
    {
        public string ContentType { get { return "audio/wav"; } }

        public object Parse(WebHeaderCollection headers, Stream stream)
        {
            MemoryStream memoryStream = new MemoryStream();
            StreamCopyTo(stream, memoryStream);
            memoryStream.Position = 0;
            string payloadHeader = headers["Payload-Header"];
            MemoryStream headerStream = new MemoryStream(Convert.FromBase64String(payloadHeader));
            BinaryReader reader = new BinaryReader(headerStream);
            WaveFormat format = new WaveFormat(reader);
            return new RawSourceWaveStream(memoryStream, format);
        }

        private static void StreamCopyTo(Stream input, Stream output)
        {
            byte[] buffer = new byte[16 * 1024];
            int bytesRead;

            while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, bytesRead);
            }
        }
    }
    
}
