﻿using System.IO;
using System.Net;

namespace Dynamic.Web.ContentParsers
{
    internal class TextHtmlContentParser : IHttpContentParser
    {
        public string ContentType { get { return "text/html"; } }

        public object Parse(WebHeaderCollection headers, Stream stream)
        {
            return (new StreamReader(stream)).ReadToEnd();
        }
    }
}
