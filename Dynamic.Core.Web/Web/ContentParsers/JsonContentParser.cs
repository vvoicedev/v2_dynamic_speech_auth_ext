﻿using System.IO;
using System.Net;

using Dynamic.Json;

namespace Dynamic.Web.ContentParsers
{
    internal class JsonContentParser : IHttpContentParser
    {
        public string ContentType { get { return "application/json"; } }

        public object Parse(WebHeaderCollection headers, Stream stream)
        {
            return JsonDoctor.Parse((new StreamReader(stream)).ReadToEnd());
        }
    }
}
