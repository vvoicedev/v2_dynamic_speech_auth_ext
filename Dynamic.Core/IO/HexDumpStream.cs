﻿using System;
using System.IO;
using System.Text;

namespace Dynamic.IO
{
    public class HexDumpStream : Stream
    {
        private int mBytesPerLine = 16;
        private Stream mStream;

        public HexDumpStream(Stream stream, int bytesPerLine = 16)
        {
            mStream = stream;
            mBytesPerLine = bytesPerLine;            
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            mStream.Write(buffer, offset, count);
            dumpHexBytes(buffer, offset, count, mBytesPerLine);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return mStream.Read( buffer, offset, count );
        }

        public override bool CanRead
        {
            get { return mStream.CanRead; }
        }

        public override bool CanWrite
        {
            get { return mStream.CanWrite; }
        }

        public override bool CanSeek
        {
            get { return mStream.CanSeek; }
        }

        public override long Length
        {
            get { return mStream.Length; }
        }

        public override long Position
        {
            get
            {
                return mStream.Position;
            }
            set
            {
                mStream.Position = value;
            }
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return mStream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            mStream.SetLength(value);
        }

        public override void Flush()
        {
            mStream.Flush();
        }

        private static char[] HexChars = "0123456789ABCDEF".ToCharArray();

        public static void dumpHexBytes(byte[] buffer, int offset, int count, int bytesPerLine = 16)
        {
            if (buffer == null) return;

            int firstHexColumn = 8 + 3;
            int firstCharColumn = firstHexColumn + bytesPerLine * 3 + (bytesPerLine - 1) + 2;
            int lineLength = firstCharColumn + bytesPerLine + Environment.NewLine.Length;
            char[] line = (new string(' ', lineLength - 2) + Environment.NewLine).ToCharArray();

            for (int i = offset; i < count; i += bytesPerLine)
            {
                line[0] = HexChars[(i >> 28) & 0xF];
                line[1] = HexChars[(i >> 24) & 0xF];
                line[2] = HexChars[(i >> 20) & 0xF];
                line[3] = HexChars[(i >> 16) & 0xF];
                line[4] = HexChars[(i >> 12) & 0xF];
                line[5] = HexChars[(i >> 8) & 0xF];
                line[6] = HexChars[(i >> 4) & 0xF];
                line[7] = HexChars[(i >> 0) & 0xF];

                int hexColumn = firstHexColumn;
                int charColumn = firstCharColumn;

                for (int j = 0; j < bytesPerLine; j++)
                {
                    if (j > 0 && (j & 7) == 0) hexColumn++;
                    if (i + j >= count)
                    {
                        line[hexColumn] = ' ';
                        line[hexColumn + 1] = ' ';
                        line[charColumn] = ' ';
                    }
                    else
                    {
                        byte b = buffer[i + j];
                        line[hexColumn] = HexChars[(b >> 4) & 0xF];
                        line[hexColumn + 1] = HexChars[b & 0xF];
                        line[charColumn] = (b < 32 ? '·' : (char)b);
                    }
                    hexColumn += 3;
                    charColumn++;
                }
                Console.Write(line);
            }
        }

    }
}
