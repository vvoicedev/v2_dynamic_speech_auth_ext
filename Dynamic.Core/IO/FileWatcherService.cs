﻿using System.Collections.Generic;
using Dynamic.Services;

namespace Dynamic.IO
{
    public class FileWatcherService : IFileWatcherManager
    {
        public IServiceManager ServiceManager { get; private set; }

        public void Initialize(IServiceManager srvMgr)
        {
            ServiceManager = srvMgr;
        }

        public void FinishedInitialization()
        {

        }

        public void Shutdown()
        {
            FileWatcherManager.Clear();
        }

        public void FinishedShutdown()
        {

        }

        public bool AddPath(string path, IFileWatcher watcher)
        {
            return FileWatcherManager.AddPath(path, watcher);
        }

        public bool AddPaths(Dictionary<string, IFileWatcher> watchers)
        {
            return FileWatcherManager.AddPaths(watchers);
        }

        public void RemovePath(string path, IFileWatcher watcher)
        {
            FileWatcherManager.RemovePath(path, watcher);
        }

        public void RemovePaths(Dictionary<string, IFileWatcher> watchers)
        {
            FileWatcherManager.RemovePaths(watchers);
        }
    }
}
