﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using Dynamic.Collections;

namespace Dynamic.IO
{
    public static class FileWatcherManager
    {
        #region Private Variables

        private static ThreadSafeDictionary<string, FileWatcher> mFileWatchers = new ThreadSafeDictionary<string, FileWatcher>();

        #endregion

        #region Methods

        public static bool AddPath(string path, IFileWatcher watcher)
        {
            FileWatcher fileWatcher;
            using (var fileWatchers = mFileWatchers.Lock())
            {
                if (fileWatchers.ContainsKey(path))
                {
                    fileWatcher = fileWatchers[path];
                }
                else
                {
                    fileWatcher = new FileWatcher(path);
                    fileWatchers.Add(path, fileWatcher);
                }
            }
            return fileWatcher.Add(watcher);
        }

        public static bool AddPaths(Dictionary<string, IFileWatcher> watchers)
        {
            using (var fileWatchers = mFileWatchers.Lock())
            {
                FileWatcher fileWatcher;
                foreach (var entry in watchers)
                {
                    if (fileWatchers.ContainsKey(entry.Key))
                    {
                        fileWatcher = fileWatchers[entry.Key];
                    }
                    else
                    {
                        fileWatcher = new FileWatcher(entry.Key);
                        fileWatchers.Add(entry.Key, fileWatcher);
                    }
                    if(!fileWatcher.Add(entry.Value))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public static void RemovePath(string path, IFileWatcher watcher)
        {
            using (var fileWatchers = mFileWatchers.Lock())
            {
                if (fileWatchers.ContainsKey(path))
                {
                    var fileWatcher = fileWatchers[path];
                    if(0 == fileWatcher.Remove(watcher))
                    {
                        fileWatcher.Dispose();
                        fileWatchers.Remove(path);
                    }
                }
            }
        }

        public static void RemovePaths(Dictionary<string, IFileWatcher> watchers)
        {
            using (var fileWatchers = mFileWatchers.Lock())
            {
                foreach (var entry in watchers)
                {
                    if (fileWatchers.ContainsKey(entry.Key))
                    {
                        var fileWatcher = fileWatchers[entry.Key];
                        if (0 == fileWatcher.Remove(entry.Value))
                        {
                            fileWatcher.Dispose();
                            fileWatchers.Remove(entry.Key);
                        }
                    }
                }
            }
        }

        public static void Clear()
        {
            using (var fileWatchers = mFileWatchers.Lock())
            {
                foreach (var watcher in fileWatchers)
                {
                    watcher.Value.Dispose();
                }
                fileWatchers.Clear();
            }
        }

        #endregion

        #region Private

        private class FileWatcher : IDisposable
        {
            internal FileSystemWatcher Watcher { get; private set; }
            internal ThreadSafeList<IFileWatcher> Callbacks { get; private set; }

            internal FileWatcher(string path)
            {
                Callbacks = new ThreadSafeList<IFileWatcher>();
                Watcher = new FileSystemWatcher();
                Watcher.Path = path;
                Watcher.NotifyFilter = NotifyFilters.Size | NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName;
                Watcher.Filter = "*.*";

                Watcher.Created += OnCreated;
                Watcher.Changed += OnChanged;
                Watcher.Deleted += OnDeleted;
                Watcher.Renamed += OnRenamed;
                Watcher.EnableRaisingEvents = true;
            }

            public bool Add(IFileWatcher watcher)
            {
                using (var callbacks = Callbacks.Lock())
                {
                    for(int i = 0; i < callbacks.Count; ++i)
                    {
                        if(callbacks[i] == watcher)
                        {
                            return false;
                        }
                    }
                    callbacks.Add(watcher);
                    RebuildExtensions(callbacks);
                }
                return true;
            }

            public int Remove(IFileWatcher watcher)
            {
                using (var callbacks = Callbacks.Lock())
                {
                    for(int i = 0; i < callbacks.Count; ++i)
                    {
                        var holder = callbacks[i];
                        if (holder == watcher)
                        {
                            callbacks.Remove(holder);
                            return callbacks.Count;
                        }
                    }
                    return callbacks.Count;
                }
            }

            public void Dispose()
            {
                Watcher.EnableRaisingEvents = false;
                Watcher.Created -= OnCreated;
                Watcher.Changed -= OnChanged;
                Watcher.Deleted -= OnDeleted;
                Watcher.Renamed -= OnRenamed;

                using(var callbacks = Callbacks.Lock())
                {
                    for (int i = 0; i < callbacks.Count; ++i)
                    {
                        callbacks[i].Dispose();
                    }
                }
            }

            private void RebuildExtensions(ILockedList<IFileWatcher> callbacks)
            {
                HashSet<string> set = new HashSet<string>();
                for (int i = 0; i < callbacks.Count; ++i)
                {
                    IFileWatcher holder = callbacks[i];
                    foreach (var ext in holder.Exts)
                    {
                        string extFilter = "*" + ext;
                        if (!set.Contains(extFilter))
                        {
                            set.Add(extFilter);
                        }
                    }
                }
                if (set.Count == 0)
                {
                    set.Add("*.*");
                }
                Watcher.Filter = string.Join(";", new List<string>(set).ToArray());
            }

            private void OnCreated(object source, FileSystemEventArgs e)
            {
                var ext = Path.GetExtension(e.FullPath);
                using (var callbacks = Callbacks.Lock())
                {
                    for (int i = 0; i < callbacks.Count; ++i)
                    {
                        if (callbacks[i].Exts.Contains("*.*") || callbacks[i].Exts.Contains(ext))
                        {
                            callbacks[i].OnCreated(source, e);
                        }
                    }
                }
            }

            private void OnChanged(object source, FileSystemEventArgs e)
            {
                var ext = Path.GetExtension(e.FullPath);
                using (var callbacks = Callbacks.Lock())
                {
                    for (int i = 0; i < callbacks.Count; ++i)
                    {
                        if (callbacks[i].Exts.Contains("*.*") || callbacks[i].Exts.Contains(ext))
                        {
                            callbacks[i].OnChanged(source, e);
                        }
                    }
                }
            }

            private void OnDeleted(object source, FileSystemEventArgs e)
            {
                var ext = Path.GetExtension(e.FullPath);
                using (var callbacks = Callbacks.Lock())
                {
                    for (int i = 0; i < callbacks.Count; ++i)
                    {
                        if (callbacks[i].Exts.Contains("*.*") || callbacks[i].Exts.Contains(ext))
                        {
                            callbacks[i].OnDeleted(source, e);
                        }
                    }
                }
            }

            private void OnRenamed(object source, RenamedEventArgs e)
            {
                var ext = Path.GetExtension(e.FullPath);
                using (var callbacks = Callbacks.Lock())
                {
                    for (int i = 0; i < callbacks.Count; ++i)
                    {
                        if (callbacks[i].Exts.Contains("*.*") || callbacks[i].Exts.Contains(ext))
                        {
                            callbacks[i].OnRenamed(source, e);
                        }
                    }
                }
            }
        }

        #endregion
    }
}
