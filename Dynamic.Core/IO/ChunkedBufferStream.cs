﻿using System;
using System.IO;

using Dynamic.Collections;
using Dynamic.Diagnostics;

namespace Dynamic.IO
{
    public class ChunkedBufferStream : Stream
    {
        #region Private Static

        private const uint CHUNK_SIZE = 8192;

        private static ObjectPool<ChunkedSegment> mChunkPool;

        static ChunkedBufferStream()
        {
            mChunkPool = new ObjectPool<ChunkedSegment>(5, null);
        }

        #endregion

        #region Private

        private ChunkedSegment mHead;
        private int mSize;

        #endregion

        #region

        public override bool CanRead => mSize != 0;

        public override bool CanSeek => false;

        public override bool CanWrite => true;

        public override long Length => mSize;

        public override long Position { get => 0; set => throw new NotImplementedException(); }

        public override void Flush()
        {
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            switch(origin)
            {
                case SeekOrigin.Current:
                    Skip((int)offset);
                    break;
                case SeekOrigin.End:
                    Skip((offset > mSize) ? mSize : mSize - (int)offset);
                    break;
                default:
                    throw new InvalidOperationException("Unable to seek the beginning of the stream.");
            }

            // the new position of the stream is always 0
            // chunked buffer stream consumes the buffer when
            // it is skipped
            return 0;
        }

        public override void SetLength(long value)
        {
        }

        public int GetByte(int pos)
        {
            AssertSizeAndCount(mSize, pos, 1, "ChunkedBufferStream.GetByte");

            for (var seg = mHead; seg != null; seg = seg.next)
            {
                int byteCount = seg.wpos - seg.rpos;
                if (pos < byteCount) return seg.data[seg.rpos + pos];
                pos -= byteCount;
            }
            throw new IndexOutOfRangeException();
        }

        public override int ReadByte()
        {
            Assert(mSize != 0, "ChunkedBufferStream.ReadByte");

            var seg = mHead;
            int rpos = seg.rpos;
            int wpos = seg.wpos;

            byte value = seg.data[rpos++];

            if(rpos == wpos)
            {
                mHead = PopSegment(seg);
                DiscardSegment(seg);
            }
            else
            {
                seg.rpos = rpos;
            }

            return value;
        }

        public int Read(byte[] buffer, int count)
        {
            return Read(buffer, 0, count);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            Assert(buffer != null && offset >= 0 && count >= 0, "ChunkedBufferStream.Read");

            var seg = mHead;
            if (seg == null) return 0;

            int toCopy = System.Math.Min(count, seg.wpos - seg.rpos);
            Buffer.BlockCopy(seg.data, seg.rpos, buffer, offset, toCopy);

            seg.rpos += toCopy;
            mSize -= toCopy;

            if(seg.rpos == seg.wpos)
            {
                mHead = PopSegment(seg);
                DiscardSegment(seg);
            }

            return toCopy;
        }

        public byte[] ReadAll()
        {
            int size = mSize;
            if(size > 0)
            {
                byte[] data = new byte[size];
                int offset = 0;
                while(offset < size)
                {
                    int amount = Read(data, offset, size - offset);
                    if (amount == -1) return data;
                    offset += amount;
                }
                return data;
            }
            return new byte[0];
        }

        public override void WriteByte(byte value)
        {
            var tail = GetWritableSegment(1);
            tail.data[tail.wpos++] = value;
            mSize++;
        }

        public void Write(byte[] buffer, int count)
        {
            Write(buffer, 0, count);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            Assert(buffer != null, "ChunkedBufferStream.Write");
            AssertCount(offset, count, "ChunkedBufferStream.Write");

            int limit = offset + count;
            while(offset < limit)
            {
                var tail = GetWritableSegment(1);
                int toCopy = (int)System.Math.Min(limit - offset, CHUNK_SIZE - tail.wpos);
                Buffer.BlockCopy(buffer, offset, tail.data, tail.wpos, toCopy);
                offset += toCopy;
                tail.wpos += toCopy;
            }

            mSize += count;
        }

        public void Skip(int count)
        {
            while(count > 0)
            {
                if (mHead == null)
                    return;

                int toSkip = System.Math.Min(count, mHead.wpos - mHead.rpos);
                mSize -= toSkip;
                count -= toSkip;
                mHead.rpos += toSkip;

                if(mHead.rpos == mHead.wpos)
                {
                    var toRecycle = mHead;
                    mHead = PopSegment(toRecycle);
                    DiscardSegment(toRecycle);
                }
            }
        }

        public void Clear()
        {
            Skip(mSize);
        }

        #endregion

        #region

        private void Assert(bool cond, string debugKey)
        {
            DebugManager.Assert(cond, new DebugKey(debugKey + ".Assert"));
        }

        private void AssertCount(int offset, int count, string debugKey)
        {
            DebugManager.Assert(offset <= count, new DebugKey(debugKey + ".AssertCount"));
        }

        private void AssertSizeAndCount(int size, int offset, int count, string debugKey)
        {
            DebugManager.Assert(offset > size || size - offset < count, new DebugKey(debugKey + ".AssertSizeAndCount"));
        }

        private ChunkedSegment GetWritableSegment(int minCapacity)
        {
            Assert(minCapacity >= 1 && minCapacity < CHUNK_SIZE, "ChunkedBufferStream.GetWritableSegment");

            ChunkedSegment tail = null;
            if (mHead == null)
            {
                tail = mChunkPool.Acquire();
                mHead = tail;
                mHead.prev = mHead.next = tail;
            }
            else
            {
                tail = mHead.prev;
                if(tail.wpos + minCapacity > CHUNK_SIZE)
                {
                    tail = PushSegment(tail, mChunkPool.Acquire());
                }
            }
            return tail;
        }

        private ChunkedSegment PushSegment(ChunkedSegment c, ChunkedSegment n)
        {
            n.prev = c;
            n.next = c.next;
            c.next.prev = n;
            c.next = n;
            return n;
        }

        private ChunkedSegment PopSegment(ChunkedSegment s)
        {
            ChunkedSegment result = s.next != s ? s.next : null;
            s.prev.next = s.next;
            s.next.prev = s.prev;
            s.prev = null;
            s.next = null;
            return result;
        }

        private void DiscardSegment(ChunkedSegment s)
        {
            Assert(s != null, "ChunkedBufferStream.DiscardSegment");
            Assert(s.prev == null && s.next == null, "ChunkedBufferStream.DiscardSegment");
            mChunkPool.Release(s);
        }

        #endregion

        #region

        class ChunkedSegment
        {
            internal ChunkedSegment prev;
            internal ChunkedSegment next;
            internal int rpos;
            internal int wpos;
            internal byte[] data;

            public ChunkedSegment()
            {
                prev = next = null;
                rpos = wpos = 0;
                data = new byte[CHUNK_SIZE];
            }
        }

        #endregion
    }
}
