﻿using System;

namespace Dynamic.Json.Composition
{
    [AttributeUsage(AttributeTargets.Property)]
    public class JsonPropertyAttribute : Attribute
    {
        public JsonPropertyAttribute()
        {
            PropertyType = null;
            Usage = JsonEnum.UseName;
        }

        public JsonPropertyAttribute(Type type)
        {
            PropertyType = type;
            Usage = JsonEnum.UseName;
        }

        public JsonPropertyAttribute(JsonEnum usage)
        {
            PropertyType = null;
            Usage = usage;
        }

        public JsonPropertyAttribute(Type type, JsonEnum usage)
        {
            PropertyType = type;
            Usage = usage;
        }

        public Type PropertyType { get; private set; }
        public JsonEnum Usage { get; private set; }
    }

}
