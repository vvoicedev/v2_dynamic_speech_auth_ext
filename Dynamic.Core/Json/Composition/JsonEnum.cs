﻿using System;

namespace Dynamic.Json.Composition
{
    public enum JsonEnum
    {
        UseValue,
        UseName
    }
}
