﻿using System;
using System.Reflection;

namespace Dynamic.Json.Composition
{

    [AttributeUsage(AttributeTargets.Property)]
    public class JsonFieldAttribute : Attribute
    {
        public JsonFieldAttribute()
        {
            PropertyType = null;
            Usage = JsonEnum.UseName;
        }

        public JsonFieldAttribute(Type type)
        {
            PropertyType = type;
            Usage = JsonEnum.UseName;
        }

        public JsonFieldAttribute(JsonEnum usage)
        {
            PropertyType = null;
            Usage = usage;
        }

        public JsonFieldAttribute(Type type, JsonEnum usage)
        {
            PropertyType = type;
            Usage = usage;
        }

        public Type PropertyType { get; private set; }
        public JsonEnum Usage { get; private set; }
    }

    public class FieldDefinition
    {
        public FieldDefinition(FieldInfo info, JsonFieldAttribute attr)
        {
            Info = info;
            PropertyType = attr.PropertyType;
            Usage = attr.Usage;
        }

        public FieldInfo Info { get; private set; }
        public Type PropertyType { get; private set; }
        public JsonEnum Usage { get; private set; }
    }
}
