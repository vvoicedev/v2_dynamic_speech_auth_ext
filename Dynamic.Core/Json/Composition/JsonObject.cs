﻿using System;

namespace Dynamic.Json.Composition
{
    public enum JsonKeys
    {
        AllUpper,
        AllLower,
        ProperCase,
        LowerCamelCase
    }

    [Flags]
    public enum JsonUsage
    {
        UseNullValues = 1,
        UseUTCDateTime = 2,
        UseValuesOfEnums = 4,
        UseFastGuid = 8,
        UseAnonymousTypes = 16,
        UseExtensions = 32,
        UseReadOnlyProperties = 64,
        UseGlobalTypes = 128,
        UseEscapedUnicode = 256,
        UseKVStyleStringDictionary = 512,
        UseParametricConstructorOverride = 1024,
        UseDateTimeMilliseconds = 2048,
        UseInlineCircularReferences = 4096,
        UseOptimizedDatasetSchema = 8192,
        UseDefaults = 443,
        UseAll = 16383,
        UseAsIs = 65535
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class JsonObjectAttribute : Attribute
    {
        public JsonObjectAttribute()
        {
            KeyCase = JsonKeys.ProperCase;
            Usage = JsonUsage.UseAsIs;
        }

        public JsonObjectAttribute(JsonKeys keyCase)
        {
            KeyCase = keyCase;
            Usage = JsonUsage.UseAsIs;
        }

        public JsonObjectAttribute(JsonUsage usage)
        {
            KeyCase = JsonKeys.ProperCase;
            Usage = usage;
        }

        public JsonObjectAttribute(JsonKeys keyCase, JsonUsage usage)
        {
            KeyCase = keyCase;
            Usage = usage;
        }

        public JsonKeys KeyCase { get; private set; }
        public JsonUsage Usage { get; private set; }
    }

}
