﻿using System.Collections.Generic;

namespace Dynamic.Json
{
    public sealed class JsonDataSetSchema
    {
        public List<string> Info { get; set; }
        public string Name { get; set; }
    }
}
