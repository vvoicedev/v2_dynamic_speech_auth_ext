﻿using System;
using System.Collections.Generic;

/// <summary>
/// fastJSON is copied into Dynamic.Json for convienece
/// 
/// Original Source: https://github.com/mgholam/fastJSON
/// 
/// Changes:
///     Swapped out SafeDictionary for Dynamic.Collections.ThreadSafeDictionary
///     Moved a few classes into their own files
///     CamelCased JSON to Json
///     Changed Json main class to JsonDoctor to prevent namespace clashes
///     Added a few custom object enhancements
///     Added Composition Handling for further customization of objects
/// </summary>

namespace Dynamic.Json
{
    public delegate string Serialize(object data);
    public delegate object Deserialize(string data);

    public delegate void CustomObjectWriter(JsonSerializer.ObjectWriter writer, object data);

    public delegate void CustomArrayWriter(JsonSerializer.ArrayWriter writer, object data);

    public class JsonDoctor
    {
        /// <summary>
        /// Globally set-able parameters for controlling the serializer
        /// </summary>
        public static JsonParameters Parameters = new JsonParameters();

        /// <summary>
        /// Create a formatted json string (beautified) from an object
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToNiceJson(object obj)
        {
            string s = ToJson(obj, Parameters); // use default params

            return Beautify(s);
        }

        /// <summary>
        /// Create a formatted json string (beautified) from an object
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static string ToNiceJson(object obj, JsonParameters param)
        {
            string s = ToJson(obj, param);

            return Beautify(s);
        }

        /// <summary>
        /// Create a json representation for an object
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToJson(object obj)
        {
            return ToJson(obj, Parameters);
        }

        /// <summary>
        /// Create a json representation for an object with parameter override on this call
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static string ToJson(object obj, JsonParameters param)
        {
            param.FixValues();
            Type t = null;

            if (obj == null)
                return "null";

            if (obj.GetType().IsGenericType)
                t = JsonReflection.Instance.GetGenericTypeDefinition(obj.GetType());
            if (t == typeof(Dictionary<,>) || t == typeof(List<>))
                param.UsingGlobalTypes = false;

            // FEATURE : enable extensions when you can deserialize anon types
            if (param.EnableAnonymousTypes) { param.UseExtensions = false; param.UsingGlobalTypes = false; }
            return new JsonSerializer(param).ConvertToJSON(obj);
        }

        /// <summary>
        /// Parse a json string and generate a Dictionary&lt;string,object&gt; or List&lt;object&gt; structure
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static object Parse(string json)
        {
            return new JsonParser(json).Decode();
        }

#if net4
        /// <summary>
        /// Create a .net4 dynamic object from the json string
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static dynamic ToDynamic(string json)
        {
            return new JsonDynamicObject(json);
        }
#endif

        /// <summary>
        /// Create a typed generic object from the json
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T ToObject<T>(string json)
        {
            return new JsonDeserializer(Parameters).ToObject<T>(json);
        }

        /// <summary>
        /// Create a typed generic object from the json with parameter override on this call
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static T ToObject<T>(string json, JsonParameters param)
        {
            return new JsonDeserializer(param).ToObject<T>(json);
        }

        /// <summary>
        /// Create an object from the json
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static object ToObject(string json)
        {
            return new JsonDeserializer(Parameters).ToObject(json, null);
        }

        /// <summary>
        /// Create an object from the json with parameter override on this call
        /// </summary>
        /// <param name="json"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static object ToObject(string json, JsonParameters param)
        {
            return new JsonDeserializer(param).ToObject(json, null);
        }

        /// <summary>
        /// Create an object of type from the json
        /// </summary>
        /// <param name="json"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static object ToObject(string json, Type type)
        {
            return new JsonDeserializer(Parameters).ToObject(json, type);
        }

        /// <summary>
        /// Fill a given object with the json represenation
        /// </summary>
        /// <param name="input"></param>
        /// <param name="json"></param>
        /// <returns></returns>
        public static object FillObject(object input, string json)
        {
            Dictionary<string, object> ht = new JsonParser(json).Decode() as Dictionary<string, object>;
            if (ht == null) return null;
            return new JsonDeserializer(Parameters).ParseDictionary(ht, null, input.GetType(), input);
        }

        /// <summary>
        /// Deep copy an object i.e. clone to a new object
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static object DeepCopy(object obj)
        {
            return new JsonDeserializer(Parameters).ToObject(ToJson(obj));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T DeepCopy<T>(T obj)
        {
            return new JsonDeserializer(Parameters).ToObject<T>(ToJson(obj));
        }

        /// <summary>
        /// Create a human readable string from the json 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string Beautify(string input)
        {
            return JsonFormatter.PrettyPrint(input);
        }

        /// <summary>
        /// Register custom type handlers for your own types not natively handled by fastJSON
        /// </summary>
        /// <param name="type"></param>
        /// <param name="serializer"></param>
        /// <param name="JsonDeserializer"></param>
        public static bool RegisterCustomType(Type type, Serialize serializer, Deserialize JsonDeserializer)
        {
            return JsonReflection.Instance.RegisterCustomType(type, serializer, JsonDeserializer);
        }

        /// <summary>
        /// Register custom object type serialize handler for your own types not natively handled by fastJSON
        /// </summary>
        /// <param name="type"></param>
        /// <param name="serializer"></param>
        public static bool RegisterCustomType(Type type, CustomObjectWriter serializer)
        {
            return JsonReflection.Instance.RegisterCustomObjectType(type, serializer);
        }

        /// <summary>
        /// Register custom array type serialize handler for your own types not natively handled by fastJSON
        /// </summary>
        /// <param name="type"></param>
        /// <param name="serializer"></param>
        public static bool RegisterCustomType(Type type, CustomArrayWriter serializer)
        {
            return JsonReflection.Instance.RegisterCustomArrayType(type, serializer);
        }

        /// <summary>
        /// Clear the internal reflection cache so you can start from new (you will loose performance)
        /// </summary>
        public static void ClearReflectionCache()
        {
            JsonReflection.Instance.ClearReflectionCache();
        }

        internal static long CreateLong(string s, int index, int count)
        {
            long num = 0;
            bool neg = false;
            for (int x = 0; x < count; x++, index++)
            {
                char cc = s[index];

                if (cc == '-')
                    neg = true;
                else if (cc == '+')
                    neg = false;
                else
                {
                    num *= 10;
                    num += (int)(cc - '0');
                }
            }
            if (neg) num = -num;

            return num;
        }
    }
}
