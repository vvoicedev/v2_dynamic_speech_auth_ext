﻿using System;
using System.Collections.Generic;

using Dynamic.Json.Composition;

namespace Dynamic.Json
{
    public sealed class JsonParameters
    {
        /// <summary>
        /// Use the optimized fast Dataset Schema format (default = True)
        /// </summary>
        public bool UseOptimizedDatasetSchema = true;
        /// <summary>
        /// Use the fast GUID format (default = True)
        /// </summary>
        public bool UseFastGuid = true;
        /// <summary>
        /// Serialize null values to the output (default = True)
        /// </summary>
        public bool SerializeNullValues = true;
        /// <summary>
        /// Use the UTC date format (default = True)
        /// </summary>
        public bool UseUTCDateTime = true;
        /// <summary>
        /// Show the readonly properties of types in the output (default = False)
        /// </summary>
        public bool ShowReadOnlyProperties = false;
        /// <summary>
        /// Use the $types extension to optimise the output json (default = True)
        /// </summary>
        public bool UsingGlobalTypes = true;
        /// <summary>
        /// Ignore case when processing json and deserializing 
        /// </summary>
        [Obsolete("Not needed anymore and will always match")]
        public bool IgnoreCaseOnDeserialize = false;
        /// <summary>
        /// Anonymous types have read only properties 
        /// </summary>
        public bool EnableAnonymousTypes = true;
        /// <summary>
        /// Enable fastJSON extensions $types, $type, $map (default = True)
        /// </summary>
        public bool UseExtensions = true;
        /// <summary>
        /// Use escaped unicode i.e. \uXXXX format for non ASCII characters (default = True)
        /// </summary>
        public bool UseEscapedUnicode = true;
        /// <summary>
        /// Output string key dictionaries as "k"/"v" format (default = False) 
        /// </summary>
        public bool KVStyleStringDictionary = false;
        /// <summary>
        /// Output Enum values instead of names (default = False)
        /// </summary>
        public JsonEnum UseValuesOfEnums = JsonEnum.UseName;
        /// <summary>
        /// Ignore attributes to check for (default : XmlIgnoreAttribute, NonSerialized)
        /// </summary>
        public List<Type> IgnoreAttributes = new List<Type> { typeof(System.Xml.Serialization.XmlIgnoreAttribute), typeof(NonSerializedAttribute) };
        /// <summary>
        /// If you have parametric and no default constructor for you classes (default = False)
        /// 
        /// IMPORTANT NOTE : If True then all initial values within the class will be ignored and will be not set
        /// </summary>
        public bool ParametricConstructorOverride = false;
        /// <summary>
        /// Serialize DateTime milliseconds i.e. yyyy-MM-dd HH:mm:ss.nnn (default = false)
        /// </summary>
        public bool DateTimeMilliseconds = false;
        /// <summary>
        /// Maximum depth for circular references in inline mode (default = 20)
        /// </summary>
        public byte SerializerMaxDepth = 20;
        /// <summary>
        /// Inline circular or already seen objects instead of replacement with $i (default = False) 
        /// </summary>
        public bool InlineCircularReferences = false;
        /// <summary>
        /// Save property/field names as lowercase (default = false)
        /// </summary>
        public bool SerializeToLowerCaseNames = false;
        /// <summary>
        /// Save property/field names as a specific typography case
        /// </summary>
        public JsonKeys SerializeCaseNames = JsonKeys.ProperCase;

        public void FixValues()
        {
            if (UseExtensions == false) // disable conflicting params
            {
                UsingGlobalTypes = false;
                InlineCircularReferences = true;
            }
            if (EnableAnonymousTypes)
            {
                ShowReadOnlyProperties = true;
            }
            if (SerializeToLowerCaseNames == true)
            {
                SerializeCaseNames = JsonKeys.AllLower;
            }
        }

        public void ApplyUsage(JsonUsage usage)
        {
            if (usage != JsonUsage.UseAsIs)
            {
                UseOptimizedDatasetSchema = ((usage & JsonUsage.UseOptimizedDatasetSchema) == JsonUsage.UseOptimizedDatasetSchema);
                UseFastGuid = ((usage & JsonUsage.UseFastGuid) == JsonUsage.UseFastGuid);
                SerializeNullValues = ((usage & JsonUsage.UseNullValues) == JsonUsage.UseNullValues);
                UseUTCDateTime = ((usage & JsonUsage.UseUTCDateTime) == JsonUsage.UseUTCDateTime);
                ShowReadOnlyProperties = ((usage & JsonUsage.UseReadOnlyProperties) == JsonUsage.UseReadOnlyProperties);
                UsingGlobalTypes = ((usage & JsonUsage.UseGlobalTypes) == JsonUsage.UseGlobalTypes);
                EnableAnonymousTypes = ((usage & JsonUsage.UseAnonymousTypes) == JsonUsage.UseAnonymousTypes);
                UseExtensions = ((usage & JsonUsage.UseExtensions) == JsonUsage.UseExtensions);
                UseEscapedUnicode = ((usage & JsonUsage.UseEscapedUnicode) == JsonUsage.UseEscapedUnicode);
                KVStyleStringDictionary = ((usage & JsonUsage.UseKVStyleStringDictionary) == JsonUsage.UseKVStyleStringDictionary);
                UseValuesOfEnums = ((usage & JsonUsage.UseValuesOfEnums) == JsonUsage.UseValuesOfEnums) ? JsonEnum.UseValue : JsonEnum.UseName;
                ParametricConstructorOverride = ((usage & JsonUsage.UseParametricConstructorOverride) == JsonUsage.UseParametricConstructorOverride);
                DateTimeMilliseconds = ((usage & JsonUsage.UseDateTimeMilliseconds) == JsonUsage.UseDateTimeMilliseconds);
                InlineCircularReferences = ((usage & JsonUsage.UseInlineCircularReferences) == JsonUsage.UseInlineCircularReferences);
                FixValues();
            }
        }

        public JsonUsage ExtractUsage()
        {
            JsonUsage usage = 0;
            usage |= UseOptimizedDatasetSchema ? JsonUsage.UseOptimizedDatasetSchema : 0;
            usage |= UseFastGuid ? JsonUsage.UseFastGuid : 0;
            usage |= SerializeNullValues ? JsonUsage.UseNullValues : 0;
            usage |= UseUTCDateTime ? JsonUsage.UseUTCDateTime : 0;
            usage |= ShowReadOnlyProperties ? JsonUsage.UseReadOnlyProperties : 0;
            usage |= UsingGlobalTypes ? JsonUsage.UseGlobalTypes : 0;
            usage |= EnableAnonymousTypes ? JsonUsage.UseAnonymousTypes : 0;
            usage |= UseExtensions ? JsonUsage.UseExtensions : 0;
            usage |= UseEscapedUnicode ? JsonUsage.UseEscapedUnicode : 0;
            usage |= KVStyleStringDictionary ? JsonUsage.UseKVStyleStringDictionary : 0;
            usage |= UseValuesOfEnums == JsonEnum.UseValue ? JsonUsage.UseValuesOfEnums : 0;
            usage |= ParametricConstructorOverride ? JsonUsage.UseParametricConstructorOverride : 0;
            usage |= DateTimeMilliseconds ? JsonUsage.UseDateTimeMilliseconds : 0;
            usage |= InlineCircularReferences ? JsonUsage.UseInlineCircularReferences : 0;
            return usage;
        }

        public JsonParameters()
        {

        }

        /// <summary>
        /// Deep Copy
        /// </summary>
        /// <param name="parms"></param>
        public JsonParameters(JsonParameters parms)
        {
            UseOptimizedDatasetSchema = parms.UseOptimizedDatasetSchema;
            UseFastGuid = parms.UseFastGuid;
            SerializeNullValues = parms.SerializeNullValues;
            UseUTCDateTime = parms.UseUTCDateTime;
            ShowReadOnlyProperties = parms.ShowReadOnlyProperties;
            UsingGlobalTypes = parms.UsingGlobalTypes;
            EnableAnonymousTypes = parms.EnableAnonymousTypes;
            UseExtensions = parms.UseExtensions;
            UseEscapedUnicode = parms.UseEscapedUnicode;
            KVStyleStringDictionary = parms.KVStyleStringDictionary;
            UseValuesOfEnums = parms.UseValuesOfEnums;
            IgnoreAttributes = new List<Type>(parms.IgnoreAttributes);
            ParametricConstructorOverride = parms.ParametricConstructorOverride;
            DateTimeMilliseconds = parms.DateTimeMilliseconds;
            SerializerMaxDepth = parms.SerializerMaxDepth;
            InlineCircularReferences = parms.InlineCircularReferences;
            SerializeToLowerCaseNames = parms.SerializeToLowerCaseNames;
            SerializeCaseNames = parms.SerializeCaseNames;
            FixValues();
        }

        public JsonParameters(JsonUsage usage)
        {
            ApplyUsage(usage);
        }

        public JsonParameters(JsonKeys keys, JsonUsage usage)
        {
            SerializeCaseNames = keys;
            ApplyUsage(usage);
        }
    }
}
