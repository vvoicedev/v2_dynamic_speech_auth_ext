﻿using System;
using System.Collections.Generic;

namespace Dynamic.Math
{
    public class Series : ISeries<double>
    {
        private List<double> mSamples;

        public Series()
        {
            Sum = 0.0;
            SumSquared = 0.0;
            mSamples = new List<double>();
        }
        
        public Series(int capacity)
        {
            Sum = 0.0;
            SumSquared = 0.0;
            mSamples = new List<double>(capacity);
        }

        public int Count => mSamples.Count;

        public int Capacity => mSamples.Capacity;

        public void Add(double sample)
        {
            mSamples.Add(sample);
            Sum += sample;
            SumSquared += (sample * sample);
        }

        public double Get(int index)
        {
            if (Count == 0)
                return 0;

            if (index < 0 || index >= Count)
                throw new IndexOutOfRangeException();

            return mSamples[index];
        }

        public double GetValue(int index)
        {
            if (Count == 0)
                return 0;

            if (index < 0 || index >= Count)
                throw new IndexOutOfRangeException();

            return mSamples[index];
        }

        public double Sum { get; private set; }

        public double SumSquared { get; private set; }

        public double Mean { get { return Count == 0 ? 0 : (Sum / Count); } }

        public void Clear()
        {
            Sum = 0.0;
            SumSquared = 0.0;
        }

        public bool IsFull { get { return Count == Capacity; } }

    }
}
