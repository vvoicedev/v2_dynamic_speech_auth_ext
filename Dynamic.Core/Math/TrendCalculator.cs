﻿using System;

using Dynamic.Extensions;

namespace Dynamic.Math
{
    public class TrendCalculator<T>
        where T : IComparable
    {
        public enum Trend
        {
            Undetermined,
            Stagnant,
            SlightlyIncreasing,
            Increasing,
            ConstantlyIncreasing,
            SlightlyDecreasing,
            Decreasing,
            ConstantlyDecreasing,
            Balanced
        }

        public struct SimpleTrendData
        {
            public int Increasing;
            public int Decreasing;
            public int Stagnant;
            public Trend Trend;

            public override string ToString()
            {
                return string.Format("I({0}) D({1}) S({2}) Trend: {3}", Increasing, Decreasing, Stagnant, Trend);
            }
        }

        private int mIncreasing;
        private int mDecreasing;

        public ISeries<T> Series { get; private set; }

        public TrendCalculator(ISeries<T> series)
        {
            Series = series;
            mIncreasing = 0;
            mDecreasing = 0;
        }

        public SimpleTrendData ComputeTrend()
        {
            SimpleTrendData data = new SimpleTrendData
            {
                Increasing = 0,
                Decreasing = 0,
                Stagnant = 0,
                Trend = Trend.Undetermined
            };

            if (Series.Count < 2)
                return data;

            T sample_b;
            T sample_a = Series.Get(0);
            for (int i = 1; i < Series.Count - 1; ++i)
            {
                sample_b = Series.Get(i);

                int test = sample_a.CompareTo(sample_b);
                if (1 == test)
                    ++data.Decreasing;
                else if (-1 == test)
                    ++data.Increasing;
                else
                    ++data.Stagnant;

                sample_a = sample_b;
            }

            if (data.Stagnant == 0)
            {
                if (data.Increasing == 0 && data.Decreasing == 0)
                    data.Trend = Trend.Stagnant;
                else if (data.Increasing != 0 && data.Decreasing == 0)
                    data.Trend = Trend.ConstantlyIncreasing;
                else if (data.Increasing == 0 && data.Decreasing != 0)
                    data.Trend = Trend.ConstantlyDecreasing;
                else if (data.Increasing > data.Decreasing)
                    data.Trend = Trend.Increasing;
                else if (data.Increasing < data.Decreasing)
                    data.Trend = Trend.Decreasing;
                else
                    data.Trend = Trend.Balanced;
            }
            else
            {
                if (data.Increasing == 0 && data.Decreasing == 0)
                    data.Trend = Trend.Stagnant;
                else if (data.Increasing != 0 && data.Decreasing == 0)
                    data.Trend = Trend.ConstantlyIncreasing;
                else if (data.Increasing == 0 && data.Decreasing != 0)
                    data.Trend = Trend.ConstantlyDecreasing;
                else if (data.Increasing > data.Decreasing)
                    data.Trend = Trend.SlightlyIncreasing;
                else if (data.Increasing < data.Decreasing)
                    data.Trend = Trend.SlightlyDecreasing;
                else
                    data.Trend = Trend.Balanced;
            }

            return data;
        }

        public Trend ComputeComplexTrend()
        {
            if (Series.Count < 2)
                return Trend.Undetermined;

            double slope = Series.ComputeSlope();

            if (slope > 0)
                ++mIncreasing;
            else if (slope < 0)
                ++mDecreasing;

            return mIncreasing == mDecreasing ? Trend.Stagnant
                : mIncreasing > mDecreasing ? Trend.Increasing
                : Trend.Decreasing;
        }
    }
}
