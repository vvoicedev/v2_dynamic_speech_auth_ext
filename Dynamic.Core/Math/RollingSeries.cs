﻿namespace Dynamic.Math
{
    public class RollingSeries : ISeries<double>
    {
        private int mNextIndex;
        private double[] mSamples;

        public RollingSeries(int maxSamples)
        {
            Sum = 0.0;
            SumSquared = 0.0;
            Count = 0;
            Capacity = maxSamples;

            mNextIndex = 0;
            mSamples = new double[maxSamples];
        }

        public int Count { get; private set; }

        public int Capacity { get; private set; }

        public void Add(double sample)
        {
            if (Count == Capacity)
            {
                double sample_to_remove = mSamples[mNextIndex];
                Sum -= sample_to_remove;
                SumSquared -= (sample_to_remove * sample_to_remove);
            }
            else
            {
                ++Count;
            }

            mSamples[mNextIndex] = sample;
            Sum += sample;
            SumSquared += (sample * sample);
            mNextIndex = (mNextIndex + 1) % Capacity;
        }

        public double Get(int index)
        {
            if (Count == 0)
                return 0;

            return mSamples[(mNextIndex + index + 1) % Count];
        }

        public double GetValue(int index)
        {
            if (Count == 0)
                return 0;

            return mSamples[(mNextIndex + index + 1) % Count];
        }

        public double Sum { get; private set; }

        public double SumSquared { get; private set; }

        public double Mean { get { return Count == 0 ? 0 : (Sum / Count); } }

        public void Clear()
        {
            Sum = 0.0;
            SumSquared = 0.0;
            Count = 0;
            mNextIndex = 0;
        }

        public bool IsFull { get { return Count == Capacity; } }

    }
}
