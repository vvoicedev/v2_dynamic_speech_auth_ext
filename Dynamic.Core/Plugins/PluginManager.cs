﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

using Dynamic.Diagnostics;
using Dynamic.Extensions;

using Dynamic.Plugins.Composition;

namespace Dynamic.Plugins
{
    public class PluginManager<TPlugin> : IPluginManager
        where TPlugin : IPlugin
    {
        private PluginRegistry mPluginRegistry = new PluginRegistry();
        private List<Assembly> mPluginAssemblies = new List<Assembly>();
        private List<string> mPluginFiles = new List<string>();

        public PluginManager()
        {
        }

        public void InitializePlugins(string pluginFile)
        {
            if (File.Exists(pluginFile))
            {
                string[] plugins = File.ReadAllLines(pluginFile);
                InitializePlugins(plugins.ToList());
            }
        }

        public void InitializePlugins(List<string> pluginPaths)
        {
            foreach (string plugin in pluginPaths.Where(p => !p.IsNullOrWhiteSpace()))
            {
                RegisterPlugin(plugin);
            }
        }

        public void UpdatePlugins(string pluginFile)
        {
            if (File.Exists(pluginFile))
            {
                string[] plugins = File.ReadAllLines(pluginFile);
                UpdatePlugins(plugins.ToList());
            }
        }

        public void UpdatePlugins(List<string> pluginPaths)
        {
            foreach (string plugin in pluginPaths.Where(p => !p.IsNullOrWhiteSpace() && !mPluginFiles.Contains(p) && !p.BeginsWith("#")))
            {
                RegisterPlugin(plugin);
            }
        }

        public void ClearPlugins()
        {
            mPluginFiles.Clear();
            foreach(var plugin in mPluginRegistry)
            {
                (plugin as PluginDefinition<TPlugin>)?.Destroy();
            }
            mPluginRegistry.Clear();
            mPluginAssemblies.Clear();
        }

        public TPlugin FindPlugin(string pluginName)
        {
            string[] parts = pluginName.Split('.');
            if (parts.Length == 0)
            {
                throw new Exception("Empty Plugin Name.");
            }

            PluginDefinition<TPlugin> pluginDefinition = mPluginRegistry.Find(parts[0]);
            if (pluginDefinition == null)
            {
                throw new Exception("Plugin '" + parts[0] + "' Not Defined.");
            }

            return pluginDefinition.CreateOrGet();
        }

        public Type FindPluginType(string pluginName)
        {
            string[] parts = pluginName.Split('.');
            if (parts.Length == 0)
            {
                throw new Exception("Empty Plugin Name.");
            }

            PluginDefinition<TPlugin> pluginDefinition = mPluginRegistry.Find(parts[0]);
            if (pluginDefinition == null)
            {
                throw new Exception("Plugin '" + parts[0] + "' Not Defined.");
            }

            return pluginDefinition.PluginType;
        }

        public IPlugin FindPluginObject(string pluginName)
        {
            string[] parts = pluginName.Split('.');
            if (parts.Length == 0)
            {
                throw new Exception("Empty Plugin Name.");
            }

            PluginDefinition<TPlugin> pluginDefinition = mPluginRegistry.Find(parts[0]);
            if (pluginDefinition == null)
            {
                throw new Exception("Plugin '" + parts[0] + "' Not Defined.");
            }

            return pluginDefinition.CreateOrGet();
        }

        public IPluginMethod FindPluginObjectMethod(string pluginMethod)
        {
            string[] parts = pluginMethod.Split('.');
            if (parts.Length == 0)
            {
                throw new Exception("Empty Plugin Method Name.");
            }

            string pluginName;
            string methodName;
            if (parts.Length == 1)
            {
                pluginName = parts[0];
                methodName = parts[0];
            }
            else if (parts.Length == 2)
            {
                pluginName = parts[0];
                methodName = parts[1];
            }
            else if (parts.Length == 3)
            {
                pluginName = parts[0];
                methodName = parts[1] + "." + parts[2];
            }
            else
            {
                throw new Exception("Invalid Plugin Method Name.");
            }

            PluginDefinition<TPlugin> pluginDefinition = mPluginRegistry.Find(pluginName);
            if (pluginDefinition == null)
            {
                throw new Exception("Plugin Not Defined.");
            }

            MethodDefinition methodDefinition = pluginDefinition.Methods[methodName];
            if (methodDefinition == null)
            {
                throw new Exception("Plugin.Method Not Defined.");
            }

            return new PluginMethod(pluginDefinition.CreateOrGet(), methodDefinition.Method);
        }

        public TPlugin FindSingletonPlugin(string pluginName)
        {
            string[] parts = pluginName.Split('.');
            if (parts.Length == 0)
            {
                throw new Exception("Empty Plugin Name.");
            }

            PluginDefinition<TPlugin> pluginDefinition = mPluginRegistry.FindSingleton(parts[0]);
            if (pluginDefinition == null)
            {
                throw new Exception("Plugin Singleton '" + parts[0] + "' Not Defined.");
            }

            return pluginDefinition.Get();
        }

        public Type FindSingletonPluginType(string pluginName)
        {
            string[] parts = pluginName.Split('.');
            if (parts.Length == 0)
            {
                throw new Exception("Empty Plugin Name.");
            }

            PluginDefinition<TPlugin> pluginDefinition = mPluginRegistry.FindSingleton(parts[0]);
            if (pluginDefinition == null)
            {
                throw new Exception("Plugin Singleton '" + parts[0] + "' Not Defined.");
            }

            return pluginDefinition.PluginType;
        }

        public IPlugin FindSingletonPluginObject(string pluginName)
        {
            string[] parts = pluginName.Split('.');
            if (parts.Length == 0)
            {
                throw new Exception("Empty Plugin Name.");
            }

            PluginDefinition<TPlugin> pluginDefinition = mPluginRegistry.FindSingleton(parts[0]);
            if (pluginDefinition == null)
            {
                throw new Exception("Plugin Singleton '" + parts[0] + "' Not Defined.");
            }

            return pluginDefinition.Get();
        }

        public IPluginMethod FindSingletonPluginObjectMethod(string pluginMethod)
        {
            string[] parts = pluginMethod.Split('.');
            if (parts.Length == 0)
            {
                throw new Exception("Empty Plugin Method Name.");
            }

            string pluginName;
            string methodName;
            if (parts.Length == 1)
            {
                pluginName = parts[0];
                methodName = parts[0];
            }
            else if (parts.Length == 2)
            {
                pluginName = parts[0];
                methodName = parts[1];
            }
            else if (parts.Length == 3)
            {
                pluginName = parts[0];
                methodName = parts[1] + "." + parts[2];
            }
            else
            {
                throw new Exception("Invalid Plugin Method Name.");
            }

            PluginDefinition<TPlugin> pluginDefinition = mPluginRegistry.FindSingleton(pluginName);
            if (pluginDefinition == null)
            {
                throw new Exception("Plugin Singleton Not Defined.");
            }

            MethodDefinition methodDefinition = pluginDefinition.Methods[methodName];
            if (methodDefinition == null)
            {
                throw new Exception("(Plugin Singleton).Method Not Defined.");
            }

            return new PluginMethod(pluginDefinition.Get(), methodDefinition.Method);
        }
        
        public string[] PluginNames
        {
            get
            {
                return mPluginRegistry.EnumerateKeys();
            }
        }

        public bool RegisterPlugin(string plugin)
        {
            try
            {
                Assembly assy = Assembly.LoadFrom(plugin);
                mPluginAssemblies.Add(assy);

                assy.GetTypes().ForEach(t =>
                {
                    var def = PluginDefinition<TPlugin>.Create(t, plugin);
                    if (def != null)
                    {
                        mPluginRegistry.Add(def);
                    }
                });

                mPluginFiles.Add(plugin);
                return true;
            }
            catch (Exception ex)
            {
                DebugManager.WarnException(plugin, ex, new DebugKey("PluginManager_LoadError"));
            }
            return false;
        }

        public bool RegisterPlugin(Type plugin)
        {
            try
            {
                var def = PluginDefinition<TPlugin>.Create(plugin);
                if (def != null)
                {
                    mPluginRegistry.Add(def);
                    return true;
                }
            }
            catch (Exception ex)
            {
                DebugManager.WarnException(ex, new DebugKey("PluginManager_LoadError"));
            }
            return false;
        }

        public bool RegisterPlugin(TPlugin plugin)
        {
            try
            {
                var def = PluginDefinition<TPlugin>.Create(plugin);
                if (def != null)
                {
                    mPluginRegistry.Add(def);
                    return true;
                }
            }
            catch (Exception ex)
            {
                DebugManager.WarnException(ex, new DebugKey("PluginManager_LoadError"));
            }
            return false;
        }

        #region Private Classes

        private class PluginRegistry
            : System.Collections.CollectionBase
        {
            public void Add(PluginDefinition<TPlugin> definition)
            {
                var def = Find(definition.Name);
                if(def != null)
                {
                    throw new Exception("Duplicate Plugin Name Found: " + definition.Name);
                }
                List.Add(definition);
            }

            public void Remove(PluginDefinition<TPlugin> definition)
            {
                List.Remove(definition);
                definition.Destroy();
            }

            public PluginDefinition<TPlugin> Find(string pluginOrPath)
            {
                foreach (PluginDefinition<TPlugin> definition in List)
                {
                    if ((definition.Name.Equals(pluginOrPath)) || definition.AssemblyPath.Equals(pluginOrPath))
                    {
                        return definition;
                    }
                }
                return null;
            }

            public PluginDefinition<TPlugin> FindSingleton(string pluginOrPath)
            {
                foreach (PluginDefinition<TPlugin> definition in List)
                {
                    if (((definition.Name.Equals(pluginOrPath)) || definition.AssemblyPath.Equals(pluginOrPath)) &&
                        definition.IsSingleton)
                    {
                        return definition;
                    }
                }
                return null;
            }

            public string[] EnumerateKeys()
            {
                int i = 0;
                string[] keys = new string[List.Count];
                foreach (PluginDefinition<TPlugin> definition in List)
                {
                    keys[i++] = definition.Name;
                }
                return keys;
            }
        }

        #endregion
    }

    public class PluginManager<TPlugin, TRequest, TResponse> : IPluginManager
        where TPlugin : IPlugin
        where TRequest : IPluginRequest
        where TResponse : IPluginResponse
    {
        private PluginRegistry mPluginRegistry = new PluginRegistry();
        private List<Assembly> mPluginAssemblies = new List<Assembly>();
        private List<string> mPluginFiles = new List<string>();

        public PluginManager()
        {
        }

        public void InitializePlugins(string pluginFile)
        {
            if (File.Exists(pluginFile))
            {
                string[] plugins = File.ReadAllLines(pluginFile);
                InitializePlugins(plugins.ToList());
            }
        }

        public void InitializePlugins(List<string> pluginPaths)
        {
            foreach (string plugin in pluginPaths.Where(p => !p.IsNullOrWhiteSpace()))
            {
                RegisterPlugin(plugin);
            }
        }

        public void UpdatePlugins(string pluginFile)
        {
            if (File.Exists(pluginFile))
            {
                string[] plugins = File.ReadAllLines(pluginFile);
                UpdatePlugins(plugins.ToList());
            }
        }

        public void UpdatePlugins(List<string> pluginPaths)
        {
            foreach (string plugin in pluginPaths.Where(p => !p.IsNullOrWhiteSpace() && !mPluginFiles.Contains(p) && !p.BeginsWith("#")))
            {
                RegisterPlugin(plugin);
            }
        }

        public void ClearPlugins()
        {
            mPluginFiles.Clear();
            foreach (var plugin in mPluginRegistry)
            {
                (plugin as PluginDefinition<TPlugin>)?.Destroy();
            }
            mPluginRegistry.Clear();
            mPluginAssemblies.Clear();
        }

        public TPlugin FindPlugin(string pluginName)
        {
            string[] parts = pluginName.Split('.');
            if (parts.Length == 0)
            {
                throw new Exception("Empty Plugin Name.");
            }

            PluginDefinition<TPlugin, TRequest, TResponse> pluginDefinition = mPluginRegistry.Find(parts[0]);
            if (pluginDefinition == null)
            {
                throw new Exception("Plugin '" + parts[0] + "' Not Defined.");
            }

            return pluginDefinition.CreateOrGet();
        }

        public Type FindPluginType(string pluginName)
        {
            string[] parts = pluginName.Split('.');
            if (parts.Length == 0)
            {
                throw new Exception("Empty Plugin Name.");
            }

            PluginDefinition<TPlugin, TRequest, TResponse> pluginDefinition = mPluginRegistry.Find(parts[0]);
            if (pluginDefinition == null)
            {
                throw new Exception("Plugin '" + parts[0] + "' Not Defined.");
            }

            return pluginDefinition.PluginType;
        }

        public IPlugin FindPluginObject(string pluginName)
        {
            string[] parts = pluginName.Split('.');
            if (parts.Length == 0)
            {
                throw new Exception("Empty Plugin Name.");
            }

            PluginDefinition<TPlugin, TRequest, TResponse> pluginDefinition = mPluginRegistry.Find(parts[0]);
            if (pluginDefinition == null)
            {
                throw new Exception("Plugin '" + parts[0] + "' Not Defined.");
            }

            return pluginDefinition.CreateOrGet();
        }

        public IPluginMethod<TRequest, TResponse> FindPluginMethod(string pluginMethod)
        {
            string[] parts = pluginMethod.Split('.');
            if (parts.Length == 0)
            {
                throw new Exception("Empty Plugin Method Name.");
            }

            string pluginName;
            string methodName;
            if (parts.Length == 1)
            {
                pluginName = parts[0];
                methodName = parts[0];
            }
            else if (parts.Length == 2)
            {
                pluginName = parts[0];
                methodName = parts[1];
            }
            else if (parts.Length == 3)
            {
                pluginName = parts[0];
                methodName = parts[1] + "." + parts[2];
            }
            else
            {
                throw new Exception("Invalid Plugin Method Name.");
            }

            PluginDefinition<TPlugin, TRequest, TResponse> pluginDefinition = mPluginRegistry.Find(pluginName);
            if (pluginDefinition == null)
            {
                throw new Exception("Plugin Not Defined.");
            }

            MethodDefinition methodDefinition = pluginDefinition.Methods[methodName];
            if (methodDefinition == null)
            {
                throw new Exception("Plugin.Method Not Defined.");
            }

            return new PluginMethod<TRequest, TResponse>(pluginDefinition.CreateOrGet(), methodDefinition.Method);
        }

        public IPluginMethod FindPluginObjectMethod(string pluginMethod)
        {
            string[] parts = pluginMethod.Split('.');
            if (parts.Length == 0)
            {
                throw new Exception("Empty Plugin Method Name.");
            }

            string pluginName;
            string methodName;
            if (parts.Length == 1)
            {
                pluginName = parts[0];
                methodName = parts[0];
            }
            else if (parts.Length == 2)
            {
                pluginName = parts[0];
                methodName = parts[1];
            }
            else if (parts.Length == 3)
            {
                pluginName = parts[0];
                methodName = parts[1] + "." + parts[2];
            }
            else
            {
                throw new Exception("Invalid Plugin Method Name.");
            }

            PluginDefinition<TPlugin, TRequest, TResponse> pluginDefinition = mPluginRegistry.Find(pluginName);
            if (pluginDefinition == null)
            {
                throw new Exception("Plugin Not Defined.");
            }

            MethodDefinition methodDefinition = pluginDefinition.Methods[methodName];
            if (methodDefinition == null)
            {
                throw new Exception("Plugin.Method Not Defined.");
            }

            return new PluginMethod(pluginDefinition.CreateOrGet(), methodDefinition.Method);
        }

        public TPlugin FindSingletonPlugin(string pluginName)
        {
            string[] parts = pluginName.Split('.');
            if (parts.Length == 0)
            {
                throw new Exception("Empty Plugin Name.");
            }

            PluginDefinition<TPlugin, TRequest, TResponse> pluginDefinition = mPluginRegistry.FindSingleton(parts[0]);
            if (pluginDefinition == null)
            {
                throw new Exception("Plugin Singleton '" + parts[0] + "' Not Defined.");
            }

            return pluginDefinition.Get();
        }

        public Type FindSingletonPluginType(string pluginName)
        {
            string[] parts = pluginName.Split('.');
            if (parts.Length == 0)
            {
                throw new Exception("Empty Plugin Name.");
            }

            PluginDefinition<TPlugin, TRequest, TResponse> pluginDefinition = mPluginRegistry.FindSingleton(parts[0]);
            if (pluginDefinition == null)
            {
                throw new Exception("Plugin Singleton '" + parts[0] + "' Not Defined.");
            }

            return pluginDefinition.PluginType;
        }

        public IPlugin FindSingletonPluginObject(string pluginName)
        {
            string[] parts = pluginName.Split('.');
            if (parts.Length == 0)
            {
                throw new Exception("Empty Plugin Name.");
            }

            PluginDefinition<TPlugin, TRequest, TResponse> pluginDefinition = mPluginRegistry.FindSingleton(parts[0]);
            if (pluginDefinition == null)
            {
                throw new Exception("Plugin Singleton '" + parts[0] + "' Not Defined.");
            }

            return pluginDefinition.Get();
        }

        public IPluginMethod<TRequest, TResponse> FindSingletonPluginMethod(string pluginMethod)
        {
            string[] parts = pluginMethod.Split('.');
            if (parts.Length == 0)
            {
                throw new Exception("Empty Plugin Method Name.");
            }

            string pluginName;
            string methodName;
            if (parts.Length == 1)
            {
                pluginName = parts[0];
                methodName = parts[0];
            }
            else if (parts.Length == 2)
            {
                pluginName = parts[0];
                methodName = parts[1];
            }
            else if (parts.Length == 3)
            {
                pluginName = parts[0];
                methodName = parts[1] + "." + parts[2];
            }
            else
            {
                throw new Exception("Invalid Plugin Method Name.");
            }

            PluginDefinition<TPlugin, TRequest, TResponse> pluginDefinition = mPluginRegistry.FindSingleton(pluginName);
            if (pluginDefinition == null)
            {
                throw new Exception("Plugin Singleton Not Defined.");
            }

            MethodDefinition methodDefinition = pluginDefinition.Methods[methodName];
            if (methodDefinition == null)
            {
                throw new Exception("(Plugin Singleton).Method Not Defined.");
            }

            return new PluginMethod<TRequest, TResponse>(pluginDefinition.Get(), methodDefinition.Method);
        }

        public IPluginMethod FindSingletonPluginObjectMethod(string pluginMethod)
        {
            string[] parts = pluginMethod.Split('.');
            if (parts.Length == 0)
            {
                throw new Exception("Empty Plugin Method Name.");
            }

            string pluginName;
            string methodName;
            if (parts.Length == 1)
            {
                pluginName = parts[0];
                methodName = parts[0];
            }
            else if (parts.Length == 2)
            {
                pluginName = parts[0];
                methodName = parts[1];
            }
            else if (parts.Length == 3)
            {
                pluginName = parts[0];
                methodName = parts[1] + "." + parts[2];
            }
            else
            {
                throw new Exception("Invalid Plugin Method Name.");
            }

            PluginDefinition<TPlugin, TRequest, TResponse> pluginDefinition = mPluginRegistry.FindSingleton(pluginName);
            if (pluginDefinition == null)
            {
                throw new Exception("Plugin Singleton Not Defined.");
            }

            MethodDefinition methodDefinition = pluginDefinition.Methods[methodName];
            if (methodDefinition == null)
            {
                throw new Exception("(Plugin Singleton).Method Not Defined.");
            }

            return new PluginMethod(pluginDefinition.Get(), methodDefinition.Method);
        }

        public string[] PluginNames
        {
            get
            {
                return mPluginRegistry.EnumerateKeys();
            }
        }

        public bool RegisterPlugin(string plugin)
        {
            try
            {
                Assembly assy = Assembly.LoadFrom(plugin);
                mPluginAssemblies.Add(assy);

                assy.GetTypes().ForEach(t =>
                {
                    var def = PluginDefinition<TPlugin, TRequest, TResponse>.Create(t, plugin);
                    if (def != null)
                    {
                        mPluginRegistry.Add(def);
                    }
                });

                mPluginFiles.Add(plugin);
                return true;
            }
            catch (Exception ex)
            {
                DebugManager.WarnException(plugin, ex, new DebugKey("PluginManager_LoadError"));
            }
            return false;
        }

        public bool RegisterPlugin(Type plugin)
        {
            try
            {
                var def = PluginDefinition<TPlugin, TRequest, TResponse>.Create(plugin);
                if (def != null)
                {
                    mPluginRegistry.Add(def);
                    return true;
                }
            }
            catch (Exception ex)
            {
                DebugManager.WarnException(ex, new DebugKey("PluginManager_LoadError"));
            }
            return false;
        }

        public bool RegisterPlugin(TPlugin plugin)
        {
            try
            {
                var def = PluginDefinition<TPlugin, TRequest, TResponse>.Create(plugin);
                if (def != null)
                {
                    mPluginRegistry.Add(def);
                    return true;
                }
            }
            catch (Exception ex)
            {
                DebugManager.WarnException(ex, new DebugKey("PluginManager_LoadError"));
            }
            return false;
        }

        #region Private Classes

        private class PluginRegistry
            : System.Collections.CollectionBase
        {
            public void Add(PluginDefinition<TPlugin, TRequest, TResponse> definition)
            {
                var def = Find(definition.Name);
                if (def != null)
                {
                    throw new Exception("Duplicate Plugin Name Found: " + definition.Name);
                }
                List.Add(definition);
            }

            public void Remove(PluginDefinition<TPlugin, TRequest, TResponse> definition)
            {
                List.Remove(definition);
                definition.Destroy();
            }

            public PluginDefinition<TPlugin, TRequest, TResponse> Find(string pluginOrPath)
            {
                foreach (PluginDefinition<TPlugin, TRequest, TResponse> definition in List)
                {
                    if ((definition.Name.Equals(pluginOrPath)) || definition.AssemblyPath.Equals(pluginOrPath))
                    {
                        return definition;
                    }
                }
                return null;
            }

            public PluginDefinition<TPlugin, TRequest, TResponse> FindSingleton(string pluginOrPath)
            {
                foreach (PluginDefinition<TPlugin, TRequest, TResponse> definition in List)
                {
                    if (((definition.Name.Equals(pluginOrPath)) || definition.AssemblyPath.Equals(pluginOrPath)) &&
                        definition.IsSingleton)
                    {
                        return definition;
                    }
                }
                return null;
            }

            public string[] EnumerateKeys()
            {
                int i = 0;
                string[] keys = new string[List.Count];
                foreach (PluginDefinition<TPlugin, TRequest, TResponse> definition in List)
                {
                    keys[i++] = definition.Name;
                }
                return keys;
            }
        }

        #endregion
    }
}
