﻿using System;

namespace Dynamic.Plugins
{
    public class PluginRequest : IPluginRequest
    {
        public string Method { get; set; }
        public object Id { get; set; }
        public object Params { get; set; }

        public PluginRequest()
            : this("")
        {

        }

        public PluginRequest(string method)
            : this(method, Guid.NewGuid().ToString(), null)
        {

        }
        
        public PluginRequest(string method, object parms)
            : this(method, Guid.NewGuid().ToString(), parms)
        {

        }

        public PluginRequest(string method, object id, object parms)
        {
            Method = method;
            Id = id;
            Params = parms;
        }
    }
}
