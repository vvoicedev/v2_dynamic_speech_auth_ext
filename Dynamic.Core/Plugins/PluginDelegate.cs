﻿using System;

namespace Dynamic.Plugins
{
    public class PluginDelegate : IPluginMethod
    {
        public delegate PluginResult Delegate(IPluginRequest request, IPluginResponse response);

        private Delegate mDelegate;

        public PluginDelegate(Delegate delgate)
        {
            mDelegate = delgate;
        }

        public PluginResult Execute(IPluginRequest request, IPluginResponse response)
        {
            try
            {
                return mDelegate.Invoke(request, response);
            }
            catch (Exception ex)
            {
                response.SetError(ex);
                return PluginResult.Error;
            }
        }
    }

    public class PluginDelegate<TRequest, TResponse> : IPluginMethod<TRequest, TResponse>
        where TRequest : IPluginRequest
        where TResponse : IPluginResponse
    {
        public delegate PluginResult Delegate(TRequest request, TResponse response);

        private Delegate mDelegate;

        public PluginDelegate(Delegate delgate)
        {
            mDelegate = delgate;
        }

        public PluginResult Execute(IPluginRequest request, IPluginResponse response)
        {
            if(request is TRequest trequest && response is TResponse tresponse)
            {
                try
                {
                    return mDelegate.Invoke(trequest, tresponse);
                }
                catch (Exception ex)
                {
                    response.SetError(ex);
                    return PluginResult.Error;
                }
            }
            return PluginResult.Error;
        }

        public PluginResult Execute(TRequest request, TResponse response)
        {
            try
            {
                return mDelegate.Invoke(request, response);
            }
            catch (Exception ex)
            {
                response.SetError(ex);
                return PluginResult.Error;
            }
        }
    }
}
