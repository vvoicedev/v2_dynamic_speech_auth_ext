﻿using System;
using System.Collections.Generic;

namespace Dynamic.Plugins
{
    public class PluginResponse : IPluginResponse
    {
        public object Id { get; set; }

        public object Results { get; set; }
        public int? Error { get; set; }
        public Exception Exception { get; set; }

        public void SetError(int error)
        {
            Error = error;
        }

        public void SetError(int error, string message)
        {
            Error = error;
            var data = new Dictionary<string, object>();
            data["message"] = message;
            Results = data;
        }

        public void SetError(Exception ex)
        {
            Error = -1;
            var data = new Dictionary<string, object>();
            data["message"] = ex.Message;
            Results = data;
            Exception = ex;
        }

        public PluginResponse()
            : this(null)
        {

        }

        public PluginResponse(IPluginRequest request)
            : this(request.Id)
        {

        }

        public PluginResponse(object id)
        {
            Id = id;
            Error = null;
            Results = null;
        }
    }
}
