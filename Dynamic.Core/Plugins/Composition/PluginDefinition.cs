﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

using Dynamic.Extensions;

namespace Dynamic.Plugins.Composition
{
    internal class PluginDefinition<TPlugin>
        where TPlugin : IPlugin
    {
        private PluginDefinition(Type type)
            : this(type, "")
        {
        }

        private PluginDefinition(Type type, string pluginFile)
        {
            PluginType = type;
            AssemblyPath = pluginFile;
            ServiceFile = Path.GetFileName(pluginFile);
            ServiceClass = type.Name;
            IsDeprecated = false;
            IsSingleton = false;
            Version = 0;
            Name = "";
            Description = "";
            Methods = new Dictionary<string, MethodDefinition>();
            InitializePluginDefinition();
        }

        private PluginDefinition(Type type, TPlugin plugin)
        {
            PluginType = type;
            AssemblyPath = "";
            ServiceFile = "";
            ServiceClass = type.Name;
            IsDeprecated = false;
            Version = 0;
            Name = "";
            Description = "";
            Methods = new Dictionary<string, MethodDefinition>();
            InitializePluginDefinition();
            mSingleton = plugin;
            IsSingleton = true;
        }

        internal static PluginDefinition<TPlugin> Create(TPlugin plugin)
        {
            return new PluginDefinition<TPlugin>(plugin.GetType(), plugin);
        }

        internal static PluginDefinition<TPlugin> Create(Type type)
        {
            Type checker = typeof(TPlugin);
            if (checker.IsInterface)
            {
                if (null != type.GetInterface(checker.FullName, true))
                {
                    return new PluginDefinition<TPlugin>(type);
                }
            }
            else if (checker.IsClass)
            {
                if (type.IsSubclassOf(typeof(TPlugin)))
                {
                    return new PluginDefinition<TPlugin>(type);
                }
            }
            return null;
        }

        internal static PluginDefinition<TPlugin> Create(Type type, string pluginFile)
        {
            Type checker = typeof(TPlugin);
            if(checker.IsInterface)
            {
                if(null != type.GetInterface(checker.FullName, true))
                {
                    return new PluginDefinition<TPlugin>(type, pluginFile);
                }
            }
            else if(checker.IsClass)
            {
                if (type.IsSubclassOf(typeof(TPlugin)))
                {
                    return new PluginDefinition<TPlugin>(type, pluginFile);
                }
            }
            return null;
        }

        #region Properties

        public Type PluginType { get; private set; }

        public string AssemblyPath { get; private set; }

        public string ServiceFile { get; private set; }

        public string ServiceClass { get; private set; }

        public bool IsDeprecated { get; private set; }

        public bool IsSingleton { get; private set; }

        public uint Version { get; private set; }

        public string Name { get; private set; }

        public string Description { get; private set; }

        public Dictionary<string, MethodDefinition> Methods { get; private set; }

        #endregion

        #region Internal Methods

        private IPlugin mSingleton;
        private readonly object mLock = new object();

        internal TPlugin CreateOrGet()
        {
            if(IsSingleton)
            {
                if(mSingleton == null)
                {
                    lock(mLock)
                    {
                        mSingleton = (TPlugin)Activator.CreateInstance(PluginType);
                    }
                }
                return (TPlugin)mSingleton;
            }

            return (TPlugin)Activator.CreateInstance(PluginType);
        }

        internal TPlugin Get()
        {
            if (IsSingleton)
            {
                if (mSingleton == null)
                {
                    lock (mLock)
                    {
                        mSingleton = (TPlugin)Activator.CreateInstance(PluginType);
                    }
                }
                return (TPlugin)mSingleton;
            }

            throw new Exception("Unable to get Plugin. Not a singleton type.");
        }

        internal void Destroy()
        {
            if (IsSingleton && mSingleton != null)
            { 
                lock (mLock)
                {
                    mSingleton.Dispose();
                    mSingleton = null;
                }
            }
        }

        #endregion

        #region Private

        private void InitializePluginDefinition()
        {
            Type typeInterface = PluginType.GetInterface(typeof(TPlugin).Name, true);
            if (null != typeInterface)
            {
                PluginInformationAttribute infoAttr = PluginType.GetAttribute<PluginInformationAttribute>();
                if (infoAttr != null)
                {
                    Version = infoAttr.Version;
                    Name = infoAttr.Name;
                    Description = infoAttr.Description;
                    IsSingleton = infoAttr.Singleton;
                }

                PluginIsDeprecated deprecatedAttr = PluginType.GetAttribute<PluginIsDeprecated>();
                if (deprecatedAttr != null)
                {
                    IsDeprecated = true;
                    if (deprecatedAttr.Message == null)
                    {
                        Description = "(Deprecated) " + Description;
                    }
                    else
                    {
                        Description = "(Deprecated: " + deprecatedAttr.Message + ") " + Description;
                    }
                }

                MethodInfo[] methodInfo = PluginType.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                foreach (var mi in methodInfo)
                {
                    Type returnType = mi.ReturnType;
                    if (returnType != typeof(PluginResult)) continue;

                    ParameterInfo[] paramInfo = mi.GetParameters();
                    if (paramInfo.Length != 2) continue;
                    if (paramInfo[0].ParameterType.IsSubclassOf(typeof(IPluginRequest))) continue;
                    if (paramInfo[1].ParameterType.IsSubclassOf(typeof(IPluginResponse))) continue;

                    MethodAttribute methodAttr = mi.GetAttribute<MethodAttribute>();
                    if (methodAttr == null) continue;

                    MethodIsDeprecatedAttribute methodDeprecatedAttr = mi.GetAttribute<MethodIsDeprecatedAttribute>();
                    if (methodDeprecatedAttr != null)
                    {
                        Methods.Add(methodAttr.VersionedName, new MethodDefinition(mi, methodAttr, methodDeprecatedAttr));
                    }
                    else
                    {
                        var def = new MethodDefinition(mi, methodAttr);
                        Methods.Add(methodAttr.Name, def);
                        Methods.Add(methodAttr.VersionedName, def);
                    }
                }
            }
        }

        #endregion
    }

    internal class PluginDefinition<TPlugin, TRequest, TResponse>
        where TPlugin : IPlugin
        where TRequest : IPluginRequest
        where TResponse : IPluginResponse
    {
        private PluginDefinition(Type type)
            : this(type, "")
        {
        }

        private PluginDefinition(Type type, string pluginFile)
        {
            PluginType = type;
            AssemblyPath = pluginFile;
            ServiceFile = Path.GetFileName(pluginFile);
            ServiceClass = type.Name;
            IsDeprecated = false;
            IsSingleton = false;
            Version = 0;
            Name = "";
            Description = "";
            Methods = new Dictionary<string, MethodDefinition>();
            InitializePluginDefinition();
        }

        private PluginDefinition(Type type, TPlugin plugin)
        {
            PluginType = type;
            AssemblyPath = "";
            ServiceFile = "";
            ServiceClass = type.Name;
            IsDeprecated = false;
            Version = 0;
            Name = "";
            Description = "";
            Methods = new Dictionary<string, MethodDefinition>();
            InitializePluginDefinition();
            mSingleton = plugin;
            IsSingleton = true;
        }

        internal static PluginDefinition<TPlugin, TRequest, TResponse> Create(TPlugin plugin)
        {
            return new PluginDefinition<TPlugin, TRequest, TResponse>(plugin.GetType(), plugin);
        }

        internal static PluginDefinition<TPlugin, TRequest, TResponse> Create(Type type)
        {
            Type checker = typeof(TPlugin);
            if (checker.IsInterface)
            {
                if (null != type.GetInterface(checker.FullName, true))
                {
                    return new PluginDefinition<TPlugin, TRequest, TResponse>(type);
                }
            }
            else if (checker.IsClass)
            {
                if (type.IsSubclassOf(typeof(TPlugin)))
                {
                    return new PluginDefinition<TPlugin, TRequest, TResponse>(type);
                }
            }
            return null;
        }

        internal static PluginDefinition<TPlugin, TRequest, TResponse> Create(Type type, string pluginFile)
        {
            Type checker = typeof(TPlugin);
            if (checker.IsInterface)
            {
                if (null != type.GetInterface(checker.FullName, true))
                {
                    return new PluginDefinition<TPlugin, TRequest, TResponse>(type, pluginFile);
                }
            }
            else if (checker.IsClass)
            {
                if (type.IsSubclassOf(typeof(TPlugin)))
                {
                    return new PluginDefinition<TPlugin, TRequest, TResponse>(type, pluginFile);
                }
            }
            return null;
        }

        #region Properties

        public Type PluginType { get; private set; }

        public string AssemblyPath { get; private set; }

        public string ServiceFile { get; private set; }

        public string ServiceClass { get; private set; }

        public bool IsDeprecated { get; private set; }

        public bool IsSingleton { get; private set; }

        public uint Version { get; private set; }

        public string Name { get; private set; }

        public string Description { get; private set; }

        public Dictionary<string, MethodDefinition> Methods { get; private set; }

        #endregion

        #region Internal Methods

        private IPlugin mSingleton;
        private readonly object mLock = new object();

        internal TPlugin CreateOrGet()
        {
            if (IsSingleton)
            {
                if (mSingleton == null)
                {
                    lock (mLock)
                    {
                        mSingleton = (TPlugin)Activator.CreateInstance(PluginType);
                    }
                }
                return (TPlugin)mSingleton;
            }

            return (TPlugin)Activator.CreateInstance(PluginType);
        }

        internal TPlugin Get()
        {
            if (IsSingleton)
            {
                if (mSingleton == null)
                {
                    lock (mLock)
                    {
                        mSingleton = (TPlugin)Activator.CreateInstance(PluginType);
                    }
                }
                return (TPlugin)mSingleton;
            }

            throw new Exception("Unable to get Plugin. Not a singleton type.");
        }

        internal void Destroy()
        {
            if (IsSingleton && mSingleton != null)
            {
                lock (mLock)
                {
                    mSingleton.Dispose();
                    mSingleton = null;
                }
            }
        }

        #endregion

        #region Private

        private void InitializePluginDefinition()
        {
            Type typeInterface = PluginType.GetInterface(typeof(TPlugin).Name, true);
            if (null != typeInterface)
            {
                PluginInformationAttribute infoAttr = PluginType.GetAttribute<PluginInformationAttribute>();
                if (infoAttr != null)
                {
                    Version = infoAttr.Version;
                    Name = infoAttr.Name;
                    Description = infoAttr.Description;
                    IsSingleton = infoAttr.Singleton;
                }

                PluginIsDeprecated deprecatedAttr = PluginType.GetAttribute<PluginIsDeprecated>();
                if (deprecatedAttr != null)
                {
                    IsDeprecated = true;
                    if (deprecatedAttr.Message == null)
                    {
                        Description = "(Deprecated) " + Description;
                    }
                    else
                    {
                        Description = "(Deprecated: " + deprecatedAttr.Message + ") " + Description;
                    }
                }

                MethodInfo[] methodInfo = PluginType.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                foreach (var mi in methodInfo)
                {
                    Type returnType = mi.ReturnType;
                    if (returnType != typeof(PluginResult)) continue;

                    ParameterInfo[] paramInfo = mi.GetParameters();
                    if (paramInfo.Length != 2) continue;
                    if (paramInfo[0].ParameterType.IsSubclassOf(typeof(TRequest))) continue;
                    if (paramInfo[1].ParameterType.IsSubclassOf(typeof(TResponse))) continue;

                    MethodAttribute methodAttr = mi.GetAttribute<MethodAttribute>();
                    if (methodAttr == null) continue;

                    MethodIsDeprecatedAttribute methodDeprecatedAttr = mi.GetAttribute<MethodIsDeprecatedAttribute>();
                    if (methodDeprecatedAttr != null)
                    {
                        Methods.Add(methodAttr.VersionedName, new MethodDefinition(mi, methodAttr, methodDeprecatedAttr));
                    }
                    else
                    {
                        var def = new MethodDefinition(mi, methodAttr);
                        Methods.Add(methodAttr.Name, def);
                        Methods.Add(methodAttr.VersionedName, def);
                    }
                }
            }
        }

        #endregion
    }
}
