﻿using System;
using System.Reflection;

namespace Dynamic.Plugins.Composition
{
    internal class MethodDefinition
    {
        internal MethodDefinition(MethodInfo method, MethodAttribute methodAttr)
        {
            Method = method;
            Version = methodAttr.Version;
            Name = methodAttr.Name;
            Description = methodAttr.Description;
            IsDeprecated = false;
        }

        internal MethodDefinition(MethodInfo method, MethodAttribute methodAttr, MethodIsDeprecatedAttribute deprecatedAttr)
        {
            Method = method;
            Version = methodAttr.Version;
            Name = methodAttr.Name;
            IsDeprecated = true;
            if (deprecatedAttr.Message == null)
            {
                Description = "(Deprecated) " + methodAttr.Description;
            }
            else
            {
                Description = "(Deprecated: " + deprecatedAttr.Message + ") " + methodAttr.Description;
            }
        }

        public uint Version { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public bool IsDeprecated { get; private set; }

        public MethodInfo Method { get; set; }
    }
}
