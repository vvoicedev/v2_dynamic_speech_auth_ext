﻿
using System;
using System.Reflection;

namespace Dynamic.Plugins
{
    public class PluginMethod : IPluginMethod
    {
        private IPlugin mPlugin;
        private MethodInfo mMethod;

        public PluginMethod(IPlugin plugin, MethodInfo method)
        {
            mPlugin = plugin;
            mMethod = method;
        }

        public PluginResult Execute(IPluginRequest request, IPluginResponse response)
        {
            try
            {
                return (PluginResult)mMethod.Invoke(mPlugin, new object[] { request, response });
            }
            catch(Exception ex)
            {
                response.SetError(ex);
                return PluginResult.Error;
            }
        }
    }

    public class PluginMethod<TRequest, TResponse> : IPluginMethod<TRequest, TResponse>
        where TRequest : IPluginRequest
        where TResponse : IPluginResponse
    {
        private IPlugin mPlugin;
        private MethodInfo mMethod;

        public PluginMethod(IPlugin plugin, MethodInfo method)
        {
            mPlugin = plugin;
            mMethod = method;
        }

        public PluginResult Execute(IPluginRequest request, IPluginResponse response)
        {
            if (request is TRequest trequest && response is TResponse tresponse)
            {
                try
                {
                    return (PluginResult)mMethod.Invoke(mPlugin, new object[] { trequest, tresponse });
                }
                catch (Exception ex)
                {
                    response.SetError(ex);
                    return PluginResult.Error;
                }
            }
            return PluginResult.Error;
        }

        public PluginResult Execute(TRequest request, TResponse response)
        {
            try
            {
                return (PluginResult)mMethod.Invoke(mPlugin, new object[] { request, response });
            }
            catch (Exception ex)
            {
                response.SetError(ex);
                return PluginResult.Error;
            }
        }
    }
}
