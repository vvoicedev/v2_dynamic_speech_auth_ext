﻿using System.Collections.Generic;

namespace Dynamic.Modules
{

    class ModuleDefinition
    {
        public List<IModuleInfo> Modules { get; private set; }

        public List<string> Plugins { get; private set; }

        internal ModuleDefinition()
        {
            Modules = new List<IModuleInfo>();
            Plugins = new List<string>();
        }

        internal ModuleDefinition(int capacity)
        {
            Modules = new List<IModuleInfo>(capacity);
            Plugins = new List<string>(capacity);
        }

        internal ModuleDefinition(int modcap, int plugcap)
        {
            Modules = new List<IModuleInfo>(modcap);
            Plugins = new List<string>(plugcap);
        }

        public void AddModule(string name)
        {
            var item = new ModuleItem(name);
            if(!Modules.Contains(item))
            {
                Modules.Add(item);
            }
        }

        public void AddModule(string name, Dictionary<string, object> args)
        {
            var item = new ModuleItem(name, args);
            if (!Modules.Contains(item))
            {
                Modules.Add(item);
            }
        }

        public void AddPlugin(string name)
        {
            if (!Plugins.Contains(name))
            {
                Plugins.Add(name);
            }
        }

        private class ModuleItem : IModuleInfo
        {
            public string Name { get; private set; }
            public Dictionary<string, object> Args { get; private set; }

            internal ModuleItem(string name)
            {
                Name = name;
                Args = new Dictionary<string, object>();
            }

            internal ModuleItem(string name, Dictionary<string, object> args)
            {
                Name = name;
                Args = args;
            }

            public override bool Equals(object obj)
            {
                if (obj is ModuleItem item)
                {
                    return Name.Equals(item.Name);
                }
                return base.Equals(obj);
            }

            public override int GetHashCode()
            {
                return Name.GetHashCode();
            }
        }
    }
}
