﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

using Dynamic.Plugins;
using Dynamic.Services;

using Dynamic.Diagnostics;
using Dynamic.Json;
using System.ComponentModel.Composition.Hosting;

namespace Dynamic.Modules
{
    public static class BootLoader
    {
        public static ServiceManager ServiceManager { get; private set; }
        
        public static void Startup(string moduleFilename = "modules.json")
        {
            ServiceManager = new ServiceManager();
            ServiceManager.RegisterSingleton<IServiceModuleManager, ServiceModuleManager>();

            try
            {
                IModuleManager moduleManager = (IModuleManager)ServiceManager.Get<IServiceModuleManager>();
                ModuleDefinition moduleDefinition = GetModuleDefinition(moduleFilename);
                moduleManager.RegisterModules(moduleDefinition.Modules);

                if (moduleDefinition.Plugins.Count > 0)
                {
                    ServiceManager.IfExists(delegate (IPluginManagerService pluginManagerService)
                    {
                        pluginManagerService.InitializePlugins(moduleDefinition.Plugins);
                    });
                }

                ServiceManager.FinishedInitialization();
            }
            catch(ReflectionTypeLoadException lex)
            {
                StringBuilder sb = new StringBuilder();

                foreach (Exception ex in lex.LoaderExceptions)
                {
                    sb.AppendLine(ex.Message);
                }

                DebugManager.FailException(new Exception(sb.ToString()), new DebugKey("Bootstrap"));
            }
            catch(Exception ex)
            {
                DebugManager.FailException(ex, new DebugKey("Bootstrap"));
            }
        }

        public static void Startup(CompositionContainer container, string moduleFilename = "modules.json")
        {
            ServiceManager = new ServiceManager(container);
            ServiceManager.RegisterSingleton<IServiceModuleManager, ServiceModuleManager>();

            try
            {
                IModuleManager moduleManager = (IModuleManager)ServiceManager.Get<IServiceModuleManager>();
                if (!string.IsNullOrEmpty(moduleFilename) && File.Exists(moduleFilename))
                {
                    ModuleDefinition moduleDefinition = GetModuleDefinition(moduleFilename);
                    moduleManager.RegisterModules(container, moduleDefinition.Modules);

                    if (moduleDefinition.Plugins.Count > 0)
                    {
                        ServiceManager.IfExists(delegate (IPluginManagerService pluginManagerService)
                        {
                            pluginManagerService.InitializePlugins(moduleDefinition.Plugins);
                        });
                    }
                }
                else
                {
                    moduleManager.RegisterModules(container);
                }

                ServiceManager.FinishedInitialization();
            }
            catch (ReflectionTypeLoadException lex)
            {
                StringBuilder sb = new StringBuilder();

                foreach (Exception ex in lex.LoaderExceptions)
                {
                    sb.AppendLine(ex.Message);
                }

                DebugManager.FailException(new Exception(sb.ToString()), new DebugKey("Bootstrap"));
            }
            catch (Exception ex)
            {
                DebugManager.FailException(ex, new DebugKey("Bootstrap"));
            }
        }

        public static void Shutdown()
        {
            ServiceManager.Shutdown();
        }

        private static ModuleDefinition GetModuleDefinition(string filename)
        {
            Assert.That(File.Exists(filename), "Module definition file " + filename + " does not exist.");
            string ext = Path.GetExtension(filename);
            if (ext.ToLower().Equals(".json"))
            {
                var data = JsonDoctor.ToObject(File.ReadAllText(filename), typeof(Dictionary<string, object>));
                if (data is List<object>)
                {
                    return GetModuleDefinition(data as List<object>);
                }
                else if (data is Dictionary<string, object>)
                {
                    return GetModuleDefinition(data as Dictionary<string, object>);
                }
                else if (data is object[])
                {
                    return GetModuleDefinition(new List<object>(data as object[]));
                }
                Assert.That(false, "Module definition file " + filename + " invalid json format.");
            }
            //else if(ext.ToLower().Equals(".xml"))
            //{
            //    XDocument xdoc = XDocument.Load(filename);
            //    return GetModuleList(xdoc);
            //}
            Assert.That(false, "Module definition file " + filename + " unknown extension.");
            // Specifiy Return here, as Assert.That does not inform compiler it is the last line
            return null;
        }

        private static ModuleDefinition GetModuleDefinition(List<object> list)
        {
            ModuleDefinition def = new ModuleDefinition(list.Count);
            foreach(var item in list)
            {
                string module = item as string;
                if(module != null)
                {
                    def.AddModule(module);
                }
            }
            return def;
        }

        private static ModuleDefinition GetModuleDefinition(Dictionary<string, object> dictionary)
        {
            ModuleDefinition def = new ModuleDefinition();
            if (dictionary.ContainsKey("Modules"))
            {
                var modules = dictionary["Modules"];
                if(modules is List<object> mods)
                {
                    foreach (var item in mods)
                    {
                        if(item is string moduleString)
                        {
                            def.AddModule(moduleString);
                        }
                        else if(item is Dictionary<string, object> moduleInfo)
                        {
                            if (!moduleInfo.ContainsKey("ModuleName"))
                                continue;

                            if (!moduleInfo.ContainsKey("ModuleArgs"))
                                continue;

                            if (!(moduleInfo["ModuleName"] is string moduleName))
                                continue;

                            if (!(moduleInfo["ModuleArgs"] is Dictionary<string, object> moduleArgs))
                                continue;

                            def.AddModule(moduleName, moduleArgs);
                        }
                    }
                }
            }
            if (dictionary.ContainsKey("Plugins"))
            {
                var plugins = dictionary["Plugins"];
                if (plugins is List<object> plugs)
                {
                    foreach (var item in plugs)
                    {
                        if (item is string plugingString)
                        {
                            def.AddPlugin(plugingString);
                        }
                    }
                }
            }
            return def;
        }

        /// <summary>
        /// Returns the list of modules specified in the XML document so we know what
        /// modules to instantiate.
        /// </summary>
        //private static List<string> GetModuleList(XDocument xdoc)
        //{
        //    List<string> assemblies = new List<string>();
        //    (from module in xdoc.Element("Modules").Elements("Module")
        //     select module.Attribute("AssemblyName").Value).ForEach(s => assemblies.Add(AssemblyFileName.Create(s)));
        //
        //    return assemblies;
        //}
    }
}
