﻿/* The MIT License (MIT)
* 
* Copyright (c) 2015 Marc Clifton
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;

using Dynamic.Diagnostics;

namespace Dynamic.Modules
{
    public class ModuleManager : IModuleManager
    {
        protected List<IModule> registrants;

        public ReadOnlyCollection<IModule> Modules { get { return registrants.AsReadOnly(); } }

        public ModuleManager()
        {
        }

        /// <summary>
        /// Register modules specified in a list of assembly filenames.
        /// </summary>
        public virtual void RegisterModules(List<IModuleInfo> moduleItems, string optionalPath = null, Func<string, Assembly> assemblyResolver = null)
        {
            List<ModuleLoader> loaders = LoadModules(moduleItems, optionalPath, assemblyResolver);
            InstantiateRegistrants(loaders);
            InitializeRegistrants(loaders);
        }

        /// <summary>
        /// Register modules specified in a list of assembly filenames.
        /// </summary>
        public virtual void RegisterModules(CompositionContainer container, List<IModuleInfo> moduleItems = null, string optionalPath = null, Func<string, Assembly> assemblyResolver = null)
        {
            List<ModuleLoader> loaders = LoadModulesFromContainer(container, moduleItems, optionalPath, assemblyResolver);
            InstantiateRegistrants(loaders);
            InitializeRegistrants(loaders);
        }

        /// <summary>
        /// Load the assemblies and return the list of loaded assemblies.  In order to register
        /// services that the module implements, we have to load the assembly.
        /// </summary>
        protected virtual List<ModuleLoader> LoadModules(List<IModuleInfo> moduleItems, string optionalPath, Func<string, Assembly> assemblyResolver)
        {
            List<ModuleLoader> modules = new List<ModuleLoader>();

            moduleItems.ForEach(a =>
            {
                Assembly assembly = LoadAssembly(a.Name, optionalPath, assemblyResolver);
                modules.Add(new ModuleLoader(a, assembly));
            });

            return modules;
        }

        protected virtual List<ModuleLoader> LoadModulesFromContainer(CompositionContainer container, List<IModuleInfo> moduleItems, string path, Func<string, Assembly> assemblyResolver = null)
        {
            List<ModuleLoader> modules = new List<ModuleLoader>();

            if (moduleItems != null)
            {
                foreach (var mod in container.GetExportedValues<IModule>())
                {
                    var type = mod.GetType();
                    ModuleLoader loader = null;
                    var info = FindModuleItem(type, ref moduleItems);
                    if (info != null)
                    {
                        loader = new ModuleLoader(info, type.Assembly);
                        loader.Module = mod;
                        modules.Add(loader);
                        continue;
                    }
                    loader = new ModuleLoader(new ModuleInfo(type.Name), type.Assembly);
                    loader.Module = mod;
                    modules.Add(loader);
                }
                
                moduleItems.ForEach(a =>
                {
                    Assembly assembly = LoadAssembly(a.Name, path, assemblyResolver);
                    modules.Add(new ModuleLoader(a, assembly));
                });
            }
            else
            {
                foreach (var mod in container.GetExportedValues<IModule>())
                {
                    var type = mod.GetType();
                    var loader = new ModuleLoader(new ModuleInfo(type.Name), type.Assembly);
                    loader.Module = mod;
                    modules.Add(loader);
                }
            }

            return modules;
        }

        private IModuleInfo FindModuleItem(Type needle, ref List<IModuleInfo> moduleItems)
        {
            if (needle.Assembly.Modules.Any())
            {
                foreach (var item in moduleItems)
                {
                    if (item.Name.Equals(needle.Name) || item.Name.Equals(needle.FullName) ||
                        item.Name.Equals(needle.Assembly.FullName) || item.Name.Equals(needle.Assembly.Modules.First().Name))
                    {
                        moduleItems.Remove(item);
                        return item;
                    }
                }
            }
            else
            {
                foreach (var item in moduleItems)
                {
                    if (item.Name.Equals(needle.Name) || item.Name.Equals(needle.FullName) ||
                        item.Name.Equals(needle.Assembly.FullName))
                    {
                        moduleItems.Remove(item);
                        return item;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Load and return an assembly given the assembly filename so we can proceed with
        /// instantiating the module and so the module can register its services.
        /// </summary>
        protected virtual Assembly LoadAssembly(string assyName, string optionalPath, Func<string, Assembly> assemblyResolver)
        {
            string fullPath = GetFullPath(assyName, optionalPath);
            Assembly assembly = null;

            if (!File.Exists(fullPath))
            {
                Assert.Not(assemblyResolver == null, "Module " + fullPath + " not found.\r\n.  An assemblyResolver must be defined when attempting to load modules from the application's resources or specify the optionalPath to locate the assembly.");
                assembly = assemblyResolver(assyName);
            }
            else
            {
                try
                {
                    assembly = Assembly.LoadFile(fullPath);
                }
                catch (Exception ex)
                {
                    throw new ModuleManagerException("Unable to load module " + assyName + ": " + ex.Message);
                }
            }

            return assembly;
        }
        
        /// <summary>
        /// Instantiate and return the list of registratants -- assemblies with classes that implement IModule.
        /// The registrants is one and only one class in the module that implements IModule, which we can then
        /// use to call the Initialize method so the module can register its services.
        /// </summary>
        protected virtual List<IModule> InstantiateRegistrants(List<ModuleLoader> modules)
        {
            registrants = new List<IModule>();
            modules.ForEach(m =>
            {
                if (m.Module == null)
                {
                    IModule registrant = InstantiateRegistrant(m.ModuleAssembly);
                    m.Module = registrant;
                    registrants.Add(registrant);
                }
                else
                {
                    registrants.Add(m.Module);
                }
            });

            return registrants;
        }

        /// <summary>
        /// Instantiate a registrant.  A registrant must have one and only one class that implements IModule.
        /// The registrant is one and only one class in the module that implements IModule, which we can then
        /// use to call the Initialize method so the module can register its services.
        /// </summary>
        protected virtual IModule InstantiateRegistrant(Assembly module)
        {
            var classesImplementingInterface = module.GetTypes().
                    Where(t => t.IsClass).
                    Where(c => c.GetInterfaces().Where(i => i.Name == "IModule").Count() > 0);

            Assert.That(classesImplementingInterface.Count() <= 1, "Module can only have one class that implements IModule");
            Assert.That(classesImplementingInterface.Count() != 0, "Module does not have any classes that implement IModule");

            Type implementor = classesImplementingInterface.Single();
            IModule instance = Activator.CreateInstance(implementor) as IModule;

            return instance;
        }

        /// <summary>
        /// Initialize each registrant.  This method should be overridden by your application needs.
        /// </summary>
        protected virtual void InitializeRegistrants(List<ModuleLoader> loaders)
        {
        }

        /// <summary>
        /// Return the full path of the executing application (here we assume that ModuleManager.dll is in that path) and concatenate the assembly name of the module.
        /// .NET requires the the full path in order to load the associated assembly.
        /// </summary>
        protected virtual string GetFullPath(string assemblyName, string optionalPath)
        {
            string appLocation;
            string assyLocation = Assembly.GetExecutingAssembly().Location;

            // An assembly that is loaded as a resource will have its assembly location as "".
            if (assyLocation == "")
            {
                Assert.Not(optionalPath == null, "Assemblies embedded as resources require that the optionalPath parameter specify the path to resolve assemblies.");
                appLocation = optionalPath;       // Must be specified!  Here the optional path is the full path?  This gives two different meanings to how optional path is used!
            }
            else
            {
                appLocation = Path.GetDirectoryName(assyLocation);

                if (optionalPath != null)
                {
                    appLocation = Path.Combine(appLocation, optionalPath);
                }
            }

            return Path.Combine(appLocation, assemblyName);
        }

        protected class ModuleLoader
        {
            public IModuleInfo ModuleInfo { get; private set; }
            public Assembly ModuleAssembly { get; private set; }
            public IModule Module { get; set; }

            internal ModuleLoader(IModuleInfo info, Assembly assembly)
            {
                ModuleInfo = info;
                ModuleAssembly = assembly;
            }
        }

        protected class ModuleInfo : IModuleInfo
        {
            public string Name { get; private set; }

            public Dictionary<string, object> Args { get; private set; }

            public ModuleInfo(string name)
            {
                Name = name;
                Args = new Dictionary<string, object>();
            }
        }
    }

    public class ModuleManagerException : ApplicationException
    {
        public ModuleManagerException(string msg) : base(msg)
        {
        }
    }
}
