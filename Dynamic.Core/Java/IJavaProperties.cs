﻿using System.Collections;

namespace Dynamic.Java
{
    public interface IJavaProperties
    {
        string GetProperty(string key);
        string GetProperty(string key, string defaultValue);
        IJavaProperties GetPropertyView(string prefixKey);
        string SetProperty(string key, string newValue);
        IEnumerable PropertyNames();
    }
}
