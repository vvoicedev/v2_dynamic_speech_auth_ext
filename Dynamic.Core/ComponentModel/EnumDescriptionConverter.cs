﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Globalization;

namespace Dynamic.ComponentModel
{
    public class EnumDescriptionConverter : EnumConverter
    {
        private Type mEnumType;

        public EnumDescriptionConverter(Type type)
            : base(type)
        {
            mEnumType = type;
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
        {
            return destType == typeof(string);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
        {
            if (value != null)
            {
                FieldInfo fi = mEnumType.GetField(Enum.GetName(mEnumType, value));
                DescriptionAttribute dna = (DescriptionAttribute)Attribute.GetCustomAttribute(fi, typeof(DescriptionAttribute));

                if (dna != null)
                {
                    return dna.Description;
                }
                else
                {
                    return value.ToString();
                }
            }
            return "";
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type srcType)
        {
            return srcType == typeof(string);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            foreach (FieldInfo fi in mEnumType.GetFields())
            {
                DescriptionAttribute dna = (DescriptionAttribute)Attribute.GetCustomAttribute(fi, typeof(DescriptionAttribute));

                if ((dna != null) && ((string)value == dna.Description))
                {
                    return Enum.Parse(mEnumType, fi.Name);
                }
            }

            return Enum.Parse(mEnumType, (string)value);
        }
    }
}
