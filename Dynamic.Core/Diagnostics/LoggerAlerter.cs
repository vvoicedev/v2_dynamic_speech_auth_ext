﻿using System;
using Dynamic.Loggers;

namespace Dynamic.Diagnostics
{
    public class LoggerAlerter : IDebugAlerter, ILogger
    {
        private ILogger mLogger;

        public LoggerAlerter(ILogger logger)
        {
            mLogger = logger;
        }

        public string LastError => mLogger.LastError;

        public void Dispose()
        {
            mLogger?.Dispose();
            mLogger = null;
        }

        public void Show(string text, string caption)
        {
            if (!string.IsNullOrEmpty(text) && !string.IsNullOrEmpty(caption))
            {
                mLogger?.WriteErrorLog("Alert({0}): {1}", caption, text);
            }
            else if (!string.IsNullOrEmpty(text))
            {
                mLogger?.WriteErrorLog("Alert: {0}", text);
            }
            else if (!string.IsNullOrEmpty(caption))
            {
                mLogger?.WriteErrorLog("Alert: {0}", caption);
            }
            else
            {
                mLogger?.WriteErrorLog("Alert: Unknown");
            }
        }

        public void WriteDebugLog(string msg, params object[] data)
        {
            mLogger?.WriteDebugLog(msg, data);
        }

        public void WriteErrorLog(string msg, params object[] data)
        {
            mLogger?.WriteErrorLog(msg, data);
        }

        public void WriteErrorLog(Exception ex)
        {
            mLogger?.WriteErrorLog(ex);
        }

        public void WriteErrorLog(Exception ex, string msg, params object[] data)
        {
            mLogger?.WriteErrorLog(ex, msg, data);
        }

        public void WriteInfoLog(string msg, params object[] data)
        {
            mLogger?.WriteInfoLog(msg, data);
        }
    }
}
