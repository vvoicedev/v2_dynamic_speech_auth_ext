﻿using System;
using System.Collections.Generic;

namespace Dynamic.Diagnostics
{
    public class SplitterAlerter : IDebugAlerter
    {
        private List<IDebugAlerter> mAlerterList;

        public SplitterAlerter()
        {
            mAlerterList = new List<IDebugAlerter>();
        }

        public void AddAlerter(IDebugAlerter alerter)
        {
            if(!mAlerterList.Contains(alerter))
            {
                mAlerterList.Add(alerter);
            }
        }

        public void RemoveAlerter(IDebugAlerter alerter)
        {
            mAlerterList.Remove(alerter);
        }

        public void Clear()
        {
            mAlerterList.Clear();
        }

        public void Show(string text, string caption)
        {
            foreach(var alerter in mAlerterList)
            {
                alerter.Show(text, caption);
            }
        }
    }
}
