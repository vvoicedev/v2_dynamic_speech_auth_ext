﻿using System.Diagnostics;

namespace Dynamic.Diagnostics
{
    public class DebuggerAlerter : IDebugAlerter
    {
        public void Show(string text, string caption)
        {
            Debugger.Log(1, caption, text);
            Debugger.Break();
        }
    }
}
