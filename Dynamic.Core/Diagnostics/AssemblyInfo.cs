﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Resources;

using Dynamic.Extensions;

namespace Dynamic.Diagnostics
{
    public class AssemblyInfo
    {
        public string Title { get; private set; }
        public string Description { get; private set; }
        public string Company { get; private set; }
        public string Product { get; private set; }
        public string Copyright { get; private set; }
        public string Trademark { get; private set; }
        public string AssemblyVersion { get; private set; }
        public string FileVersion { get; private set; }
        public string Guid { get; private set; }
        public string NeutralLanguage { get; private set; }
        public bool IsComVisible { get; private set; }

        public AssemblyInfo()
            : this(Assembly.GetExecutingAssembly())
        {
        }

        public AssemblyInfo(Assembly assembly)
        {
            // Get values from the assembly.
            AssemblyTitleAttribute titleAttr = assembly.GetAttribute<AssemblyTitleAttribute>();
            if (titleAttr != null) Title = titleAttr.Title;

            AssemblyDescriptionAttribute assemblyAttr = assembly.GetAttribute<AssemblyDescriptionAttribute>();
            if (assemblyAttr != null) Description = assemblyAttr.Description;

            AssemblyCompanyAttribute companyAttr = assembly.GetAttribute<AssemblyCompanyAttribute>();
            if (companyAttr != null) Company = companyAttr.Company;

            AssemblyProductAttribute productAttr = assembly.GetAttribute<AssemblyProductAttribute>();
            if (productAttr != null) Product = productAttr.Product;

            AssemblyCopyrightAttribute copyrightAttr = assembly.GetAttribute<AssemblyCopyrightAttribute>();
            if (copyrightAttr != null) Copyright = copyrightAttr.Copyright;

            AssemblyTrademarkAttribute trademarkAttr = assembly.GetAttribute<AssemblyTrademarkAttribute>();
            if (trademarkAttr != null) Trademark = trademarkAttr.Trademark;

            AssemblyVersion = assembly.GetName().Version.ToString();

            AssemblyFileVersionAttribute fileVersionAttr = assembly.GetAttribute<AssemblyFileVersionAttribute>();
            if (fileVersionAttr != null) FileVersion = fileVersionAttr.Version;

            GuidAttribute guidAttr = assembly.GetAttribute<GuidAttribute>();
            if (guidAttr != null) Guid = guidAttr.Value;

            NeutralResourcesLanguageAttribute languageAttr = assembly.GetAttribute<NeutralResourcesLanguageAttribute>();
            if (languageAttr != null) NeutralLanguage = languageAttr.CultureName;

            ComVisibleAttribute comAttr = assembly.GetAttribute<ComVisibleAttribute>();
            if (comAttr != null) IsComVisible = comAttr.Value;
        }
    }
}
