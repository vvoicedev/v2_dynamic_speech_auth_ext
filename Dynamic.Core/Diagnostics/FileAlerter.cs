﻿using System;
using System.Diagnostics;
using System.IO;

namespace Dynamic.Diagnostics
{
    public class FileAlerter : IDebugAlerter
    {
        #region Private Variables

        private string mPath;

        #endregion

        #region Constructor

        public FileAlerter(string path)
        {
            mPath = path;
        }

        #endregion

        #region

        public void Show(string text, string caption)
        {
            if (!string.IsNullOrEmpty(text) && !string.IsNullOrEmpty(caption))
            {
                using (var writer = new StreamWriter(mPath, true))
                {
                    writer.WriteLine("{0}: Error({1}): {2}", DateTime.Now.ToString(), caption, text);
                }
            }
            else if (!string.IsNullOrEmpty(text))
            {
                using (var writer = new StreamWriter(mPath, true))
                {
                    writer.WriteLine("{0}: Error: {1}", DateTime.Now.ToString(), text);
                }
            }
            else if (!string.IsNullOrEmpty(caption))
            {
                using (var writer = new StreamWriter(mPath, true))
                {
                    writer.WriteLine("{0}: Error: {1}", DateTime.Now.ToString(), caption);
                }
            }
            else
            {
                using (var writer = new StreamWriter(mPath, true))
                {
                    writer.WriteLine("{0}: Error: Unknown", DateTime.Now.ToString());
                }
            }
        }

        #endregion
    }
}
