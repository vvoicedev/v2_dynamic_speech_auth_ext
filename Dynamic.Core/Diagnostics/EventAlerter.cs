﻿using System.Diagnostics;

namespace Dynamic.Diagnostics
{
    public class EventAlerter : IDebugAlerter
    {
        #region Private Variables

        private EventLog mEventLog;

        #endregion

        #region Constructor

        public EventAlerter(string source)
        {
            mEventLog = new EventLog();
            if (!EventLog.SourceExists(source))
            {
                EventLog.CreateEventSource(source, "Application");
            }
            mEventLog.Source = source;
            mEventLog.Log = "Application";
        }

        public EventAlerter(string source, string log)
        {
            mEventLog = new EventLog();
            if (!EventLog.SourceExists(source))
            {
                EventLog.CreateEventSource(source, log);
            }
            mEventLog.Source = source;
            mEventLog.Log = log;
        }

        #endregion

        #region

        public void Show(string text, string caption)
        {
            mEventLog.WriteEntry(string.Format("Error({0}): {1}", caption, text), EventLogEntryType.Error);
        }

        #endregion
    }
}
