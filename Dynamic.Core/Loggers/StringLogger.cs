﻿using System;
using System.Text;

namespace Dynamic.Loggers
{
    public class StringLogger : ILogger
    {
        private StringBuilder mStringBuilder;

        public StringLogger()
        {
            mStringBuilder = new StringBuilder();
        }

        public void Dispose()
        {

        }

        public void Clear()
        {
            mStringBuilder.Length = 0;
        }

        public string GetLogData()
        {
            return mStringBuilder.ToString();
        }

        public string LastError { get; private set; }

        public void WriteInfoLog(string msg, params object[] data)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                AppendToLog("Info: " + string.Format(msg, data));
            }
        }

        public void WriteDebugLog(string msg, params object[] data)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                AppendToLog("Debug: " + string.Format(msg, data));
            }
        }

        public void WriteErrorLog(string msg, params object[] data)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                if (data.Length > 0)
                {
                    AppendToLog("Error: " + (LastError = string.Format(msg, data)));
                }
                else
                {
                    AppendToLog("Error: " + (LastError = msg));
                }
            }
        }

        public void WriteErrorLog(Exception ex)
        {
            AppendToLog("Exception: " + (LastError = ex.Message.ToString().Trim()));
            AppendToLog("StackTrace: " + ex.StackTrace.Trim());
        }

        public void WriteErrorLog(Exception ex, string fmt, params object[] args)
        {
            AppendToLog("Exception: " + (LastError = ex.Message.ToString().Trim()));
            AppendToLog("StackTrace: " + ex.StackTrace.Trim());
            if (!string.IsNullOrEmpty(fmt))
            {
                AppendToLog("ErrorData: " + string.Format(fmt, args));
            }
        }

        private void AppendToLog(string msg)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                mStringBuilder.AppendLine(msg);
            }
        }
    }
}
