﻿using System;

namespace Dynamic.Loggers
{
    [Flags]
    public enum LogLevel
    {
        None,
        Error,
        Info,
        Debug,
        All
    }

    public class LogLevelLogger : ILogger
    {
        private ILogger mLogger;

        public LogLevelLogger(ILogger logger)
        {
            mLogger = logger;
            LogLevel = LogLevel.Error;
        }

        public LogLevelLogger(ILogger logger, LogLevel logLevel)
        {
            mLogger = logger;
            LogLevel = logLevel;
        }

        public void Dispose()
        {
            if(mLogger != null)
            {
                mLogger.Dispose();
            }
        }

        public LogLevel LogLevel { get; set; }

        public string LastError { get { return mLogger.LastError; } }

        public void WriteDebugLog(string msg, params object[] data)
        {
            if (LogLevel >= LogLevel.Debug)
            {
                mLogger.WriteDebugLog(msg, data);
            }
        }

        public void WriteInfoLog(string msg, params object[] data)
        {
            if (LogLevel >= LogLevel.Info)
            {
                mLogger.WriteInfoLog(msg, data);
            }
        }

        public void WriteErrorLog(string msg, params object[] data)
        {
            if (LogLevel >= LogLevel.Error)
            {
                mLogger.WriteErrorLog(msg, data);
            }
        }

        public void WriteErrorLog(Exception ex)
        {
            if (LogLevel >= LogLevel.Error)
            {
                mLogger.WriteErrorLog(ex);
            }
        }

        public void WriteErrorLog(Exception ex, string msg, params object[] data)
        {
            if (LogLevel >= LogLevel.Error)
            {
                mLogger.WriteErrorLog(ex, msg, data);
            }
        }
    }
}
