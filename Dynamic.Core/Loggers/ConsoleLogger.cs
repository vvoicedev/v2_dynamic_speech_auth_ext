﻿using System;

namespace Dynamic.Loggers
{
    public class ConsoleLogger : ILogger
    {
        private string mTag;

        public ConsoleLogger()
        {
            mTag = "";
        }

        public ConsoleLogger(string tag)
        {
            mTag = tag;
        }

        public void Dispose()
        {

        }

        public string LastError { get; private set; }

        public void WriteInfoLog(string msg, params object[] data)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                if(data.Length != 0)
                {
                    AppendToLog("[Info]: " + string.Format(msg, data));
                }
                else
                {
                    AppendToLog("[Info]: " + msg);
                }
            }
        }

        public void WriteDebugLog(string msg, params object[] data)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                if (data.Length != 0)
                {
                    AppendToLog("[Debug]: " + string.Format(msg, data));
                }
                else
                {
                    AppendToLog("[Debug]: " + msg);
                }
            }
        }

        public void WriteErrorLog(string msg, params object[] data)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                if (data.Length != 0)
                {
                    AppendToLog("[Error]: " + (LastError = string.Format(msg, data)));
                }
                else
                {
                    AppendToLog("[Error]: " + (LastError = msg));
                }
            }
        }

        public void WriteErrorLog(Exception ex)
        {
            AppendToLog("[Exception]: " + (LastError = ex.Message.ToString().Trim()));
            AppendToLog("[StackTrace]: " + ex.StackTrace.Trim());
        }

        public void WriteErrorLog(Exception ex, string msg, params object[] data)
        {
            AppendToLog("[Exception]: " + (LastError = ex.Message.ToString().Trim()));
            AppendToLog("[StackTrace]: " + ex.StackTrace.Trim());
            if (!string.IsNullOrEmpty(msg))
            {
                if (data.Length != 0)
                {
                    AppendToLog("[Error]: " + string.Format(msg, data));
                }
                else
                {
                    AppendToLog("[Error]: " + msg);
                }
            }
        }

        private void AppendToLog(string msg)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                if (!string.IsNullOrEmpty(mTag))
                {
                    Console.WriteLine("[" + mTag + "] " + msg);
                }
                else
                {
                    Console.WriteLine(msg);
                }
            }
        }
    }
}
