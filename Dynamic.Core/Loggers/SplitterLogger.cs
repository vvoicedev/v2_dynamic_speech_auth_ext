﻿using System;
using System.Collections.Generic;

namespace Dynamic.Loggers
{
    public class SplitterLogger : ILogger
    {
        #region Private

        private List<ILogger> mLoggers;

        #endregion

        public SplitterLogger()
        {
            mLoggers = new List<ILogger>();
        }

        public SplitterLogger(List<ILogger> list)
        {
            mLoggers = new List<ILogger>(list);
        }

        public void Dispose()
        {

        }

        public void AddLogger(ILogger logger)
        {
            if(-1 == mLoggers.IndexOf(logger))
            {
                mLoggers.Add(logger);
            }
        }

        public void RemoveLogger(ILogger logger)
        {
            mLoggers.Remove(logger);
        }

        public bool HasLogger(ILogger logger)
        {
            return mLoggers.IndexOf(logger) != -1;
        }

        public string LastError { get; private set; }

        public void WriteInfoLog(string msg, params object[] data)
        {
            foreach (var logger in mLoggers)
            {
                logger.WriteInfoLog(msg, data);
            }
        }

        public void WriteDebugLog(string msg, params object[] data)
        {
            foreach (var logger in mLoggers)
            {
                logger.WriteDebugLog(msg, data);
            }
        }

        public void WriteErrorLog(string msg, params object[] data)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                if (data.Length > 0)
                {
                    LastError = string.Format(msg, data);
                }
                else
                {
                    LastError = msg;
                }
            }
            foreach (var logger in mLoggers)
            {
                logger.WriteErrorLog(msg, data);
            }
        }

        public void WriteErrorLog(Exception ex)
        {
            LastError = ex.Message.ToString().Trim();
            foreach (var logger in mLoggers)
            {
                logger.WriteErrorLog(ex);
            }
        }

        public void WriteErrorLog(Exception ex, string fmt, params object[] args)
        {
            LastError = ex.Message.ToString().Trim();
            foreach (var logger in mLoggers)
            {
                logger.WriteErrorLog(ex, fmt, args);
            }
        }
    }
}
