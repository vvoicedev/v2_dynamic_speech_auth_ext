﻿using System;

namespace Dynamic.Loggers
{
    public class NullLogger : ILogger
    {
        public string LastError { get { return ""; } }

        public void Dispose()
        {   
        }

        public void WriteDebugLog(string msg, params object[] data)
        {   
        }

        public void WriteErrorLog(string msg, params object[] data)
        {   
        }

        public void WriteErrorLog(Exception ex)
        {   
        }

        public void WriteErrorLog(Exception ex, string msg, params object[] data)
        {   
        }

        public void WriteInfoLog(string msg, params object[] data)
        {
        }
    }
}
