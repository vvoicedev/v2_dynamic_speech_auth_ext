﻿using System;
using System.Threading;

using Dynamic.Collections;

namespace Dynamic.Loggers
{
    public class BackgroundLogger : ILogger, ISourceLogger
    {
        private ILogger mLogger;
        private ProducerConsumerQueue<LogItem> mPendingItems;
        private bool mIsWorking;

        public BackgroundLogger()
        {
            mLogger = null;
        }

        public BackgroundLogger(ILogger logger)
        {
            mLogger = logger;
        }

        ~BackgroundLogger()
        {
            Dispose();
        }

        public string LastError { get; set; }

        public void WriteInfoLog(string msg, params object[] data)
        {
            if (mLogger != null)
            {
                var item = new LogItem();
                item.Type = LogItem.LogType.Info;
                item.Message = msg;
                item.Data = data;
                item.TargetLogger = mLogger;
                item.SourceLogger = this;
                mPendingItems.Add(item);
            }
        }

        public void WriteDebugLog(string msg, params object[] data)
        {
            if (mLogger != null)
            {
                var item = new LogItem();
                item.Type = LogItem.LogType.Debug;
                item.Message = msg;
                item.Data = data;
                item.TargetLogger = mLogger;
                item.SourceLogger = this;
                mPendingItems.Add(item);
            }
        }

        public void WriteErrorLog(string msg, params object[] data)
        {
            if (mLogger != null)
            {
                var item = new LogItem();
                item.Type = LogItem.LogType.ErrorMessage;
                item.Message = msg;
                item.Data = data;
                item.TargetLogger = mLogger;
                item.SourceLogger = this;
                mPendingItems.Add(item);
            }
        }

        public void WriteErrorLog(Exception ex)
        {
            if (mLogger != null)
            {
                var item = new LogItem();
                item.Type = LogItem.LogType.ErrorException;
                item.MessageException = ex;
                item.TargetLogger = mLogger;
                item.SourceLogger = this;
                mPendingItems.Add(item);
            }
        }

        public void WriteErrorLog(Exception ex, string msg, params object[] data)
        {
            if (mLogger != null)
            {
                var item = new LogItem();
                item.Type = LogItem.LogType.ErrorExceptionMessage;
                item.MessageException = ex;
                item.Message = msg;
                item.Data = data;
                item.TargetLogger = mLogger;
                item.SourceLogger = this;
                mPendingItems.Add(item);
            }
        }
        
        public void Dispose()
        {
            Stop();
        }

        public void Start()
        {
            if (!mIsWorking)
            {
                mIsWorking = true;
                mPendingItems = new ProducerConsumerQueue<LogItem>(16);
                ThreadPool.QueueUserWorkItem
                (
                    delegate
                    {
                        foreach (LogItem item in mPendingItems)
                        {
                            HandleLogItem(item);
                        }
                        mIsWorking = false;
                    }
                );
            }
        }

        public void Stop()
        {
            if (mPendingItems != null)
            {
                mPendingItems.Close(true);
                mPendingItems = null;
            }
        }

        public bool IsWorking()
        {
            return mIsWorking;
        }

        public ILogger CreateLogTarget(ILogger logger)
        {
            return new BackgroundTargetLogger(logger, mPendingItems);
        }

        private void HandleLogItem(LogItem item)
        {
            switch (item.Type)
            {
                case LogItem.LogType.Info:
                    item.TargetLogger.WriteInfoLog(item.Message, item.Data);
                    break;
                case LogItem.LogType.Debug:
                    item.TargetLogger.WriteDebugLog(item.Message, item.Data);
                    break;
                case LogItem.LogType.ErrorMessage:
                    item.TargetLogger.WriteErrorLog(item.Message, item.Data);
                    item.SourceLogger.LastError = item.TargetLogger.LastError;
                    break;
                case LogItem.LogType.ErrorException:
                    item.TargetLogger.WriteErrorLog(item.MessageException);
                    item.SourceLogger.LastError = item.TargetLogger.LastError;
                    break;
                case LogItem.LogType.ErrorExceptionMessage:
                    item.TargetLogger.WriteErrorLog(item.MessageException, item.Message, item.Data);
                    item.SourceLogger.LastError = item.TargetLogger.LastError;
                    break;
            }
        }

        private struct LogItem
        {
            public enum LogType
            {
                Info,
                Debug,
                ErrorMessage,
                ErrorException,
                ErrorExceptionMessage
            }

            public LogType Type { get; set; }
            public string Message { get; set; }
            public object[] Data { get; set; }
            public Exception MessageException { get; set; }
            public ILogger TargetLogger { get; set; }
            public ISourceLogger SourceLogger { get; set; }
        }

        private class BackgroundTargetLogger : ILogger, ISourceLogger
        {
            private ILogger mLogger;
            private ProducerConsumerQueue<LogItem> mPendingItems;

            public BackgroundTargetLogger(ILogger logger, ProducerConsumerQueue<LogItem> pendingItems)
            {
                mLogger = logger;
                mPendingItems = pendingItems;
            }

            ~BackgroundTargetLogger()
            {
                Dispose();
            }

            public void Dispose()
            {
                if (mLogger != null)
                {
                    mLogger.Dispose();
                    mLogger = null;
                    mPendingItems = null;
                }
            }

            public string LastError { get; set; }

            public void WriteInfoLog(string msg, params object[] data)
            {
                if (mPendingItems != null && !mPendingItems.IsClosed)
                {
                    var item = new LogItem();
                    item.Type = LogItem.LogType.Info;
                    item.Message = msg;
                    item.Data = data;
                    item.TargetLogger = mLogger;
                    item.SourceLogger = this;
                    mPendingItems.Add(item);
                }
            }

            public void WriteDebugLog(string msg, params object[] data)
            {
                if (mPendingItems != null && !mPendingItems.IsClosed)
                {
                    var item = new LogItem();
                    item.Type = LogItem.LogType.Debug;
                    item.Message = msg;
                    item.Data = data;
                    item.TargetLogger = mLogger;
                    item.SourceLogger = this;
                    mPendingItems.Add(item);
                }
            }

            public void WriteErrorLog(string msg, params object[] data)
            {
                if (mPendingItems != null && !mPendingItems.IsClosed)
                {
                    var item = new LogItem();
                    item.Type = LogItem.LogType.ErrorMessage;
                    item.Message = msg;
                    item.Data = data;
                    item.TargetLogger = mLogger;
                    item.SourceLogger = this;
                    mPendingItems.Add(item);
                }
            }

            public void WriteErrorLog(Exception ex)
            {
                if (mPendingItems != null && !mPendingItems.IsClosed)
                {
                    var item = new LogItem();
                    item.Type = LogItem.LogType.ErrorException;
                    item.MessageException = ex;
                    item.TargetLogger = mLogger;
                    item.SourceLogger = this;
                    mPendingItems.Add(item);
                }
            }

            public void WriteErrorLog(Exception ex, string msg, params object[] data)
            {
                if (mPendingItems != null && !mPendingItems.IsClosed)
                {
                    var item = new LogItem();
                    item.Type = LogItem.LogType.ErrorExceptionMessage;
                    item.MessageException = ex;
                    item.Message = msg;
                    item.Data = data;
                    item.TargetLogger = mLogger;
                    item.SourceLogger = this;
                    mPendingItems.Add(item);
                }
            }
        }
    }
}
