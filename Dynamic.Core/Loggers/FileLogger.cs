﻿using System;
using System.IO;

namespace Dynamic.Loggers
{
    public class FileLogger : ILogger
    {
        private FileStream mFileStream;
        private StreamWriter mWriter;

        public FileLogger(string path)
        {
            Path = path;
            mFileStream = new FileStream(path, FileMode.Append, FileAccess.Write, FileShare.Read);
            mWriter = new StreamWriter(mFileStream);
            mWriter.AutoFlush = true;
        }

        public void Dispose()
        {
            if (mFileStream != null)
            {
                mWriter.Flush();
                mWriter.Close();
                mWriter = null;
                mFileStream = null;
            }
        }

        ~FileLogger()
        {
            Dispose();
        }

        public void Clear()
        {
            if (mFileStream != null)
            {
                mFileStream.SetLength(0);
            }
        }

        public string Path { get; private set; }

        public string LastError { get; private set; }

        public void WriteInfoLog(string msg, params object[] data)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                if (data.Length != 0)
                {
                    AppendToLog("[Info]: " + string.Format(msg, data));
                }
                else
                {
                    AppendToLog("[Info]: " + msg);
                }
            }
        }

        public void WriteDebugLog(string msg, params object[] data)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                if (data.Length != 0)
                {
                    AppendToLog("[Debug]: " + string.Format(msg, data));
                }
                else
                {
                    AppendToLog("[Debug]: " + msg);
                }
            }
        }

        public void WriteErrorLog(string msg, params object[] data)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                if (data.Length != 0)
                {
                    AppendToLog("[Error]: " + (LastError = string.Format(msg, data)));
                }
                else
                {
                    AppendToLog("[Error]: " + (LastError = msg));
                }
            }
        }

        public void WriteErrorLog(Exception ex)
        {
            AppendToLog("[Exception]: " + (LastError = ex.Message.ToString().Trim()));
            AppendToLog("[StackTrace]: " + ex.StackTrace.Trim());
        }

        public void WriteErrorLog(Exception ex, string msg, params object[] data)
        {
            AppendToLog("[Exception]: " + (LastError = ex.Message.ToString().Trim()));
            AppendToLog("[StackTrace]: " + ex.StackTrace.Trim());
            if (!string.IsNullOrEmpty(msg))
            {
                if (data.Length != 0)
                {
                    AppendToLog("[Error]: " + string.Format(msg, data));
                }
                else
                {
                    AppendToLog("[Error]: " + msg);
                }
            }
        }

        private void AppendToLog(string msg)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                mWriter.WriteLine("{0}: {1}", DateTime.Now.ToString(), msg);
                mWriter.Flush();
                mFileStream.Flush();
            }
        }
    }
}
