﻿using System;
using System.Diagnostics;

namespace Dynamic.Loggers
{
    public class EventLogger : ILogger
    {
        #region Private Variables

        private EventLog mEventLog;

        #endregion

        #region Constructor

        public EventLogger(string source)
        {
            mEventLog = new EventLog();
            if (!EventLog.SourceExists(source))
            {
                EventLog.CreateEventSource(source, "Application");
            }
            mEventLog.Source = source;
            mEventLog.Log = "Application";
        }

        public EventLogger(string source, string log)
        {
            mEventLog = new EventLog();
            if (!EventLog.SourceExists(source))
            {
                EventLog.CreateEventSource(source, log);
            }
            mEventLog.Source = source;
            mEventLog.Log = log;
        }

        public void Dispose()
        {

        }

        #endregion

        #region Public Getters

        public string LastError { get; private set; }

        #endregion

        #region Public Methods

        public void WriteInfoLog(string msg, params object[] data)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                mEventLog.WriteEntry("Info: " + string.Format(msg, data), EventLogEntryType.Information);
            }
        }

        public void WriteDebugLog(string msg, params object[] data)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                mEventLog.WriteEntry("Debug: " + string.Format(msg, data), EventLogEntryType.Information);
            }
        }

        public void WriteErrorLog(string msg, params object[] data)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                if (data.Length > 0)
                {
                    mEventLog.WriteEntry("Error: " + (LastError = string.Format(msg, data)), EventLogEntryType.Error);
                }
                else
                {
                    mEventLog.WriteEntry("Error: " + (LastError = msg), EventLogEntryType.Error);
                }
            }
        }

        public void WriteErrorLog(Exception ex)
        {
            LastError = ex.Message.ToString().Trim();
            mEventLog.WriteEntry(string.Format("Exception: %s\r\nStackTrace: %s", LastError, ex.StackTrace.Trim()), EventLogEntryType.Error);
        }

        public void WriteErrorLog(Exception ex, string fmt, params object[] args)
        {
            LastError = ex.Message.ToString().Trim();
            if (!string.IsNullOrEmpty(fmt))
            {
                mEventLog.WriteEntry(string.Format("Exception: %s\r\nErrorData: %s\r\nStackTrace: %s", LastError, string.Format(fmt, args), ex.StackTrace.Trim()), EventLogEntryType.Error);
            }
            else
            {
                mEventLog.WriteEntry(string.Format("Exception: %s\r\nStackTrace: %s", LastError, ex.StackTrace.Trim()), EventLogEntryType.Error);
            }
        }

        #endregion

    }
}
