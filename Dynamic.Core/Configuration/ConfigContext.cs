﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

using Dynamic.Java;
using Dynamic.IO;

// Deprecated
namespace Dynamic.Configuration
{
    public class ConfigContext : IConfigContext
    {
        #region Private

        private string mFilePath = "";
        private bool mEncrypted = false;
        private object mLock = new object();
        private int mChanges = 0;
        private bool mDeferUpdate = false;
        private JavaProperties mProperties = new JavaProperties();
        private ConfigWatcher mConfigWatcher = null;

        #endregion

        #region Private LockedConfigContext

        private sealed class LockedConfigContext : IConfigContext
        {
            private string mLockedName;
            private ConfigContext mOuter;

            internal LockedConfigContext(ConfigContext outer, string lockedName)
            {
                mLockedName = lockedName;
                mOuter = outer;
                Monitor.Enter(mOuter.mLock);
                Console.WriteLine("Locked: " + lockedName);
            }

            #region ILockedConfigContext

            public int Count { get { return mOuter.mProperties.Count; } }

            public bool Has(string key)
            {
                return mOuter.mProperties.ContainsKey(key);
            }
            
            public bool HasPrefix(string keyPrefix)
            {
                foreach (var key in mOuter.mProperties.Keys)
                {
                    string inspectKey = key as string ?? "";
                    if (inspectKey.StartsWith(keyPrefix))
                    {
                        return true;
                    }
                }
                return false;
            }

            public string Get(string key)
            {
                return mOuter.mProperties.GetProperty(key);
            }

            public string Get(string key, string def)
            {
                return mOuter.mProperties.GetProperty(key, def);
            }

            public IEnumerable GetKeys()
            {
                return mOuter.mProperties.PropertyNames();
            }

            public IConfigContext GetView(string prefixKey)
            {
                return new ConfigContextView(mOuter, prefixKey);
            }

            public void Put(string key, string value)
            {
                mOuter.mProperties.SetProperty(key, value);
                mOuter.mChanges++;
                mOuter.UpdateFile();
            }

            public void Append(string key, string value)
            {
                Append(key, value, "");
            }

            public void Append(string key, string value, string seperator)
            {
                string data = mOuter.mProperties.GetProperty(key);
                if (string.IsNullOrEmpty(data))
                {
                    data = "";
                }
                else if (seperator != string.Empty && !data.EndsWith(seperator))
                {
                    data = data + seperator;
                }
                mOuter.mProperties.SetProperty(key, data + value + seperator);
                mOuter.mChanges++;
                mOuter.UpdateFile();
            }

            public void Remove(string key)
            {
                mOuter.mProperties.Remove(key);
                mOuter.mChanges++;
                mOuter.UpdateFile();
            }

            public void Remove(string key, string value)
            {
                Remove(key, value, "");
            }

            public void Remove(string key, string value, string seperator)
            {
                string data = mOuter.mProperties.GetProperty(key);
                if (!string.IsNullOrEmpty(data) && !string.IsNullOrEmpty(value) && data.Contains(value + seperator))
                {
                    mOuter.mProperties.SetProperty(key, data.Replace(value + seperator, ""));
                    mOuter.mChanges++;
                    mOuter.UpdateFile();
                }
            }

            public void Clear()
            {
                mOuter.mProperties.Clear();
                mOuter.mChanges++;
                mOuter.UpdateFile();
            }

            public void BeginUpdate()
            {
                mOuter.mDeferUpdate = true;
            }

            public void EndUpdate()
            {
                mOuter.mDeferUpdate = false;
                mOuter.UpdateFile();
            }

            public IConfigContext Lock(string lockerName)
            {
                throw new Exception("Lock attempted by: " + lockerName + ". Config Context already locked");
            }

            #endregion

            #region IDisposable Members

            public void Dispose()
            {
                Console.WriteLine("Legally Unlocking: " + mLockedName);
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            private void Dispose(bool disposing)
            {
                Debug.Assert(disposing, "ILockedConfigContext implementations must be explicitly disposed");
                if (disposing)
                {
                    Monitor.Exit(mOuter.mLock);
                }
            }

            ~LockedConfigContext()
            {
                Console.WriteLine("Illegally Unlocking: " + mLockedName);
                Dispose(false);
            }

            #endregion
        }

        #endregion

        #region Private LockedConfigContextView

        private sealed class LockedConfigContextView : IConfigContext
        {
            private string mLockedName;
            private string mPrefixKey;
            private bool mIsAlreadyLocked;
            private ConfigContext mOuter;

            internal LockedConfigContextView(ConfigContext outer, string lockedName, string prefixKey, bool isAlreadyLocked)
            {
                mLockedName = lockedName;
                mPrefixKey = prefixKey;
                mIsAlreadyLocked = isAlreadyLocked;
                mOuter = outer;
                if (!isAlreadyLocked)
                {
                    Monitor.Enter(mOuter.mLock);
                    Console.WriteLine("Locked: " + lockedName + ", PrefixKey: " + prefixKey);
                }
            }

            #region ILockedConfigContext

            public int Count
            {
                get
                {
                    int i = 0;
                    foreach (var key in GetKeys())
                    {
                        ++i;
                    }
                    return i;
                }
            }

            public bool Has(string key)
            {
                if (!key.StartsWith(mPrefixKey))
                {
                    key = mPrefixKey + "." + key;
                }
                return mOuter.mProperties.ContainsKey(key);
            }
            
            public bool HasPrefix(string keyPrefix)
            {
                if (!keyPrefix.StartsWith(mPrefixKey))
                {
                    keyPrefix = mPrefixKey + "." + keyPrefix;
                }
                foreach (var key in mOuter.mProperties.Keys)
                {
                    string inspectKey = key as string ?? "";
                    if (inspectKey.StartsWith(keyPrefix))
                    {
                        return true;
                    }
                }
                return false;
            }

            public string Get(string key)
            {
                if (!key.StartsWith(mPrefixKey))
                {
                    key = mPrefixKey + "." + key;
                }
                return mOuter.mProperties.GetProperty(key);
            }

            public string Get(string key, string def)
            {
                if (!key.StartsWith(mPrefixKey))
                {
                    key = mPrefixKey + "." + key;
                }
                return mOuter.mProperties.GetProperty(key, def);
            }

            public IEnumerable GetKeys()
            {
                Hashtable table = new Hashtable();
                foreach (string key in mOuter.mProperties.PropertyNames())
                {
                    if (key.StartsWith(mPrefixKey))
                    {
                        table.Add(key, key);
                    }
                }
                return table.Keys;
            }

            public IConfigContext GetView(string prefixKey)
            {
                return new LockedConfigContextView(mOuter, mLockedName, mPrefixKey + "." + prefixKey, true);
            }

            public void Put(string key, string value)
            {
                if (!key.StartsWith(mPrefixKey))
                {
                    key = mPrefixKey + "." + key;
                }
                mOuter.mProperties.SetProperty(key, value);
                mOuter.mChanges++;
                mOuter.UpdateFile();
            }

            public void Append(string key, string value)
            {
                Append(key, value, "");
            }

            public void Append(string key, string value, string seperator)
            {
                if (!key.StartsWith(mPrefixKey))
                {
                    key = mPrefixKey + "." + key;
                }
                string data = mOuter.mProperties.GetProperty(key);
                if (string.IsNullOrEmpty(data))
                {
                    data = "";
                }
                else if (seperator != string.Empty && !data.EndsWith(seperator))
                {
                    data = data + seperator;
                }
                mOuter.mProperties.SetProperty(key, data + value + seperator);
                mOuter.mChanges++;
                mOuter.UpdateFile();
            }

            public void Remove(string key)
            {
                if (!key.StartsWith(mPrefixKey))
                {
                    key = mPrefixKey + "." + key;
                }
                mOuter.mProperties.Remove(key);
                mOuter.mChanges++;
                mOuter.UpdateFile();
            }

            public void Remove(string key, string value)
            {
                Remove(key, value, "");
            }

            public void Remove(string key, string value, string seperator)
            {
                if (!key.StartsWith(mPrefixKey))
                {
                    key = mPrefixKey + "." + key;
                }
                string data = mOuter.mProperties.GetProperty(key);
                if (!string.IsNullOrEmpty(data) && !string.IsNullOrEmpty(value) && data.Contains(value + seperator))
                {
                    mOuter.mProperties.SetProperty(key, data.Replace(value + seperator, ""));
                    mOuter.mChanges++;
                    mOuter.UpdateFile();
                }
            }

            public void Clear()
            {
                if(string.IsNullOrEmpty(mPrefixKey))
                {
                    mOuter.mProperties.Clear();
                }
                else
                {
                    List<string> keysToRemove = new List<string>();
                    foreach(string key in mOuter.mProperties.Keys)
                    {
                        if(key.StartsWith(mPrefixKey))
                        {
                            keysToRemove.Add(key);
                        }
                    }
                    foreach (var key in keysToRemove)
                    {
                        mOuter.mProperties.Remove(key);
                    }
                }
                mOuter.mChanges++;
                mOuter.UpdateFile();
            }

            public void BeginUpdate()
            {
                mOuter.mDeferUpdate = true;
            }

            public void EndUpdate()
            {
                mOuter.mDeferUpdate = false;
                mOuter.UpdateFile();
            }

            public IConfigContext Lock(string lockerName)
            {
                throw new Exception("Lock attempted by: " + lockerName + ", PrefixKey: " + mPrefixKey + ". Config Context already locked");
            }

            #endregion

            #region IDisposable Members

            public void Dispose()
            {
                if (!mIsAlreadyLocked)
                {
                    Console.WriteLine("Legally Unlocking: " + mLockedName + ", PrefixKey: " + mPrefixKey);
                    Dispose(true);
                    GC.SuppressFinalize(this);
                }
            }

            private void Dispose(bool disposing)
            {
                if (!mIsAlreadyLocked)
                {
                    Debug.Assert(disposing, "ILockedConfigContext implementations must be explicitly disposed");
                    if (disposing)
                    {
                        Monitor.Exit(mOuter.mLock);
                    }
                }
            }

            ~LockedConfigContextView()
            {
                if (!mIsAlreadyLocked)
                {
                    Console.WriteLine("Illegally Unlocking: " + mLockedName + ", PrefixKey: " + mPrefixKey);
                    Dispose(false);
                }
            }

            #endregion
        }

        #endregion

        #region Private ConfigContextView

        private sealed class ConfigContextView : IConfigContext
        {
            private string mPrefixKey;
            private ConfigContext mOuter;

            internal ConfigContextView(ConfigContext outer, string prefixKey)
            {
                mOuter = outer;
                mPrefixKey = prefixKey;
            }

            #region ILockedConfigContext

            public int Count
            {
                get
                {
                    int i = 0;
                    foreach(var key in GetKeys())
                    {
                        ++i;
                    }
                    return i;
                }
            }

            public bool Has(string key)
            {
                if (!key.StartsWith(mPrefixKey))
                {
                    key = mPrefixKey + "." + key;
                }
                return mOuter.Has(key);
            }

            public bool HasPrefix(string keyPrefix)
            {
                if (!keyPrefix.StartsWith(mPrefixKey))
                {
                    keyPrefix = mPrefixKey + "." + keyPrefix;
                }
                return mOuter.HasPrefix(keyPrefix);
            }

            public string Get(string key)
            {
                if (!key.StartsWith(mPrefixKey))
                {
                    key = mPrefixKey + "." + key;
                }
                return mOuter.Get(key);
            }

            public string Get(string key, string def)
            {
                if (!key.StartsWith(mPrefixKey))
                {
                    key = mPrefixKey + "." + key;
                }
                return mOuter.Get(key, def);
            }

            public IConfigContext GetView(string prefixKey)
            {
                return new ConfigContextView(mOuter, mPrefixKey + "." + prefixKey);
            }

            public void Put(string key, string value)
            {
                if (!key.StartsWith(mPrefixKey))
                {
                    key = mPrefixKey + "." + key;
                }
                mOuter.Put(key, value);
            }

            public void Append(string key, string value)
            {
                Append(key, value, "");
            }

            public void Append(string key, string value, string seperator)
            {
                if (!key.StartsWith(mPrefixKey))
                {
                    key = mPrefixKey + "." + key;
                }
                string data = mOuter.Get(key);
                if (string.IsNullOrEmpty(data))
                {
                    data = "";
                }
                else if (seperator != string.Empty && !data.EndsWith(seperator))
                {
                    data = data + seperator;
                }
                mOuter.Put(key, data + value + seperator);
            }

            public void Remove(string key)
            {
                if (!key.StartsWith(mPrefixKey))
                {
                    key = mPrefixKey + "." + key;
                }
                mOuter.Remove(key);
            }

            public void Remove(string key, string value)
            {
                Remove(key, value, "");
            }

            public void Remove(string key, string value, string seperator)
            {
                if (!key.StartsWith(mPrefixKey))
                {
                    key = mPrefixKey + "." + key;
                }
                string data = mOuter.Get(key);
                if (!string.IsNullOrEmpty(data) && !string.IsNullOrEmpty(value) && data.Contains(value + seperator))
                {
                    mOuter.Put(key, data.Replace(value + seperator, ""));
                }
            }

            public void Clear()
            {
                foreach(string key in GetKeys())
                {
                    mOuter.Remove(key);
                }
            }

            public void BeginUpdate()
            {
                mOuter.BeginUpdate();
            }

            public void EndUpdate()
            {
                mOuter.EndUpdate();
            }

            public IEnumerable GetKeys()
            {
                Hashtable table = new Hashtable();
                foreach (string key in mOuter.GetKeys())
                {
                    if (key.StartsWith(mPrefixKey))
                    {
                        table.Add(key, key);
                    }
                }
                return table.Keys;
            }

            public IConfigContext Lock(string lockerName)
            {
                return new LockedConfigContextView(mOuter, lockerName, mPrefixKey, false);
            }

            #endregion

            #region IDisposable Members

            public void Dispose()
            {
                mOuter.Dispose();
            }

            #endregion
        }

        #endregion

        #region Constructors

        protected ConfigContext()
        {
        }

        ~ConfigContext()
        {
            Dispose();
        }

        #endregion

        #region Generic Methods

        public int Count
        {
            get
            {
                lock (mLock)
                {
                    return mProperties.Count;
                }
            }
        }

        public bool Has(string key)
        {
            lock (mLock)
            {
                return mProperties.ContainsKey(key);
            }
        }

        public bool HasPrefix(string keyPrefix)
        {
            lock (mLock)
            {
                foreach(var key in mProperties.Keys)
                {
                    string inspectKey = key as string ?? "";
                    if (inspectKey.StartsWith(keyPrefix))
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public string Get(string key)
        {
            lock (mLock)
            {
                return mProperties.GetProperty(key);
            }
        }

        public string Get(string key, string def)
        {
            lock (mLock)
            {
                return mProperties.GetProperty(key, def);
            }
        }

        public IEnumerable GetKeys()
        {
            lock (mLock)
            {
                return mProperties.PropertyNames();
            }
        }

        public IConfigContext GetView(string prefixKey)
        {
            return new ConfigContextView(this, prefixKey);
        }

        public void Put(string key, string value)
        {
            lock (mLock)
            {
                mProperties.SetProperty(key, value);
                mChanges++;
                UpdateFile();
            }
        }

        public void Append(string key, string value)
        {
            Append(key, value, "");
        }

        public void Append(string key, string value, string seperator)
        {
            lock (mLock)
            {
                string data = mProperties.GetProperty(key);
                if (string.IsNullOrEmpty(data))
                {
                    data = "";
                }
                else if (seperator != string.Empty && !data.EndsWith(seperator))
                {
                    data = data + seperator;
                }
                mProperties.SetProperty(key, data + value + seperator);
                mChanges++;
                UpdateFile();
            }
        }

        public void Remove(string key)
        {
            lock (mLock)
            {
                mProperties.Remove(key);
                mChanges++;
                UpdateFile();
            }
        }

        public void Remove(string key, string value)
        {
            Remove(key, value, "");
        }

        public void Remove(string key, string value, string seperator)
        {
            lock (mLock)
            {
                string data = mProperties.GetProperty(key);
                if (!string.IsNullOrEmpty(data) && !string.IsNullOrEmpty(value) && data.Contains(value + seperator))
                {
                    mProperties.SetProperty(key, data.Replace(value + seperator, ""));
                    mChanges++;
                    UpdateFile();
                }
            }
        }

        public void Clear()
        {
            lock (mLock)
            {
                mProperties.Clear();
                mChanges++;
                UpdateFile();
            }
        }

        public IConfigContext Lock(string lockerName)
        {
            return new LockedConfigContext(this, lockerName);
        }

        public void BeginUpdate()
        {
            lock (mLock)
            {
                mDeferUpdate = true;
            }
        }

        public void EndUpdate()
        {
            lock (mLock)
            {
                mDeferUpdate = false;
                UpdateFile();
            }
        }

        public void Dump(Stream stream)
        {
            lock (mLock)
            {
                mProperties.Store(stream, null);
            }
        }

        public void Dispose()
        {
            if(mConfigWatcher != null)
            {
                FileWatcherManager.RemovePath(mFilePath, mConfigWatcher);
            }
        }

        #endregion

        #region Private Methods

        private bool LoadFile()
        {
            if (!string.IsNullOrEmpty(mFilePath) && File.Exists(mFilePath))
            {
                using (var fileStream = new FileStream(mFilePath, FileMode.Open))
                {
                    if (mEncrypted)
                    {
                        byte[] key = BuildKey();
                        RijndaelManaged rmCrypto = rmCrypto = new RijndaelManaged();
                        using (var cryptStream = new CryptoStream(fileStream, rmCrypto.CreateDecryptor(key, key), CryptoStreamMode.Read))
                        {
                            mProperties.Load(cryptStream);
                        }
                    }
                    else
                    {
                        mProperties.Load(fileStream);
                    }
                    return true;
                }
            }
            return false;
        }

        private void UpdateFile()
        {
            if (!string.IsNullOrEmpty(mFilePath) && !mDeferUpdate && mChanges > 0)
            {
                if(mConfigWatcher != null)
                {
                    mConfigWatcher.Context = null;
                }
                using (var fileStream = new FileStream(mFilePath, FileMode.Create))
                {
                    if (mEncrypted)
                    {
                        byte[] key = BuildKey();
                        RijndaelManaged rmCrypto = rmCrypto = new RijndaelManaged();
                        using (var cryptStream = new CryptoStream(fileStream, rmCrypto.CreateEncryptor(key, key), CryptoStreamMode.Write))
                        {
                            try
                            {
                                mProperties.Store(cryptStream, null);
                            }
                            catch (Exception)
                            {
                                // if an exception occurred, something went wrong with decryption
                                // attempt to load the file again, plainly
                                using (var anotherFileStream = new FileStream(mFilePath, FileMode.Create))
                                {
                                    mProperties.Store(anotherFileStream, null);
                                }
                            }
                        }
                    }
                    else
                    {
                        mProperties.Store(fileStream, null);
                    }
                }
                mChanges = 0;
                if (mConfigWatcher != null)
                {
                    mConfigWatcher.Context = this;
                }
            }
        }

        private byte[] BuildKey()
        {
            return Encoding.UTF8.GetBytes((Environment.UserName + Environment.MachineName + Environment.OSVersion.VersionString).Substring(0, 16));
        }

        #endregion

        #region Public Static Methods

        public static ConfigContext GetContext(string name)
        {
            ConfigContext config = new ConfigContext();
            config.mFilePath = name + ".properties";
            config.mEncrypted = false;
            config.LoadFile();
            return config;
        }

        public static ConfigContext GetContext(string name, bool monitor)
        {
            ConfigContext config = new ConfigContext();
            config.mFilePath = name + ".properties";
            config.mEncrypted = false;
            config.LoadFile();
            if (monitor)
            {
                config.mConfigWatcher = new ConfigWatcher(config);
                FileWatcherManager.AddPath(config.mFilePath, config.mConfigWatcher);
            }
            return config;
        }

        public static T GetContext<T>(string name)
            where T : ConfigContext
        {
            T configType = Activator.CreateInstance<T>();
            ConfigContext config = configType as ConfigContext;
            config.mFilePath = name + ".properties";
            config.mEncrypted = false;
            config.LoadFile();
            return configType;
        }

        public static T GetContext<T>(string name, bool monitor)
            where T : ConfigContext
        {
            T configType = Activator.CreateInstance<T>();
            ConfigContext config = configType as ConfigContext;
            config.mFilePath = name + ".properties";
            config.mEncrypted = false;
            config.LoadFile();
            if(monitor)
            {
                config.mConfigWatcher = new ConfigWatcher(config);
                FileWatcherManager.AddPath(config.mFilePath, config.mConfigWatcher);
            }
            return configType;
        }

        public static ConfigContext GetEncryptedContext(string name)
        {
            ConfigContext config = new ConfigContext();
            config.mFilePath = name + ".dat";
            config.mEncrypted = true;
            config.LoadFile();
            return config;
        }

        public static ConfigContext GetEncryptedContext(string name, bool monitor)
        {
            ConfigContext config = new ConfigContext();
            config.mFilePath = name + ".dat";
            config.mEncrypted = true;
            config.LoadFile();
            if (monitor)
            {
                config.mConfigWatcher = new ConfigWatcher(config);
                FileWatcherManager.AddPath(config.mFilePath, config.mConfigWatcher);
            }
            return config;
        }

        public static T GetEncryptedContext<T>(string name)
            where T : ConfigContext
        {
            T configType = Activator.CreateInstance<T>();
            ConfigContext config = configType as ConfigContext;
            config.mFilePath = name + ".dat";
            config.mEncrypted = true;
            config.LoadFile();
            return configType;
        }

        public static T GetEncryptedContext<T>(string name, bool monitor)
            where T : ConfigContext
        {
            T configType = Activator.CreateInstance<T>();
            ConfigContext config = configType as ConfigContext;
            config.mFilePath = name + ".dat";
            config.mEncrypted = true;
            config.LoadFile();
            if (monitor)
            {
                config.mConfigWatcher = new ConfigWatcher(config);
                FileWatcherManager.AddPath(config.mFilePath, config.mConfigWatcher);
            }
            return configType;
        }

        #endregion
        
        #region Private Watcher Class

        private class ConfigWatcher : IFileWatcher
        {
            internal string ConfigPath { get; private set; }
            internal ConfigContext Context { get; set; }

            internal ConfigWatcher(ConfigContext context)
            {
                Context = context;
                ConfigPath = context.mFilePath;
                Exts = new string[] { Path.GetExtension(context.mFilePath) };
            }

            public string[] Exts { get; private set; }

            public void OnChanged(object source, FileSystemEventArgs e)
            {
                if(e.FullPath.Equals(ConfigPath) && Context != null)
                {
                    Context.LoadFile();
                }
            }

            public void OnCreated(object source, FileSystemEventArgs e)
            {
            }

            public void OnDeleted(object source, FileSystemEventArgs e)
            {
            }

            public void OnRenamed(object source, RenamedEventArgs e)
            {
            }

            public void Dispose()
            {

            }
        }

        #endregion

    }
}
