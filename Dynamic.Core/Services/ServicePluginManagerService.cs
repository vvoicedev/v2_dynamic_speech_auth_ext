﻿using System;
using System.Collections.Generic;

using Dynamic.Modules;
using Dynamic.Plugins;

namespace Dynamic.Services
{
    public class ServicePluginManagerServiceModule : IModule
    {
        public void InitializeServices(IServiceManager serviceManager, Dictionary<string, object> args)
        {
            if (!serviceManager.Exists<IServicePluginManagerService>())
            {
                serviceManager.RegisterSingleton<IServicePluginManagerService, ServicePluginManagerService>();
            }
        }
    }

    class ServicePluginManagerService : ServiceBase, IServicePluginManagerService
    {
        private PluginManager<IServicePlugin> mPluginManager = new PluginManager<IServicePlugin>();

        public override void FinishedInitialization()
        {
            base.FinishedInitialization();

            foreach (var pluginName in mPluginManager.PluginNames)
            {
                mPluginManager.FindSingletonPlugin(pluginName).Initialize(ServiceManager);
            }
        }

        public override void Shutdown()
        {
            base.Shutdown();
            mPluginManager.ClearPlugins();
        }

        public void InitializePlugins(string pluginFile)
        {
            mPluginManager.InitializePlugins(pluginFile);
        }

        public void InitializePlugins(List<string> pluginPaths)
        {
            mPluginManager.InitializePlugins(pluginPaths);
        }

        public void UpdatePlugins(string pluginFile)
        {
            mPluginManager.UpdatePlugins(pluginFile);
        }

        public void UpdatePlugins(List<string> pluginPaths)
        {
            mPluginManager.UpdatePlugins(pluginPaths);
        }

        public IPlugin FindPluginObject(string pluginName)
        {
            return mPluginManager.FindPluginObject(pluginName);
        }

        public Type FindPluginType(string pluginName)
        {
            return mPluginManager.FindPluginType(pluginName);
        }

        public IPluginMethod FindPluginObjectMethod(string pluginMethod)
        {
            return mPluginManager.FindPluginObjectMethod(pluginMethod);
        }

        public IPlugin FindSingletonPluginObject(string pluginName)
        {
            return mPluginManager.FindSingletonPluginObject(pluginName);
        }

        public Type FindSingletonPluginType(string pluginName)
        {
            return mPluginManager.FindSingletonPluginType(pluginName);
        }

        public IPluginMethod FindSingletonPluginObjectMethod(string pluginName)
        {
            return mPluginManager.FindSingletonPluginObjectMethod(pluginName);
        }

        public string[] PluginNames { get { return mPluginManager.PluginNames; } }

        public bool RegisterPlugin(string plugin)
        {
            return mPluginManager.RegisterPlugin(plugin);
        }

        public bool RegisterPlugin(Type plugin)
        {
            return mPluginManager.RegisterPlugin(plugin);
        }

        public bool RegisterPlugin(IServicePlugin plugin)
        {
            return mPluginManager.RegisterPlugin(plugin);
        }

        public void ClearPlugins()
        {
            mPluginManager.ClearPlugins();
        }
    }
}
