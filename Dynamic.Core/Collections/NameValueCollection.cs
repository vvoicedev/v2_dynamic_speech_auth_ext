﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dynamic.Collections
{
    public class NameValueCollection : System.Collections.CollectionBase
    {
        public void Set(string name, string value)
        {
            NameValuePair item = FindItem(name);
            if (item == null)
            {
                this.List.Add(new NameValuePair(name, value));
            }
            else
            {
                item.Value = value;
            }
        }

        public void Remove(string name)
        {
            NameValuePair item = FindItem(name);
            if (item != null)
            {
                this.List.Remove(item);
            }
        }

        public NameValuePair FindItem(string name)
        {
            foreach (NameValuePair item in this.List)
            {
                if (item.Name.Equals(name))
                {
                    return item;
                }
            }
            return null;
        }

        public string FindValue(string name)
        {
            foreach (NameValuePair item in this.List)
            {
                if (item.Name.Equals(name))
                {
                    return item.Value;
                }
            }
            return null;
        }

        public string[] EnumerateKeys()
        {
            int i = 0;
            string[] keys = new string[this.List.Count];
            foreach (NameValuePair item in this.List)
            {
                keys[i++] = item.Name;
            }
            return keys;
        }
    }
}
