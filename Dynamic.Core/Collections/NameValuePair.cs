﻿namespace Dynamic.Collections
{
    public class NameValuePair
    {
        public NameValuePair()
            : this(null, null)
        {
        }

        public NameValuePair(string name)
            : this(name, null)
        {
        }

        public NameValuePair(string name, string value)
        {
            Name = name;
            Value = value;
        }

        public string Name { get; private set; }

        public string Value { get; set; }
    }
}
