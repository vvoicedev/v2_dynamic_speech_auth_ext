﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Dynamic.Collections
{
    public class ThreadSafeList<T>
    {
        #region LockedList

        private sealed class LockedList : ILockedList<T>
        {
            private ThreadSafeList<T> mOuter;

            internal LockedList(ThreadSafeList<T> outer)
            {
                mOuter = outer;
                Monitor.Enter(mOuter.mLock);
            }

            #region ILockedList Methods

            public int Count { get { return mOuter.mList.Count; } }

            public bool Contains(T item)
            {
                return mOuter.mList.Contains(item);
            }

            public void Add(T item)
            {
                mOuter.mList.Add(item);
            }

            public void AddRange(IEnumerable<T> collection)
            {
                mOuter.mList.AddRange(collection);
            }

            public void Remove(T item)
            {
                mOuter.mList.Remove(item);
            }

            public void Clear()
            {
                mOuter.mList.Clear();
            }

            public T this[int index]
            {
                get
                {
                    return mOuter.mList[index];
                }
                set
                {
                    mOuter.mList[index] = value;
                }
            }

            public T[] ToArray()
            {
                return mOuter.mList.ToArray();
            }

            public IEnumerator<T> GetEnumerator()
            {
                return mOuter.mList.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return mOuter.mList.GetEnumerator();
            }

            #endregion

            #region IDisposable Members

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            private void Dispose(bool disposing)
            {
                Debug.Assert(disposing, "LockedList implementations must be explicitly disposed");
                if (disposing)
                {
                    Monitor.Exit(mOuter.mLock);
                }
            }

            ~LockedList()
            {
                Dispose(false);
            }

            #endregion
        }

        #endregion

        #region Private Variables

        private readonly List<T> mList;
        private readonly object mLock = new object();

        #endregion

        #region Constructors

        public ThreadSafeList()
        {
            mList = new List<T>();
        }

        public ThreadSafeList(int capacity)
        {
            mList = new List<T>(capacity);
        }

        public ThreadSafeList(IEnumerable<T> collection)
        {
            mList = new List<T>(collection);
        }

        #endregion

        #region Locker

        public ILockedList<T> Lock()
        {
            return new LockedList(this);
        }

        #endregion

        #region ILockedList

        public int Count
        {
            get
            {
                lock (mLock)
                {
                    return mList.Count;
                }
            }
        }

        public bool Contains(T item)
        {
            lock (mLock)
            {
                return mList.Contains(item);
            }
        }

        public void Add(T item)
        {
            lock (mLock)
            {
                mList.Add(item);
            }
        }

        public void AddRange(IEnumerable<T> collection)
        {
            lock (mLock)
            {
                mList.AddRange(collection);

            }
        }

        public void Remove(T item)
        {
            lock (mLock)
            {
                mList.Remove(item);
            }
        }

        public void Clear()
        {
            lock (mLock)
            {
                mList.Clear();
            }
        }

        public T this[int index]
        {
            get
            {
                lock (mLock)
                {
                    return mList[index];
                }
            }
            set
            {
                lock (mLock)
                {
                    mList[index] = value;
                }
            }
        }

        public T[] ToArray()
        {
            lock(mLock)
            {
                return mList.ToArray();
            }
        }

        #endregion

    }
}
