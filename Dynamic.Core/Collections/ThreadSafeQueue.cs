﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Dynamic.Collections
{
    public sealed class ThreadSafeQueue<T>
    {
        #region LockedQueue

        private sealed class LockedQueue : ILockedQueue<T>
        {
            private ThreadSafeQueue<T> mOuter;

            internal LockedQueue(ThreadSafeQueue<T> outer)
            {
                mOuter = outer;
                Monitor.Enter(mOuter.mLock);
            }

            #region ILockedQueue<T> Members

            public int Count
            {
                get { return mOuter.mQueue.Count; }
            }

            public bool Contains(T value)
            {
                return mOuter.mQueue.Contains(value);
            }

            public void Enqueue(T value)
            {
                mOuter.mQueue.Enqueue(value);
            }

            public T Dequeue()
            {
                return mOuter.mQueue.Dequeue();
            }

            public T Peek()
            {
                return mOuter.mQueue.Peek();
            }

            public void Clear()
            {
                mOuter.mQueue.Clear();
            }

            #endregion

            #region IDisposable Members

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            private void Dispose(bool disposing)
            {
                Debug.Assert(disposing, "ILockedQueue implementations must be explicitly disposed");
                if (disposing)
                {
                    Monitor.Exit(mOuter.mLock);
                }
            }

            ~LockedQueue()
            {
                Dispose(false);
            }

            #endregion
        }

        #endregion

        private Queue<T> mQueue;
        private object mLock = new object();

        public ThreadSafeQueue()
        {
            mQueue = new Queue<T>();
        }

        public ThreadSafeQueue(int capacity)
        {
            mQueue = new Queue<T>(capacity);
        }

        public ThreadSafeQueue(IEnumerable<T> collection)
        {
            mQueue = new Queue<T>(collection);
        }

        public void Enqueue(T value)
        {
            lock (mLock)
            {
                mQueue.Enqueue(value);
            }
        }

        public void Clear()
        {
            lock (mLock)
            {
                mQueue.Clear();
            }
        }

        public ILockedQueue<T> Lock()
        {
            return new LockedQueue(this);
        }
    }
}
