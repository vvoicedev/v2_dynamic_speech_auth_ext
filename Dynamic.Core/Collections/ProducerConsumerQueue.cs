﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics;


namespace Dynamic.Collections
{
    /// <summary>
    /// Manages a threadsafe queue for a producer/consumer pattern of usage.
    /// Any number of threads can safely add items to the queue while any number of
    /// threads remove items from the queue.
    /// Read access to the queue items is through the enumerator only.
    /// </summary>
    /// <typeparam name="T">The queue element type.</typeparam>

    [
        SuppressMessage
        (
            "Microsoft.Naming",
            "CA1710:IdentifiersShouldHaveCorrectSuffix",
            Scope = "type",
            Target = "Dmr.Common.Threads.ProducerConsumerQueue`1",
            Justification = "This class is intended to be used as a produce/consume queue, and therefore has a 'Queue' suffix."
        )
    ]

    [
        SuppressMessage
        (
            "Microsoft.Naming",
            "CA1711:IdentifiersShouldNotHaveIncorrectSuffix",
            Scope = "type",
            Target = "Dmr.Common.Threads.ProducerConsumerQueue`1",
            Justification = "This class is intended to be used as a produce/consume queue, and therefore has a 'Queue' suffix."
        )
    ]

    public sealed class ProducerConsumerQueue<T> : IEnumerable<T>
    {

        #region Private Variables

        private readonly Queue<T> mQueue;
        private readonly object mSyncLock = new object();

        private bool mIsLocked;

        #endregion

        #region Constructors

        public ProducerConsumerQueue()
        {
            mQueue = new Queue<T>();
        }

        public ProducerConsumerQueue(int capacity)
        {
            mQueue = new Queue<T>(capacity);
        }

        #endregion

        #region Public Methods

        /// <summary>Has the queue been closed?</summary>

        public bool IsClosed
        {
            get
            {
                return mIsLocked;
            }
        }

        /// <summary>The object used to synchronize access to the queue.</summary>

        public object SyncLock
        {
            get
            {
                return mSyncLock;
            }
        }

        /// <summary>Tell the queue that no more items will be added.</summary>
        /// <param name="waitUntilEmpty">Wait until the queue is empty before returning?</param>

        public void Close(bool waitUntilEmpty)
        {
            lock (mSyncLock)
            {
                mIsLocked = true;
                Monitor.PulseAll(mSyncLock);     // Wake up all consumers.

                if (waitUntilEmpty)
                {
                    while (mQueue.Count > 0)
                    {
                        Monitor.Wait(mSyncLock);
                    }
                }
            }
        }

        /// <summary>Adds an item the the queue.</summary>
        /// <param name="item">The item to be added.</param>

        public void Add(T item)
        {
            lock (mSyncLock)
            {
                if (mIsLocked)
                {
                    throw new InvalidOperationException("The queue has been closed.");
                }

                mQueue.Enqueue(item);

                if (mQueue.Count == 1)
                {
                    Monitor.Pulse(mSyncLock);    // Added first item to the queue; wake up a consumer.
                }
            }
        }

        /// <summary>Typesafe Enumerator access to the queue items.</summary>

        public IEnumerator<T> GetEnumerator()
        {
            while (true)
            {
                T item;

                lock (mSyncLock)
                {
                    if (mQueue.Count > 0)
                    {
                        item = mQueue.Dequeue();

                        if (mIsLocked && (mQueue.Count == 0))
                        {
                            Monitor.PulseAll(mSyncLock);    // Tell producer we're done.
                        }
                    }
                    else if (mIsLocked)
                    {
                        yield break;                // All done.
                    }
                    else
                    {
                        Monitor.Wait(mSyncLock);    // Waits for another item to enter the queue or the queue to be closed.
                        continue;                   // Back to "while".
                    }
                }

                yield return item;  // Yield outside of the lock.
            }
        }

        /// <summary>Non-typesafe Enumerator access to the queue items.</summary>

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

    }
}