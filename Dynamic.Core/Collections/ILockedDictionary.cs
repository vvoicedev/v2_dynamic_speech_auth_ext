﻿using System;
using System.Collections.Generic;

namespace Dynamic.Collections
{
    public interface ILockedDictionary<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>, IDisposable
    {
        bool Any { get; }
        int Count { get; }
        ICollection<TKey> Keys { get; }
        ICollection<TValue> Values { get; }
        KeyValuePair<TKey, TValue> First { get; }
        KeyValuePair<TKey, TValue> Last { get; }
        TValue this[TKey key] { get; set; }
        bool Add(TKey key, TValue value);
        bool ContainsKey(TKey key);
        bool ContainsValue(TValue value);
        bool Remove(TKey key);
        bool TryGetValue(TKey key, out TValue value);
        void Clear();
    }
}
