﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Dynamic.Collections
{
    public class ThreadSafeDictionary<TKey, TValue>
    {
        #region LockedDictionary

        private sealed class LockedDictionary : ILockedDictionary<TKey, TValue>
        {
            private ThreadSafeDictionary<TKey, TValue> mOuter;

            internal LockedDictionary(ThreadSafeDictionary<TKey, TValue> outer)
            {
                mOuter = outer;
                Monitor.Enter(mOuter.mLock);
            }

            #region ILockedDictionary Methods

            public bool Any { get { return mOuter.mDictionary.Count != 0; } }

            public int Count { get { return mOuter.mDictionary.Count; } }

            public ICollection<TKey> Keys { get { return mOuter.mDictionary.Keys; } }

            public ICollection<TValue> Values { get { return mOuter.mDictionary.Values; } }

            public KeyValuePair<TKey, TValue> First { get { return mOuter.mDictionary.First(); } }

            public KeyValuePair<TKey, TValue> Last { get { return mOuter.mDictionary.Last(); } }

            public TValue this[TKey key]
            {
                get
                {
                    return mOuter.mDictionary[key];
                }
                set
                {
                    mOuter.mDictionary[key] = value;
                }
            }

            public bool Add(TKey key, TValue value)
            {
                if (!mOuter.mDictionary.ContainsKey(key))
                {
                    mOuter.mDictionary.Add(key, value);
                    return true;
                }
                return false;
            }

            public bool ContainsKey(TKey key)
            {
                return mOuter.mDictionary.ContainsKey(key);
            }

            public bool ContainsValue(TValue value)
            {
                return mOuter.mDictionary.ContainsValue(value);
            }

            public bool Remove(TKey key)
            {
                return mOuter.mDictionary.Remove(key);
            }

            public bool TryGetValue(TKey key, out TValue value)
            {
                return mOuter.mDictionary.TryGetValue(key, out value);
            }

            public void Clear()
            {
                mOuter.mDictionary.Clear();
            }

            public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
            {
                return mOuter.mDictionary.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return mOuter.mDictionary.GetEnumerator();
            }

            #endregion

            #region IDisposable Members

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            private void Dispose(bool disposing)
            {
                Debug.Assert(disposing, "IDictionary implementations must be explicitly disposed");
                if (disposing)
                {
                    Monitor.Exit(mOuter.mLock);
                }
            }

            ~LockedDictionary()
            {
                Dispose(false);
            }

            #endregion
        }

        #endregion

        #region Private Variables

        private Dictionary<TKey, TValue> mDictionary;
        private object mLock = new object();

        #endregion

        #region Constructors

        public ThreadSafeDictionary()
        {
            mDictionary = new Dictionary<TKey, TValue>();
        }

        public ThreadSafeDictionary(IEqualityComparer<TKey> comparer)
        {
            mDictionary = new Dictionary<TKey, TValue>(comparer);
        }

        public ThreadSafeDictionary(int capacity)
        {
            mDictionary = new Dictionary<TKey, TValue>(capacity);
        }

        public ThreadSafeDictionary(int capacity, IEqualityComparer<TKey> comparer)
        {
            mDictionary = new Dictionary<TKey, TValue>(capacity, comparer);
        }

        #endregion

        #region Locker

        public ILockedDictionary<TKey, TValue> Lock()
        {
            return new LockedDictionary(this);
        }

        #endregion

        #region IDictionary Methods

        public bool Any
        {
            get
            {
                lock (mLock)
                {
                    return mDictionary.Count != 0;
                }
            }
        }

        public int Count
        {
            get
            {
                lock (mLock)
                {
                    return mDictionary.Count;
                }
            }
        }

        public KeyValuePair<TKey, TValue> First
        {
            get
            {
                lock (mLock)
                {
                    return mDictionary.First();
                }
            }
        }

        public KeyValuePair<TKey, TValue> Last
        {
            get
            {
                lock (mLock)
                {
                    return mDictionary.Last();
                }
            }
        }

        public TValue this[TKey key]
        {
            get
            {
                lock (mLock)
                {
                    return mDictionary[key];
                }
            }
            set
            {
                lock (mLock)
                {
                    mDictionary[key] = value;
                }
            }
        }

        public bool Add(TKey key, TValue value)
        {
            lock (mLock)
            {
                if (!mDictionary.ContainsKey(key))
                {
                    mDictionary.Add(key, value);
                    return true;
                }
                return false;
            }
        }

        public bool ContainsKey(TKey key)
        {
            lock (mLock)
            {
                return mDictionary.ContainsKey(key);
            }
        }

        public bool ContainsValue(TValue value)
        {
            lock (mLock)
            {
                return mDictionary.ContainsValue(value);
            }
        }

        public bool Remove(TKey key)
        {
            lock (mLock)
            {
                return mDictionary.Remove(key);
            }
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            lock (mLock)
            {
                return mDictionary.TryGetValue(key, out value);
            }
        }

        public void Clear()
        {
            lock (mLock)
            {
                mDictionary.Clear();
            }
        }

        #endregion

    }
}
