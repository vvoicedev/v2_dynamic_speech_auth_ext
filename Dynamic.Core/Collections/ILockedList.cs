﻿using System;
using System.Collections.Generic;

namespace Dynamic.Collections
{
    public interface ILockedList<T> : IEnumerable<T>, IDisposable
    {
        int Count { get; }

        bool Contains(T item);

        void Add(T item);

        void AddRange(IEnumerable<T> collection);

        void Remove(T item);

        void Clear();

        T this[int index] { get; set; }

        T[] ToArray();
    }
}
