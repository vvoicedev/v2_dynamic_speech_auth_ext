﻿using System;

namespace Dynamic.Collections
{
    public interface ILockedQueue<T> : IDisposable
    {
        int Count { get; }
        bool Contains(T value);
        void Enqueue(T value);
        T Dequeue();
        T Peek();
        void Clear();
    }
}
