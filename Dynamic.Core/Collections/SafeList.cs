﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dynamic.Collections
{
    public class SafeList<TValue>
    {
        private readonly object mLock = new object();
        private readonly List<TValue> mList;

        public SafeList()
        {
            mList = new List<TValue>();
        }

        public SafeList(int capacity)
        {
            mList = new List<TValue>(capacity);
        }

        public SafeList(IEnumerable<TValue> collection)
        {
            mList = new List<TValue>(collection);
        }

        public int Count
        {
            get
            {
                lock (mLock)
                {
                    return mList.Count;
                }
            }
        }

        public bool Contains(TValue item)
        {
            lock (mLock)
            {
                return mList.Contains(item);
            }
        }

        public void Add(TValue item)
        {
            lock (mLock)
            {
                mList.Add(item);
            }
        }

        public void AddRange(IEnumerable<TValue> collection)
        {
            lock (mLock)
            {
                mList.AddRange(collection);

            }
        }

        public void Remove(TValue item)
        {
            lock (mLock)
            {
                mList.Remove(item);
            }
        }

        public TValue this[int index]
        {
            get
            {
                lock (mLock)
                {
                    return mList[index];
                }
            }
            set
            {
                lock (mLock)
                {
                    mList[index] = value;
                }
            }
        }

    }
}
