﻿using System;
using System.Collections.Generic;

namespace Dynamic.Collections
{
    /// <summary>
    /// A generic-type object pool that can create new objects if empty.
    /// </summary>
    /// <author>Daniel Jost</author>
    /// <remarks>
    /// https://gist.github.com/PxlBuzzard/4704862
    /// 
    /// Modified by TarSylas for Dynamic.Lib
    /// </remarks>
    public class ObjectPool<T>
    {
        public delegate T Initializer(T item, uint index);

        #region Private Variables

        private List<T> mList;
        private Queue<T> mPool;
        private Initializer mInitializer;
        private readonly object locker = new object();

        private static T DefaultInitializer(T item, uint index)
        {
            return item;
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Create an expandable pool with a number of starting elements.
        /// </summary>
        /// <param name="startingSize">The size of the pool to start.</param>
        /// <param name="theObject">The object to be used as the master clone object.</param>
        public ObjectPool(int startingSize, Initializer theInitializer)
        {
            mList = new List<T>(startingSize);
            mPool = new Queue<T>(startingSize);
            mInitializer = theInitializer ?? DefaultInitializer;

            //fill up the pool
            for (int i = 0; i < startingSize; ++i)
            {
                mPool.Enqueue(GetNewObject());
            }
        }

        #endregion

        #region Functions

        /// <summary>
        /// Returns an object, either from the pool, or a new object if the pool is empty.
        /// </summary>
        /// <returns>An object of type T.</returns>
        public T Acquire()
        {
            lock(locker)
            {
                if(mPool.Count > 0)
                {
                    return mPool.Dequeue();
                }
                return GetNewObject();
            }
        }

        /// <summary>
        /// Adds an object into the pool.
        /// </summary>
        /// <param name="data">The object being returned to the pool.</param>
        public void Release(T data)
        {
            lock (locker)
            {
                mPool.Enqueue(data);
            }
        }
        
        /// <summary>
        /// Note: Not locked, the count never goes down and the list is never modified outside of adding to it
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public T Get(uint index)
        {
            // Grab a reference to the list,
            // in case its about to change due to growth
            var list = mList;

            if (index >= list.Count)
                return default(T);

            return list[(int)index];
        }

        /// <summary>
        /// Creates a new object if the pool is empty.
        /// </summary>
        /// <returns>A new object of the type T.</returns>
        private T GetNewObject()
        {
            if(mList.Count == mList.Capacity)
            {
                // create new list
                var newList = new List<T>((mList.Capacity * 2) + 8);

                // add old items to it
                newList.AddRange(mList);

                // fetch old list
                var oldList = mList;

                // put new list in place
                mList = newList;

                // clear and null old list
                oldList.Clear();
                oldList = null;
            }

            int index = mList.Count;
            var newObj = Activator.CreateInstance<T>();
            mList.Add(newObj);
            return mInitializer.Invoke(newObj, (uint)index);
        }

        /// <summary>
        /// The total number of items in the pool
        /// </summary>
        public int Count
        {
            get
            {
                var list = mList;
                return list.Count;
            }
        }

        #endregion
    }
}
